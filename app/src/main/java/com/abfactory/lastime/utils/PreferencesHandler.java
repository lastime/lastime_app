package com.abfactory.lastime.utils;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.draggablelist.DraggableListItem;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class PreferencesHandler {

    private SharedPreferences sharedPrefs;
    private static final String SAVED_PREFS_BUNDLE_KEY_SEPARATOR = "§§";
    private static final int DEFAULT_NUMBER_OF_TIMES_BEFORE_ASKING_FOR_RATING = 10;

    public PreferencesHandler(Context applicationContext) {
        // Get app preferences
        sharedPrefs = PreferenceManager.getDefaultSharedPreferences(applicationContext);
    }

    /**
     * DISPLAY PREFERENCE HANDLING
     **/

    public String getDisplayMode() {
        return sharedPrefs.getString(Settings.DISPLAY_MODE.toString(), DisplayModePreference.TIMELINE.toString());
    }

    public boolean isDisplayModeSetByTimeLine() {
        return DisplayModePreference.TIMELINE.toString().equals(getDisplayMode());
    }

    public boolean isDisplayModeSetByAlphabeticalOrder() {
        return DisplayModePreference.ALPHABETIC.toString().equals(getDisplayMode());
    }

    public boolean isDisplayModeSetByFrequency() {
        return DisplayModePreference.FREQUENCY.toString().equals(getDisplayMode());
    }

    public void saveDisplayPreference(DisplayModePreference displayMode) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putString(Settings.DISPLAY_MODE.toString(), displayMode.toString());
        // Save preferences
        edit.apply();
    }

    /**
     * ANONYMOUS_LOGIN_CHOSEN handling
     **/

    public boolean isAnonymousLoginChosen() {
        return sharedPrefs.getBoolean(Settings.ANONYMOUS_LOGIN_CHOSEN.toString(), false);
    }

    public void setAnonymousLoginChosen(boolean isAnonymousLoginChosen) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.ANONYMOUS_LOGIN_CHOSEN.toString(), isAnonymousLoginChosen);
        // Save preferences
        edit.apply();
    }

    /**
     * PWD HANDLING
     **/

    public boolean isAppToBeProtectedWithPassword() {
        return sharedPrefs.getBoolean(Settings.PROTECT_APP_WITH_PWD.toString(), false);
    }

    public String getPasswordForApp() {
        return sharedPrefs.getString(Settings.PASSWORD.toString(), null);
    }

    public String getBackUpEmailForApp() {
        return sharedPrefs.getString(Settings.BACKUP_EMAIL.toString(), null);
    }

    public void setPassword(String newPassword) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.PROTECT_APP_WITH_PWD.toString(), true);
        edit.putString(Settings.PASSWORD.toString(), newPassword);
        // Save preferences
        edit.apply();
    }

    public void setBackupEmail(String backUpEmail) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putString(Settings.BACKUP_EMAIL.toString(), backUpEmail);
        // Save preferences
        edit.apply();
    }

    public void setPasswordAndBackup(String newPassword, String backUpEmail) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.PROTECT_APP_WITH_PWD.toString(), true);
        edit.putString(Settings.PASSWORD.toString(), newPassword);
        edit.putString(Settings.BACKUP_EMAIL.toString(), backUpEmail);
        // Save preferences
        edit.apply();
    }

    public void resetPwdPreference() {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.PROTECT_APP_WITH_PWD.toString(), false);
        edit.putString(Settings.PASSWORD.toString(), "");
        edit.putString(Settings.BACKUP_EMAIL.toString(), "");
        // Save preferences
        edit.apply();
    }

    /**
     * UTILS
     **/

    public int getRadioButtonIdToCheck() {
        if (DisplayModePreference.TIMELINE.toString().equals(getDisplayMode())) {
            return R.id.radioTimelineDisplay;
        } else if (DisplayModePreference.ALPHABETIC.toString().equals(getDisplayMode())) {
            return R.id.radioAlphabeticDisplay;
        } else {
            return R.id.radioFrequencyDisplay;
        }
    }

    public DisplayModePreference getDisplayModePreferenceFromRadioButtonId(int id) {
        switch (id) {
            case R.id.radioTimelineDisplay:
                return DisplayModePreference.TIMELINE;
            case R.id.radioAlphabeticDisplay:
                return DisplayModePreference.ALPHABETIC;
            case R.id.radioFrequencyDisplay:
                return DisplayModePreference.FREQUENCY;
            default:
                return null;
        }
    }

    public boolean isACRAActivated() {
        return sharedPrefs.getBoolean(Settings.ACRA.toString(), true);
    }

    public void setACRAActivated(boolean isActivated) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.ACRA.toString(), isActivated);
        // Save preferences
        edit.apply();
    }

    public void saveSettings(String displayMode, boolean protectMissionWithPassword, String pwd) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putString(Settings.DISPLAY_MODE.toString(), displayMode);
        edit.putBoolean(Settings.PROTECT_APP_WITH_PWD.toString(), protectMissionWithPassword);
        edit.putString(Settings.PASSWORD.toString(), pwd);
        // Save preferences
        edit.apply();
    }

    /**
     * NOTIFICATIONS HANDLING
     **/

    public boolean isNotificationsActivated() {
        return sharedPrefs.getBoolean(Settings.NOTIFICATIONS_ACTIVATED.toString(), true);
    }

    public void saveNotificationsSetting(boolean notificationsActivated) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.NOTIFICATIONS_ACTIVATED.toString(), notificationsActivated);
        // Save preferences
        edit.apply();
    }

    /**
     * DEV OPTIONS DISPLAY
     **/

    public boolean hasToDisplayDevOptions() {
        return sharedPrefs.getBoolean(Settings.DEV_OPTIONS_DISPLAY_ACTIVATED.toString(), false);
    }

    public void saveDevOptionsDisplaySetting(boolean hasToDisplayDevOptions) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.DEV_OPTIONS_DISPLAY_ACTIVATED.toString(), hasToDisplayDevOptions);
        // Save preferences
        edit.apply();
    }

    public String getIOFolderPath() {
        return sharedPrefs.getString(Settings.IO_FOLDER.toString(), "");
    }

    public void setIOFolderPath(String path) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.IO_FOLDER.toString(), path);
        // Save preferences
        edit.apply();
    }

    public String getUserId() {
        return sharedPrefs.getString(Settings.USER_ID.toString(), "");
    }

    public void setUserId(String userId) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.USER_ID.toString(), userId);
        // Save preferences
        edit.apply();
    }

    public boolean isGoogleAnalyticActivated() {
        return sharedPrefs.getBoolean(Settings.GOOGLE_ANALYTIC.toString(), true);
    }

    public void setGoogleAnalyticActivated(boolean isActivated) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.GOOGLE_ANALYTIC.toString(), isActivated);
        // Save preferences
        edit.apply();
    }

    public String getGoogleAccountName() {
        return sharedPrefs.getString(Settings.GOOGLE_ACCOUNT_NAME.toString(), "");
    }

    public void setGoogleAccountName(String googleAccountName) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.GOOGLE_ACCOUNT_NAME.toString(), googleAccountName);
        // Save preferences
        edit.apply();
    }

    public String getGoogleUserName() {
        return sharedPrefs.getString(Settings.GOOGLE_ACCOUNT_USERNAME.toString(), "");
    }

    public void setGoogleUserName(String googleUserName) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.GOOGLE_ACCOUNT_USERNAME.toString(), googleUserName);
        // Save preferences
        edit.apply();
    }

    public Boolean hasGoogleAccountImage() {
        return sharedPrefs.getBoolean(Settings.GOOGLE_ACCOUNT_IMAGE.toString(), false);
    }

    public void setHasGoogleAccountImage(Boolean hasGoogleAccountImage) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putBoolean(Settings.GOOGLE_ACCOUNT_IMAGE.toString(), hasGoogleAccountImage);
        // Save preferences
        edit.apply();
    }

    public boolean hasGoogleAccountCoverImage() {
        return sharedPrefs.getBoolean(Settings.GOOGLE_ACCOUNT_COVER_IMAGE.toString(), false);
    }

    public void setHasGoogleAccountCoverImage(boolean hasGoogleAccountCoverImage) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putBoolean(Settings.GOOGLE_ACCOUNT_COVER_IMAGE.toString(), hasGoogleAccountCoverImage);
        // Save preferences
        edit.apply();
    }

    public void resetGoogleAccountSignIn() {
        setHasGoogleAccountImage(false);
        setHasGoogleAccountCoverImage(false);
        setGoogleUserName(null);
        setGoogleAccountName(null);
    }

    public String getLastNotificationExecution() {
        return sharedPrefs.getString(Settings.LAST_NOTIFICATION_EXECUTION.toString(), null);
    }

    public void setLastNotificationExecution(String timestamp) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.LAST_NOTIFICATION_EXECUTION.toString(), timestamp);
        // Save preferences
        edit.apply();
    }

    public String getLastAppExecution() {
        return sharedPrefs.getString(Settings.LAST_APP_EXECUTION.toString(), "");
    }

    public void setLastAppExecution(String timestamp) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.LAST_APP_EXECUTION.toString(), timestamp);
        // Save preferences
        edit.apply();
    }

    public boolean getInspireMeUsed() {
        return sharedPrefs.getBoolean(Settings.INSPIRE_ME_USED.toString(), false);
    }

    public void setInspireMeUsed(boolean used) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putBoolean(Settings.INSPIRE_ME_USED.toString(), used);
        // Save preferences
        edit.apply();
    }

    public long getNumberOfConnections() {
        return sharedPrefs.getLong(Settings.NUMBER_OF_CONNECTIONS.toString(), 1);
    }

    public void incrementNumberOfConnections() {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putLong(Settings.NUMBER_OF_CONNECTIONS.toString(), getNumberOfConnections() + 1);
        // Save preferences
        edit.apply();
    }

    public int getNumberOfConnectionsBeforeAskingForRating() {
        return sharedPrefs.getInt(Settings.NUMBER_OF_CONNECTIONS_BEFORE_ASKING_FOR_RATING.toString(), DEFAULT_NUMBER_OF_TIMES_BEFORE_ASKING_FOR_RATING);
    }

    public void decrementNumberOfConnectionsBeforeAskingForRating() {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putInt(Settings.NUMBER_OF_CONNECTIONS_BEFORE_ASKING_FOR_RATING.toString(), getNumberOfConnectionsBeforeAskingForRating() - 1);
        // Save preferences
        edit.apply();
    }

    public void resetNumberOfConnectionsBeforeAskingForRating() {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putInt(Settings.NUMBER_OF_CONNECTIONS_BEFORE_ASKING_FOR_RATING.toString(), DEFAULT_NUMBER_OF_TIMES_BEFORE_ASKING_FOR_RATING);
        // Save preferences
        edit.apply();
    }

    public void cancelAskingForRating() {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putInt(Settings.NUMBER_OF_CONNECTIONS_BEFORE_ASKING_FOR_RATING.toString(), -1);
        // Save preferences
        edit.apply();
    }

    public void registerGoogleTokenId(String token) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.GOOGLE_TOKEN_ID.toString(), token);
        // Save preferences
        edit.apply();
    }

    public String retrieveGoogleTokenId() {
        return sharedPrefs.getString(Settings.GOOGLE_TOKEN_ID.toString(), "");
    }

    public boolean isTutorialPopupToBeShownAtStartup() {
        return sharedPrefs.getBoolean(Settings.SHOW_AGAIN_TUTORIAL_POPUP.toString(), true);
    }

    public void setTutorialPopupToBeShownAtStartup(boolean toBeShown) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.SHOW_AGAIN_TUTORIAL_POPUP.toString(), toBeShown);
        // Save preferences
        edit.apply();
    }

    public void setInvalidState(boolean invalid) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.DATABASE_IN_INVALID_STATE.toString(), invalid);
        // Save preferences
        edit.apply();
    }

    public boolean getInvalidState() {
        return sharedPrefs.getBoolean(Settings.DATABASE_IN_INVALID_STATE.toString(), false);
    }

    public boolean isAutoSaveActivated() {
        return sharedPrefs.getBoolean(Settings.AUTOSAVE.toString(), false);
    }

    public void setAutoSaveActivated(boolean isActivated) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        // Set changes to perform
        edit.putBoolean(Settings.AUTOSAVE.toString(), isActivated);
        // Save preferences
        edit.apply();
    }

    public String getLastAutoSaveExecution() {
        return sharedPrefs.getString(Settings.LAST_AUTOSAVE_EXECUTION.toString(), "");
    }

    public void setLastAutoSaveExecution(String timestamp) {
        SharedPreferences.Editor edit = sharedPrefs.edit();
        edit.putString(Settings.LAST_AUTOSAVE_EXECUTION.toString(), timestamp);
        // Save preferences
        edit.apply();
    }

    public enum Settings {
        DISPLAY_MODE,
        NOTIFICATIONS_ACTIVATED,
        PROTECT_APP_WITH_PWD,
        PASSWORD,
        BACKUP_EMAIL,
        ACRA,
        DEV_OPTIONS_DISPLAY_ACTIVATED,
        IO_FOLDER,
        ANONYMOUS_LOGIN_CHOSEN,
        ENABLE_LOGIN,
        DISPLAY_PREFERENCES,
        USER_ID,
        GOOGLE_ANALYTIC,
        GOOGLE_ACCOUNT_NAME,
        GOOGLE_ACCOUNT_IMAGE,
        GOOGLE_ACCOUNT_USERNAME,
        GOOGLE_ACCOUNT_COVER_IMAGE,
        LAST_NOTIFICATION_EXECUTION,
        INSPIRE_ME_USED,
        NUMBER_OF_CONNECTIONS,
        NUMBER_OF_CONNECTIONS_BEFORE_ASKING_FOR_RATING,
        LAST_APP_EXECUTION,
        GOOGLE_TOKEN_ID,
        SHOW_AGAIN_TUTORIAL_POPUP,
        DATABASE_IN_INVALID_STATE,
        AUTOSAVE,
        LAST_AUTOSAVE_EXECUTION
    }

    public enum DisplayModePreference {
        TIMELINE("TIMELINE"),
        ALPHABETIC("ALPHABETIC"),
        FREQUENCY("FREQUENCY");

        private String description;

        DisplayModePreference(String smsOrEmail) {
            description = smsOrEmail;
        }

        public String getDescription() {
            return description;
        }
    }

    /**
     * Save a Bundle object to SharedPreferences.
     * <p/>
     * NOTE: The editor must be writable, and this function does not commit.
     *
     * @param key         SharedPreferences key under which to store the bundle data. Note this key must
     *                    not contain '§§' as it's used as a delimiter
     * @param preferences Bundled preferences
     */
    private void savePreferencesBundle(String key, Bundle preferences) {

        SharedPreferences.Editor editor = sharedPrefs.edit();

        Set<String> keySet = preferences.keySet();
        Iterator<String> it = keySet.iterator();
        String prefKeyPrefix = key + SAVED_PREFS_BUNDLE_KEY_SEPARATOR;

        while (it.hasNext()) {
            String bundleKey = it.next();
            Object o = preferences.get(bundleKey);
            if (o == null) {
                editor.remove(prefKeyPrefix + bundleKey);
            } else if (o instanceof Integer) {
                editor.putInt(prefKeyPrefix + bundleKey, (Integer) o);
            } else if (o instanceof Long) {
                editor.putLong(prefKeyPrefix + bundleKey, (Long) o);
            } else if (o instanceof Boolean) {
                editor.putBoolean(prefKeyPrefix + bundleKey, (Boolean) o);
            } else if (o instanceof CharSequence) {
                editor.putString(prefKeyPrefix + bundleKey, ((CharSequence) o).toString());
            } else if (o instanceof Bundle) {
                savePreferencesBundle(prefKeyPrefix + bundleKey, ((Bundle) o));
            }
        }
    }

    /**
     * Load a Bundle object from SharedPreferences.
     * (that was previously stored using savePreferencesBundle())
     * <p/>
     * NOTE: The editor must be writable, and this function does not commit.
     *
     * @param key SharedPreferences key under which to store the bundle data. Note this key must
     *            not contain '§§' as it's used as a delimiter
     * @return bundle loaded from SharedPreferences
     */
    private Bundle loadPreferencesBundle(String key) {
        Bundle bundle = new Bundle();
        Map<String, ?> all = sharedPrefs.getAll();
        Iterator<String> it = all.keySet().iterator();
        String prefKeyPrefix = key + SAVED_PREFS_BUNDLE_KEY_SEPARATOR;
        Set<String> subBundleKeys = new HashSet<>();

        while (it.hasNext()) {

            String prefKey = it.next();

            if (prefKey.startsWith(prefKeyPrefix)) {
                String bundleKey = StringUtils.removeStart(prefKey, prefKeyPrefix);

                if (!bundleKey.contains(SAVED_PREFS_BUNDLE_KEY_SEPARATOR)) {

                    Object o = all.get(prefKey);
                    if (o == null) {
                        // Ignore null keys
                    } else if (o instanceof Integer) {
                        bundle.putInt(bundleKey, (Integer) o);
                    } else if (o instanceof Long) {
                        bundle.putLong(bundleKey, (Long) o);
                    } else if (o instanceof Boolean) {
                        bundle.putBoolean(bundleKey, (Boolean) o);
                    } else if (o instanceof CharSequence) {
                        bundle.putString(bundleKey, ((CharSequence) o).toString());
                    }
                } else {
                    // Key is for a sub bundle
                    String subBundleKey = StringUtils.substringBefore(bundleKey, SAVED_PREFS_BUNDLE_KEY_SEPARATOR);
                    subBundleKeys.add(subBundleKey);
                }
            } else {
                // Key is not related to this bundle.
            }
        }
        // Recursively process the sub-bundles
        for (String subBundleKey : subBundleKeys) {
            Bundle subBundle = loadPreferencesBundle(prefKeyPrefix + subBundleKey);
            bundle.putBundle(subBundleKey, subBundle);
        }
        return bundle;
    }
}

