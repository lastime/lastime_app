package com.abfactory.lastime.utils;

import android.app.Activity;
import android.app.ActivityManager;
import android.app.job.JobInfo;
import android.app.job.JobScheduler;
import android.content.ComponentName;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.media.ExifInterface;
import android.media.ThumbnailUtils;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.support.v7.widget.CardView;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.abfactory.lastime.Lastime;
import com.abfactory.lastime.R;
import com.abfactory.lastime.component.BadgeDrawable;
import com.abfactory.lastime.component.draggablelist.DraggableListItem;
import com.abfactory.lastime.model.ColorPaletteElement;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.service.LastimeJobService;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.common.base.Strings;

import org.joda.time.DateTime;
import org.joda.time.Days;
import org.joda.time.Interval;
import org.joda.time.Months;
import org.joda.time.Period;
import org.joda.time.Weeks;
import org.joda.time.Years;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

import java.io.File;
import java.text.DecimalFormat;
import java.text.Normalizer;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import javax.annotation.Nullable;

/**
 * Use this class only for common methods that could be shared between activities
 */
public class LasTimeUtils {

    public static final String TAG = LasTimeUtils.class.getName();

    // Image file storage
    public static final String IMAGE_FOLDER_NAME = "images";

    // Google images
    public static final String GOOGLE_ACCOUNT_IMAGE = "googleAccountImage.jpg";
    public static final String GOOGLE_ACCOUNT_COVER_IMAGE = "googleAccountCoverImage.jpg";

    // Strings
    public static final String WEEKS = "WEEKS";
    public static final String MONTHS = "MONTHS";
    public static final String YEARS = "YEARS";
    public static final String DAYS = "DAYS";


    public enum HighlightableFields {
        NothingToHighLight(Lastime.getContext().getString(R.string.nothing_to_highlight)),
        Note(Lastime.getContext().getString(R.string.note_field_name)),
        Location(Lastime.getContext().getString(R.string.location_field_name));

        private String value;

        HighlightableFields(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }
    }

    /**
     * Method to generate a thumbnails based on bitmap picture and view
     *
     * @param source
     * @param imageView
     * @return scaled picture as bitmap
     */
    public static Bitmap cropAndScale(Bitmap source, View imageView) {
        imageView.setMinimumHeight((int) LasTimeUtils.convertDpToPixel(172, imageView.getContext()));
        imageView.setMinimumWidth(imageView.getContext().getResources().getDisplayMetrics().widthPixels);
        return ThumbnailUtils.extractThumbnail(source, imageView.getMinimumWidth(), imageView.getMinimumHeight());
    }

    /**
     * list all photo contained in folder that are not linked to an event
     */
    public static String listStorageFolder(IEventDAO eventDAO, File storageDirectory) {
        Set<String> imagesPath = eventDAO.retrieveAllImagePaths();
        StringBuilder stringBuilder = new StringBuilder();
        File[] files = storageDirectory.listFiles();
        if (files != null) {
            for (File file : files) {
                if (!imagesPath.contains(file.getAbsolutePath())) {
                    stringBuilder.append(file.getName() + ", size: " + file.length() / 1024 + " Ko \n");
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * Clean up images folder
     */
    public static int cleanStorageFolder(IEventDAO eventDAO, File storageDirectory) {
        Set<String> imagesPath = eventDAO.retrieveAllImagePaths();
        File[] files = storageDirectory.listFiles();
        Map<File, Boolean> imagesToDelete = new HashMap<>();
        if (files != null) {
            for (File file : files) {
                imagesToDelete.put(file, true);
                for (String path : imagesPath)
                    if (file.getName().equals(path.substring(path.lastIndexOf('/') + 1))) {
                        imagesToDelete.put(file, false);
                        break;
                    }
            }
        }
        int counter = 0;
        for (Map.Entry<File, Boolean> entry : imagesToDelete.entrySet()) {
            if (entry.getValue() && entry.getKey().delete()) {
                Log.i(TAG, entry.getKey() + " has been deleted, size: " + entry.getKey().length() / 1024 + " Ko");
                counter++;
            }
        }
        return counter;
    }

    /**
     * Deleted image by path
     */
    public static void deleteImage(String path) {
        if (new File(path).delete())
            Log.d(TAG, path + " has been deleted");
    }

    public static Bitmap createCircleImageSized(int size, int color, String text, int textSize) {
        // Get first letter
        String letter = "X"; // To be aware of a trouble with title
        // Find a better algo!!! :D
        int space = textSize / 3;
        if (textSize == 14)
            space = textSize / 4;
        else if (textSize == 10)
            space = textSize / 3;
        if (text != null && text.length() > 1) {
            space = space * 2 + 1;
        }

        // Draw background
        Bitmap bitmap = Bitmap.createBitmap(size, size, android.graphics.Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        paint.setDither(true);

        Canvas canvas = new Canvas(bitmap);
        paint.setColor(color);
        canvas.drawRect(0, 0, size, size, paint);

        // Draw letter
        paint.setColor(Color.WHITE);
        paint.setTextSize(textSize);
        paint.setTypeface(Typeface.MONOSPACE);
        paint.setAntiAlias(true);
        int xPos = canvas.getWidth() / 2 - space;
        int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2));
        if (text != null)
            canvas.drawText(text, xPos, yPos, paint);
        else
            canvas.drawText(letter, xPos, yPos, paint);

        return bitmap;
    }

    /**
     * Create small bitmap with letter and random background color
     *
     * @param event
     * @return bitmap generated
     */
    public static Bitmap createCircleImage(Event event) {
        // Get first letter
        String letter = "Z"; // To be aware of a trouble with title
        if (event.getTitle() != null && event.getTitle().length() > 0)
            letter = "" + Character.toUpperCase(flattenToAscii(event.getTitle()).charAt(0));

        // Draw background
        Bitmap bitmap = Bitmap.createBitmap(128, 128, android.graphics.Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        paint.setDither(true);

        Canvas canvas = new Canvas(bitmap);
        paint.setColor(event.getColor());
        canvas.drawRect(0, 0, 128, 128, paint);

        // Draw letter
        paint.setColor(Color.parseColor("#FFEBEFF1"));
        paint.setTextSize(200);
        paint.setTypeface(Typeface.MONOSPACE);
        paint.setAntiAlias(true);
        int xPos = canvas.getWidth() / 2 - 80;
        int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2) + 25);
        canvas.drawText(letter, xPos, yPos, paint);

        return bitmap;
    }

    /**
     * Create small bitmap with letter and random background color
     *
     * @param event
     * @return bitmap generated
     */
    public static Bitmap createCircleNotification(Event event, Bitmap pic) {
        // Draw background
        Bitmap bitmap = Bitmap.createBitmap(128, 128, android.graphics.Bitmap.Config.ARGB_8888);
        Paint paint = new Paint();
        paint.setDither(true);

        Canvas canvas = new Canvas(bitmap);
        paint.setColor(event.getColor());
        canvas.drawRect(0, 0, 128, 128, paint);

        // Draw letter
        paint.setColor(Color.WHITE);
        paint.setTextSize(70);
        int xPos = canvas.getWidth() / 2 - 34;
        int yPos = canvas.getHeight() / 2 - 34;
        canvas.drawBitmap(pic, xPos, yPos, paint);

        return bitmap;
    }

    /**
     * Get randoms color to use as background on panels
     *
     * @return int color
     */
    public static int getRandomColor() {
        Random rand = new Random();
        int randomNumber = rand.nextInt(ColorPaletteElement.colors.size());
        return Color.parseColor(ColorPaletteElement.colors.get(randomNumber).getCode());
    }

    /**
     * Use to hide keyboard from an activity
     *
     * @param activity
     */
    public static void hideKeyboard(Activity activity) {
        InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    /**
     * Get radio button Mark by int
     *
     * @param mark
     */
    public static int getRadioButtonIdForMark(int mark) {
        switch (mark) {
            case 1:
                return R.id.radiogood;
            case 2:
                return R.id.radioaverage;
            case 3:
                return R.id.radiobad;
            default:
                return -1;
        }
    }

    public static double gps2m(EventLocation a, EventLocation b) {
        float pk = (float) (180 / 3.14169);

        double lat_a = a.getLatitude();
        double lng_a = a.getLongitude();
        double lat_b = b.getLatitude();
        double lng_b = b.getLongitude();

        double a1 = lat_a / pk;
        double a2 = lng_a / pk;
        double b1 = lat_b / pk;
        double b2 = lng_b / pk;

        double t1 = Math.cos(a1) * Math.cos(a2) * Math.cos(b1) * Math.cos(b2);
        double t2 = Math.cos(a1) * Math.sin(a2) * Math.cos(b1) * Math.sin(b2);
        double t3 = Math.sin(a1) * Math.sin(b1);
        double tt = Math.acos(t1 + t2 + t3);

        return 6366000 * tt;
    }

    public static String getDisplayableDistanceBetweenTwoLocations(EventLocation a, EventLocation b) {

        NumberFormat formatter = new DecimalFormat("#0.0");

        double result = gps2m(a, b);
        if (result < 100) {
            return Math.round(result) + " m";
        } else {
            return formatter.format(result / 1000) + " km";
        }
    }

    /**
     * Method auto rotates photo depending on orientation
     */
    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {
        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            case ExifInterface.ORIENTATION_NORMAL:
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        } catch (OutOfMemoryError e) {
            Log.e(TAG, "Critical error due to OOM!! :(", e);
            return null;
        }
    }

    public static boolean hasLocationOtherReferences(Long locationId, List<Event> allEvents) {
        int count = 0;
        for (Event e : allEvents) {
            if (e.getLocation() == locationId) {
                count++;
            }
            // Means we have at least 2 events (including the one we are deleting) that reference the same location
            if (count > 1) {
                return true;
            }
        }
        return false;
    }

    public static boolean isValidEmail(String email) {
        return email != null && email.length() > 2 && email.contains("@");
    }

    // Correct password check (ie as per settings in current preferences)
    public static boolean isPasswordCorrect(PreferencesHandler ph, String pwd) {
        return pwd != null && pwd.equals(ph.getPasswordForApp());
    }

    // Valid password check (ie definition of possible passwords)
    public static boolean isPasswordValid(String pwd) {
        return pwd != null && !pwd.isEmpty();
    }

    // Valid password creation checks
    public static boolean isValidPasswordCreation(String newPassword, String confPassword) {
        return isPasswordValid(newPassword) && isPasswordValid(confPassword) && confPassword.equals(newPassword);
    }

    public static String generateRandom4DigitsCode() {
        StringBuilder result = new StringBuilder();
        Random rand = new Random();
        for (int i = 0; i < 4; i++) {
            result.append(Integer.toString(rand.nextInt(10)));
        }
        return result.toString();
    }

    public static String flattenToAscii(String text) {
        char[] out = new char[text.length()];
        text = Normalizer.normalize(text, Normalizer.Form.NFD);
        int j = 0;
        for (int i = 0, n = text.length(); i < n; ++i) {
            char c = text.charAt(i);
            if (c <= '\u007F')
                out[j++] = c;
        }
        return new String(out);
    }

    public static String displayTime(int minutes) {
        int hour = minutes / 60;
        int minute = minutes % 60;
        return hour + ":" + String.format("%02d", minute);
    }

    public static int parseTime(String time) {
        if (time == null || time.length() < 1)
            return -1;
        String[] entries = time.split(":");
        int hour = Integer.parseInt(entries[0]);
        int minutes = Integer.parseInt(entries[1]);
        return hour * 60 + minutes;
    }

    /**
     * Method that computes key section
     *
     * @param eventDate
     * @param referenceDate
     * @return days as section identifier
     */
    public static int computeDaysDifference(Date eventDate, Date referenceDate) {
        Interval interval = new Interval(eventDate.getTime(), referenceDate.getTime());
        Period period = interval.toPeriod();

        // Logic for more than one week, one month, etc...
        if (period.getWeeks() > 0 && period.getMonths() == 0 && period.getYears() == 0) {
            return Weeks.weeksIn(interval).getWeeks() * 7 + 1;
        } else if (period.getMonths() > 0 && period.getYears() == 0) {
            return Months.monthsIn(interval).getMonths() * 30;
        } else if (period.getYears() > 0)
            return Years.yearsIn(interval).getYears() * 365;

        return Days.daysBetween(new DateTime(eventDate).toLocalDate(), new DateTime(referenceDate).toLocalDate()).getDays();
    }

    /**
     * Get a diff between two dates
     *
     * @param date1    the oldest date
     * @param date2    the newest date
     * @param timeUnit the unit in which you want the diff
     * @return the diff value, in the provided unit
     */
    public static long getDateDiff(Date date1, Date date2, TimeUnit timeUnit) {
        long diffInMillies = date2.getTime() - date1.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    /**
     * Method that creates labels on main activity
     *
     * @param context
     * @param startDate
     * @param endDate
     * @return label localized
     */
    public static String computeTimelineOutput(Context context, Date startDate, Date endDate, int numberOfDays) {
        Interval interval = new Interval(startDate.getTime(), endDate.getTime());
        Period period = interval.toPeriod();

        PeriodFormatter formatter = null;

        boolean isFirstWeek = numberOfDays < 8;

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE", Locale.getDefault());

        if (numberOfDays == 0)
            return context.getString(R.string.today);
        else if (numberOfDays == 1)
            return context.getString(R.string.yesterday);
        else if (isFirstWeek) {
            String day = sdf.format(startDate);
            return context.getString(R.string.day_prefix) + " " +
                    day.substring(0, 1).toUpperCase() + day.substring(1) + " " +
                    context.getString(R.string.day_suffix);
        } else if (period.getMonths() == 0 && period.getYears() == 0) {
            formatter = new PeriodFormatterBuilder()
                    .appendPrefix(context.getString(R.string.date_prefix) + " ")
                    .appendWeeks()
                    .appendSuffix(" " + context.getString(R.string.week) + " ", " " + context.getString(R.string.weeks) + " ")
                    .appendSuffix(context.getString(R.string.date_suffix))
                    .printZeroNever().toFormatter();
        } else if (period.getMonths() > 0 && period.getYears() == 0) {
            formatter = new PeriodFormatterBuilder()
                    .appendPrefix(context.getString(R.string.date_prefix) + " ")
                    .appendMonths()
                    .appendSuffix(" " + context.getString(R.string.month) + " ", " " + context.getString(R.string.months) + " ")
                    .appendSuffix(context.getString(R.string.date_suffix))
                    .printZeroNever().toFormatter();
        } else if (period.getMonths() == 0 && period.getYears() > 0) {
            formatter = new PeriodFormatterBuilder()
                    .appendPrefix(context.getString(R.string.date_prefix) + " ")
                    .appendYears().appendSuffix(" " + context.getString(R.string.year) + " ", " " + context.getString(R.string.years) + " ")
                    .appendSuffix(context.getString(R.string.date_suffix))
                    .printZeroNever().toFormatter();
        } else if (period.getMonths() > 0 && period.getYears() > 0)
            formatter = new PeriodFormatterBuilder()
                    .appendPrefix(context.getString(R.string.date_prefix) + " ")
                    .appendYears().appendSuffix(" " + context.getString(R.string.year) + " ", " " + context.getString(R.string.years) + " ")
                    .appendPrefix(period.getYears() < 1 ? context.getString(R.string.date_prefix) + " " : "")
                    .appendPrefix(period.getYears() > 0 ? context.getString(R.string.and) + " " : "")
                    .appendMonths().appendSuffix(" " + context.getString(R.string.month) + " ", " " + context.getString(R.string.months) + " ")
                    .appendSuffix(context.getString(R.string.date_suffix))
                    .printZeroNever().toFormatter();

        //TODO display hours if time defined? (EVOLUTION)

        if (formatter != null)
            return formatter.print(period);
        else return "Error";
    }

    public static void setBadgeCount(Context context, LayerDrawable icon, int count) {

        BadgeDrawable badge;

        // Reuse drawable if possible
        Drawable reuse = icon.findDrawableByLayerId(R.id.ic_badge);
        if (reuse != null && reuse instanceof BadgeDrawable) {
            badge = (BadgeDrawable) reuse;
        } else {
            badge = new BadgeDrawable(context);
        }

        badge.setCount(count);
        icon.mutate();
        icon.setDrawableByLayerId(R.id.ic_badge, badge);
    }

    /**
     * Display purpose. Converts tag into displayable text.
     *
     * @param tag - Format is [NUMBER]_[UNIT]
     * @return string to display
     */
    public static String convertRecurrenceTagToString(String tag, Activity activity) {

        if (tag == null || Recurrence.NONE.name().equals(tag) || tag.isEmpty()) {
            return "";
        }

        if ("1_DAYS".equals(tag)) {
            return activity.getResources().getString(R.string.every_day);
        } else if ("1_WEEKS".equals(tag)) {
            return activity.getResources().getString(R.string.every_week);
        } else if ("1_MONTHS".equals(tag)) {
            return activity.getResources().getString(R.string.every_month);
        } else if ("1_YEARS".equals(tag)) {
            return activity.getResources().getString(R.string.every_year);
        } else {
            String number = tag.split("_")[0];
            String unit = tag.split("_")[1];
            String localizedUnit;
            switch (unit) {
                case DAYS:
                    localizedUnit = activity.getResources().getString(R.string.duration_days);
                    break;
                case WEEKS:
                    localizedUnit = activity.getResources().getString(R.string.duration_weeks);
                    break;
                case MONTHS:
                    localizedUnit = activity.getResources().getString(R.string.duration_months);
                    break;
                case YEARS:
                    localizedUnit = activity.getResources().getString(R.string.duration_years);
                    break;
                default:
                    localizedUnit = "";
                    break;
            }
            return activity.getResources().getString(R.string.event_repeats_text) + " " + number + " " + localizedUnit;
        }
    }

    /**
     * @param number                 from edit text
     * @param durationPopupSelection 0-> days
     *                               1-> weeks
     *                               2-> months
     *                               3-> years
     * @return
     */
    public static String generateTagFromRecurrencePopupSelection(String number, int durationPopupSelection) {
        switch (durationPopupSelection) {
            case 0:
                return number + "_DAYS";
            case 1:
                return number + "_WEEKS";
            case 2:
                return number + "_MONTHS";
            case 3:
                return number + "_YEARS";
            default:
                return Recurrence.NONE.name();
        }
    }

    public static boolean isRecurrenceActivated(String tag) {
        return !(tag == null || Recurrence.NONE.name().equals(tag) || tag.isEmpty());
    }

    public static boolean isReminderActivated(String tag) {
        return !(tag == null || Recurrence.NONE.name().equals(tag) || tag.isEmpty());
    }

    public static Period extractRecurrenceOrReminderUnitAsPeriod(String tag) {
        switch (tag.split("_")[1]) {
            case DAYS:
                return Period.days(Integer.parseInt(tag.split("_")[0]));
            case WEEKS:
                return Period.weeks(Integer.parseInt(tag.split("_")[0]));
            case MONTHS:
                return Period.months(Integer.parseInt(tag.split("_")[0]));
            case YEARS:
                return Period.years(Integer.parseInt(tag.split("_")[0]));
            default:
                return Period.days(0);
        }
    }

    public static boolean areValidsRecurrenceAndReminder(String recurrenceTag, String reminderTag, Date referenceDate) {
        DateTime dateForCheck = new DateTime(referenceDate);
        dateForCheck = dateForCheck.plus(extractRecurrenceOrReminderUnitAsPeriod(recurrenceTag));
        dateForCheck = dateForCheck.minus(extractRecurrenceOrReminderUnitAsPeriod(reminderTag));
        return dateForCheck.isBefore(new DateTime(referenceDate));
    }

    /**
     * Display purpose. Converts tag into displayable text.
     *
     * @param tag - Format is [NUMBER]_[UNIT]
     * @return string to display
     */
    public static String convertReminderTagToString(String tag, Activity activity) {

        if (tag == null || "NONE".equals(tag) || tag.isEmpty()) {
            return "";
        }

        if ("1_DAYS".equals(tag)) {
            return activity.getResources().getString(R.string.one_day_before);
        } else if ("1_WEEKS".equals(tag)) {
            return activity.getResources().getString(R.string.one_week_before);
        } else if ("1_MONTHS".equals(tag)) {
            return activity.getResources().getString(R.string.one_month_before);
        } else {
            String number = tag.split("_")[0];
            String unit = tag.split("_")[1];
            String localizedUnit;
            switch (unit) {
                case DAYS:
                    localizedUnit = activity.getResources().getString(R.string.duration_days);
                    break;
                case WEEKS:
                    localizedUnit = activity.getResources().getString(R.string.duration_weeks);
                    break;
                case MONTHS:
                    localizedUnit = activity.getResources().getString(R.string.duration_months);
                    break;
                case YEARS:
                    localizedUnit = activity.getResources().getString(R.string.duration_years);
                    break;
                default:
                    localizedUnit = "";
                    break;
            }

            return "" + number + " " + localizedUnit + " " + activity.getResources().getString(R.string.reminder_text);
        }
    }

    /**
     * Simple days difference between dates - No logic
     **/
    public static int getDaysDifference(Date eventDate, Date referenceDate) {
        return Days.daysBetween(new DateTime(eventDate).toLocalDate(), new DateTime(referenceDate).toLocalDate()).getDays();
    }

    /**
     * Create an array of int with colors
     */
    public static int[] colorChoice() {
        int[] mColorChoices = null;
        if (ColorPaletteElement.colors != null && !ColorPaletteElement.colors.isEmpty()) {
            mColorChoices = new int[ColorPaletteElement.colors.size()];
            for (int i = 0; i < ColorPaletteElement.colors.size(); i++) {
                mColorChoices[i] = Color.parseColor(ColorPaletteElement.colors.get(i).getCode());
            }
        }
        return mColorChoices;
    }

    /**
     * Parse whiteColor
     *
     * @return
     */
    public static int parseWhiteColor() {
        return Color.parseColor("#FFFFFF");
    }

    /**
     * Check if dates are in same year
     */
    public static boolean areInSameYear(Date d1, Date d2) {
        DateTime dt1 = new DateTime(d1);
        DateTime dt2 = new DateTime(d2);

        return dt1.getYear() == dt2.getYear();
    }

    /**
     * Computes all colors in a gradient
     *
     * @param mainColor
     * @param numberOfColors
     * @return list of colors
     */
    public static List<Integer> generateGradient(int mainColor, int numberOfColors) {

        numberOfColors = numberOfColors * 2;

        ArrayList<Integer> colors = new ArrayList<>();

        int toColor = Color.WHITE;

        int r = (Color.red(mainColor) - Color.red(toColor)) / ((numberOfColors == 2 ? 1 : numberOfColors) - 2);
        int g = (Color.green(mainColor) - Color.green(toColor)) / ((numberOfColors == 2 ? 1 : numberOfColors) - 2);
        int b = (Color.blue(mainColor) - Color.blue(toColor)) / ((numberOfColors == 2 ? 1 : numberOfColors) - 2);

        for (int i = 0; i < numberOfColors; i++) {
            Paint p = new Paint();
            p.setColor(Color.rgb(Color.red(mainColor) - i * r, Color.green(mainColor) - i * g, Color.blue(mainColor) - i * b));
            colors.add(p.getColor());
        }
        return colors;
    }

    /**
     * Method checks if phone has a network connection
     *
     * @param context
     * @return true if network detected
     */
    public static boolean isDeviceOnline(Context context) {
        ConnectivityManager connMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connMgr.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }

    /**
     * Check that Google Play services APK is installed and up to date. Will
     * launch an error dialog for the user to update Google Play Services if
     * possible.
     *
     * @return true if Google Play Services is available and up to
     * date on this device; false otherwise.
     */
    public static boolean isGooglePlayServicesAvailable(Context context) {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        return !(connectionStatusCode == ConnectionResult.SERVICE_DISABLED ||
                connectionStatusCode == ConnectionResult.SERVICE_MISSING);
    }

    /**
     * Check Google play services - Only success accepted
     **/
    public static boolean isGooglePlayServiceUpToDate(Context context) {
        final int connectionStatusCode = GooglePlayServicesUtil.isGooglePlayServicesAvailable(context);
        return connectionStatusCode == ConnectionResult.SUCCESS;
    }

    /**
     * Current image file storage directory
     **/
    public static File getImageStorageDir(Context ctx) {
        return ctx.getDir(LasTimeUtils.IMAGE_FOLDER_NAME, Context.MODE_PRIVATE);
    }

    /**
     * Logic to save image file coming from internet
     *
     * @param path
     */
    public static String saveImageFile(String path, Context ctx, boolean forceOverwrite, String forcedFileName) {
        // If image is coming from internet
        if (path.startsWith("http://") || path.startsWith("https://")) {
            String filename;
            if (forcedFileName == null)
                filename = path.substring(path.lastIndexOf('/') + 1);
            else
                filename = forcedFileName;

            boolean isExisting = false;

            if (!forceOverwrite) {
                String[] imageNames = LasTimeUtils.getImageStorageDir(ctx).list();
                // Check image does not exist
                for (String imageName : imageNames) {
                    if (imageName.equals(filename)) {
                        isExisting = true;
                        //TODO handle existing and rename
                        break;
                    }
                }
            }

            if (!isExisting) {
                // Download file
                new DownloadImageFromServerTask(ctx).execute(path, filename);
            }
            return filename;
        }
        return null;
    }

    /**
     * Method to retrieve color based on SDK used
     **/
    public static int getColor(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return context.getResources().getColor(id, null);
        else
            return context.getResources().getColor(id);
    }

    /**
     * Method to retrieve ressources based on SDK used
     **/
    public static Drawable getDrawable(Context context, int id) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M)
            return context.getResources().getDrawable(id, null);
        else
            return context.getResources().getDrawable(id);
    }

    /**
     * Get width in dp of screen
     **/
    public static int getWidthScreen(Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        return (int) (displayMetrics.widthPixels / displayMetrics.density);
    }

    /**
     * Get width in dp of screen
     **/
    public static int getWidthCardView(CardView cardView) {
        DisplayMetrics displayMetrics = cardView.getResources().getDisplayMetrics();
        return (int) (displayMetrics.widthPixels / displayMetrics.density);
    }

    /**
     * Get height in dp of screen
     **/
    public static int getHeightCardView(CardView cardView) {
        DisplayMetrics displayMetrics = cardView.getResources().getDisplayMetrics();
        return (int) (displayMetrics.heightPixels / displayMetrics.density);
    }

    /**
     * Check availability of LasTime service
     **/
    public static boolean isLastimeServiceRunning(Activity activity, Class<?> serviceClass) {
        ActivityManager manager = (ActivityManager) activity.getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.getName().equals(service.service.getClassName())) {
                return true;
            }
        }
        return false;
    }

    // schedule the start of the service every 10 - 30 seconds
    public static void scheduleJob(Context context) {
        ComponentName serviceComponent = new ComponentName(context, LastimeJobService.class);
        JobInfo.Builder builder = new JobInfo.Builder(0, serviceComponent);
        builder.setMinimumLatency(60 * 60 * 1000); // wait at least 30 min
        builder.setOverrideDeadline(180 * 30 * 1000); // maximum delay 2 H
        JobScheduler jobScheduler = context.getSystemService(JobScheduler.class);
        jobScheduler.schedule(builder.build());
        Log.i(TAG, "Lastime service has been scheduled");
    }

    /**
     * Testing string content
     **/
    public static boolean isNotEmpty(CharSequence str) {
        return !isEmpty(str);
    }

    public static boolean isEmpty(CharSequence str) {
        return str == null || str.length() == 0;
    }

    /**
     * list events converter to string
     * shouldnot be called on empty list
     */
    public static String listASeriesOfEvents(List<Event> events) {
        StringBuilder stringBuilder = new StringBuilder();
        for (Event e : events) {
            stringBuilder.append(e.getTitle() + ", ");
        }
        if (!events.isEmpty()) {
            stringBuilder.setLength(stringBuilder.length() - 2);
            stringBuilder.append(".");
        }
        return stringBuilder.toString();
    }

    /**
     * Return locale, based on API version
     **/
    public static Locale getLocale(Context context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N)
            return context.getResources().getConfiguration().getLocales().get(0);
        else
            return context.getResources().getConfiguration().locale;
    }

    public enum Recurrence {
        NONE
    }

    /**
     * DISPLAY PREFERENCES
     **/

    public static List<DraggableListItem> getDisplayPreferences(Event event) {
        List<DraggableListItem> result = new ArrayList<>();
        String displayPreferences = DraggableListItem.FIELDS_DEFAULT;
        if (event != null && !Strings.isNullOrEmpty(event.getFieldList())) {
            displayPreferences = checkIfNewFields(event.getFieldList());
        }
        //FIXME: what to do if new filed added after fieldlist creation????
        result.add(new DraggableListItem(R.string.draggable_list_item_default_shown, true, false));
        String[] elements = displayPreferences.split(";");
        for (String ele : elements) {
            if (ele.equals("DIVIDER")) {
                result.add(new DraggableListItem(R.string.draggable_list_item_default_hidden, false, true));
            } else {
                result.add(DraggableListItem.getDraggableListItemForId(Integer.parseInt(ele)));
            }
        }
        return result;
    }

    private static String checkIfNewFields(String displayPref) {
        String[] userDef = displayPref.split(";");
        String[] defaultDef = DraggableListItem.FIELDS_DEFAULT.split(";");
        Predicate<String> notInDefault = s -> !Arrays.asList(userDef).stream().anyMatch(value -> s.equals(value));
        List<String> missingFields = Arrays.asList(defaultDef).stream().filter(notInDefault).collect(Collectors.toList());
        for (String ele : missingFields) {
            displayPref = displayPref.concat(";" + ele);
        }
        return displayPref;
    }

    public static String formatDisplayPreferences(List<DraggableListItem> displayPreferences) {
        String displayPreferencesString = "";
        for (int i = 1; i < displayPreferences.size(); i++) {
            if (displayPreferences.get(i).isHiddenItemSeparator()) {
                displayPreferencesString += "DIVIDER";
            } else {
                displayPreferencesString += displayPreferences.get(i).getId();
            }
            if (i != displayPreferences.size() - 1) {
                displayPreferencesString += ";";
            }
        }
        return displayPreferencesString;
    }

    public static List<DraggableListItem> getOrderedSections(Event event) {
        List<DraggableListItem> result = getOrderedSectionsToBeDisplayed(event);
        result.addAll(getOrderedSectionsToBeHidden(event));
        return result;
    }

    public static List<DraggableListItem> getOrderedSectionsToBeDisplayed(Event event) {
        List<DraggableListItem> result = getDisplayPreferences(event);
        if (result != null) {
            int dividerPosition = getPositionForDivider(result);
            return result.subList(1, dividerPosition);
        }
        return null;
    }

    public static List<DraggableListItem> getOrderedSectionsToBeHidden(Event event) {
        List<DraggableListItem> result = getDisplayPreferences(event);
        if (result != null) {
            int dividerPosition = getPositionForDivider(result);
            return result.subList(dividerPosition + 1, result.size());
        }
        return null;
    }

    private static int getPositionForDivider(List<DraggableListItem> result) {
        for (int i = 0; i < result.size(); i++) {
            if (result.get(i).isHiddenItemSeparator()) {
                return i;
            }
        }
        return 0;
    }

    /**
     * Get radio button Weather by int
     *
     * @param weather
     */
    public static int getRadioButtonIdForWeather(int weather) {
        switch (weather) {
            case 1:
                return R.id.radioSunny;
            case 2:
                return R.id.radioCloudy;
            case 3:
                return R.id.radioWindy;
            case 4:
                return R.id.radioRaining;
            default:
                return -1;
        }
    }

    /**
     * This method converts dp unit to equivalent pixels, depending on device density.
     *
     * @param dp      A value in dp (density independent pixels) unit. Which we need to convert into pixels
     * @param context Context to get resources and device specific display metrics
     * @return A float value to represent px equivalent to dp depending on device density
     */
    public static float convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return px;
    }

    @Nullable
    public static String getHighlightValue(Event event) {
        String output = null;
        if (event.getHighlight().equals(HighlightableFields.Note.getValue())) {
            output = event.getNote().replaceAll("\\*", "\n");
        } else if (event.getHighlight().equals(HighlightableFields.Location.getValue()) && event.getLocation() > -1) {
            EventLocation location = new EventDAO(Lastime.getContext()).getLocationById(event.getLocation());
            output = location.getName();
        } else if (event.getHighlight() != null) {
            output = event.getHighlight();
        }
        return output;
    }

    public static List<String> getFieldsToHighlight() {
        ArrayList fields = new ArrayList();
        fields.add(HighlightableFields.NothingToHighLight.getValue());
        fields.add(HighlightableFields.Note.getValue());
        fields.add(HighlightableFields.Location.getValue());
        return fields;
    }
}
