package com.abfactory.lastime.utils;

import android.util.Base64;
import android.util.Log;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.Key;

import javax.crypto.Cipher;
import javax.crypto.CipherInputStream;
import javax.crypto.CipherOutputStream;
import javax.crypto.spec.SecretKeySpec;

/**
 * Class used to export or import events stored in db
 */
public class Codec {

    private static final String ALGO = "AES";
    private static final byte[] keyValue = new byte[]{'L', 'a', 's', 'T', 'i', 'm', 'e', ' ', 'p', 'o', 'w', 'e', 'r', 'k', 'e', 'y'};
    private File file;

    public Codec(File file) {
        this.file = file;
    }

    // ------------------------ Encryption ------------------------------------------

    private Key generateKey() {
        return new SecretKeySpec(keyValue, ALGO);
    }

    public void encryptFile() {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        CipherOutputStream cos = null;

        // File you are reading from
        try {
            fis = new FileInputStream(file);
            // File output
            File tempFile = new File(file.getParent(), "tmpEncrypted.txt");
            fos = new FileOutputStream(tempFile);

            // Here the file is encrypted. The cipher1 has to be created.
            // Key Length should be 128, 192 or 256 bit => i.e. 16 byte
            Key skeySpec = generateKey();
            Cipher cipher1 = Cipher.getInstance(ALGO);
            cipher1.init(Cipher.ENCRYPT_MODE, skeySpec);
            cos = new CipherOutputStream(fos, cipher1);
            // Here you read from the file in fis and write to cos.
            byte[] b = new byte[8];
            int i = fis.read(b);
            while (i != -1) {
                cos.write(b, 0, i);
                i = fis.read(b);
            }
            cos.flush();
            fis.close();
            fos.close();
            tempFile.renameTo(file);
            tempFile.delete();
        } catch (Exception e) {
            Log.e(Codec.class.getName(), "Error occurred while encrypting file: " + e);
        } finally {
            try {
                if (fis != null)
                    fis.close();
                if (fos != null)
                    fos.close();
                if (cos != null)
                    cos.close();
            } catch (IOException e) {
                // Don't care
            }
        }
    }

    public void decryptFile() {
        FileInputStream fis = null;
        FileOutputStream fos = null;
        CipherInputStream cis = null;

        // File you are reading from
        try {
            fis = new FileInputStream(file);
            // File output
            File tempFile = new File(file.getParent(), "tmpDecrypted.txt");
            fos = new FileOutputStream(tempFile);

            // Here the file is encrypted. The cipher1 has to be created.
            // Key Length should be 128, 192 or 256 bit => i.e. 16 byte
            Key skeySpec = generateKey();
            Cipher cipher1 = Cipher.getInstance(ALGO);
            cipher1.init(Cipher.ENCRYPT_MODE, skeySpec);
            cis = new CipherInputStream(fis, cipher1);

            int b;
            byte[] d = new byte[8];
            while ((b = cis.read(d)) != -1) {
                fos.write(d, 0, b);
            }
            fis.close();
            fos.close();

            tempFile.renameTo(file);
            tempFile.delete();
        } catch (Exception e) {
            Log.e(Codec.class.getName(), "Error occurred while decrypting file: " + e);
        } finally {
            try {
                fis.close();
                fos.close();
                cis.close();
            } catch (IOException e) {
                // Don't care
            }
        }
    }

    public String encryptData(String data) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.ENCRYPT_MODE, key);
        byte[] encVal = c.doFinal(data.getBytes());
        return Base64.encodeToString(encVal, Base64.DEFAULT);
    }

    public String decryptData(String encryptedData) throws Exception {
        Key key = generateKey();
        Cipher c = Cipher.getInstance(ALGO);
        c.init(Cipher.DECRYPT_MODE, key);
        byte[] decodedValue = Base64.decode(encryptedData, Base64.DEFAULT);
        byte[] decValue = c.doFinal(decodedValue);
        return new String(decValue);
    }
}
