package com.abfactory.lastime.utils;

import android.content.Context;
import android.util.Log;

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * All manipulation around images
 */
public class ImageUtils {

    public static final String TAG = ImageUtils.class.getName();

    public static String encodeImageBase64(File image, Context ctx) {
        // Reading a Image file from file system
        FileInputStream imageInFile = null;
        String imageDataString = null;
        try {
            imageInFile = new FileInputStream(image);
            byte imageData[] = new byte[(int) image.length()];
            imageInFile.read(imageData);

            // Converting Image byte array into Base64 String
            imageDataString = encodeImage(imageData);
        } catch (IOException e) {
            Log.e(TAG, "Error occurred to encode image", e);
        } finally {
            try {
                imageInFile.close();
            } catch (IOException e) {
                Log.e(TAG, "Can't close input stream", e);
            }
        }
        return imageDataString;
    }

    public static File decodeImageBase64(String imageEncoded, Context ctx) {
        // Converting a Base64 String into Image byte array
        byte[] imageByteArray = decodeImage(imageEncoded);
        FileOutputStream imageOutFile = null;
        File outFile = null;
        try {
            // Write a image byte array into file system
            //TODO: generate path?
            outFile = new File(LasTimeUtils.getImageStorageDir(ctx) + "/");
            imageOutFile = new FileOutputStream(outFile);

            imageOutFile.write(imageByteArray);
        } catch (IOException e) {
            Log.e(TAG, "Error occurred to decode image", e);
        } finally {
            try {
                imageOutFile.close();
            } catch (IOException e) {
                Log.e(TAG, "Can't close output stream", e);
            }
        }
        return outFile;
    }

    private static String encodeImage(byte[] imageByteArray) {
        return Base64.encodeBase64URLSafeString(imageByteArray);
    }

    private static byte[] decodeImage(String imageDataString) {
        return Base64.decodeBase64(imageDataString);
    }

}