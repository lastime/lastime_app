package com.abfactory.lastime.utils;

import android.content.Context;
import android.content.res.AssetManager;
import android.util.Log;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Class that reads properties files
 */
public class AssetsPropertiesReader {
    private Context context;
    private Properties properties;

    public static final String LASTIME_PROPERTY_FILE = "lastime.properties";

    public AssetsPropertiesReader(Context context) {
        this.context = context;
        /**
         * Constructs a new Properties object.
         */
        properties = new Properties();
    }

    public Properties getProperties() {
        InputStream inputStream = null;
        try {
            /**
             * getAssets() Return an AssetManager instance for your
             * application's package. AssetManager Provides access to an
             * application's raw asset files;
             */
            AssetManager assetManager = context.getAssets();
            /**
             * Open an asset using ACCESS_STREAMING mode. This
             */
            inputStream = assetManager.open(LASTIME_PROPERTY_FILE);
            /**
             * Loads properties from the specified InputStream,
             */
            properties.load(inputStream);

        } catch (IOException e) {
            Log.e(AssetsPropertiesReader.class.getName(), "Unable to read property file");
        } finally {
            if (inputStream != null)
                try {
                    inputStream.close();
                } catch (IOException e) {
                    Log.e(AssetsPropertiesReader.class.getName(), "Can't close input stream");
                }
        }
        return properties;

    }

}