package com.abfactory.lastime.utils;

import android.app.NotificationManager;
import android.content.Context;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.model.Notification;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Class dedicated to notification business
 */

public class NotificationUtils {

    // Icon
    public static int LASTIME_ICON = R.drawable.icon_lastime_white_48;

    public static void activateNotifications(Context context) {
        raiseAllRelevantNotifications(context);
    }

    public static void deactivateNotifications(Context context) {
        cleanTablesOnNotificationFeatureDeactivation(context);
    }

    public static void cleanNotificationsWhenDeletingEvent(IEventDAO eventDAO, int eventUpdatedOrDeleted, Context ctx) {

        /* First delete DB entries for notifs that are not relevant anymore*/

        List<Notification> notifsToDelete = new ArrayList<Notification>();

        for (Notification notif : eventDAO.getAllActiveNotifications()) {
            // Loop other all events to check if corresponding event still there
            boolean foundRelevantEvent = false;
            for (Event event : eventDAO.getAllActiveEvents()) {
                if (notif.getEventId() == event.getId()) {
                    foundRelevantEvent = true;
                }
            }
            // Mark notif as to be deleted if relevant
            if (!foundRelevantEvent) {
                notifsToDelete.add(notif);
            }
        }

        for (Notification notif : notifsToDelete) {
            eventDAO.deleteNotification(notif.getNotificationId());
        }

        /* Then handle phone notifications */
        ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(eventUpdatedOrDeleted);

    }

    public static void createNotification(Event event, Context ctx) {
        // Calculate new occurrence to know if recurrence or reminder LASTIME_ICON to display
        Date nextEventDate = (new DateTime(event.getDate())).plus(LasTimeUtils.extractRecurrenceOrReminderUnitAsPeriod(event.getRecurrence())).toDate();

        NotificationHelper notificationHelper = new NotificationHelper(ctx);
        android.app.Notification.Builder builder = notificationHelper.getNotification(event.getTitle(),
                ctx.getResources().getString(R.string.recc_reached_on) + " " + Event.CALENDAR_DISPLAY_NOTIF.format(nextEventDate));

        notificationHelper.notify(NotificationHelper.notification_one, builder);
    }

    public static void createNotification(Context ctx){
        NotificationHelper notificationHelper = new NotificationHelper(ctx);
        android.app.Notification.Builder builder = notificationHelper.getNotification(ctx.getResources().getString(R.string.app_name),
                ctx.getResources().getString(R.string.lastime_notif_reminder));

        notificationHelper.notify(NotificationHelper.notification_one, builder);
    }

    private static void cleanTablesOnNotificationFeatureDeactivation(Context ctx) {

        IEventDAO eventDAO = new EventDAO(ctx);

        // We delete all active entries in notifications to old
        // Since setting to old would prevent the notifications to be raised if relevant
        // When reactivating the feature
        for (Notification notif : eventDAO.getAllActiveNotifications()) {
            eventDAO.deleteNotification(notif.getNotificationId());
        }

        // Clean all phone notifications but only those related to events (anticipating when we will
        // raise notifications not for events)
        for (Event e : eventDAO.getAllActiveEvents()) {
            // Event could not have reference yet
            if (e.getReference() != null)
                ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(Integer.parseInt(e.getReference()));
        }

    }

    public static void cleanPhoneNotificationForEvent(Context ctx, Event event) {
        ((NotificationManager) ctx.getSystemService(Context.NOTIFICATION_SERVICE)).cancel(Integer.parseInt(event.getReference()));
    }

    public static boolean raiseAllRelevantNotifications(Context ctx) {

        boolean newNotificationRaised = false;
        IEventDAO eventDAO = new EventDAO(ctx);

        for (Event event : eventDAO.getAllActiveEvents()) {
            if (isNotificationToBeRaisedForEvent(event) && !hasAlreadyANotification(event, eventDAO)
                    && !wasANotificationAlreadyRaisedAndSkipped(event, eventDAO)) {
                // Create notification
                com.abfactory.lastime.utils.NotificationUtils.createNotification(event, ctx);
                // Add entry to DB
                eventDAO.createNotification(new Notification(event.getId(), IEventDAO.NOTIFICATION_STATE.ACTIVE));
                // Updated ne notif identification
                newNotificationRaised = true;
            }
        }

        return newNotificationRaised;

    }

    private static boolean hasAlreadyANotification(Event event, IEventDAO eventDAO) {
        // If notification is set for an event which has same active as the event considered
        Event activeEvent;
        for (Notification notification : eventDAO.getAllActiveNotifications()) {
            activeEvent = eventDAO.getActiveEvent(eventDAO.retrieveEventById(notification.getEventId()));
            if (eventDAO.getActiveEvent(event).equals(activeEvent)) {
                return true;
            }
        }
        return false;
    }

    private static boolean wasANotificationAlreadyRaisedAndSkipped(Event event, IEventDAO eventDAO) {
        for (Notification notification : eventDAO.getAllNotifications()) {
            if (notification.getEventId() == event.getId() && IEventDAO.NOTIFICATION_STATE.OLD.equals(notification.getState())) {
                return true;
            }
        }
        return false;
    }

    /**
     * Determines if notification is to be raised i.e. relevant been given the event recurrence / reminder set-ups and last occurence
     *
     * @return true if notification to be raised for the event, false otherwise
     */
    private static boolean isNotificationToBeRaisedForEvent(Event event) {

        if (LasTimeUtils.isRecurrenceActivated(event.getRecurrence())) {
            DateTime targetDateForNotificationToBeRaised = new DateTime(event.getDate());

            // Exact time if set
            if (event.getTimeOfEvent() > 0) {
                int minutes = event.getTimeOfEvent();
                int hour = minutes / 60;
                int minute = minutes % 60;
                targetDateForNotificationToBeRaised = targetDateForNotificationToBeRaised.withHourOfDay(hour).withMinuteOfHour(minute);
            }

            // Recurrence
            targetDateForNotificationToBeRaised = targetDateForNotificationToBeRaised.plus(LasTimeUtils.extractRecurrenceOrReminderUnitAsPeriod(event.getRecurrence()));

            // Reminder
            if (LasTimeUtils.isReminderActivated(event.getReminder())) {
                targetDateForNotificationToBeRaised = targetDateForNotificationToBeRaised.minus(LasTimeUtils.extractRecurrenceOrReminderUnitAsPeriod(event.getReminder()));
            }

            // Return true if reminder computed date is past
            if (targetDateForNotificationToBeRaised.isBeforeNow()) {
                return true;
            }
        }

        return false;
    }
}
