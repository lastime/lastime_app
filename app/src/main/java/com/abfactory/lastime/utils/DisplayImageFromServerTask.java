package com.abfactory.lastime.utils;

import android.content.ContentResolver;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ImageView;

import com.abfactory.lastime.R;

import java.io.InputStream;

public class DisplayImageFromServerTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    Resources resources;

    public DisplayImageFromServerTask(Resources resources, ImageView bmImage) {
        this.bmImage = bmImage;
        this.resources = resources;
    }

    protected Bitmap doInBackground(String... urls) {
        String urldisplay = urls[0];
        Bitmap mIcon11;
        try {
            InputStream in = new java.net.URL(urldisplay).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e(DisplayImageFromServerTask.class.getName(), e.getMessage(), e);
            mIcon11 = BitmapFactory.decodeResource(resources, R.drawable.no_img);
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        bmImage.setImageBitmap(result);
    }
}
