package com.abfactory.lastime.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;
import android.widget.TextView;

import com.abfactory.lastime.activity.ImportExportActivity;
import com.abfactory.lastime.model.*;
import com.google.common.io.Files;

import java.io.*;
import java.lang.reflect.Field;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

/**
 * Task that handle import or export async
 */
public class IOEventTask extends AsyncTask<File, String, Boolean> {

    public static final String IO_FILE = "lastime_export.txt";
    public static final String EXPORTED_ZIP = "lastime_images.zip";
    private static final String EVENT = "EVENT=";
    private static final String LOCATION = "LOCATION=";
    private static final String CONTACT = "CONTACT=";
    private static final int BUFFER = 2048;

    private IEventDAO eventDAO;
    private IOTask ioTask;
    private Context context;
    private boolean isSilentMode = false;
    private TextView output;

    public IOEventTask(ImportExportActivity activity, IOTask ioTask) {
        this.context = activity;
        output = activity.getOutput();
        output.setText("");
        eventDAO = new EventDAO(activity);
        this.ioTask = ioTask;
    }

    /**
     * ONLY USED IN CASE OF SILENT EXPORT - AUTOSAVE
     **/
    public IOEventTask(Context context) {
        this.context = context;
        eventDAO = new EventDAO(context);
        isSilentMode = true;
        this.ioTask = IOTask.EXPORT;
    }

    @Override
    protected Boolean doInBackground(File... folder) {
        // Import or Export switch
        switch (ioTask) {
            case IMPORT:
                //Comment it if you need to debug
                codecFile(folder[0], false);
                int numberOfFile = importEventsFromFile(folder[0]);
                publishProgress("Total events imported : " + numberOfFile + "\n");
                importImages(folder[0]);
                break;
            case EXPORT:
                exportImages(folder[0]);
                exportAllEventsToFile(folder[0]);
                //Comment it if you need to debug
                codecFile(folder[0], true);
                break;
            default:
                Log.e(IOEventTask.class.getName(), "No task defined!");
                break;
        }
        return Boolean.TRUE;
    }

    @Override
    protected void onProgressUpdate(String... values) {
        // In silent mode no need of progress status
        if (!isSilentMode)
            output.append(values[0]);
    }

    @Override
    protected void onPostExecute(Boolean result) {
        // Nothing to be done
    }

    private File getIOFile(File folder) {
        if (folder.isDirectory()) {
            if (folder.listFiles() != null) {
                for (File file : folder.listFiles()) {
                    if (!file.isDirectory() && file.getName().equals(IO_FILE))
                        return file;
                }
            }
            return new File(folder, IO_FILE);
        }
        return null;
    }

    private void exportAllEventsToFile(File folder) {
        publishProgress("Events export started ...\n");
        // Export event
        File file = getIOFile(folder);

        List<Event> events = eventDAO.retrieveAllEvents();
        StringBuilder output = new StringBuilder();
        // Adding date of export
        output.append("Export_" + Event.CALENDAR_DISPLAY_NOTIF.format(new Date()) + "\n");

        try (BufferedWriter writer = new BufferedWriter(new FileWriter(file, false))) {
            for (Event event : events) {
                // Notify progress
                publishProgress("Exporting " + event.getTitle() + " {" +
                        (event.getCreation() != null ? event.getCreationAsString() : "") + "} ...\n");

                output.append(EVENT);
                output.append(event.exportToString());
                output.append("\n");
                // Export location related
                if (event.getLocation() > 0) {
                    publishProgress("Export associated location" + " ...\n");
                    EventLocation loc = eventDAO.getLocationById(event.getLocation());
                    if (loc != null) {
                        output.append(LOCATION);
                        output.append(loc.exportToString());
                        output.append("\n");
                    }
                }
                // Export contact related
                if (event.getContact() > 0) {
                    publishProgress("Export associated contact" + " ...\n");
                    Contact contact = eventDAO.getContactById(event.getContact());
                    if (contact != null) {
                        output.append(CONTACT);
                        output.append(contact.exportToString());
                        output.append("\n");
                    }
                }
            }
            writer.write(output.toString());
            writer.flush();
            publishProgress("Export finished with "+ events.size()+ " events\n");
        } catch (IOException e) {
            publishProgress("Process stopped - Error occured : " + e.getMessage() + "\n");
            Log.e(IOEventTask.class.getName(), "Error occurred on writing in file: " + file, e);
        }
    }

    /**
     * Main method called to import events from backup file
     *
     * @param folder containing backup file
     */
    private int importEventsFromFile(File folder) {
        File file = getIOFile(folder);
        List<Event> events = new ArrayList<>();
        Map<Long, EventLocation> locationsImported = new HashMap<>();
        Map<Long, Contact> contactsImported = new HashMap<>();
        if (!file.isDirectory()) {
            String sCurrentLine;
            try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
                while ((sCurrentLine = reader.readLine()) != null) {

                    // Use to debug
                    // publishProgress(sCurrentLine + "\n");

                    // Get date of exported file
                    if(sCurrentLine.startsWith("Export_")) {
                        publishProgress("Exported file date : " + sCurrentLine.substring(7) + "\n");
                        continue;
                    }

                    // Parsing event
                    if (sCurrentLine.startsWith(EVENT)) {
                        String[] entriesLine = sCurrentLine.replace(EVENT, "").split(";;");
                        Event event = Event.createEmptyEvent();
                        for (int i = 0; i < entriesLine.length; i++) {
                            String[] values = entriesLine[i].split("::");
                            if (values.length == 2) {
                                for (Field field : event.getClass().getDeclaredFields()) {
                                    field.setAccessible(true);
                                    if (values[0].equals(field.getName())) {
                                        if (values[1] != null && !values[1].equals("null")) {
                                            if (field.getType().getName().toLowerCase().contains("int"))
                                                field.set(event, Integer.valueOf(values[1]));
                                            else if (field.getType().getName().toLowerCase().contains("long"))
                                                field.set(event, Long.valueOf(values[1]));
                                            else if (field.getType().getName().toLowerCase().contains("float"))
                                                field.set(event, Float.valueOf(values[1]));
                                            else if (field.getType().getName().toLowerCase().contains("date"))
                                                field.set(event, Event.DATE_FOR_EXPORT.parse(values[1]));
                                            else if (field.getType().getName().toLowerCase().contains("event_state"))
                                                field.set(event, IEventDAO.EVENT_STATE.valueOf(values[1]));
                                            else if (field.getName().equals("note"))
                                                field.set(event, values[1].replaceAll("\\*", "\n"));
                                            else
                                                field.set(event, values[1]);
                                        }
                                    }
                                }
                            }
                        }
                        events.add(event);
                    }
                    // Parsing location
                    else if (sCurrentLine.startsWith(LOCATION)) {
                        String[] entriesLine = sCurrentLine.replace(LOCATION, "").split(";;");
                        EventLocation eventLocation = new EventLocation();
                        for (int i = 0; i < entriesLine.length; i++) {
                            String[] values = entriesLine[i].split("::");
                            if (values.length == 2) {
                                for (Field field : eventLocation.getClass().getDeclaredFields()) {
                                    field.setAccessible(true);
                                    if (values[0].equals(field.getName()) && values[1] != null
                                            && !values[1].equals("null")) {
                                        if (field.getType().getName().toLowerCase().contains("double"))
                                            field.set(eventLocation, Double.valueOf(values[1]));
                                        else if (field.getType().getName().toLowerCase().contains("long"))
                                            field.set(eventLocation, Long.valueOf(values[1]));
                                        else
                                            field.set(eventLocation, values[1]);
                                    }
                                }
                            }
                        }
                        locationsImported.put(eventLocation.getLocationId(), eventLocation);
                    }
                    // Parsing contact
                    else if (sCurrentLine.startsWith(CONTACT)) {
                        String[] entriesLine = sCurrentLine.replace(CONTACT, "").split(";;");
                        Contact contact = new Contact();
                        for (int i = 0; i < entriesLine.length; i++) {
                            String[] values = entriesLine[i].split("::");
                            if (values.length == 2) {
                                for (Field field : contact.getClass().getDeclaredFields()) {
                                    field.setAccessible(true);
                                    if (values[0].equals(field.getName()) && values[1] != null
                                            && !values[1].equals("null")) {
                                        if (field.getType().getName().toLowerCase().contains("double"))
                                            field.set(contact, Double.valueOf(values[1]));
                                        else if (field.getType().getName().toLowerCase().contains("long"))
                                            field.set(contact, Long.valueOf(values[1]));
                                        else
                                            field.set(contact, values[1]);
                                    }
                                }
                            }
                        }
                        contactsImported.put(contact.getContactId(), contact);
                    }
                }
            } catch (IOException e) {
                Log.e(IOEventTask.class.getName(), "Error occurred on reading file: " + file, e);
            } catch (IllegalAccessException e) {
                Log.e(IOEventTask.class.getName(), "Error occurred on filling event: " + file, e);
            } catch (ParseException e) {
                Log.e(IOEventTask.class.getName(), "Error occurred on parsing date: " + file, e);
            }
        }
        return insertEvent(events, locationsImported, contactsImported);
    }

    /**
     * All logic to avoid insertion of existing event
     *
     * @param eventsImported list of events to be inserted
     */
    private int insertEvent(List<Event> eventsImported, Map<Long, EventLocation> locationsImported,
                            Map<Long, Contact> contactsImported) {
        publishProgress("Event import started ...  Total events parsed: " + eventsImported.size() + " ... \n");
        List<Event> eventsToBeInserted = new ArrayList<>();
        List<Event> existingEvents = eventDAO.retrieveAllEvents();
        List<EventLocation> existingLocations;
        List<Contact> existingContacts;
        boolean isExisting;
        for (Event eventImported : eventsImported) {
            isExisting = false;
            // Import old event logic
            if (eventImported.getState().equals(IEventDAO.EVENT_STATE.OLD)) {
                for (Event existingEvent : existingEvents) {
                    if (existingEvent.similarTo(eventImported)) {
                        publishProgress("Event has been ignored: " + eventImported.getTitle() +
                                (eventImported.getCreation() != null ? " - " + eventImported.getCreationAsString() : "") + " ... already existing! \n");
                        Log.i(Codec.class.getName(), "Event has been ignored: " + eventImported);
                        isExisting = true;
                        break;
                    }
                }
            }
            // Import Active event logic
            else if (eventImported.getState().equals(IEventDAO.EVENT_STATE.ACTIVE)) {
                Event existingActiveEvent = eventDAO.findEventByTitle(eventImported.getTitle());
                if (existingActiveEvent != null) {
                    if (existingActiveEvent.getReference() == null && eventImported.getReference() == null)
                        isExisting = true;
                    else if (existingActiveEvent.getReference() != null && existingActiveEvent.getReference().isEmpty()
                            && eventImported.getReference() != null && eventImported.getReference().isEmpty())
                        isExisting = true;
                    else if (existingActiveEvent.getReference() != null && !existingActiveEvent.getReference().isEmpty()
                            && eventImported.getReference() != null && !eventImported.getReference().isEmpty()
                            && existingActiveEvent.getReference().equals(eventImported.getReference()))
                        isExisting = true;
                }
            }
            if (!isExisting) {
                //Logic around the location
                if (eventImported.getLocation() > 0) {
                    long id = eventImported.getLocation();
                    // Check existence of location based on ID
                    final EventLocation existingLocation = eventDAO.getLocationById(id);
                    if (existingLocation == null || !existingLocation.equals(locationsImported.get(id))) {
                        // Try to find if one is similar
                        existingLocations = eventDAO.getAllEventLocations();
                        boolean locExists = false;
                        for (EventLocation existingLoc : existingLocations) {
                            if (existingLoc.similarTo(locationsImported.get(id))) {
                                eventImported.setLocation(existingLoc.getLocationId());
                                locExists = true;
                                Log.i(Codec.class.getName(), "Location has been reused: " + locationsImported.get(id));
                                break;
                            }
                        }
                        if (!locExists && locationsImported.get(id) != null) {
                            Long idLoc = eventDAO.insertLocation(locationsImported.get(id));
                            eventImported.setLocation(idLoc);
                            Log.i(IOEventTask.class.getName(), "Location has been created: " + locationsImported.get(id));
                        }
                    } else {
                        Log.i(IOEventTask.class.getName(), "Location has been ignored: " + locationsImported.get(id));
                    }
                }
                // Logic around contact
                if (eventImported.getContact() > 0) {
                    long id = eventImported.getContact();
                    // Check existence of location based on ID
                    final Contact existingContact = eventDAO.getContactById(id);
                    if (existingContact == null || !existingContact.equals(locationsImported.get(id))) {
                        // Try to find if one is similar
                        existingContacts = eventDAO.getAllContacts();
                        boolean contactExists = false;
                        for (Contact exContact : existingContacts) {
                            if (exContact.similarTo(contactsImported.get(id))) {
                                eventImported.setLocation(exContact.getContactId());
                                contactExists = true;
                                Log.i(IOEventTask.class.getName(), "Contact has been reused: " + contactsImported.get(id));
                                break;
                            }
                        }
                        if (!contactExists) {
                            Long idContact = eventDAO.insertContact(contactsImported.get(id));
                            eventImported.setContact(idContact);
                            Log.i(IOEventTask.class.getName(), "Contact has been created: " + contactsImported.get(id));
                        }
                    } else {
                        Log.i(IOEventTask.class.getName(), "Contact has been ignored: " + contactsImported.get(id));
                    }
                }
                eventsToBeInserted.add(eventImported);
            } else {
                publishProgress("Event has been ignored: " + eventImported.getTitle() + " - "
                        + (eventImported.getCreation() != null ? eventImported.getCreationAsString() : "")
                        + " ... already existing! \n");
                Log.i(IOEventTask.class.getName(), "Event has been ignored: " + eventImported);
            }
        }
        for (Event eventTobeInserted : eventsToBeInserted) {
            eventDAO.insertEvent(eventTobeInserted);
            publishProgress("Event has been inserted: " + eventTobeInserted + " ... \n");
            Log.i(IOEventTask.class.getName(), "Event has been inserted: " + eventTobeInserted);
        }
        Log.i(IOEventTask.class.getName(), "Number of event inserted: " + eventsToBeInserted.size());

        return eventsToBeInserted.size();
    }

    public void exportImages(File folder) {
        publishProgress("Exporting images ...\n");
        List<File> fileExported = new ArrayList<>();
        File imageFolder = context.getDir(LasTimeUtils.IMAGE_FOLDER_NAME, Context.MODE_PRIVATE);
        if (imageFolder != null) {
            File[] imageFiles = imageFolder.listFiles();
            for (int i = 0; i < imageFiles.length; i++) {
                Log.i(IOEventTask.class.getName(), "Image file to be copied: " + imageFiles[i].getName() + "\n");
                File tmpFile = new File(folder, imageFiles[i].getName());
                this.copy(imageFiles[i], tmpFile);
                fileExported.add(tmpFile);
            }
        }
        // Once done.. zip all files!
        publishProgress("Zipping images ... \n");
        if(!fileExported.isEmpty())
            this.zip(fileExported, new File(folder, EXPORTED_ZIP));

        publishProgress("Image export finished with " + fileExported.size() + " images \n");
    }

    // --------------------- Images export ------------------------------

    public void importImages(File folder) {
        File imageFolder = context.getDir(LasTimeUtils.IMAGE_FOLDER_NAME, Context.MODE_PRIVATE);
        this.unzip(new File(folder, EXPORTED_ZIP), imageFolder);
    }

    protected void copy(File src, File dst) {
        try (OutputStream out = new FileOutputStream(dst);
             InputStream in = new FileInputStream(src)) {
            // Transfer bytes from in to out
            byte[] buf = new byte[BUFFER];
            int len;
            while ((len = in.read(buf)) > 0) {
                out.write(buf, 0, len);
            }
            out.flush();
        } catch (IOException e) {
            Log.e(IOEventTask.class.getName(), "Error to copy file: " + src + " to: " + dst, e);
        }
    }

    public void zip(List<File> files, File zipFileName) {
        try (ZipOutputStream out = new ZipOutputStream(new BufferedOutputStream(new FileOutputStream(zipFileName)))) {
            byte data[] = new byte[BUFFER];

            for (File file : files) {
                Log.i(IOEventTask.class.getName(), "Adding to zip file: " + file);
                publishProgress("Adding image to zip: " + file + "\n");

                try (BufferedInputStream origin = new BufferedInputStream(new FileInputStream(file), BUFFER)) {
                    ZipEntry entry = new ZipEntry(file.getName());
                    out.putNextEntry(entry);
                    int count;
                    while ((count = origin.read(data, 0, BUFFER)) != -1) {
                        out.write(data, 0, count);
                    }
                    file.delete();
                }
            }
            out.finish();
            publishProgress("Zip done \n");
        } catch (FileNotFoundException e) {
            Log.e(IOEventTask.class.getName(), "Error occurred, zip file not found", e);
        } catch (IOException e) {
            Log.e(IOEventTask.class.getName(), "Error occurred while zipping file", e);
        }
    }

    // ------------------------ Zipping --------------------------------------------

    public void unzip(File zipFile, File _targetLocation) {
        ZipEntry ze;
        if (zipFile != null && zipFile.exists() && zipFile.length() > 0) {
            try (ZipInputStream zin = new ZipInputStream(new FileInputStream(zipFile))) {
                while ((ze = zin.getNextEntry()) != null) {
                    // No directory support needed
                    if (!ze.isDirectory()) {
                        try (BufferedOutputStream bufout = new BufferedOutputStream(new FileOutputStream(_targetLocation + "/" + ze.getName()))) {
                            byte[] buffer = new byte[BUFFER];
                            int read = 0;
                            while ((read = zin.read(buffer)) != -1) {
                                bufout.write(buffer, 0, read);
                            }
                            bufout.flush();
                            zin.closeEntry();
                        } catch (Exception e) {
                            Log.e(IOEventTask.class.getName(), "Error occurred while writing output file from zip: " + e.getMessage(), e);
                        }
                    }
                    Log.i(IOEventTask.class.getName(), "Image copied: " + ze.getName());
                    publishProgress("Image inserted: " + ze.getName() + "\n");
                }
                zin.close();
            } catch (Exception e) {
                Log.e(IOEventTask.class.getName(), "Error occurred while unzipping file: " + e.getMessage(), e);
            }
        }
    }

    public void codecFile(File folder, boolean encode) {
        File file = getIOFile(folder);
        try (FileInputStream fis = new FileInputStream(file)) {
            // File output
            File tempFile = new File(file.getParent(), "tmpEncrypted.txt");
            byte[] bytes = new byte[(int) file.length()];
            fis.read(bytes);

            if (encode)
                Files.write(Base64.encode(bytes, Base64.DEFAULT), tempFile);
            else
                Files.write(Base64.decode(bytes, Base64.DEFAULT), tempFile);

            fis.close();
            tempFile.renameTo(file);
            tempFile.delete();
        } catch (Exception e) {
            Log.e(Codec.class.getName(), "Error occurred while encrypting file: " + e);
        }
    }

    // --------------------------- Encoding 64 --------------------------------------------------------

    public enum IOTask {
        IMPORT, EXPORT
    }

}
