package com.abfactory.lastime.utils;

import com.abfactory.lastime.model.Event;

import org.joda.time.DateTime;

import java.util.Date;
import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;
import java.util.stream.Collectors;

public class StatisticsCalculator {
    public enum STATS_TYPE {
        TOTAL, YEAR, MONTH, WEEK
    }


    private StatisticsCalculator() {
    }

    public static OptionalDouble getTotalExpenses(List<Event> eventList, STATS_TYPE statsType) {
        return eventList.stream()
                .filter(e -> filtering(statsType, e.getDate()))
                .mapToDouble(e -> e.getExpenses())
                .reduce((e1, e2) -> e1 + e2);
    }


    public static long getTotalInstanceNumber(List<Event> eventList, STATS_TYPE statsType) {
        return eventList.stream()
                .filter(e -> filtering(statsType, e.getDate()))
                .count();
    }

    public static OptionalInt getBiggestDiffBetweenEvents(List<Event> eventList, STATS_TYPE statsType) {
        int max = 0;
        List<Date> eventDateList = eventList
                .stream()
                .filter(date -> filtering(statsType, date.getDate()))
                .map(event -> event.getDate())
                .sorted()
                .collect(Collectors.toList());
        if (eventDateList.size() > 1) {
            int interval;
            for (int i = 0; i < eventDateList.size(); i++) {
                if (i + 1 >= eventDateList.size()) {
                    break;
                }
                interval = LasTimeUtils.computeDaysDifference(eventDateList.get(i), eventDateList.get(i + 1));
                if (interval > max)
                    max = interval;
            }
        }
        return OptionalInt.of(max);
    }

    public static OptionalInt getSmallestDiffBetweenEvents(List<Event> eventList, STATS_TYPE statsType) {
        int min = 0;
        List<Date> eventDateList = eventList
                .stream()
                .filter(date -> filtering(statsType, date.getDate()))
                .map(event -> event.getDate())
                .sorted()
                .collect(Collectors.toList());
        if (eventDateList.size() > 1) {
            int interval;
            for (int i = 0; i < eventDateList.size(); i++) {
                if (i + 1 >= eventDateList.size()) {
                    break;
                }
                interval = LasTimeUtils.computeDaysDifference(eventDateList.get(i), eventDateList.get(i + 1));
                if (i == 0)
                    min = interval;
                if (interval < min)
                    min = interval;
            }
        }
        return OptionalInt.of(min);
    }

    private static boolean filtering(STATS_TYPE statsType, Date date){
        DateTime dateTimeOrg = new DateTime(date);
        DateTime current = new DateTime();
        if (statsType.equals(STATS_TYPE.WEEK)) {
            return dateTimeOrg.getYear() == current.getYear()
                    && dateTimeOrg.getWeekOfWeekyear() == current.getWeekOfWeekyear();
        } else if (statsType.equals(STATS_TYPE.MONTH)) {
            return dateTimeOrg.getYear() == current.getYear()
                    && dateTimeOrg.getMonthOfYear() == current.getMonthOfYear();
        } else if (statsType.equals(STATS_TYPE.YEAR)) {
            return dateTimeOrg.getYear() == current.getYear();
        }
        return true;
    }
}
