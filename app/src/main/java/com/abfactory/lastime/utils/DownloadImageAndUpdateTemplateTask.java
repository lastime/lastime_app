package com.abfactory.lastime.utils;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadImageAndUpdateTemplateTask extends AsyncTask<String, Void, Boolean> {

    private final static String TAG = DownloadImageAndUpdateTemplateTask.class.getName();
    private Context ctx;
    private long eventTemplateToUpdateId;
    private String localImagePath;

    public DownloadImageAndUpdateTemplateTask(Context ctx, long eventTemplateToUpdateId) {
        this.ctx = ctx;
        this.eventTemplateToUpdateId = eventTemplateToUpdateId;
    }

    protected Boolean doInBackground(String... values) {
        FileOutputStream fos = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(values[0]);
            File file = new File(LasTimeUtils.getImageStorageDir(ctx), values[1]);

            long startTime = System.currentTimeMillis();
            Log.d(TAG, "download starting");
            Log.d(TAG, "download url:" + url);
            Log.d(TAG, "downloaded file name:" + values[1]);

            // Open a connection to that URL. */
            httpURLConnection = (HttpURLConnection) url.openConnection();
            int responseCode = httpURLConnection.getResponseCode();

            // If response is correct
            if (responseCode == HttpURLConnection.HTTP_OK) {

                // Define InputStreams to read from the URLConnection.
                InputStream is = new BufferedInputStream(httpURLConnection.getInputStream());
                fos = new FileOutputStream(file);
                // Write bytes to the Buffer until there is nothing more to read(-1).
                int bytesRead = -1;
                byte[] buffer = new byte[4096];
                while ((bytesRead = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                fos.flush();
                is.close();
                fos.close();
                localImagePath = file.getAbsolutePath();
                Log.d(TAG, "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");

            } else {
                Log.e(TAG, "Error opening connection: ");
                return false;
            }
        }catch(IOException e){
            Log.e(TAG, "Error saving file: ", e);
            return false;
        }finally{
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error", e);
                }
            }
        }
        return true;
    }

    protected void onPostExecute(Boolean feed) {
        if(feed){
            IEventDAO eventDAO = new EventDAO(ctx);
            eventDAO.updateEventTemplateImagePath(eventTemplateToUpdateId, localImagePath);
        }
    }
}
