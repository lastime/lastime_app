package com.abfactory.lastime.model;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * New Location activity result holder
 */
public class NewLocationResult implements Parcelable {

    private String googlePlaceId;
    private double customLocationLat;
    private double customLocationLong;
    private String customLocationName;

    public NewLocationResult() {}

    public NewLocationResult(String gID, double cLat, double cLong, String cName) {
        googlePlaceId = gID;
        customLocationLat = cLat;
        customLocationLong = cLong;
        customLocationName = cName;
    }

    public boolean isGooglePlace(){
        return googlePlaceId!=null;
    }

    public boolean isCustomLocation(){
        return googlePlaceId==null;
    }

    public String getGooglePlaceId() {
        return googlePlaceId;
    }

    public void setGooglePlaceId(String googlePlaceId) {
        this.googlePlaceId = googlePlaceId;
    }

    public double getCustomLocationLat() {
        return customLocationLat;
    }

    public void setCustomLocationLat(double customLocationLat) {
        this.customLocationLat = customLocationLat;
    }

    public double getCustomLocationLong() {
        return customLocationLong;
    }

    public void setCustomLocationLong(double customLocationLong) {
        this.customLocationLong = customLocationLong;
    }

    public String getCustomLocationName() {
        return customLocationName;
    }

    public void setCustomLocationName(String customLocationName) {
        this.customLocationName = customLocationName;
    }

        /* Parcelable implementation */

    private NewLocationResult(Parcel in) {
        googlePlaceId = in.readString();
        customLocationLat = in.readDouble();
        customLocationLong = in.readDouble();
        customLocationName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(googlePlaceId);
        dest.writeDouble(customLocationLat);
        dest.writeDouble(customLocationLong);
        dest.writeString(customLocationName);
    }

    public static final Parcelable.Creator CREATOR = new Parcelable.Creator<NewLocationResult>() {
        public NewLocationResult createFromParcel(Parcel in) {
            return new NewLocationResult(in);
        }
        public NewLocationResult[] newArray(int size) {
            return new NewLocationResult[size];
        }
    };

}