package com.abfactory.lastime.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.abfactory.lastime.utils.PreferencesHandler;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE;
import static com.abfactory.lastime.model.ItemEnum.ITEM.*;

/**
 * Class that handles database creation and access
 * CRUD methods defined there also but accessed by DAO object
 * Do not use it directly, use DAO object
 */

public class DatabaseHandler extends SQLiteOpenHelper {

    // Date formatter to store in SQLLite
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.ENGLISH);
    // All Static variables
    // Database Version
    public static final int DATABASE_VERSION = 21;

    // Database Name
    private static final String DATABASE_NAME = "LastTimeDatabase";

    // Table and Columns (see ItemEnum class)
    private static final String TABLE_EVENT = "event";
    private static final String TABLE_LOCATION = "location";
    private static final String TABLE_CONTACT = "contact";
    private static final String TABLE_NOTIFICATION = "notification";
    private static final String TABLE_EVENT_TEMPLATE = "event_template";
    // Database helper
    private SQLiteDatabase db;
    private String TAG = DatabaseHandler.class.getName();
    private Context context;

    public DatabaseHandler(Context ctx) {
        super(ctx, DATABASE_NAME, null, DATABASE_VERSION);
        context = ctx;
    }

    // Init database helper
    private SQLiteDatabase getDatabase() {
        if (db == null || !db.isOpen()) {
            db = this.getReadableDatabase();
        }
        return db;
    }

    // Close database helper
    private void closeDatabase() {
        if (db != null && db.isOpen())
            db.close();
    }

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {
        //Event table creation
        String CREATE_EVENT_TABLE = "CREATE TABLE " + TABLE_EVENT + "("
                + EVENT_KEY_ID.getColumnName() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + EVENT_KEY_TITLE.getColumnName() + " TEXT,"
                + EVENT_KEY_DATE.getColumnName() + " DATETIME,"
                + EVENT_KEY_LOCATION.getColumnName() + " LONG,"
                + EVENT_KEY_MARK.getColumnName() + " INTEGER,"
                + EVENT_KEY_IMAGE_PATH.getColumnName() + " TEXT,"
                + EVENT_KEY_THUMBNAIL_PATH.getColumnName() + " TEXT,"
                + EVENT_KEY_WEIGHT.getColumnName() + " INTEGER,"
                + EVENT_KEY_REMINDER.getColumnName() + " TEXT,"
                + EVENT_KEY_NOTE.getColumnName() + " TEXT,"
                + EVENT_KEY_COLOR.getColumnName() + " INTEGER,"
                + EVENT_KEY_STATE.getColumnName() + " TEXT,"
                + EVENT_KEY_TIME.getColumnName() + " INTEGER,"
                + EVENT_KEY_CREATION.getColumnName() + " DATETIME,"
                + EVENT_KEY_REFERENCE.getColumnName() + " TEXT,"
                + EVENT_KEY_RECURRENCE.getColumnName() + " TEXT,"
                + EVENT_KEY_CONTACT_IDS.getColumnName() + " TEXT,"
                + EVENT_KEY_BACKEND_ID.getColumnName() + " TEXT,"
                + EVENT_KEY_END_DATE.getColumnName() + " DATETIME,"
                + EVENT_KEY_EXPENSES.getColumnName() + " REAL,"
                + EVENT_KEY_HIGHLIGHT.getColumnName() + " TEXT,"
                + EVENT_KEY_FIELD_LIST.getColumnName() + " TEXT,"
                + EVENT_KEY_WEATHER.getColumnName() + " TEXT"
                + ")";

        // Location table creation
        String CREATE_LOCATION_TABLE = "CREATE TABLE " + TABLE_LOCATION + "("
                + LOCATION_KEY_ID.getColumnName() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + LOCATION_KEY_NAME.getColumnName() + " TEXT,"
                + LOCATION_KEY_LATITUDE.getColumnName() + " DOUBLE,"
                + LOCATION_KEY_LONGITUDE.getColumnName() + " DOUBLE,"
                + LOCATION_KEY_GOOGLE_PLACE_ID.getColumnName() + " TEXT,"
                + LOCATION_KEY_BACKEND_ID.getColumnName() + " TEXT"
                + ")";

        //Execute creation scripts
        db.execSQL(CREATE_EVENT_TABLE);
        db.execSQL(CREATE_LOCATION_TABLE);
        createNotificationTable(db);
        createEventTemplateTable(db);
        createContactTable(db);
    }

    // Creating Notification Table
    public void createNotificationTable(SQLiteDatabase db) {
        // Notification table creation
        String CREATE_NOTIFICATION_TABLE = "CREATE TABLE " + TABLE_NOTIFICATION + "("
                + NOTIFICATION_KEY_ID.getColumnName() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + NOTIFICATION_KEY_EVENT_ID.getColumnName() + " INTEGER,"
                + NOTIFICATION_KEY_STATE.getColumnName() + " TEXT,"
                + NOTIFICATION_KEY_PLANNED_DATE.getColumnName() + " DATETIME,"
                + NOTIFICATION_KEY_PLANNED_TIME.getColumnName() + " INTEGER"
                + ")";
        //Execute creation script
        db.execSQL(CREATE_NOTIFICATION_TABLE);
    }

    // Creating EventTemplate Table
    public void createEventTemplateTable(SQLiteDatabase db) {
        // EventTemplate table creation
        String CREATE_EVENT_TEMPLATE_TABLE = "CREATE TABLE " + TABLE_EVENT_TEMPLATE + "("
                + EVENT_TEMPLATE_KEY_ID.getColumnName() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + EVENT_TEMPLATE_KEY_TITLE.getColumnName() + " TEXT,"
                + EVENT_TEMPLATE_KEY_DESCRIPTION.getColumnName() + " TEXT,"
                + EVENT_TEMPLATE_KEY_IMAGE_PATH.getColumnName() + " TEXT,"
                + EVENT_TEMPLATE_KEY_COLOR.getColumnName() + " TEXT,"
                + EVENT_TEMPLATE_KEY_CREATION_DATE.getColumnName() + " DATETIME"
                + ")";
        //Execute creation script
        db.execSQL(CREATE_EVENT_TEMPLATE_TABLE);
    }

    // Creating Contact Table
    public void createContactTable(SQLiteDatabase db) {
        // Contacts table creation
        String CREATE_CONTACT_TABLE = "CREATE TABLE " + TABLE_CONTACT + "("
                + CONTACT_KEY_ID.getColumnName() + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + CONTACT_KEY_NAME.getColumnName() + " TEXT,"
                + CONTACT_KEY_PHONE_NUMBER.getColumnName() + " TEXT,"
                + CONTACT_KEY_PHONE_ID.getColumnName() + " TEXT,"
                + CONTACT_KEY_BACKEND_ID.getColumnName() + " TEXT"
                + ")";
        //Execute creation script
        db.execSQL(CREATE_CONTACT_TABLE);
    }

    // Upgrading database
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i(TAG, "Upgrading DB from version " + oldVersion + " to " + newVersion);
        long startWhen = System.nanoTime();

        // Migration process 1->2
        // First big refactoring
        // Need to reinstall everything
        if (oldVersion == 2) {
            // Drop older table if existed
            db.execSQL("DROP TABLE IF EXISTS events");
            // Create tables again
            onCreate(db);
        }

        // Migration process 2->3
        if (oldVersion < 3) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_COLOR.getColumnName(), "INTEGER", db);
            addNewColumn(TABLE_LOCATION, LOCATION_KEY_GOOGLE_PLACE_ID.getColumnName(), "TEXT", db);
        }

        // Migration process 3->4
        if (oldVersion < 4) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_STATE.getColumnName(), "TEXT", db);
            db.execSQL("UPDATE " + TABLE_EVENT + " SET state = 'ACTIVE'");
        }

        // Migration process 4->5
        if (oldVersion < 5) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_TIME.getColumnName(), "INTEGER", db);
        }

        // Migration process 4->5
        if (oldVersion < 6) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_CREATION.getColumnName(), "DATETIME", db);
            addNewColumn(TABLE_EVENT, EVENT_KEY_REFERENCE.getColumnName(), "TEXT", db);
        }

        // Migration process 6->7
        if (oldVersion < 7) {
            createNotificationTable(db);
        }

        // Migration process 7->8
        if (oldVersion < 8) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_RECURRENCE.getColumnName(), "TEXT", db);
        }

        // Migration process 8->9
        if (oldVersion < 9) {
            addNewColumn(TABLE_NOTIFICATION, NOTIFICATION_KEY_STATE.getColumnName(), "TEXT", db);
            db.execSQL("UPDATE " + TABLE_NOTIFICATION + " SET state = 'ACTIVE'");
        }

        // Migration process 9->10
        if (oldVersion <  10) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_CONTACT_IDS.getColumnName(), "TEXT", db);
        }

        // Migration process 10->11
        if (oldVersion < 11) {
            createEventTemplateTable(db);
        }

        // Migration process 11->12
        if (oldVersion <  12) {
            addNewColumn(TABLE_NOTIFICATION, NOTIFICATION_KEY_PLANNED_DATE.getColumnName(), "DATETIME", db);
            addNewColumn(TABLE_NOTIFICATION, NOTIFICATION_KEY_PLANNED_TIME.getColumnName(), "INTEGER", db);
        }

        // Migration process 12->13
        if (oldVersion <  13) {
            //New behavior on time, so set time with 0 value to -1
            db.execSQL("UPDATE " + TABLE_EVENT + " SET " + EVENT_KEY_TIME.getColumnName() + " = '-1' WHERE "
                    + EVENT_KEY_TIME.getColumnName() + " = '0'");
        }

        // Migration process 13->14
        if (oldVersion <  14) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_BACKEND_ID.getColumnName(), "TEXT", db);
            addNewColumn(TABLE_LOCATION, LOCATION_KEY_BACKEND_ID.getColumnName(), "TEXT", db);
        }

        // Migration process 14->15
        if (oldVersion <  15) {
            createContactTable(db);
        }

        // Migration process 15->17
        if (oldVersion < 17) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_END_DATE.getColumnName(), "DATETIME", db);
        }

        // Migration process 17->18
        if (oldVersion < 18) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_EXPENSES.getColumnName(), "REAL", db);
        }

        // Migration process 18->19
        if (oldVersion < 19) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_HIGHLIGHT.getColumnName(), "TEXT", db);
        }

        // Migration process 19->20
        if (oldVersion < 20) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_FIELD_LIST.getColumnName(), "TEXT", db);
        }

        // Migration process 20->21
        if (oldVersion < 21) {
            addNewColumn(TABLE_EVENT, EVENT_KEY_WEATHER.getColumnName(), "TEXT", db);
        }

        new PreferencesHandler(context).setInvalidState(false);

        long endWhen = System.nanoTime();
        Log.d(TAG, "LastTime db upgrade took " + ((endWhen - startWhen) / 1000000) + "ms");
    }

    // Downgrade database
    @Override
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "LastTime db downgrade not supported!");
        new PreferencesHandler(context).setInvalidState(true);
    }

    // SQL to add a new column
    private void addNewColumn(String table, String columnName, String columnType, SQLiteDatabase db) {
        try {
            db.execSQL("ALTER TABLE " + table + " ADD COLUMN " + columnName + " " + columnType);
        } catch (SQLException e) {
            Log.i(TAG, columnName + " already exists", e);
        }
    }

    // *************** CRUD location table ******************/
    protected long createLocation(String name, double latitude, double longitude, String googlePlacesId) {
        // Prepare values to set
        ContentValues values = new ContentValues();
        values.put(LOCATION_KEY_NAME.getColumnName(), name);
        values.put(LOCATION_KEY_LATITUDE.getColumnName(), latitude);
        values.put(LOCATION_KEY_LONGITUDE.getColumnName(), longitude);
        values.put(LOCATION_KEY_GOOGLE_PLACE_ID.getColumnName(), googlePlacesId);
        // Inserting Row
        long id = getDatabase().insert(TABLE_LOCATION, null, values);
        // Closing database connection
        closeDatabase();
        return id;
    }

    /**
     * Getting all event locations
     */
    protected List<EventLocation> getAllEventLocations() {
        List<EventLocation> locationsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_LOCATION;

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EventLocation location = createLocation(cursor);
                // Adding location to list
                locationsList.add(location);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return events list
        return locationsList;
    }

    protected EventLocation retrieveLocation(long id) {
        EventLocation location = null;

        Cursor cursor = getDatabase().query(TABLE_LOCATION, null, LOCATION_KEY_ID.getColumnName() + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            location = createLocation(cursor);
            cursor.close();
        }

        closeDatabase();

        return location;
    }

    protected void updateLocation(long id, String name, double latitude, double longitude, String googlePlacesId, String backendId) {
        ContentValues con = new ContentValues();
        con.put(LOCATION_KEY_NAME.getColumnName(), name);
        con.put(LOCATION_KEY_LATITUDE.getColumnName(), latitude);
        con.put(LOCATION_KEY_LONGITUDE.getColumnName(), longitude);
        con.put(LOCATION_KEY_GOOGLE_PLACE_ID.getColumnName(), googlePlacesId);
        con.put(LOCATION_KEY_BACKEND_ID.getColumnName(), backendId);
        getDatabase().update(TABLE_LOCATION, con, LOCATION_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    protected void deleteLocation(long id) {
        getDatabase().delete(TABLE_LOCATION, LOCATION_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Create Location based on cursor
     */
    private EventLocation createLocation(Cursor cursor) {
        return new EventLocation(Integer.parseInt(cursor.getString(LOCATION_KEY_ID.getColumnPosition())),
                cursor.getString(LOCATION_KEY_NAME.getColumnPosition()),
                cursor.getDouble(LOCATION_KEY_LATITUDE.getColumnPosition()),
                cursor.getDouble(LOCATION_KEY_LONGITUDE.getColumnPosition()),
                cursor.getString(LOCATION_KEY_GOOGLE_PLACE_ID.getColumnPosition()),
                cursor.getString(LOCATION_KEY_BACKEND_ID.getColumnPosition()));
    }

    // *************** CRUD contact table ******************/

    protected long createContact(String name, String phoneNumber, String phoneId) {
        // Prepare values to set
        ContentValues values = new ContentValues();
        values.put(CONTACT_KEY_NAME.getColumnName(), name);
        values.put(CONTACT_KEY_PHONE_NUMBER.getColumnName(), phoneNumber);
        values.put(CONTACT_KEY_PHONE_ID.getColumnName(), phoneId);
        // Inserting Row
        long id = getDatabase().insert(TABLE_CONTACT, null, values);
        // Closing database connection
        closeDatabase();
        return id;
    }

    /**
     * Getting all event contacts
     */
    protected List<Contact> getAllContacts() {
        List<Contact> contactsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_CONTACT;
        Cursor cursor = getDatabase().rawQuery(selectQuery, null);
        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Contact contact = createContact(cursor);
                // Adding contact to list
                contactsList.add(contact);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return contacts list
        return contactsList;
    }

    protected Contact retrieveContact(long id) {
        Contact contact = null;

        Cursor cursor = getDatabase().query(TABLE_CONTACT, null, CONTACT_KEY_ID.getColumnName() + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            contact = createContact(cursor);
            cursor.close();
        }

        closeDatabase();

        return contact;
    }

    protected void updateContact(Contact contact) {
        ContentValues con = new ContentValues();
        con.put(CONTACT_KEY_NAME.getColumnName(), contact.getContactName());
        con.put(CONTACT_KEY_PHONE_NUMBER.getColumnName(), contact.getPhoneNumber());
        con.put(CONTACT_KEY_PHONE_ID.getColumnName(), contact.getPhoneId());
        con.put(CONTACT_KEY_BACKEND_ID.getColumnName(), contact.getBackendId());
        getDatabase().update(TABLE_CONTACT, con, CONTACT_KEY_ID.getColumnName() + "=" + contact.getContactId(), null);
        closeDatabase();
    }

    protected void deleteContact(long id) {
        getDatabase().delete(TABLE_CONTACT, CONTACT_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Create Contact based on cursor
     */
    private Contact createContact(Cursor cursor) {
        return new Contact(Integer.parseInt(cursor.getString(CONTACT_KEY_ID.getColumnPosition())),
                cursor.getString(CONTACT_KEY_NAME.getColumnPosition()),
                cursor.getString(CONTACT_KEY_PHONE_NUMBER.getColumnPosition()),
                cursor.getString(CONTACT_KEY_PHONE_ID.getColumnPosition()),
                cursor.getString(CONTACT_KEY_BACKEND_ID.getColumnPosition()));
    }

    // *************** CRUD event table *********************/

    protected long createEvent(String title, String date, Integer timeOfEvent, long loc, int m, String imagePath,
                               String thumbnailPath, int weight, String reminder, String note, int color,
                               String state, String creationDate, String reference, String recurrence, long contact, String endDate,
                               float expenses, String highlight, String fieldList, int weather) {
        // Prepare values to set
        ContentValues values = new ContentValues();
        values.put(EVENT_KEY_TITLE.getColumnName(), title);
        values.put(EVENT_KEY_DATE.getColumnName(), date);
        values.put(EVENT_KEY_TIME.getColumnName(), timeOfEvent);
        values.put(EVENT_KEY_LOCATION.getColumnName(), loc);
        values.put(EVENT_KEY_MARK.getColumnName(), m);
        values.put(EVENT_KEY_IMAGE_PATH.getColumnName(), imagePath);
        values.put(EVENT_KEY_THUMBNAIL_PATH.getColumnName(), thumbnailPath);
        values.put(EVENT_KEY_WEIGHT.getColumnName(), weight);
        values.put(EVENT_KEY_REMINDER.getColumnName(), reminder);
        values.put(EVENT_KEY_NOTE.getColumnName(), note);
        values.put(EVENT_KEY_COLOR.getColumnName(), color);
        values.put(EVENT_KEY_STATE.getColumnName(), state);
        values.put(EVENT_KEY_CREATION.getColumnName(), creationDate);
        values.put(EVENT_KEY_REFERENCE.getColumnName(), reference);
        values.put(EVENT_KEY_RECURRENCE.getColumnName(), recurrence);
        values.put(EVENT_KEY_CONTACT_IDS.getColumnName(), contact);
        values.put(EVENT_KEY_END_DATE.getColumnName(), endDate);
        values.put(EVENT_KEY_EXPENSES.getColumnName(), expenses);
        values.put(EVENT_KEY_HIGHLIGHT.getColumnName(), highlight);
        values.put(EVENT_KEY_FIELD_LIST.getColumnName(), fieldList);
        values.put(EVENT_KEY_WEATHER.getColumnName(), weather);
        // Inserting Row
        long id = getDatabase().insert(TABLE_EVENT, null, values);
        // Closing database connection
        closeDatabase();

        return id;
    }

    /**
     * Getting event for Id
     */
    protected Event retrieveEvent(long id) {
        Event event = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_ID.getColumnName() + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            event = createEvent(cursor);
            cursor.close();
        } else {
            Log.w(TAG, "Event with id " + id + " not found");
        }

        closeDatabase();

        return event;
    }

    protected void updateEvent(long key, String title, String date, Integer timeOfEvent, long loc, int m, String imagePath,
                               String thumbnailPath, int weight, String reminder, String note, int color, String recurrence,
                               long contact, String backendId, String endDate, float expenses, String highlight ,String fieldList,
                               int weather) {
        ContentValues values = new ContentValues();
        // This is a special case where user can correct title name
        values.put(EVENT_KEY_ID.getColumnName(), key);
        values.put(EVENT_KEY_TITLE.getColumnName(), title);
        values.put(EVENT_KEY_DATE.getColumnName(), date);
        values.put(EVENT_KEY_TIME.getColumnName(), timeOfEvent);
        values.put(EVENT_KEY_LOCATION.getColumnName(), loc);
        values.put(EVENT_KEY_MARK.getColumnName(), m);
        values.put(EVENT_KEY_IMAGE_PATH.getColumnName(), imagePath);
        values.put(EVENT_KEY_THUMBNAIL_PATH.getColumnName(), thumbnailPath);
        values.put(EVENT_KEY_WEIGHT.getColumnName(), weight);
        values.put(EVENT_KEY_REMINDER.getColumnName(), reminder);
        values.put(EVENT_KEY_NOTE.getColumnName(), note);
        values.put(EVENT_KEY_COLOR.getColumnName(), color);
        values.put(EVENT_KEY_RECURRENCE.getColumnName(), recurrence);
        values.put(EVENT_KEY_CONTACT_IDS.getColumnName(), contact);
        values.put(EVENT_KEY_BACKEND_ID.getColumnName(), backendId);
        values.put(EVENT_KEY_END_DATE.getColumnName(), endDate);
        values.put(EVENT_KEY_EXPENSES.getColumnName(), expenses);
        values.put(EVENT_KEY_HIGHLIGHT.getColumnName(), highlight);
        values.put(EVENT_KEY_FIELD_LIST.getColumnName(), fieldList);
        values.put(EVENT_KEY_WEATHER.getColumnName(), weather);
        getDatabase().update(TABLE_EVENT, values, EVENT_KEY_ID.getColumnName() + "=" + key, null);
        closeDatabase();
    }

    protected void updateEvent(long key, String date, int weight, String endDate) {
        ContentValues values = new ContentValues();
        values.put(EVENT_KEY_DATE.getColumnName(), date);
        values.put(EVENT_KEY_WEIGHT.getColumnName(), weight);
        values.put(EVENT_KEY_END_DATE.getColumnName(), endDate);
        getDatabase().update(TABLE_EVENT, values, EVENT_KEY_ID.getColumnName() + "=" + key, null);
        closeDatabase();
    }

    protected void updateEventField(long key, String fieldList) {
        ContentValues values = new ContentValues();
        // update only state of event
        values.put(EVENT_KEY_FIELD_LIST.getColumnName(), fieldList);
        getDatabase().update(TABLE_EVENT, values, EVENT_KEY_ID.getColumnName() + "=" + key, null);
        closeDatabase();
    }

    /**
     * Deleting event
     */
    protected void deleteEvent(long id) {
        getDatabase().delete(TABLE_EVENT, EVENT_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Deleting event by reference
     */
    protected void deleteEventByReference(String id) {
        getDatabase().delete(TABLE_EVENT, EVENT_KEY_REFERENCE.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Create Event based on cursor
     */
    private Event createEvent(Cursor cursor) {
        Event event = null;
        try {
            String endDate = cursor.getString(EVENT_KEY_END_DATE.getColumnPosition());
            event = new Event.EventBuilder(cursor.getString(EVENT_KEY_TITLE.getColumnPosition()))
                    .addInternalId(cursor.getLong(EVENT_KEY_ID.getColumnPosition()))
                    .addDate(DATE_FORMAT.parse(cursor.getString(EVENT_KEY_DATE.getColumnPosition())))
                    .addTime(cursor.getInt(EVENT_KEY_TIME.getColumnPosition()))
                    .addLocationId(cursor.getLong(EVENT_KEY_LOCATION.getColumnPosition()))
                    .addMark(cursor.getInt(EVENT_KEY_MARK.getColumnPosition()))
                    .addImagePath(cursor.getString(EVENT_KEY_IMAGE_PATH.getColumnPosition()))
                    .addThumbnailPath(cursor.getString(EVENT_KEY_THUMBNAIL_PATH.getColumnPosition()))
                    .addWeight(cursor.getInt(EVENT_KEY_WEIGHT.getColumnPosition()))
                    .addReminder(cursor.getString(EVENT_KEY_REMINDER.getColumnPosition()))
                    .addNote(cursor.getString(EVENT_KEY_NOTE.getColumnPosition()))
                    .addColor(cursor.getInt(EVENT_KEY_COLOR.getColumnPosition()))
                    .addState(EVENT_STATE.valueOf(cursor.getString(EVENT_KEY_STATE.getColumnPosition())))
                    .addCreationDate(cursor.getString(EVENT_KEY_CREATION.getColumnPosition()) != null ? DATE_FORMAT.parse(cursor.getString(EVENT_KEY_CREATION.getColumnPosition())) : null)
                    .addReference(cursor.getString(EVENT_KEY_REFERENCE.getColumnPosition()))
                    .addRecurrence(cursor.getString(EVENT_KEY_RECURRENCE.getColumnPosition()))
                    .addContactId(cursor.getLong(EVENT_KEY_CONTACT_IDS.getColumnPosition()))
                    .addBackendId(cursor.getString(EVENT_KEY_BACKEND_ID.getColumnPosition()))
                    .addEventEndDate(endDate != null ? DATE_FORMAT.parse(endDate) : null)
                    .addExpenses(cursor.getFloat(EVENT_KEY_EXPENSES.getColumnPosition()))
                    .addHighlight(cursor.getString(EVENT_KEY_HIGHLIGHT.getColumnPosition()))
                    .addFieldList(cursor.getString(EVENT_KEY_FIELD_LIST.getColumnPosition()))
                    .addWeather(cursor.getInt(EVENT_KEY_WEATHER.getColumnPosition()))
                    .build();
        } catch (ParseException e) {
            Log.e(TAG, "Error occurred during event creation: " + e.getMessage(), e);
        }
        return event;
    }

    /**
     * Getting all events
     */
    protected List<Event> retrieveAllEvents() {
        List<Event> eventsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT;

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Event event = createEvent(cursor);
                // Adding event to list
                eventsList.add(event);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return events list
        return eventsList;
    }

    /**
     * Getting all active events
     */
    protected List<Event> getAllActiveEvents() {
        List<Event> eventsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT + " WHERE state = 'ACTIVE'";

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Event event = createEvent(cursor);
                // Adding event to list
                eventsList.add(event);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return events list
        return eventsList;
    }

    /**
     * Getting only top events based on weight
     *
     * @param max events number to return
     */
    protected List<Event> getTopEvents(String max) {
        List<Event> eventsList = new ArrayList<>();

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_STATE.getColumnName() + "='ACTIVE'", null, null, null, EVENT_KEY_WEIGHT.getColumnName() + " DESC", max);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Event event = createEvent(cursor);
                // Adding event to list
                eventsList.add(event);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return events list
        return eventsList;
    }

    /**
     * Getting events count
     */
    protected int getEventsCount() {
        String countQuery = "SELECT " + EVENT_KEY_ID.getColumnName() + " FROM " + TABLE_EVENT;
        Cursor cursor = getDatabase().rawQuery(countQuery, null);
        int result = cursor.getCount();
        cursor.close();
        closeDatabase();
        // return count
        return result;
    }

    /**
     * Getting event by description
     */
    protected Long getEventIdByTitle(String description) {
        Long id = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, new String[]{EVENT_KEY_ID.getColumnName()}, EVENT_KEY_TITLE.getColumnName() + "=?",
                new String[]{description}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getLong(0);
            cursor.close();
        }

        closeDatabase();

        // return event
        return id;
    }

    /**
     * Getting event by description
     */
    protected Event getEventByTitle(String description) {
        Event event = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_TITLE.getColumnName() + "=?",
                new String[]{description}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            event = this.createEvent(cursor);
            cursor.close();
        }

        closeDatabase();

        // return event
        return event;
    }

    /**
     * Getting active event by description
     */
    protected Event getActiveEventByTitle(String title) {
        Event event = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_TITLE.getColumnName()
                        + "=? AND " + EVENT_KEY_STATE.getColumnName() + "=?",
                new String[]{title, "ACTIVE"}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            event = this.createEvent(cursor);
            cursor.close();
        }

        closeDatabase();

        // return event
        return event;
    }

    protected Long getLocationIdByEventId(long id) {
        Long locationId = null;
        Cursor cursor = getDatabase().query(TABLE_EVENT, new String[]{EVENT_KEY_LOCATION.getColumnName()},
                EVENT_KEY_ID.getColumnName() + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            locationId = cursor.getLong(0);
            cursor.close();
        }

        closeDatabase();

        return locationId;
    }

    protected List<String> getImagePathsByEventId(long id) {
        List<String> imagePaths = new ArrayList<>();
        Cursor cursor = getDatabase().query(TABLE_EVENT, new String[]{EVENT_KEY_IMAGE_PATH.getColumnName(),
                        EVENT_KEY_THUMBNAIL_PATH.getColumnName()},
                EVENT_KEY_ID.getColumnName() + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            imagePaths.add(cursor.getString(0));
            imagePaths.add(cursor.getString(1));
            cursor.close();
        }

        closeDatabase();

        return imagePaths;
    }

    protected Set<String> getAllImagePaths() {
        Set<String> imagePaths = new HashSet<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT;

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Event event = createEvent(cursor);
                // Listing images
                if (event.getImagePath() != null)
                    imagePaths.add(event.getImagePath());
                if (event.getThumbnailPath() != null)
                    imagePaths.add(event.getThumbnailPath());
            } while (cursor.moveToNext());
        }

        closeDatabase();

        return imagePaths;
    }

    protected void updateEventState(long key, EVENT_STATE state) {
        ContentValues values = new ContentValues();
        // update only state of event
        values.put(EVENT_KEY_STATE.getColumnName(), state.name());
        getDatabase().update(TABLE_EVENT, values, EVENT_KEY_ID.getColumnName() + "=" + key, null);
        closeDatabase();
    }

    /**
     * Getting all events with a specific state
     */
    protected List<Event> getAllEventsByState(EVENT_STATE state) {
        List<Event> eventsList = new ArrayList<>();
        // Select All Query
        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_STATE.getColumnName() + "=?",
                new String[]{state.name()}, null, null, null, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Event event = createEvent(cursor);
                // Adding event to list
                eventsList.add(event);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return events list
        return eventsList;
    }

    /**
     * Deleting all events with a specific state
     */
    protected void deleteAllEventWithState(EVENT_STATE state) {
        int rows = getDatabase().delete(TABLE_EVENT, EVENT_KEY_STATE.getColumnName() + "= '" + state.name() + "'", null);
        closeDatabase();

        if (rows > 0)
            Log.i(TAG, "Number of events deleted: " + rows);
    }

    /**
     * Get all event history
     */
    protected List<Event> getAllEventsByStateAndCommonId(EVENT_STATE eventState, String reference) {
        List<Event> eventsList = new ArrayList<>();
        if (reference != null) {
            // Select All Query
            Cursor cursor = getDatabase().query(TABLE_EVENT, null,
                    EVENT_KEY_STATE.getColumnName() + "=? AND " + EVENT_KEY_REFERENCE.getColumnName() + "=?",
                    new String[]{eventState.name(), reference}, null, null, EVENT_KEY_DATE.getColumnName() + " DESC", null);

            // looping through all rows and adding to list
            if (cursor.getCount() > 0 && cursor.moveToFirst()) {
                do {
                    Event event = createEvent(cursor);
                    // Adding event to list
                    eventsList.add(event);
                } while (cursor.moveToNext());
            }

            cursor.close();
            closeDatabase();
        }
        return eventsList;
    }

    protected void updateEventReference(long key, String reference) {
        ContentValues values = new ContentValues();
        // update only state of event
        values.put(EVENT_KEY_REFERENCE.getColumnName(), reference);
        getDatabase().update(TABLE_EVENT, values, EVENT_KEY_ID.getColumnName() + "=" + key, null);
        closeDatabase();
    }

    protected Event retrieveActiveEventByReference(String reference) {
        Event event = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_REFERENCE.getColumnName() + "=?" +
                        " AND " + EVENT_KEY_STATE.getColumnName() + "=?",
                new String[]{reference, "ACTIVE"}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            event = this.createEvent(cursor);
            cursor.close();
        }

        closeDatabase();

        // return event
        return event;
    }

    protected Event findPreviousEventBasedOnReference(String reference) {
        Event event = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_REFERENCE.getColumnName() + "=?" +
                        " AND " + EVENT_KEY_STATE.getColumnName() + "=?",
                new String[]{reference, "OLD"}, null, null, EVENT_KEY_CREATION.getColumnName() + " DESC", null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            event = this.createEvent(cursor);
            cursor.close();
        }

        closeDatabase();

        // return event
        return event;
    }

    protected Event findActiveEventByReference(String reference) {
        Event event = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_REFERENCE.getColumnName() + "=?" +
                        " AND " + EVENT_KEY_STATE.getColumnName() + "=?",
                new String[]{reference, "ACTIVE"}, null, null, EVENT_KEY_CREATION.getColumnName() + " DESC", null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            event = this.createEvent(cursor);
            cursor.close();
        }

        closeDatabase();

        // return event
        return event;
    }

    protected void updateEventColor(String key, Integer color) {
        ContentValues values = new ContentValues();
        // update only color of event
        values.put(EVENT_KEY_COLOR.getColumnName(), color);
        getDatabase().update(TABLE_EVENT, values, EVENT_KEY_REFERENCE.getColumnName() + "=" + key, null);
        closeDatabase();
    }

    /**
     * Getting event by description
     */
    protected Long getActiveEventIdByTitle(String description) {
        Long id = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT, new String[]{EVENT_KEY_ID.getColumnName()}, EVENT_KEY_TITLE.getColumnName() + "=?" +
                        " AND " + EVENT_KEY_STATE.getColumnName() + "=?",
                new String[]{description, "ACTIVE"}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            id = cursor.getLong(0);
            cursor.close();
        }

        closeDatabase();

        // return event
        return id;
    }

    protected void deleteFutureEvents(String today) {
        int rows = getDatabase().delete(TABLE_EVENT, EVENT_KEY_DATE.getColumnName() + "> '" + today + "'", null);
        closeDatabase();
        if (rows > 0)
            Log.i(TAG, "Number of events deleted: " + rows);
    }

    /******
     * Careful do not use it, only for dev purpose!
     ****/
    protected void deleteAllEvents() {
        int rows = getDatabase().delete(TABLE_EVENT, null, null);
        closeDatabase();
        if (rows > 0)
            Log.i(TAG, "Number of events deleted: " + rows);
    }

    // *************** CRUD notification table *********************/

    protected long createNotification(Notification notification) {
        // Prepare values to set
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_KEY_EVENT_ID.getColumnName(), notification.getEventId());
        values.put(NOTIFICATION_KEY_STATE.getColumnName(), notification.getState().toString());
        values.put(NOTIFICATION_KEY_PLANNED_TIME.getColumnName(), -1);
        // Inserting Row
        long id = getDatabase().insert(TABLE_NOTIFICATION, null, values);
        // Closing database connection
        closeDatabase();

        return id;
    }

    /**
     * Deleting notification
     */
    protected void deleteNotification(long id) {
        getDatabase().delete(TABLE_NOTIFICATION, NOTIFICATION_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Set notification to OLD
     */
    protected void setNotificationToOld(long id) {
        ContentValues values = new ContentValues();
        // update only color of event
        values.put(NOTIFICATION_KEY_STATE.getColumnName(), IEventDAO.NOTIFICATION_STATE.OLD.toString());
        getDatabase().update(TABLE_NOTIFICATION, values, NOTIFICATION_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Set notification to ACTIVE
     */
    protected void setNotificationToActive(long id) {
        ContentValues values = new ContentValues();
        // update only color of event
        values.put(NOTIFICATION_KEY_STATE.getColumnName(), IEventDAO.NOTIFICATION_STATE.ACTIVE.toString());
        getDatabase().update(TABLE_NOTIFICATION, values, NOTIFICATION_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Create Notification based on cursor
     */
    private Notification createNotification(Cursor cursor) {
        Notification notification = null;
        try {
            notification = new Notification(cursor.getInt(NOTIFICATION_KEY_ID.getColumnPosition()),
                    cursor.getInt(NOTIFICATION_KEY_EVENT_ID.getColumnPosition()),
                    IEventDAO.NOTIFICATION_STATE.valueOf(cursor.getString(NOTIFICATION_KEY_STATE.getColumnPosition())),
                    cursor.getString(NOTIFICATION_KEY_PLANNED_DATE.getColumnPosition()) != null ?
                            DATE_FORMAT.parse(cursor.getString(NOTIFICATION_KEY_PLANNED_DATE.getColumnPosition())) : null,
                    cursor.getInt(NOTIFICATION_KEY_PLANNED_TIME.getColumnPosition())
            );
        } catch (ParseException e) {
            Log.e(TAG, "Error occurred during notification creation: " + e.getMessage(), e);
        }
        return notification;
    }

    /**
     * Getting all notifications
     */
    protected List<Notification> getAllNotifications() {
        List<Notification> notificationsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION;

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notification notification = createNotification(cursor);
                // Adding event to list
                notificationsList.add(notification);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return events list
        return notificationsList;
    }

    /**
     * Getting all notifications
     */
    protected List<Notification> getAllActiveNotifications() {
        List<Notification> notificationsList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_NOTIFICATION + " WHERE state = '" + IEventDAO.NOTIFICATION_STATE.ACTIVE.toString() + "'";

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Notification notification = createNotification(cursor);
                // Adding event to list
                notificationsList.add(notification);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return events list
        return notificationsList;
    }

    /**
     * Getting notifications count
     */
    protected int getNotificationsCount() {
        String countQuery = "SELECT " + NOTIFICATION_KEY_ID.getColumnName() + " FROM " + TABLE_NOTIFICATION;
        Cursor cursor = getDatabase().rawQuery(countQuery, null);
        int result = cursor.getCount();
        cursor.close();
        closeDatabase();
        // return count
        return result;
    }

    /**
     * Getting notifications count
     */
    protected int getActiveNotificationsCount() {
        String countQuery = "SELECT " + NOTIFICATION_KEY_ID.getColumnName() + " FROM " + TABLE_NOTIFICATION + " WHERE state = '" + IEventDAO.NOTIFICATION_STATE.ACTIVE.toString() + "'";
        Cursor cursor = getDatabase().rawQuery(countQuery, null);
        int result = cursor.getCount();
        cursor.close();
        closeDatabase();
        // return count
        return result;
    }

    /**
     * Set event planned date
     **/
    protected void setNotificationEventPlannedDate(long id, String date) {
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_KEY_PLANNED_DATE.getColumnName(), date);
        getDatabase().update(TABLE_NOTIFICATION, values, NOTIFICATION_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    /**
     * Set event planned date
     **/
    protected void setNotificationEventPlannedTime(long id, int time) {
        ContentValues values = new ContentValues();
        values.put(NOTIFICATION_KEY_PLANNED_TIME.getColumnName(), time);
        getDatabase().update(TABLE_NOTIFICATION, values, NOTIFICATION_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    // *************** CRUD event template table ******************/

    protected long createEventTemplate(EventTemplate eventTemplate) {
        // Prepare values to set
        ContentValues values = new ContentValues();
        values.put(EVENT_TEMPLATE_KEY_TITLE.getColumnName(), eventTemplate.getTitle());
        values.put(EVENT_TEMPLATE_KEY_DESCRIPTION.getColumnName(), eventTemplate.getDescription());
        values.put(EVENT_TEMPLATE_KEY_IMAGE_PATH.getColumnName(), eventTemplate.getImagePath());
        values.put(EVENT_TEMPLATE_KEY_COLOR.getColumnName(), eventTemplate.getColor());
        values.put(EVENT_TEMPLATE_KEY_CREATION_DATE.getColumnName(), EventTemplate.DATE_FOR_DB.format(eventTemplate.getCreationDate()));
        // Inserting Row
        long id = getDatabase().insert(TABLE_EVENT_TEMPLATE, null, values);
        // Closing database connection
        closeDatabase();
        return id;
    }

    protected long createEventTemplateWithoutImage(EventTemplate eventTemplate) {
        // Prepare values to set
        ContentValues values = new ContentValues();
        values.put(EVENT_TEMPLATE_KEY_TITLE.getColumnName(), eventTemplate.getTitle());
        values.put(EVENT_TEMPLATE_KEY_DESCRIPTION.getColumnName(), eventTemplate.getDescription());
        values.put(EVENT_TEMPLATE_KEY_COLOR.getColumnName(), eventTemplate.getColor());
        values.put(EVENT_TEMPLATE_KEY_CREATION_DATE.getColumnName(), EventTemplate.DATE_FOR_DB.format(eventTemplate.getCreationDate()));
        // Inserting Row
        long id = getDatabase().insert(TABLE_EVENT_TEMPLATE, null, values);
        // Closing database connection
        closeDatabase();
        return id;
    }

    protected void updateEventTemplateImagePath(long id, String imagePath) {
        ContentValues con = new ContentValues();
        con.put(EVENT_TEMPLATE_KEY_IMAGE_PATH.getColumnName(), imagePath);
        getDatabase().update(TABLE_EVENT_TEMPLATE, con, EVENT_TEMPLATE_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    protected void updateEventContact(long id, long contactId) {
        ContentValues con = new ContentValues();
        con.put(EVENT_KEY_CONTACT_IDS.getColumnName(), contactId);
        getDatabase().update(TABLE_EVENT, con, EVENT_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    private EventTemplate createEventTemplate(Cursor cursor) {
        EventTemplate eventTemplate = null;

        try {

            eventTemplate = new EventTemplate(Long.parseLong(cursor.getString(EVENT_TEMPLATE_KEY_ID.getColumnPosition())),
                    cursor.getString(EVENT_TEMPLATE_KEY_TITLE.getColumnPosition()),
                    cursor.getString(EVENT_TEMPLATE_KEY_IMAGE_PATH.getColumnPosition()),
                    cursor.getString(EVENT_TEMPLATE_KEY_DESCRIPTION.getColumnPosition()),
                    cursor.getString(EVENT_TEMPLATE_KEY_COLOR.getColumnPosition()),
                    cursor.getString(EVENT_TEMPLATE_KEY_CREATION_DATE.getColumnPosition()) != null ? EventTemplate.DATE_FOR_DB.parse(cursor.getString(EVENT_TEMPLATE_KEY_CREATION_DATE.getColumnPosition())) : null);
        } catch (ParseException e) {
            Log.e(TAG, "Error occurred during event template creation: " + e.getMessage(), e);
        }

        return eventTemplate;
    }

    /**
     * Getting all event templates
     */
    protected List<EventTemplate> getAllEventTemplates() {
        List<EventTemplate> eventTemplateList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT_TEMPLATE;

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                EventTemplate eventTemplate = createEventTemplate(cursor);
                // Adding template to list
                if (eventTemplate != null) {
                    eventTemplateList.add(eventTemplate);
                }
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return event templates list
        return eventTemplateList;
    }

    protected EventTemplate retrieveEventTemplate(long id) {
        EventTemplate eventTemplate = null;

        Cursor cursor = getDatabase().query(TABLE_EVENT_TEMPLATE, null, EVENT_TEMPLATE_KEY_ID.getColumnName() + "=?",
                new String[]{String.valueOf(id)}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            cursor.moveToFirst();
            eventTemplate = createEventTemplate(cursor);
            cursor.close();
        }

        closeDatabase();

        return eventTemplate;
    }

    protected void updateEventTemplate(long id, String title, String description, String imageUrl, String color) {
        ContentValues con = new ContentValues();
        con.put(EVENT_TEMPLATE_KEY_TITLE.getColumnName(), title);
        con.put(EVENT_TEMPLATE_KEY_DESCRIPTION.getColumnName(), description);
        con.put(EVENT_TEMPLATE_KEY_IMAGE_PATH.getColumnName(), imageUrl);
        con.put(EVENT_TEMPLATE_KEY_COLOR.getColumnName(), color);
        getDatabase().update(TABLE_EVENT_TEMPLATE, con, EVENT_TEMPLATE_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    protected void deleteEventTemplate(long id) {
        getDatabase().delete(TABLE_EVENT_TEMPLATE, EVENT_TEMPLATE_KEY_ID.getColumnName() + "=" + id, null);
        closeDatabase();
    }

    // ******************************** Event statistics queries **********************************

    protected int countAllEventLinkedByReference(String reference) {
        int result = 0;
        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_REFERENCE.getColumnName() + "=?",
                new String[]{reference}, null, null, null, null);

        if (cursor != null && cursor.getCount() > 0) {
            result = cursor.getCount();
            cursor.close();
        }

        closeDatabase();

        return result;
    }

    // Retrieve all events sharing same reference (active and old) organized by date
    protected List<Date> retrieveAllEventDateByReference(String reference) {
        List<Date> eventDateList = new ArrayList<>();

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_REFERENCE.getColumnName() + "=?",
                new String[]{reference}, null, null, EVENT_KEY_DATE.getColumnName() + " DESC", null);

        if (cursor.moveToFirst()) {
            do {
                Event event = this.createEvent(cursor);
                // Adding event to list
                eventDateList.add(event.getDate());
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return event
        return eventDateList;
    }

    // Retrieve all events sharing same reference (active and old) organized by date
    protected List<Event> retrieveAllEventsByReference(String reference) {
        List<Event> eventExpensesList = new ArrayList<>();

        Cursor cursor = getDatabase().query(TABLE_EVENT, null, EVENT_KEY_REFERENCE.getColumnName() + "=?",
                new String[]{reference}, null, null, EVENT_KEY_EXPENSES.getColumnName() + " DESC", null);

        if (cursor.moveToFirst()) {
            do {
                Event event = this.createEvent(cursor);
                // Adding event to list
                eventExpensesList.add(event);
            } while (cursor.moveToNext());
        }

        cursor.close();
        closeDatabase();

        // return event
        return eventExpensesList;
    }

    /**
     * Count all active events
     */
    protected int countAllActiveEvents() {
        int result = 0;
        // Select All Query
        String selectQuery = "SELECT  * FROM " + TABLE_EVENT + " WHERE state = 'ACTIVE'";

        Cursor cursor = getDatabase().rawQuery(selectQuery, null);

        if (cursor != null && cursor.getCount() > 0) {
            result = cursor.getCount();
            cursor.close();
        }

        closeDatabase();

        // return events list
        return result;
    }

    // *************** NO Business request there !!! See DAO object *********************/

}
