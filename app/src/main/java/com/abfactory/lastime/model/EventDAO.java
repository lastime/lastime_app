package com.abfactory.lastime.model;

import android.content.Context;
import android.util.Log;

import com.abfactory.lastime.utils.LasTimeUtils;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static com.abfactory.lastime.model.DatabaseHandler.DATE_FORMAT;
import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.ACTIVE;
import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.OLD;
import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.TO_BE_DELETED;

/**
 * DAO class that would encapsulate event call and structure
 * Implements IEventDAO defining methods that could be call
 */
public class EventDAO implements IEventDAO {

    private DatabaseHandler databaseHandler;

    public EventDAO(Context context) {
        databaseHandler = new DatabaseHandler(context);
    }

    //**************************** EVENT ********************************

    @Override
    public List<Event> retrieveAllEvents() {
        return databaseHandler.retrieveAllEvents();
    }

    @Override
    public List<Event> getAllActiveEvents() {
        return databaseHandler.getAllActiveEvents();
    }

    @Override
    public int getEventsCount() {
        return databaseHandler.getEventsCount();
    }

    @Override
    public List<Event> getAllEventsForLocation(EventLocation eventLocation) {
        List<Event> eventsForLocationRequested = new ArrayList<>();
        // Go through all ACTIVE events
        for (Event e : getAllActiveEvents()) {
            // If id matches the one of the current location we are analyzing, add to the list to be returned
            if (e.getLocation() == eventLocation.getLocationId()) {
                eventsForLocationRequested.add(e);
            }
        }
        return eventsForLocationRequested;
    }

    /**
     * Adding a new event
     */
    @Override
    public long addEvent(Event event, EventLocation location, Contact contact) {
        // Handle location
        long locId = -1;
        if (location != null) {
            // If location already in DB
            if (location.getLocationId() != -1) {
                locId = location.getLocationId();
            } else {
                // If new location and therefore to add to DB
                locId = databaseHandler.createLocation(location.getName(),
                        location.getLatitude(), location.getLongitude(), null);
            }
        }
        // Handle contact
        long contactId = -1;
        if (contact != null) {
            // If contact already in DB
            if (contact.getContactId() != -1) {
                contactId = contact.getContactId();
            } else {
                // If new contact and therefore to add to DB
                contactId = databaseHandler.createContact(contact.getContactName(),
                        contact.getPhoneNumber(), contact.getPhoneId());
            }
        }

        String reference = String.valueOf(event.hashCode());
        if (event.getReference() != null && event.getReference().isEmpty())
            reference = event.getReference();
        return databaseHandler.createEvent(event.getTitle(), DATE_FORMAT.format(event.getDate()), event.getTimeOfEvent(), locId, event.getMark(),
                event.getImagePath(), event.getThumbnailPath(), event.getWeight(),
                event.getReminder(), event.getNote(), event.getColor(), ACTIVE.name(), DATE_FORMAT.format(new Date()),
                reference, event.getRecurrence(), contactId,
                event.getEndDate() != null ? DATE_FORMAT.format(event.getEndDate()) : null, event.getExpenses(), event.getHighlight(),
                event.getFieldList(), event.getWeather());
    }

    /**
     * Adding a new event without location
     */
    @Override
    public long addEvent(Event event) {
        return this.addEvent(event, null, null);
    }

    /**
     * Insert a new event
     * Based on import feature - Do not use it somewhere else!!!
     */
    @Override
    public long insertEvent(Event event) {
        return databaseHandler.createEvent(event.getTitle(), DATE_FORMAT.format(event.getDate()), event.getTimeOfEvent(), event.getLocation(), event.getMark(),
                event.getImagePath(), event.getThumbnailPath(), event.getWeight(),
                event.getReminder(), event.getNote(), event.getColor(), event.getState().name(),
                event.getCreation() != null ? DATE_FORMAT.format(event.getCreation()) : null,
                event.getReference(), event.getRecurrence(), event.getContact(), event.getEndDate() != null ? DATE_FORMAT.format(event.getEndDate()) : null,
                event.getExpenses(), event.getHighlight(), event.getFieldList(), event.getWeather());
    }

    /**
     * Deleting event and all related to it
     */
    @Override
    public void deleteEvent(long id) {
        Long locationId = databaseHandler.getLocationIdByEventId(id);
        // Delete location if needed
        if (locationId != null && locationId > 0 && !LasTimeUtils.hasLocationOtherReferences(locationId, databaseHandler.retrieveAllEvents()))
            databaseHandler.deleteLocation(locationId);
        // Delete images if needed
        List<String> imagePaths = databaseHandler.getImagePathsByEventId(id);
        for (String imagePath : imagePaths) {
            if (imagePath != null) {
                LasTimeUtils.deleteImage(imagePath);
            }
        }
        // Delete event
        databaseHandler.deleteEvent(id);
    }

    /**
     * Each update of event increase his weight by 1
     * and set date to now
     * It does not change the title or anything else
     */
    @Override
    public Long refreshEvent(Event event) {
        //Keep period time interval start/end dates
        Calendar calendar = Calendar.getInstance();
        int days = 0;
        if (event.getEndDate() != null) {
            days = LasTimeUtils.getDaysDifference(event.getDate(), event.getEndDate());
        }
        event.setDate(calendar.getTime());
        if (event.getEndDate() != null && days > 0) {
            calendar.add(Calendar.DAY_OF_YEAR, days);
            event.setEndDate(calendar.getTime());
        }

        return this.modifyEvent(event, databaseHandler.retrieveLocation(event.getLocation()),
                databaseHandler.retrieveContact(event.getContact()), false);
    }

    /**
     * Updating event based on id and new event data
     * Each update of event increase his weight by 1
     * (Used by history feature)
     */
    @Override
    public Long modifyEvent(Event event, EventLocation eventLocation, Contact contact, boolean forceHistory) {

        Long eventId = event.getId();

        long locId = -1;
        long contactId = -1;

        // Logic to determine if history of event is needed based on if date of event has been modified
        boolean isHistoryNeeded = false;
        Event existingEvent = this.retrieveEventById(event.getId());
        String reference = String.valueOf(event.hashCode());
        if (forceHistory || existingEvent != null && !existingEvent.getDate().equals(event.getDate())
                && event.getDate().compareTo(existingEvent.getDate()) > 0) {
            isHistoryNeeded = true;
            // Change event state to OLD before creating a new one with same reference id
            databaseHandler.updateEventState(event.getId(), OLD);
            if (event.getReference() == null)
                databaseHandler.updateEventReference(event.getId(), reference);
            else
                reference = event.getReference();
        }

        // Location handling
        if (eventLocation != null) {
            // If location already in DB
            if (eventLocation.getLocationId() != -1) {
                databaseHandler.updateLocation(eventLocation.getLocationId(), eventLocation.getName(),
                        eventLocation.getLatitude(), eventLocation.getLongitude(), eventLocation.getGooglePlacesId(), eventLocation.getBackendId());
                locId = eventLocation.getLocationId();
            } else {
                // If new location and therefore to add to DB
                locId = databaseHandler.createLocation(eventLocation.getName(),
                        eventLocation.getLatitude(), eventLocation.getLongitude(), null);
            }
        }

        // Contact handling
        if (contact != null) {
            // If contact already in DB
            if (contact.getContactId() != -1) {
                databaseHandler.updateContact(contact);
                contactId = contact.getContactId();
            } else {
                // If new contact and therefore to add to DB
                contactId = databaseHandler.createContact(contact.getContactName(),
                        contact.getPhoneNumber(), contact.getPhoneId());
            }
        }

        if (isHistoryNeeded) {
            eventId = databaseHandler.createEvent(event.getTitle(), DATE_FORMAT.format(event.getDate()),
                    event.getTimeOfEvent(), locId,
                    event.getMark(), event.getImagePath(), event.getThumbnailPath(), event.getWeight() + 1,
                    event.getReminder(), event.getNote(), event.getColor(), ACTIVE.name(), DATE_FORMAT.format(new Date()),
                    reference, event.getRecurrence(), contactId, event.getEndDate() != null ? DATE_FORMAT.format(event.getEndDate()) : null,
                    event.getExpenses(), event.getHighlight(), event.getFieldList(), event.getWeather());
        } else {
            databaseHandler.updateEvent(event.getId(), event.getTitle(), DATE_FORMAT.format(event.getDate()),
                    event.getTimeOfEvent(), locId,
                    event.getMark(), event.getImagePath(), event.getThumbnailPath(), event.getWeight() + 1,
                    event.getReminder(), event.getNote(), event.getColor(), event.getRecurrence(),
                    contactId, event.getBackendId(), event.getEndDate() != null ? DATE_FORMAT.format(event.getEndDate()) : null,
                    event.getExpenses(), event.getHighlight(), event.getFieldList(), event.getWeather());
        }

        designateActiveInEventFamily(event);
        return eventId;
    }

    /**
     * Search which event of the family should be the ACTIVE one and do the changes in DB.
     * We do not consider state TO_BE_DELETED: can an event to be deleted be the ACTIVE one?...
     * Return the unique ACTIVE event
     *
     * @param event
     */
    private Event designateActiveInEventFamily(Event event) {
        List<Event> activeEvents = getAllEventsByStateAndCommonId(EVENT_STATE.ACTIVE, event.getReference());
        List<Event> oldEvents = getAllEventsByStateAndCommonId(EVENT_STATE.OLD, event.getReference());
        List<Event> siblings = new ArrayList<>();
        siblings.addAll(activeEvents);
        siblings.addAll(oldEvents);

        Date closestDate = null;
        Event realActiveEvent = null;
        for (Event sibling : siblings) {
            if (isDateMoreActive(sibling.getDate(), closestDate)) {
                closestDate = sibling.getDate();
                realActiveEvent = sibling;
            }
        }
        // cleaning: remove ACTIVE events that are not supposed to be ACTIVE
        for (Event activeEvent : activeEvents) { // cannot be null
            if (activeEvent != realActiveEvent) {
                updateEventState(activeEvent, EVENT_STATE.OLD);
            }
        }
        // flag ACTIVE: make sure ACTIVE event is recognized as such
        if (realActiveEvent.getState() != EVENT_STATE.ACTIVE) {
            updateEventState(realActiveEvent, EVENT_STATE.ACTIVE);
        }
        return realActiveEvent;
    }

    private boolean isDateMoreActive(Date date, Date referenceDate) {
        return (date != null &&  // if date is null we will suppose referenceDate is more recent
                (referenceDate == null || date.compareTo(referenceDate) > 0));
    }

    public void updateEventState(Event event, EVENT_STATE state) {
        if (event.getState() != state) {
            databaseHandler.updateEventState(event.getId(), state);
        }
    }

    @Override
    public Event findEventByTitle(String title) {
        return databaseHandler.getEventByTitle(title);
    }

    @Override
    public Event findActiveEventByTitle(String title) {
        return databaseHandler.getActiveEventByTitle(title);
    }

    @Override
    public Long findEventIdByTitle(String title) {
        return databaseHandler.getEventIdByTitle(title);
    }

    @Override
    public List<Event> getTopEvents(String max) {
        return databaseHandler.getTopEvents(max);
    }

    @Override
    public Set<String> retrieveAllImagePaths() {
        return databaseHandler.getAllImagePaths();
    }

    @Override
    public void markEventToBeDeleted(long id) {
        databaseHandler.updateEventState(id, TO_BE_DELETED);
    }

    @Override
    public void markEventToActive(long id) {
        databaseHandler.updateEventState(id, ACTIVE);
    }

    @Override
    public List<Event> findEventsToBeDeleted() {
        return databaseHandler.getAllEventsByState(TO_BE_DELETED);
    }

    @Override
    public void refreshEvent(Event event, String date, String endDate) {
        databaseHandler.updateEvent(event.getId(), date, event.getWeight(), endDate);
    }

    @Override
    public void refreshFieldEvent(Event event) {
        databaseHandler.updateEventField(event.getId(), event.getFieldList());
    }

    @Override
    public void cleanAllEventToBeDeleted() {
        databaseHandler.deleteAllEventWithState(TO_BE_DELETED);
        // Clean up process - delete also event with no reference and in state old
        List<Event> events = databaseHandler.getAllEventsByState(OLD);
        for (Event event : events) {
            if (event.getReference() == null || getActiveEvent(event) == null)
                databaseHandler.deleteEvent(event.getId());
        }
    }

    @Override
    public Event retrieveEventById(long id) {
        return databaseHandler.retrieveEvent(id);
    }

    @Override
    public void markEventToOld(long id) {
        databaseHandler.updateEventState(id, OLD);
    }

    @Override
    public List<Event> findHistoryOfEvent(Event event) {
        return databaseHandler.getAllEventsByStateAndCommonId(OLD, event.getReference());
    }

    /**
     * Updating event based on id and new event data
     * Each update of event increase his weight by 1
     */
    @Override
    public void updateEvent(Event event, EventLocation location, Contact contact) {

        long locId = -1;
        long contactId = -1;

        // Location handling
        if (location != null) {
            // If location already in DB
            if (location.getLocationId() != -1) {
                databaseHandler.updateLocation(location.getLocationId(), location.getName(),
                        location.getLatitude(), location.getLongitude(), location.getGooglePlacesId(), location.getBackendId());
                locId = location.getLocationId();
            } else {
                // If new location and therefore to add to DB
                locId = databaseHandler.createLocation(location.getName(),
                        location.getLatitude(), location.getLongitude(), null);
            }
        }

        // Contact handling
        if (contact != null) {
            // If contact already in DB
            if (contact.getContactId() != -1) {
                databaseHandler.updateContact(contact);
                contactId = contact.getContactId();
            } else {
                // If new contact and therefore to add to DB
                contactId = databaseHandler.createContact(contact.getContactName(),
                        contact.getPhoneNumber(), contact.getPhoneId());
            }
        }

        databaseHandler.updateEvent(event.getId(), event.getTitle(), DATE_FORMAT.format(event.getDate()),
                event.getTimeOfEvent(), locId,
                event.getMark(), event.getImagePath(), event.getThumbnailPath(), event.getWeight() + 1,
                event.getReminder(),
                event.getNote(), event.getColor(), event.getRecurrence(), contactId, event.getBackendId(),
                event.getEndDate() != null ? DATE_FORMAT.format(event.getEndDate()) : null, event.getExpenses(),
                event.getHighlight(), event.getFieldList(), event.getWeather());

    }

    @Override
    public Event findActiveEvent(Event event) {
        return databaseHandler.retrieveActiveEventByReference(event.getReference());
    }

    /**
     * Delete everything related to event
     *
     * @param reference
     */
    @Override
    public void deleteEventAndHistory(String reference) {
        databaseHandler.deleteEventByReference(reference);
    }

    /**
     * Delete current active event and activate previous one
     *
     * @param event
     */
    @Override
    public Event deleteEvent(Event event) {
        // Get previous event
        Event eventToActivate = databaseHandler.findPreviousEventBasedOnReference(event.getReference());
        // Delete event
        databaseHandler.deleteEvent(event.getId());
        // Pass active previous one if history exists
        if (eventToActivate != null) {
            databaseHandler.updateEventState(eventToActivate.getId(), ACTIVE);
            eventToActivate.setState(ACTIVE);
        }

        return eventToActivate;
    }

    /**
     * Find the active event of a given reference
     *
     * @param event
     * @return active event
     */
    @Override
    public Event getActiveEvent(Event event) {
        return databaseHandler.findActiveEventByReference(event.getReference());
    }

    /**
     * Change event color randomly
     */
    @Override
    public Integer changeEventColor(Event event) {
        Integer color = LasTimeUtils.getRandomColor();
        databaseHandler.updateEventColor(event.getReference(), color);
        return color;
    }

    @Override
    /**
     * Looking for existing event only in active state
     */
    public Long findActiveEventIdByTitle(String title) {
        return databaseHandler.getActiveEventIdByTitle(title);
    }

    /**
     * Used to solve dev problem.. in case same event got several active states
     */
    @Override
    public void cleanActiveEventDuplicated() {
        List<Event> allEvents = databaseHandler.getAllEventsByState(ACTIVE);
        for (Event activeEvent : allEvents) {
            List<Event> events = databaseHandler.getAllEventsByStateAndCommonId(ACTIVE, activeEvent.getReference());
            if (events.size() > 1) {
                boolean keepItDone = false;
                for (Event tmpEvent : events) {
                    // Just keep one
                    if (keepItDone)
                        databaseHandler.deleteEvent(tmpEvent.getId());
                    keepItDone = true;
                }
            }
        }
    }

    /**
     * Only used by dev team
     */
    @Override
    public void cleanEventsDefinedInFuture() {
        databaseHandler.deleteFutureEvents(DATE_FORMAT.format(new Date()));
    }

    /**
     * DO NOT USE THIS !
     * ONLY FOR DEV PURPOSE
     * COULD NOT BE UNDONE!
     */
    @Override
    public void eraseAllEvents() {
        databaseHandler.deleteAllEvents();
    }

    //**************************** LOCATION ************************************

    @Override
    public List<EventLocation> getAllEventLocations() {
        return databaseHandler.getAllEventLocations();
    }

    @Override
    public EventLocation getLocationByEvent(Event event) {
        Long eventLocationId = databaseHandler.getLocationIdByEventId(event.getId());
        if (eventLocationId != null)
            return databaseHandler.retrieveLocation(databaseHandler.getLocationIdByEventId(event.getId()));
        return null;
    }

    @Override
    public EventLocation getLocationById(long id) {
        return databaseHandler.retrieveLocation(id);
    }

    @Override
    public void deleteLocationById(long id) {
        databaseHandler.deleteLocation(id);
    }

    @Override
    public Long insertLocation(EventLocation eventLocation) {
        return databaseHandler.createLocation(eventLocation.getName(), eventLocation.getLatitude(),
                eventLocation.getLongitude(), eventLocation.getGooglePlacesId());
    }

    //**************************** CONTACT ************************************

    @Override
    public List<Contact> getAllContacts() {
        return databaseHandler.getAllContacts();
    }

    @Override
    public Contact getContactById(long id) {
        return databaseHandler.retrieveContact(id);
    }

    @Override
    public Long insertContact(Contact contact) {
        return databaseHandler.createContact(contact.getContactName(), contact.getPhoneNumber(), contact.getPhoneId());
    }

    @Override
    public void updateContact(Contact contact) {
        databaseHandler.updateContact(contact);
    }

    @Override
    public void updateEventContact(long id, long contactId) {
        databaseHandler.updateEventContact(id, contactId);
    }

    @Override
    public void deleteContact(Contact contact) {
        databaseHandler.deleteContact(contact.getContactId());
    }

    //**************************** NOTIFICATION ********************************

    @Override
    public long createNotification(Notification notification) {
        return databaseHandler.createNotification(notification);
    }

    @Override
    public void deleteNotification(long id) {
        databaseHandler.deleteNotification(id);
    }

    @Override
    public List<Notification> getAllNotifications() {
        return databaseHandler.getAllNotifications();
    }

    @Override
    public List<Notification> getAllActiveNotifications() {
        return databaseHandler.getAllActiveNotifications();
    }

    @Override
    public int getActiveNotificationsCount() {
        return databaseHandler.getActiveNotificationsCount();
    }

    @Override
    public void setNotificationToOld(long id) {
        databaseHandler.setNotificationToOld(id);
    }

    @Override
    public void setNotificationToActive(long id) {
        databaseHandler.setNotificationToActive(id);
    }

    @Override
    public void setNotificationEventPlannedDate(long id, Date date) {
        if (date != null)
            databaseHandler.setNotificationEventPlannedDate(id, DATE_FORMAT.format(date));
        else
            databaseHandler.setNotificationEventPlannedDate(id, null);
    }

    @Override
    public void setNotificationEventPlannedTime(long id, int time) {
        databaseHandler.setNotificationEventPlannedTime(id, time);
    }

//**************************** EVENT TEMPLATES ********************************

    @Override
    public long createEventTemplate(EventTemplate eventTemplate) {
        return databaseHandler.createEventTemplate(eventTemplate);
    }

    @Override
    public long createEventTemplateWithoutImage(EventTemplate eventTemplate) {
        return databaseHandler.createEventTemplateWithoutImage(eventTemplate);
    }

    @Override
    public void updateEventTemplateImagePath(long id, String imagePath) {
        databaseHandler.updateEventTemplateImagePath(id, imagePath);
    }

    @Override
    public List<EventTemplate> getAllEventTemplates() {
        List<EventTemplate> result = databaseHandler.getAllEventTemplates();
        //Order by decreasing creation date
        Collections.sort(result, new Comparator<EventTemplate>() {
            @Override
            public int compare(EventTemplate eventTemplate1, EventTemplate eventTemplate2) {
                return eventTemplate2.getCreationDate().compareTo(eventTemplate1.getCreationDate());
            }
        });
        return result;
    }

    @Override
    public void deleteEventTemplate(long id) {
        databaseHandler.deleteEventTemplate(id);
    }

    @Override
    public EventTemplate retrieveEventTemplateById(long id) {
        return databaseHandler.retrieveEventTemplate(id);
    }

    // ******************************** Event statistics queries **********************************
    @Override
    public List<Event> getAllEventHistory(Event event) {
        if (event.getReference() != null)
            return databaseHandler.retrieveAllEventsByReference(event.getReference());
        else
            return Collections.emptyList();
    }

    @Override
    public int getAllActiveEventNumber() {
        return databaseHandler.countAllActiveEvents();
    }

    // Returns the smallest time interval between all dates of same event
    // In days
    // @return null if event got no child
    @Override
    public Map<String, Integer> computeIntervalsTimeInDays(Event event) {
        Map<String, Integer> result = new HashMap<>();
        int min = 1;
        int max = 0;
        if (event.getReference() != null) {
            List<Date> eventDateList = databaseHandler.retrieveAllEventDateByReference(event.getReference());
            if (eventDateList.size() > 1) {
                Collections.sort(eventDateList);
                int interval;
                for (int i = 0; i < eventDateList.size(); i++) {
                    if (i + 1 >= eventDateList.size()) {
                        break;
                    }
                    interval = LasTimeUtils.computeDaysDifference(eventDateList.get(i), eventDateList.get(i + 1));
                    if (interval < min)
                        min = interval;
                    if (interval > max)
                        max = interval;
                }
            } else {
                return null;
            }
        } else {
            throw new IllegalStateException("Event should have a reference!");
        }
        result.put(INTERVAL.MAX.name(), max);
        result.put(INTERVAL.MIN.name(), min);
        return result;
    }

    // Returns the smallest time interval between all dates in all events (Could be time consuming)
    // In days
    @Override
    public Map<String, String> computeIntervalsTimeInDays(List<Event> eventList) {
        Map<String, String> result = new HashMap<>();
        int min = 1;
        int max = 0;
        String eventMin = "";
        String eventMax = "";
        for (Event event : eventList) {
            try {
                Map<String, Integer> eventResult = this.computeIntervalsTimeInDays(event);
                if (eventResult != null) {
                    if (eventResult.get(INTERVAL.MIN.name()) < min) {
                        min = eventResult.get(INTERVAL.MIN.name());
                        eventMin = event.getTitle();
                    }
                    if (eventResult.get(INTERVAL.MAX.name()) > max) {
                        max = eventResult.get(INTERVAL.MAX.name());
                        eventMax = event.getTitle();
                    }
                }
            } catch (IllegalStateException e) {
                Log.i(EventDAO.class.getName(), "Event seems unique! " + e.getMessage(), e);
            }
        }
        result.put(INTERVAL.MAX.name(), eventMax + " " + max);
        result.put(INTERVAL.MIN.name(), eventMin + " " + min);

        return result;
    }

    @Override
    public Event getOldestEvent() {

        Event result = null;
        Date resultDate = null;

        // Initialize data
        if (!getAllActiveEvents().isEmpty()) {
            result = getAllActiveEvents().get(0);
            resultDate = getAllActiveEvents().get(0).getCreation();
        }

        // Go through all active events
        for (Event e : getAllActiveEvents()) {
            if (e.getCreation() != null && e.getCreation().before(resultDate)) {
                result = e;
                resultDate = e.getCreation();
            }
        }

        return result;

    }

    public int[] getEventsByMonths(Event event, int year) {
        int[] arrangedEventList = new int[12];
        List<Event> eventHistory = getAllEventHistory(event);

        int month;
        DateTime datetime;

        if (!eventHistory.isEmpty()) {
            for (Event eventH : eventHistory) {
                datetime = new DateTime(eventH.getDate());
                if (datetime.getYear() == year) {
                    month = new DateTime(eventH.getDate()).monthOfYear().get() - 1;
                    arrangedEventList[month]++;
                }
            }
        } else {
            datetime = new DateTime(event.getDate());
            // Current event
            if (datetime.getYear() == year) {
                month = new DateTime(event.getDate()).monthOfYear().get() - 1;
                arrangedEventList[month]++;
            }
        }

        return arrangedEventList;
    }

    public int[] getEventsByDays(Event event, int month, int year) {
        int[] arrangedEventList = new int[7];
        List<Event> eventHistory = getAllEventHistory(event);

        int day;
        DateTime datetime;

        if (!eventHistory.isEmpty()) {
            for (Event eventH : eventHistory) {
                datetime = new DateTime(eventH.getDate());
                if (datetime.getYear() == year && datetime.getMonthOfYear() == month) {
                    day = new DateTime(eventH.getDate()).dayOfWeek().get() - 1;
                    arrangedEventList[day]++;
                }
            }
        } else {
            datetime = new DateTime(event.getDate());
            // Current event
            if (datetime.getYear() == year && datetime.getMonthOfYear() == month) {
                day = new DateTime(event.getDate()).dayOfWeek().get() - 1;
                arrangedEventList[day]++;
            }
        }

        return arrangedEventList;
    }

    @Override
    public List<Event> getAllEventsByStateAndCommonId(EVENT_STATE eventState, String reference) {
        return databaseHandler.getAllEventsByStateAndCommonId(eventState, reference);
    }


    //*************************** MIGRATION UTILS ******************************

    // To be used when migrating to DB level 15
    @Deprecated
    public void migrateContactsToNewFormat(Context ctx) {

        for (Event e : retrieveAllEvents()) {
            Contact contactFromTel = null;
            Contact contactFromAppDB = null;
            // Initialize only if potentially valid contact only
            if (e.getContact() != -1) {
                contactFromTel = new Contact(String.valueOf(e.getContact()), ctx);
                contactFromAppDB = getContactById(e.getContact());
            }
            // If contact seems to be defined in app but seems not valid
            if (contactFromAppDB != null) {
                if (contactFromAppDB.getContactName() == null || contactFromAppDB.getPhoneNumber() == null) {
                    // Clean contact
                    updateEventContact(e.getId(), -1);
                }
            } else { // If contact does not correspond to any app contact
                // If contact seems to correspond to phone contact
                if (contactFromTel == null || contactFromTel.getContactName() == null || contactFromTel.getPhoneNumber() == null) {
                    // Clean contact
                    updateEventContact(e.getId(), -1);
                } else {
                    contactFromTel.setPhoneId(String.valueOf(e.getContact()));
                    long id = insertContact(contactFromTel);
                    updateEventContact(e.getId(), id);
                }
            }
        }
    }

}
