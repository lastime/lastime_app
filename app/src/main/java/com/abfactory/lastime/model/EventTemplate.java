package com.abfactory.lastime.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class EventTemplate implements Parcelable {

    public transient static final SimpleDateFormat DATE_FOR_DB = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.ENGLISH);

    private long event_template_id = -1;
    private String title;
    private String imagePath;
    private String description;
    private String color;
    private Date creationDate;

    private OnCardDismissedListener mOnCardDismissedListener = null;

    public interface OnCardDismissedListener {
        void onLike();
        void onDislike();
    }

    public EventTemplate(String title, String imagePath, String description, String color){
        this.title = title;
        this.imagePath = imagePath;
        this. description = description;
        this. color = color;
    }

    public EventTemplate(long id, String title, String imagePath, String description, String color, Date creationDate){
        this.event_template_id = id;
        this.title = title;
        this.imagePath = imagePath;
        this. description = description;
        this. color = color;
        this.creationDate = creationDate;
    }

    public long getId() {
        return event_template_id;
    }

    public void setId(long id) {
        this.event_template_id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Date getCreationDate() {
        return creationDate;
    }

    public String getCreationDateAsString() {
        return DATE_FOR_DB.format(this.creationDate);
    }

    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    public void setOnCardDismissedListener( OnCardDismissedListener listener ) {
        this.mOnCardDismissedListener = listener;
    }

    public OnCardDismissedListener getOnCardDismissedListener() {
        return this.mOnCardDismissedListener;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    private EventTemplate(Parcel in) {
        event_template_id = in.readLong();
        title = in.readString();
        imagePath = in.readString();
        description = in.readString();
        color = in.readString();
        try {
            creationDate = DATE_FOR_DB.parse(in.readString());
        } catch (ParseException e) {
            creationDate = null;
        }
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(event_template_id);
        parcel.writeString(title);
        parcel.writeString(imagePath);
        parcel.writeString(description);
        parcel.writeString(color);
        parcel.writeString(getCreationDateAsString());
    }

    public transient static final Parcelable.Creator CREATOR = new Parcelable.Creator<EventTemplate>() {
        public EventTemplate createFromParcel(Parcel in) {
            return new EventTemplate(in);
        }
        public EventTemplate[] newArray(int size) {
            return new EventTemplate[size];
        }
    };

}
