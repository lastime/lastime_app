package com.abfactory.lastime.model;

import android.graphics.Color;

import java.util.ArrayList;
import java.util.List;

public class ColorPaletteElement {

    private String code;
    private String displayableName;

    private int id;

    public static String DEFAULT_COLOR = "#757575";

    public ColorPaletteElement(String code, String name){
        this.code = code;
        this.displayableName = name;
        this.id = Color.parseColor(code);
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDisplayableName() {
        return displayableName;
    }

    public void setDisplayableName(String displayableName) {
        this.displayableName = displayableName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public static List<ColorPaletteElement> colors = new ArrayList<>(); // See ref there: http://www.google.com/design/spec/style/color.html#color-color-palette
    static {
        colors.add(new ColorPaletteElement("#F44336","Red"));
        colors.add(new ColorPaletteElement("#E91E63","Pink"));
        colors.add(new ColorPaletteElement("#9C27B0","Purple"));
        colors.add(new ColorPaletteElement("#673AB7","Deep Purple"));
        colors.add(new ColorPaletteElement("#3F51B5","Indigo"));
        colors.add(new ColorPaletteElement("#2196F3","Blue"));
        colors.add(new ColorPaletteElement("#03A9F4","Light Blue"));
        colors.add(new ColorPaletteElement("#00BCD4","Cyan"));
        colors.add(new ColorPaletteElement("#009688","Teal"));
        colors.add(new ColorPaletteElement("#4CAF50","Green"));
        colors.add(new ColorPaletteElement("#8BC34A","Light Green"));
        colors.add(new ColorPaletteElement("#CDDC39","Lime"));
//        colors.add(new ColorPaletteElement("#FFEB3B","Yellow"));
        colors.add(new ColorPaletteElement("#FFC107","Amber"));
        colors.add(new ColorPaletteElement("#FF9800","Orange"));
//        colors.add(new ColorPaletteElement("#FF5722","Deep Orange"));
        colors.add(new ColorPaletteElement("#795548","Brown"));
        colors.add(new ColorPaletteElement("#9E9E9E","Grey"));
        colors.add(new ColorPaletteElement("#607D8B","Blue Grey"));
        colors.add(new ColorPaletteElement("#000000","Black"));
    }

    public static ColorPaletteElement getColorPaletteElementFromCode(String code){

        if(code!=null){
            for(ColorPaletteElement cpe: colors){
                if(code.equals(cpe.getCode())){
                    return cpe;
                }
            }
        }

        return null;
    }

    public static ColorPaletteElement getColorPaletteElementFromId(int id){

        for(ColorPaletteElement cpe: colors){
            if(id == cpe.getId()){
                return cpe;
            }
        }

        return null;
    }

}
