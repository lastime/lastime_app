package com.abfactory.lastime.model;

import com.abfactory.lastime.Lastime;

import org.restlet.Request;
import org.restlet.data.Form;
import org.restlet.representation.Representation;
import org.restlet.resource.Get;
import org.restlet.resource.Post;
import org.restlet.resource.ServerResource;

import java.util.Map;

public class EventResource extends ServerResource{
    @Get("txt")
    public String retrieveEvent() {
        Request request = getRequest();
        DatabaseHandler dbh = new DatabaseHandler(Lastime.getContext());
        Map<String, Object> requestAttributes = request.getAttributes();
        Event event = dbh.retrieveEvent(Long.parseLong((String)requestAttributes.get("id")));
        return "Event to retrieve is " + event.toString();
    }

    @Post("json")
    public String saveEvent(Representation entity){
        final Form form = new Form(entity);
        String name = form.getFirstValue("name");
        return "Save event has been called!";
    }
}
