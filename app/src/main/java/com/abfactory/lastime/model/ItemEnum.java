package com.abfactory.lastime.model;

/**
 * Mapping item column name/position
 */
class ItemEnum {
    enum ITEM {
        // Event table
        EVENT_KEY_ID("_id", 0), EVENT_KEY_TITLE("title", 1), EVENT_KEY_DATE("date", 2), EVENT_KEY_LOCATION("location", 3),
        EVENT_KEY_MARK("mark", 4), EVENT_KEY_IMAGE_PATH("image_path", 5), EVENT_KEY_THUMBNAIL_PATH("thumbnail_path", 6),
        EVENT_KEY_WEIGHT("weight", 7), EVENT_KEY_REMINDER("reminder", 8), EVENT_KEY_NOTE("note",9), EVENT_KEY_COLOR("color",10),
        EVENT_KEY_STATE("state",11), EVENT_KEY_TIME("time", 12), EVENT_KEY_CREATION("creation", 13),
        EVENT_KEY_REFERENCE("reference", 14), EVENT_KEY_RECURRENCE("recurrence", 15), EVENT_KEY_CONTACT_IDS("contact_ids", 16),
        EVENT_KEY_BACKEND_ID("backend_id", 17), EVENT_KEY_END_DATE("end_date", 18), EVENT_KEY_EXPENSES("expenses", 19),
        EVENT_KEY_HIGHLIGHT("highlight", 20), EVENT_KEY_FIELD_LIST("field_list", 21), EVENT_KEY_WEATHER("weather", 22),

        // Location table
        LOCATION_KEY_ID("_id", 0),
        LOCATION_KEY_NAME("name",1),
        LOCATION_KEY_LATITUDE("latitude",2),
        LOCATION_KEY_LONGITUDE("longitude",3),
        LOCATION_KEY_GOOGLE_PLACE_ID("googlePlaceId",4),
        LOCATION_KEY_BACKEND_ID("backend_id", 5),

        // Contacts table
        CONTACT_KEY_ID("_id", 0),
        CONTACT_KEY_NAME("name",1),
        CONTACT_KEY_PHONE_NUMBER("phone_number",2),
        CONTACT_KEY_PHONE_ID("phone_id",3),
        CONTACT_KEY_BACKEND_ID("backend_id", 4),

        // Notification table
        NOTIFICATION_KEY_ID("_id", 0),
        NOTIFICATION_KEY_EVENT_ID("event_id", 1),
        NOTIFICATION_KEY_STATE("state", 2),
        NOTIFICATION_KEY_PLANNED_DATE("planned_date", 3),
        NOTIFICATION_KEY_PLANNED_TIME("planned_time", 4),

        // Event Template table
        EVENT_TEMPLATE_KEY_ID("_id", 0),
        EVENT_TEMPLATE_KEY_TITLE("title", 1),
        EVENT_TEMPLATE_KEY_DESCRIPTION("description", 2),
        EVENT_TEMPLATE_KEY_IMAGE_PATH("image", 3),
        EVENT_TEMPLATE_KEY_COLOR("color", 4),
        EVENT_TEMPLATE_KEY_CREATION_DATE("creation_date", 5);

        private String columnName;
        private int columnPosition;

        ITEM(String columnName, int columnPosition) {
            this.columnName = columnName;
            this.columnPosition = columnPosition;
        }

        public String getColumnName() {
            return this.columnName;
        }

        public int getColumnPosition() {
            return this.columnPosition;
        }
    }

}
