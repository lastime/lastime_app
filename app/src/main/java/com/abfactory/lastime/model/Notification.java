package com.abfactory.lastime.model;

import java.util.Date;

/**
 * Class that map relation between event and notification
 */
public class Notification {

    private int notificationId;
    private long eventId;
    private IEventDAO.NOTIFICATION_STATE state;
    private Date eventDatePlanned;
    private int eventTimePlanned;

    public Notification(int notificationId, int eventId, IEventDAO.NOTIFICATION_STATE state, Date date, int time) {
        this.notificationId = notificationId;
        this.eventId = eventId;
        this.state = state;
        this.eventDatePlanned = date;
        this.eventTimePlanned = time;
    }

    public Notification(long eventId, IEventDAO.NOTIFICATION_STATE state) {
        this.eventId = eventId;
        this.state = state;
    }

    public int getNotificationId() {
        return notificationId;
    }

    public void setNotificationId(int notificationId) {
        this.notificationId = notificationId;
    }

    public long getEventId() {
        return eventId;
    }

    public void setEventId(long eventId) {
        this.eventId = eventId;
    }

    public IEventDAO.NOTIFICATION_STATE getState() {
        return state;
    }

    public void setState(IEventDAO.NOTIFICATION_STATE state) {
        this.state = state;
    }

    public Date getEventDatePlanned() {
        return eventDatePlanned;
    }

    public void setEventDatePlanned(Date eventDatePlanned) {
        this.eventDatePlanned = eventDatePlanned;
    }

    public int getEventTimePlanned() {
        return eventTimePlanned;
    }

    public void setEventTimePlanned(int eventTimePlanned) {
        this.eventTimePlanned = eventTimePlanned;
    }

    @Override
    public String toString() {
        return "Notification{" +
                "notificationId=" + notificationId +
                ", eventId=" + eventId +
                ", state=" + state +
                ", eventDatePlanned=" + eventDatePlanned +
                ", eventTimePlanned=" + eventTimePlanned +
                '}';
    }
}
