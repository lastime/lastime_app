package com.abfactory.lastime.model;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.util.Log;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.List;

public class Contact implements Serializable, Export {

    private long contactId = -1;
    private String contactName;
    private String phoneNumber;
    private String phoneId;
    private String backendId;

    public Contact(){};

    public Contact(long contactId, String contactName, String phoneNumber, String phoneId, String backendId){
        this.contactId = contactId;
        this.contactName = contactName;
        this.phoneNumber = phoneNumber;
        this.phoneId = phoneId;
        this.backendId = backendId;
    }

    public Contact(String id, Context ctx){
        this.phoneId =id;

        // Build the Uri to query to table
        Uri contactUri = Uri.withAppendedPath(
                ContactsContract.CommonDataKinds.Phone.CONTENT_URI, id);

        if(contactUri!=null){
            this.contactName = retrieveContactName(contactUri, ctx);
            this.phoneNumber = retrieveContactNumber(contactUri, ctx);
        }

    }

    public Contact(Uri contactUri, Context ctx){
        this.phoneId = retrieveContactId(contactUri, ctx);
        this.contactName = retrieveContactName(contactUri, ctx);
        this.phoneNumber = retrieveContactNumber(contactUri, ctx);
    }

    private String retrieveContactId(Uri uriContact, Context ctx){
        String contactId = null;
        Cursor cursor = ctx.getContentResolver().query(uriContact, null, null, null,   null);
        if (cursor.moveToFirst()) {
            contactId = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
        }
        return contactId;
    }

    private String retrieveContactName(Uri uriContact, Context ctx) {

        String contactName = null;

        // querying contact data store
        Cursor cursor = ctx.getContentResolver().query(uriContact, null, null, null, null);

        if (cursor.moveToFirst()) {

            // DISPLAY_NAME = The display name for the contact.
            // HAS_PHONE_NUMBER =   An indicator of whether this contact has at least one phone number.

            contactName = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        }

        cursor.close();

        return contactName;

    }

    private String retrieveContactNumber(Uri contactUri, Context ctx) {

        String contactNumber = null;

        // querying contact data store
        Cursor cursor = ctx.getContentResolver().query(contactUri, null, null, null, null);

        if (cursor.moveToFirst()) {
            contactNumber = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
        }

        cursor.close();

        return contactNumber;
    }

    public boolean isContactSynchronizedWithPhone(Context ctx){
        if(this.phoneId != null){
            Contact contactFromPhoneBasedOnId = new Contact(this.phoneId, ctx);
            if (contactFromPhoneBasedOnId.getContactName() == null
                    || !contactFromPhoneBasedOnId.getContactName().equals(this.contactName)) {
                return false;
            }
            if (!contactFromPhoneBasedOnId.getPhoneNumber().equals(this.phoneNumber)) {
                return false;
            }
        }
        return true;
    }

    public long getContactId() {
        return contactId;
    }

    public void setContactId(long id) {
        this.contactId = id;
    }

    public String getPhoneId() {
        return phoneId;
    }

    public void setPhoneId(String id) {
        this.phoneId = id;
    }

    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getBackendId() {
        return backendId;
    }

    public void setBackendId(String backendId) {
        this.backendId = backendId;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "contactiId='" + contactId + '\'' +
                ", contactName='" + contactName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", phoneId='" + phoneId + '\'' +
                ", backendId='" + backendId + '\'' +
                '}';
    }

    /**
     * Method used to export this object in a string format
     * Linked to export/import feature
     * @return output object as a String
     */
    @Override
    public String exportToString() {
        StringBuilder output = new StringBuilder();
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (!Modifier.isTransient(field.getModifiers()) && field.get(this) != null) {
                    output.append(field.getName());
                    output.append("::");
                    output.append(field.get(this));
                    output.append(";;");
                }
            } catch (IllegalAccessException e) {
                Log.e(Contact.class.getName(), "Error occurred exporting field: " + field.getName());
            }
        }
        return output.toString();
    }

    public boolean similarTo(Contact that){
        if (contactName != null ? !contactName.equals(that.contactName) : that.contactName != null) return false;
        if (phoneNumber != null ? !phoneNumber.equals(that.phoneNumber) : that.phoneNumber != null) return false;
        return (phoneId != null ? !phoneId.equals(that.phoneId) : that.phoneId != null);
    }

}
