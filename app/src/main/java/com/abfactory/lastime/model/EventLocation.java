package com.abfactory.lastime.model;

import android.location.Location;
import android.os.Parcel;
import android.os.Parcelable;
import android.util.Log;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;

/**
 * Class mapping gps location data
 * Used in Event table by secondary key
 */

public class EventLocation implements Serializable, Cloneable, Parcelable, Export {
    public transient static final Parcelable.Creator CREATOR = new Parcelable.Creator<EventLocation>() {
        public EventLocation createFromParcel(Parcel in) {
            return new EventLocation(in);
        }

        public EventLocation[] newArray(int size) {
            return new EventLocation[size];
        }
    };
    private long locationId = -1;
    private double latitude;
    private double longitude;
    private String name;
    private String googlePlacesId;
    /**
     * ID on backend side
     **/
    private String backendId;

    public EventLocation() {
    }

    public EventLocation(long locationId, String name, double latitude, double longitude, String googlePlacesId, String backendId) {
        this.locationId = locationId;
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.googlePlacesId = googlePlacesId;
        this.backendId = backendId;
    }

    public EventLocation(String name, double latitude, double longitude, String googlePlacesId, String backendId) {
        this.latitude = latitude;
        this.longitude = longitude;
        this.name = name;
        this.googlePlacesId = googlePlacesId;
        this.backendId = backendId;
    }

    private EventLocation(Parcel in) {
        name = in.readString();
        latitude = in.readDouble();
        longitude = in.readDouble();
        googlePlacesId = in.readString();
    }

    public long getLocationId() {
        return locationId;
    }

    public void setLocationId(int locationId) {
        this.locationId = locationId;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public String getName() {
        return name;
    }

    public void setName(String place) {
        this.name = place;
    }

    public String getGooglePlacesId() {
        return googlePlacesId;
    }

    public void setGooglePlacesId(String googlePlacesId) {
        this.googlePlacesId = googlePlacesId;
    }

    public boolean isGooglePlace() {
        return googlePlacesId != null;
    }

    public String getBackendId() {
        return backendId;
    }
    /* Parcelable implementation */

    public void setBackendId(String backendId) {
        this.backendId = backendId;
    }

    @Override
    public String toString() {
        return "EventLocation{" +
                "locationId=" + locationId +
                ", latitude=" + latitude +
                ", longitude=" + longitude +
                ", name='" + name + '\'' +
                ", googlePlacesId='" + googlePlacesId + '\'' +
                ", backendId='" + backendId + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeDouble(latitude);
        dest.writeDouble(longitude);
        dest.writeString(googlePlacesId);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        EventLocation that = (EventLocation) o;
        if (locationId != that.locationId) return false;

        return this.similarTo(that);
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = (int) locationId;
        temp = Double.doubleToLongBits(latitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(longitude);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (googlePlacesId != null ? googlePlacesId.hashCode() : 0);
        return result;
    }

    /**
     * Method used to export this object in a string format
     * Linked to export/import feature
     *
     * @return output object as a String
     */
    @Override
    public String exportToString() {
        StringBuilder output = new StringBuilder();
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (!Modifier.isTransient(field.getModifiers()) && field.get(this) != null) {
                    output.append(field.getName());
                    output.append("::");
                    output.append(field.get(this));
                    output.append(";;");
                }
            } catch (IllegalAccessException e) {
                Log.e(EventLocation.class.getName(), "Error occurred exporting field: " + field.getName());
            }
        }
        return output.toString();
    }

    public boolean similarTo(EventLocation eventLocation) {
        if(eventLocation != null) {
            if (Double.compare(eventLocation.latitude, latitude) != 0) return false;
            if (Double.compare(eventLocation.longitude, longitude) != 0) return false;
            if (name != null ? !name.equals(eventLocation.name) : eventLocation.name != null)
                return false;
            return !(googlePlacesId != null ? !googlePlacesId.equals(eventLocation.googlePlacesId) : eventLocation.googlePlacesId != null);
        }
        return false;
    }

    public static EventLocation parseCurrentLocation(Location location) {
        return new EventLocation("Current position", location.getLatitude(), location.getLongitude(), null, null);
    }
}
