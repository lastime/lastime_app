package com.abfactory.lastime.model;

import android.util.Log;

import com.abfactory.lastime.utils.LasTimeUtils;

import java.io.Serializable;
import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.text.SimpleDateFormat;
import java.util.Comparator;
import java.util.Date;
import java.util.Locale;

/**
 * Class mapping event data
 * Primary table
 */
public class Event implements Serializable, Comparable<Event>, Export {

    public transient static final SimpleDateFormat CALENDAR_DISPLAY = new SimpleDateFormat("EEEE dd MMMM yyyy", Locale.getDefault());
    public transient static final SimpleDateFormat CALENDAR_DISPLAY_NoYEAR = new SimpleDateFormat("EEEE dd MMMM", Locale.getDefault());
    public transient static final SimpleDateFormat CALENDAR_DISPLAY_NOTIF = new SimpleDateFormat("dd MMMM yyyy", Locale.getDefault());
    public transient static final SimpleDateFormat TIME_DISPLAY = new SimpleDateFormat("HH:mm", Locale.getDefault());
    public transient static final SimpleDateFormat DATE_FOR_EXPORT = new SimpleDateFormat("yyyy-MM-dd-HH-mm-ss", Locale.ENGLISH);
    /**
     * Comparator for ordering by title
     **/
    public transient static Comparator<Event> TitleEventComparator = new Comparator<Event>() {
        @Override
        public int compare(Event event1, Event event2) {
            return event1.getTitle().compareTo(event2.getTitle());
        }
    };
    /**
     * Comparator for ordering by event date
     **/
    public transient static Comparator<Event> DateEventComparator = new Comparator<Event>() {
        @Override
        public int compare(Event event1, Event event2) {
            return event1.getDate().compareTo(event2.getDate());
        }
    };
    /*
        Comparator for ordering from more to less frequent, default on date for equivalent frequency
     */
    public transient static Comparator<Event> FrequencyEventComparator = new Comparator<Event>() {
        @Override
        public int compare(Event event1, Event event2) {
            if (event2.getWeight() - event1.getWeight() != 0) {
                return event2.getWeight() - event1.getWeight();
            } else {
                return event1.getDate().compareTo(event2.getDate());
            }
        }

    };
    // (package util)Date of event
    private Date date;
    // Internal id used by SQLLite
    private transient long id;
    // Title of event (mandatory)
    private String title;
    // Time of event - integer representing HH:MM
    private Integer timeOfEvent;
    // Primary key of Location table
    private long location;
    // Mark id - feeling of event
    private int mark;
    // Path of image took by camera
    private String imagePath;
    // Path of thumbnail - taken by camera or used from gallery
    private String thumbnailPath;
    // Number of times an event has been refreshed
    private int weight;
    // Simple text note
    private String note;
    // Integer of color selected for event
    private int color;
    // Tag defining recurrence - like N_DAYS
    private String recurrence;
    // Tag defining recurrence
    private String reminder;
    // Id of contact
    private long contact;

    /**
     * Used to define the state of your event like 'To be deleted', etc...
     */
    private IEventDAO.EVENT_STATE state;
    /**
     * Date of event creation - not linked with date of event itself
     */
    private Date creation;
    // Reference is shared between events to establish history
    private String reference;
    /**
     * ID on backend side
     **/
    private String backendId;

    /**
     * End date of event
     **/
    private Date endDate;

    // Expenses of event, like price of cinema ticket
    private float expenses;

    // Highlight on main view
    private String highlight;
    private String fieldList;
    private int weather;

    private Event(EventBuilder builder) {
        this.title = builder.title;
        this.id = builder.id;
        this.date = builder.date;
        this.timeOfEvent = builder.timeOfEvent;
        this.location = builder.location;
        this.mark = builder.mark;
        this.imagePath = builder.imagePath;
        this.thumbnailPath = builder.thumbnailPath;
        this.weight = builder.weight;
        this.note = builder.note;
        this.color = builder.color;
        this.recurrence = builder.recurrence;
        this.reminder = builder.reminder;
        this.contact = builder.contact;
        this.state = builder.state;
        this.creation = builder.creation;
        this.reference = builder.reference;
        this.backendId = builder.backendId;
        this.endDate = builder.endDate;
        this.expenses = builder.expenses;
        this.highlight = builder.highlight;
        this.fieldList = builder.fieldList;
        this.weather = builder.weather;
    }

    /**
     * Factory method used only by import feature
     */
    public static Event createEmptyEvent() {
        return new EventBuilder("TO BE IMPORTED").build();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getMark() {
        return mark;
    }

    public void setMark(int mark) {
        this.mark = mark;
    }

    public String getThumbnailPath() {
        return thumbnailPath;
    }

    public void setThumbnailPath(String thumbnailPath) {
        this.thumbnailPath = thumbnailPath;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }

    public String getReminder() {
        return reminder;
    }

    public long getLocation() {
        return location;
    }

    public void setLocation(long location) {
        this.location = location;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getEndDate() {
        return endDate;
    }

    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    public Integer getTimeOfEvent() {
        return timeOfEvent;
    }

    public void setTimeOfEvent(Integer timeOfEvent) {
        this.timeOfEvent = timeOfEvent;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getHighlight() {
        return highlight;
    }

    public void setHighlight(String highlight) {
        this.highlight = highlight;
    }

    public String getDateAsString() {
        return CALENDAR_DISPLAY.format(this.date);
    }

    public String getEndDateAsString() {
        return this.getEndDate() != null ? CALENDAR_DISPLAY.format(this.endDate) : null;
    }

    public String getDateAsStringNoYear() {
        return CALENDAR_DISPLAY_NoYEAR.format(this.date);
    }

    public String getDateAsStringNoYearAndTime() {
        return CALENDAR_DISPLAY_NoYEAR.format(this.date) + " - " + LasTimeUtils.displayTime(getTimeOfEvent());
    }

    public String getDateAsStringNoYearAndEndDate() {
        return CALENDAR_DISPLAY_NoYEAR.format(this.date) + " - " +
                CALENDAR_DISPLAY_NoYEAR.format(this.endDate);
    }

    public String getDateAsStringAndEndDate() {
        return CALENDAR_DISPLAY.format(this.date) + " - " +
                CALENDAR_DISPLAY.format(this.endDate);
    }

    public String getDateAsStringAndTime() {
        return CALENDAR_DISPLAY.format(this.date) + " - " + LasTimeUtils.displayTime(getTimeOfEvent());
    }

    public int getColor() {
        return color;
    }

    public void setColor(int color) {
        this.color = color;
    }

    public IEventDAO.EVENT_STATE getState() {
        return state;
    }

    public void setState(IEventDAO.EVENT_STATE state) {
        this.state = state;
    }

    public Date getCreation() {
        return creation;
    }

    public void setCreation(Date creation) {
        this.creation = creation;
    }

    public String getReference() {
        return reference;
    }

    public void setReference(String reference) {
        this.reference = reference;
    }

    public String getCreationAsString() {
        return CALENDAR_DISPLAY_NoYEAR.format(this.creation);
    }

    public String getCreationTimeAsString() {
        return TIME_DISPLAY.format(this.creation);
    }

    public String getRecurrence() {
        return recurrence;
    }

    public void setRecurrence(String recurrence) {
        this.recurrence = recurrence;
    }

    public long getContact() {
        return contact;
    }

    public void setContact(long contact) {
        this.contact = contact;
    }

    public String getBackendId() {
        return backendId;
    }

    public void setBackendId(String backendId) {
        this.backendId = backendId;
    }

    public float getExpenses() {
        return expenses;
    }

    public void setExpenses(float expenses) {
        this.expenses = expenses;
    }

    public String getFieldList() {
        return fieldList;
    }

    public void setFieldList(String fieldList) {
        this.fieldList = fieldList;
    }

    public int getWeather() {
        return weather;
    }

    public void setWeather(int weather) {
        this.weather = weather;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Event event = (Event) o;

        if (id != event.id) return false;
        if (location != event.location) return false;
        if (mark != event.mark) return false;
        if (weight != event.weight) return false;
        if (color != event.color) return false;
        if (contact != event.contact) return false;
        if (Float.compare(event.expenses, expenses) != 0) return false;
        if (weather != event.weather) return false;
        if (date != null ? !date.equals(event.date) : event.date != null) return false;
        if (title != null ? !title.equals(event.title) : event.title != null) return false;
        if (timeOfEvent != null ? !timeOfEvent.equals(event.timeOfEvent) : event.timeOfEvent != null)
            return false;
        if (imagePath != null ? !imagePath.equals(event.imagePath) : event.imagePath != null)
            return false;
        if (thumbnailPath != null ? !thumbnailPath.equals(event.thumbnailPath) : event.thumbnailPath != null)
            return false;
        if (note != null ? !note.equals(event.note) : event.note != null) return false;
        if (recurrence != null ? !recurrence.equals(event.recurrence) : event.recurrence != null)
            return false;
        if (reminder != null ? !reminder.equals(event.reminder) : event.reminder != null)
            return false;
        if (state != event.state) return false;
        if (creation != null ? !creation.equals(event.creation) : event.creation != null)
            return false;
        if (reference != null ? !reference.equals(event.reference) : event.reference != null)
            return false;
        if (backendId != null ? !backendId.equals(event.backendId) : event.backendId != null)
            return false;
        if (endDate != null ? !endDate.equals(event.endDate) : event.endDate != null) return false;
        if (highlight != null ? !highlight.equals(event.highlight) : event.highlight != null)
            return false;
        return fieldList != null ? fieldList.equals(event.fieldList) : event.fieldList == null;
    }

    @Override
    public int hashCode() {
        int result = date != null ? date.hashCode() : 0;
        result = 31 * result + (int) (id ^ (id >>> 32));
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (timeOfEvent != null ? timeOfEvent.hashCode() : 0);
        result = 31 * result + (int) (location ^ (location >>> 32));
        result = 31 * result + mark;
        result = 31 * result + (imagePath != null ? imagePath.hashCode() : 0);
        result = 31 * result + (thumbnailPath != null ? thumbnailPath.hashCode() : 0);
        result = 31 * result + weight;
        result = 31 * result + (note != null ? note.hashCode() : 0);
        result = 31 * result + color;
        result = 31 * result + (recurrence != null ? recurrence.hashCode() : 0);
        result = 31 * result + (reminder != null ? reminder.hashCode() : 0);
        result = 31 * result + (int) (contact ^ (contact >>> 32));
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (creation != null ? creation.hashCode() : 0);
        result = 31 * result + (reference != null ? reference.hashCode() : 0);
        result = 31 * result + (backendId != null ? backendId.hashCode() : 0);
        result = 31 * result + (endDate != null ? endDate.hashCode() : 0);
        result = 31 * result + (expenses != +0.0f ? Float.floatToIntBits(expenses) : 0);
        result = 31 * result + (highlight != null ? highlight.hashCode() : 0);
        result = 31 * result + (fieldList != null ? fieldList.hashCode() : 0);
        result = 31 * result + weather;
        return result;
    }

    @Override
    public String toString() {
        return "Event{" +
                "id=" + id +
                ", title='" + title + '\'' +
                ", date=" + CALENDAR_DISPLAY.format(date) +
                ", timeOfEvent=" + (timeOfEvent != null && timeOfEvent > 0 ? LasTimeUtils.displayTime(timeOfEvent) : null) +
                ", location=" + location +
                ", mark=" + mark +
                ", imagePath='" + imagePath + '\'' +
                ", thumbnailPath='" + thumbnailPath + '\'' +
                ", weight=" + weight +
                ", note='" + note + '\'' +
                ", color=" + color +
                ", recurrence='" + recurrence + '\'' +
                ", reminder='" + reminder + '\'' +
                ", contact='" + contact + '\'' +
                ", state=" + state +
                ", creation=" + creation +
                ", reference='" + reference + '\'' +
                ", backendId='" + backendId + '\'' +
                ", endDate='" + endDate + '\'' +
                ", expenses='" + expenses + '\'' +
                ", highlight='" + highlight + '\'' +
                ", fieldList='" + fieldList + '\'' +
                ", weather='" + weather + '\'' +
                '}';
    }

    /**
     * Method used to export this object in a string format
     * Linked to export/import feature
     *
     * @return output object as a String
     */
    @Override
    public String exportToString() {
        StringBuilder output = new StringBuilder();
        for (Field field : this.getClass().getDeclaredFields()) {
            try {
                if (!Modifier.isTransient(field.getModifiers()) && field.get(this) != null) {
                    output.append(field.getName());
                    output.append("::");
                    if ("note".equals(field.getName())) {
                        output.append(((String) field.get(this)).replaceAll("\n", "*"));
                    } else if (field.getType().getName().toLowerCase().contains("date")) {
                        output.append(DATE_FOR_EXPORT.format(field.get(this)));
                    } else {
                        output.append(field.get(this));
                    }
                    output.append(";;");
                }
            } catch (IllegalAccessException e) {
                Log.e(Event.class.getName(), "Error occurred exporting field: " + field.getName(), e);
            }
        }
        return output.toString();
    }

    /*
    Default comparator order checking first date (the closest gets to the top of the list) and
    for equals date, frequency order (from most to less frequent) rules
 */
    @Override
    public int compareTo(Event compareEvent) {
        if (this.getDate().compareTo(compareEvent.getDate()) != 0) {
            return this.getDate().compareTo(compareEvent.getDate());
        } else {
            return compareEvent.getWeight() - this.getWeight();
        }

    }

    public boolean isActive() {
        return this.getState().equals(IEventDAO.EVENT_STATE.ACTIVE);
    }

    /**
     * Method used as equals method but removing the ID (not needed for import)
     *
     * @param event event to check
     * @return true if similar
     */
    public boolean similarTo(Event event) {
        if (location != event.location) return false;
        if (mark != event.mark) return false;
        if (weight != event.weight) return false;
        if (color != event.color) return false;
        if (!date.equals(event.date)) return false;
        if (timeOfEvent != null ? !timeOfEvent.equals(event.timeOfEvent) : event.timeOfEvent != null)
            return false;
        if (!title.equals(event.title)) return false;
        if (imagePath != null ? !imagePath.equals(event.imagePath) : event.imagePath != null)
            return false;
        if (thumbnailPath != null ? !thumbnailPath.equals(event.thumbnailPath) : event.thumbnailPath != null)
            return false;
        if (note != null && !note.isEmpty()
                && event.note != null && !event.note.isEmpty()
                && !note.equals(event.note))
            return false;
        if (highlight != null && !highlight.isEmpty()
                && event.highlight != null && !event.highlight.isEmpty()
                && !highlight.equals(event.highlight))
            return false;
        if (!state.equals(event.state)) return false;
        if (creation != null ? !creation.equals(event.creation) : event.creation != null)
            return false;
        if (reference != null && !reference.isEmpty()
                && event.reference != null && !event.reference.isEmpty()
                && !reference.equals(event.reference))
            return false;
        if (recurrence != null && !recurrence.isEmpty()
                && event.recurrence != null && !event.recurrence.isEmpty()
                && !recurrence.equals(event.recurrence))
            return false;
        if (reminder != null ? !reminder.equals(event.reminder) : event.reminder != null)
            return false;
        if (contact != event.contact) return false;
        if (endDate != null && !endDate.equals(event.getEndDate())
                || endDate == null && event.getEndDate() != null)
            return false;
        if (expenses != event.expenses) return false;
        if (weather != event.weather) return false;
        return true;

    }

    /**
     * Builder class of event object
     */
    public static class EventBuilder {
        final String title;
        long id;
        Date date;
        Integer timeOfEvent;
        long location;
        int mark;
        String imagePath;
        String thumbnailPath;
        int weight;
        String note;
        String highlight;
        int color;
        String recurrence;
        String reminder;
        long contact;
        IEventDAO.EVENT_STATE state;
        Date creation;
        String reference;
        String backendId;
        Date endDate;
        float expenses;
        String fieldList;
        int weather;

        public EventBuilder(String title) {
            this.title = title;
            id = -1;
            timeOfEvent = -1;
            location = -1;
            mark = -1;
            contact = -1;
            color = -1;
            expenses = 0;
            weather = -1;
        }

        public EventBuilder addInternalId(long id){
            this.id = id;
            return this;
        }

        public EventBuilder addDate(Date date) {
            this.date = date;
            return this;
        }

        public EventBuilder addRecurrence(String recurence) {
            this.recurrence = recurence;
            return this;
        }

        public EventBuilder addReference(String reference){
            this.reference = reference;
            return this;
        }

        public EventBuilder addTime(Integer timeOfEvent){
            this.timeOfEvent = timeOfEvent;
            return this;
        }

        public EventBuilder addLocationId(long location){
            this.location = location;
            return this;
        }

        public EventBuilder addMark(int mark){
            this.mark = mark;
            return this;
        }

        public EventBuilder addImagePath(String path){
            this.imagePath = path;
            return this;
        }

        public EventBuilder addThumbnailPath(String path){
            this.thumbnailPath = path;
            return this;
        }

        public EventBuilder addWeight(int weight){
            this.weight = weight;
            return this;
        }

        public EventBuilder addReminder(String reminder){
            this.reminder = reminder;
            return this;
        }

        public EventBuilder addNote(String note){
            this.note = note;
            return this;
        }

        public EventBuilder addHighlight(String highlight){
            this.highlight = highlight;
            return this;
        }

        public EventBuilder addColor(int color){
            this.color = color;
            return this;
        }

        public EventBuilder addState(IEventDAO.EVENT_STATE state){
            this.state = state;
            return this;
        }

        public EventBuilder addCreationDate(Date creation){
            this.creation = creation;
            return this;
        }

        public EventBuilder addContactId(long id){
            this.contact = id;
            return this;
        }

        public EventBuilder addBackendId(String id){
            this.backendId = id;
            return this;
        }

        public EventBuilder addEventEndDate(Date endDate){
            this.endDate = endDate;
            return this;
        }

        public EventBuilder addExpenses(float expenses){
            this.expenses = expenses;
            return this;
        }

        public EventBuilder addFieldList(String fieldList){
            this.fieldList = fieldList;
            return this;
        }

        public EventBuilder addWeather(int weather){
            this.weather = weather;
            return this;
        }

        public Event build() {
            return new Event(this);
        }

    }
}
