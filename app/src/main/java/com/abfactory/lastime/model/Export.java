package com.abfactory.lastime.model;

/**
 * Export interface to be implemented by object model that could be exported to file
 */
public interface Export {
    String exportToString();
}
