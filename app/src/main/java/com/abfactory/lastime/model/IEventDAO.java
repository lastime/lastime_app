package com.abfactory.lastime.model;

import android.content.Context;

import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Set;

/**
 * DAO methods for Event and location tables
 */
public interface IEventDAO {

    enum INTERVAL{
        MIN, MAX;
    }

    List<Event> retrieveAllEvents();

    List<Event> getAllActiveEvents();

    List<EventLocation> getAllEventLocations();

    int getEventsCount();

    /*
     * Returns all events done or to be done in a given location
     */
    List<Event> getAllEventsForLocation(EventLocation eventLocation);

    EventLocation getLocationByEvent(Event event);

    EventLocation getLocationById(long id);

    void deleteLocationById(long id);

    Long insertLocation(EventLocation eventLocation);

    long addEvent(Event event, EventLocation location, Contact contact);

    long addEvent(Event event);

    /**
     * Only for import feature - DO NOT USE IT!
     **/
    long insertEvent(Event event);

    void deleteEvent(long id);

    Long refreshEvent(Event event);

    void refreshFieldEvent(Event event);

    /**
     * Modify event using history behavior
     */
    Long modifyEvent(Event event, EventLocation location, Contact contact, boolean forceHistory);

    /**
     * Modify event not using history implementation
     */
    void updateEvent(Event event, EventLocation location, Contact contact);

    Event findEventByTitle(String title);

    Event findActiveEventByTitle(String title);

    Long findEventIdByTitle(String title);

    List<Event> getTopEvents(String max);

    /**
     * Get a set of all images (images+thumbnails)
     **/
    Set<String> retrieveAllImagePaths();

    void markEventToBeDeleted(long id);

    void markEventToActive(long id);

    List<Event> findEventsToBeDeleted();

    void refreshEvent(Event event, String date, String endDate);

    /**
     * Used to clean all events with state = TO_BE_DELETED
     */
    void cleanAllEventToBeDeleted();

    Event retrieveEventById(long id);

    void markEventToOld(long id);

    List<Event> findHistoryOfEvent(Event event);

    Event findActiveEvent(Event event);

    void deleteEventAndHistory(String eventId);

    Event deleteEvent(Event event);

    Event getActiveEvent(Event event);

    Integer changeEventColor(Event event);

    Long findActiveEventIdByTitle(String title);

    // For dev only
    void cleanActiveEventDuplicated();

    void cleanEventsDefinedInFuture();

    /**
     * DO NOT USE THIS !
     * ONLY FOR DEV PURPOSE
     * COULD NOT BE UNDONE!
     */
    void eraseAllEvents();

    long createNotification(Notification notification);

    void deleteNotification(long id);

    List<Notification> getAllNotifications();

    //**************************** NOTIFICATION ********************************

    List<Notification> getAllActiveNotifications();

    int getActiveNotificationsCount();

    void setNotificationToOld(long id);

    void setNotificationToActive(long id);

    void setNotificationEventPlannedDate(long id, Date date);

    void setNotificationEventPlannedTime(long id, int time);

    long createEventTemplate(EventTemplate eventTemplate);

    long createEventTemplateWithoutImage(EventTemplate eventTemplate);

    void updateEventTemplateImagePath(long id, String imagePath);

    //**************************** CONTACTS ********************************

    List<Contact> getAllContacts();

    Contact getContactById(long id);

    Long insertContact(Contact contact);

    void updateContact(Contact contact);

    void deleteContact(Contact contact);

    void updateEventContact(long id, long contactId);


        //**************************** EVENT TEMPLATES ********************************

    List<EventTemplate> getAllEventTemplates();

    void deleteEventTemplate(long id);

    EventTemplate retrieveEventTemplateById(long id);

    //Different state event can have
    enum EVENT_STATE {
        ACTIVE, TO_BE_DELETED, OLD
    }

    enum NOTIFICATION_STATE {ACTIVE, OLD}

    enum EVENT_TEMPLATE_STATE {ACTIVE, OLD}

    //*************************** EVENT STATISTICS ******************************

    List<Event> getAllEventHistory(Event event);


    int getAllActiveEventNumber();

    Map<String, Integer> computeIntervalsTimeInDays(Event event);
    Map<String, String> computeIntervalsTimeInDays (List<Event> eventList);

    Event getOldestEvent();

    List<Event> getAllEventsByStateAndCommonId(EVENT_STATE eventState, String reference);

    int[] getEventsByMonths(Event event, int year);

    int[] getEventsByDays(Event event, int month, int year);

    //*************************** MIGRATION UTILS ******************************

    // To be used when migrating to DB level 15
    void migrateContactsToNewFormat(Context ctx);

}
