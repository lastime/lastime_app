package com.abfactory.lastime.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.tabs.SlidingTabLayout;
import com.abfactory.lastime.component.tabs.ViewPagerAdapter;
import com.abfactory.lastime.utils.LasTimeUtils;

public class LocationsHandlingActivity extends BaseActivity {

    // Declaring Your View and Variables

    Toolbar toolbar;
    ViewPager pager;
    ViewPagerAdapter adapter;
    SlidingTabLayout tabs;
    CharSequence[] titles;
    int numbOftabs = 2;
    private boolean isSelectionActivated;

    public LocationsHandlingActivity() {
        titles = new CharSequence[]{"List", "Map"};
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_locations_handling);

        // Creating The Toolbar and setting it as the Toolbar for the activity
        toolbar = (Toolbar) findViewById(R.id.tool_bar);
        toolbar.setNavigationIcon(R.drawable.ic_action_cancel);
        setSupportActionBar(toolbar);

        // Creating The ViewPagerAdapter and Passing Fragment Manager, titles fot the Tabs and Number Of Tabs.
        adapter = new ViewPagerAdapter(getSupportFragmentManager(), titles, numbOftabs);

        // Assigning ViewPager View and setting the adapter
        pager = (ViewPager) findViewById(R.id.pager);
        pager.setAdapter(adapter);

        // Assiging the Sliding Tab Layout View
        tabs = (SlidingTabLayout) findViewById(R.id.tabs);
        tabs.setDistributeEvenly(true); // To make the Tabs Fixed set this true, This makes the tabs Space Evenly in Available width

        // Setting Custom Color for the Scroll bar indicator of the Tab View
        tabs.setCustomTabColorizer(new SlidingTabLayout.TabColorizer() {
            @Override
            public int getIndicatorColor(int position) {
                return LasTimeUtils.getColor(getApplicationContext(), R.color.ColorSecondary);
            }
        });

        // Setting the ViewPager For the SlidingTabsLayout
        tabs.setViewPager(pager);

        // If selection mode should be activated
        Intent intent = getIntent();
        if (intent != null) {
            isSelectionActivated = intent.getBooleanExtra("activateSelection", false);
        }
    }

    public boolean hasSelectionActivated(){
        return isSelectionActivated;
    }
}
