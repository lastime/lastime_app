package com.abfactory.lastime.activity.edit;

import android.os.AsyncTask;
import android.util.Log;

import com.abfactory.lastime.utils.LasTimeUtils;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;

public class DownloadImageTask extends AsyncTask<String, Void, Boolean> {

    private static final String TAG = DownloadImageTask.class.getName();
    private EditEventActivity editEventActivity;

    public DownloadImageTask(EditEventActivity editEventActivity){
        this.editEventActivity = editEventActivity;
    }

    protected Boolean doInBackground(String... values) {

        FileOutputStream fos = null;
        HttpURLConnection httpURLConnection = null;
        try {
            URL url = new URL(values[0]);
            File file = new File(LasTimeUtils.getImageStorageDir(editEventActivity), values[1]);

            long startTime = System.currentTimeMillis();
            Log.d(TAG, "download starting");
            Log.d(TAG, "download url:" + url);
            Log.d(TAG, "downloaded file name:" + values[1]);

            // Open a connection to that URL. */
            httpURLConnection = (HttpURLConnection) url.openConnection();
            int responseCode = httpURLConnection.getResponseCode();

            // If response is correct
            if (responseCode == HttpURLConnection.HTTP_OK) {

                // Define InputStreams to read from the URLConnection.
                InputStream is = new BufferedInputStream(httpURLConnection.getInputStream());
                fos = new FileOutputStream(file);
                // Write bytes to the Buffer until there is nothing more to read(-1).
                int bytesRead = -1;
                byte[] buffer = new byte[4096];
                while ((bytesRead = is.read(buffer)) != -1) {
                    fos.write(buffer, 0, bytesRead);
                }
                fos.flush();
                is.close();
                fos.close();
                Log.d(TAG, "download ready in" + ((System.currentTimeMillis() - startTime) / 1000) + " sec");

            } else {
                Log.e(TAG, "Error opening connection: ");
                return false;
            }
        }catch(IOException e){
            Log.e(TAG, "Error saving file: ", e);
            return false;
        }finally{
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error", e);
                }
            }
        }
        return true;
    }

    protected void onPostExecute(Boolean feed) {

    }
}
