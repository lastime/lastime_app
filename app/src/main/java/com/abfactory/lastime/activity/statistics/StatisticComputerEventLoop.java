package com.abfactory.lastime.activity.statistics;

import com.abfactory.lastime.activity.StatisticsActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.IEventDAO;

public abstract class StatisticComputerEventLoop extends StatisticComputer {

    public StatisticComputerEventLoop(StatisticsActivity activity, IEventDAO eventDAO) {
        super(activity, eventDAO);
    }

    /**
     * This method is called on every event
     * @param event
     */
    public abstract void executeOnEvent(Event event);

    /**
     * This method must be called after the iteration on events
     */
    @Override
    public abstract void execute();
}
