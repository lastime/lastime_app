package com.abfactory.lastime.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.abfactory.lastime.R;

public class BlankActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_blank);
    }
}
