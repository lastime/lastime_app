package com.abfactory.lastime.activity.statistics;

import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.StatisticsActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.IEventDAO;

import java.util.List;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.OLD;

public class LastEventCreated extends StatisticComputerEventLoop {

    private Event lastEventCreated = null;

    public LastEventCreated(StatisticsActivity activity, IEventDAO eventDAO) {
        super(activity, eventDAO);
    }

    @Override
    public void executeOnEvent(Event event) {
        // Get current event history,do not consider active events
        List<Event> eventHistory = eventDAO.getAllEventsByStateAndCommonId(OLD, event.getReference());
        Event firstOccurenceForCurrentEvent = event;
        // Go through full history
        for (Event h : eventHistory) {
            // If ongoing element in history of ongoing event is the oldest ever checked store it
            if (h.getCreation() != null && h.getCreation().before(firstOccurenceForCurrentEvent.getCreation())) {
                firstOccurenceForCurrentEvent = h;
            }
        }
        //Keep only if newest of oldest
        if (lastEventCreated == null || firstOccurenceForCurrentEvent.getCreation().after(lastEventCreated.getCreation())) {
            lastEventCreated = firstOccurenceForCurrentEvent;
        }
    }

    @Override
    public void execute() {
        if (lastEventCreated != null) {
            ((TextView) findViewById(R.id.stats_newest_event)).setText(lastEventCreated.getTitle());
        }
    }

}
