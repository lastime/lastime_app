package com.abfactory.lastime.activity.display;

import android.app.FragmentManager;
import android.graphics.Point;
import android.graphics.drawable.ShapeDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.Snackbar;
import android.view.Display;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.component.DeselectableRadioButton;
import com.abfactory.lastime.component.GraphBuilder;
import com.abfactory.lastime.component.dialogs.BasicDialogFragment;
import com.abfactory.lastime.component.dialogs.DatePickerFragment;
import com.abfactory.lastime.component.dialogs.DesynchronizedContactDialogFragment;
import com.abfactory.lastime.component.dialogs.GraphSelectionFragment;
import com.abfactory.lastime.component.twowayview.HorizontalListAdapter;
import com.abfactory.lastime.component.twowayview.HorizontalListFragment;
import com.abfactory.lastime.model.Contact;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.jjoe64.graphview.GraphView;
import com.squareup.picasso.Picasso;

import org.joda.time.DateTime;

import java.io.File;
import java.util.Collections;
import java.util.Currency;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.ACTIVE;
import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.OLD;

/**
 * Class handles update of view in display event activity
 */
public class UpdateDisplayEvent {

    private DisplayEventActivity activity;

    public UpdateDisplayEvent(DisplayEventActivity activity) {
        this.activity = activity;
    }

    public void refresh(final Event event) {
        if (event != null) {
            // All fields to be displayed or not
            TextView event_menu_title = ((TextView) activity.findViewById(R.id.display_menu_title));
            TextView event_title = ((TextView) activity.findViewById(R.id.display_title));
            TextView event_subtitle = ((TextView) activity.findViewById(R.id.display_subtitle));
            TextView event_date_text = ((TextView) activity.findViewById(R.id.event_date_text));
            TextView event_end_date_text = ((TextView) activity.findViewById(R.id.event_end_date_text));
            TextView event_time_text = ((TextView) activity.findViewById(R.id.event_time_text));
            TextView recurrence_text = ((TextView) activity.findViewById(R.id.event_recurrence));
            TextView reminder_text = ((TextView) activity.findViewById(R.id.event_remind));
            TextView contact_text = ((TextView) activity.findViewById(R.id.contact_text));
            LinearLayout end_date = ((LinearLayout) activity.findViewById(R.id.end_date));
            LinearLayout event_time = ((LinearLayout) activity.findViewById(R.id.hour));
            LinearLayout noteLayout = ((LinearLayout) activity.findViewById(R.id.note));
            LinearLayout highlightLayout = ((LinearLayout) activity.findViewById(R.id.highlight));
            LinearLayout ratingLayout = ((LinearLayout) activity.findViewById(R.id.rating));
            LinearLayout locationLayout = ((LinearLayout) activity.findViewById(R.id.map_section));
            LinearLayout recurrence_layout = ((LinearLayout) activity.findViewById(R.id.recurrence_panel));
            LinearLayout reminder_layout = ((LinearLayout) activity.findViewById(R.id.remind));
            LinearLayout contact_layout = ((LinearLayout) activity.findViewById(R.id.contact));
            LinearLayout color_layout = ((LinearLayout) activity.findViewById(R.id.color));
            LinearLayout title_layout = ((LinearLayout) activity.findViewById(R.id.display_linearlayout_title));
            TextView days_number_count = ((TextView) activity.findViewById(R.id.days_number_count));
            TextView days_number_last_count = ((TextView) activity.findViewById(R.id.days_number_last_count));
            LinearLayout expensesLayout = ((LinearLayout) activity.findViewById(R.id.expenses));
            RelativeLayout bottomSheetLayout = ((RelativeLayout) activity.findViewById(R.id.design_bottom_sheet));
            LinearLayout historyPanel = (LinearLayout) activity.findViewById(R.id.historyPanel);
            LinearLayout weatherLayout = activity.findViewById(R.id.weather);

            //Statistics graph - Current month by default
            DateTime currentTime = DateTime.now();
            GraphBuilder.createEventOccurrenceGraphPerMonth(activity.getGraph(), event, currentTime.getMonthOfYear(), currentTime.getYear());


            // No need to display color
            color_layout.setVisibility(View.GONE);

            // Title in toolbar
            activity.getCollapsingToolbar().setTitleEnabled(false);

            // Days count number
            int numberOfDays;
            Date today = new Date();
            if (event.getEndDate() != null) {
                if (today.after(event.getEndDate())) {
                    numberOfDays = LasTimeUtils.getDaysDifference(event.getEndDate(), today);
                } else {
                    numberOfDays = 0;
                }
            } else {
                numberOfDays = LasTimeUtils.getDaysDifference(event.getDate(), today);
            }
            if (numberOfDays > 0) {
                ShapeDrawable daysRecurrence = new ShapeDrawable();
                daysRecurrence.getPaint().setColor(LasTimeUtils.getColor(days_number_count.getContext(), R.color.ColorPrimaryDark));
                daysRecurrence.setAlpha(250);
                days_number_count.setBackground(daysRecurrence);
                days_number_count.setText(numberOfDays + " " + this.activity.getString(R.string.days));
                days_number_count.setVisibility(View.VISIBLE);
            } else {
                days_number_count.setVisibility(View.GONE);
            }

            // Event duration days
            if (event.getEndDate() != null && event.getEndDate().after(event.getDate())) {
                days_number_last_count.setText(LasTimeUtils.computeDaysDifference(event.getDate(), event.getEndDate()) + 1 + " " + this.activity.getString(R.string.days));
                days_number_last_count.setVisibility(View.VISIBLE);
            } else {
                days_number_last_count.setVisibility(View.GONE);
            }

            event_menu_title.setText(event.getTitle());
            event_title.setText(event.getTitle());
            event_subtitle.setText(event.getDateAsString());

            // Event time
            event_date_text.setText(event.getDateAsString());
            if (event.getTimeOfEvent() != null && event.getTimeOfEvent() > 0) {
                event_time.setVisibility(View.VISIBLE);
                event_time_text.setText(LasTimeUtils.displayTime(event.getTimeOfEvent()));
            } else {
                event_time.setVisibility(View.GONE);
            }

            // Event end date
            if (event.getEndDate() != null) {
                end_date.setVisibility(View.VISIBLE);
                event_end_date_text.setText(event.getEndDateAsString());
            } else {
                end_date.setVisibility(View.GONE);
            }

            // Recurrence
            if (LasTimeUtils.isRecurrenceActivated(event.getRecurrence())) {
                recurrence_layout.setVisibility(View.VISIBLE);
                recurrence_text.setText(LasTimeUtils.convertRecurrenceTagToString(event.getRecurrence(), activity));
                if (LasTimeUtils.isReminderActivated(event.getReminder())) {
                    reminder_layout.setVisibility(View.VISIBLE);
                    reminder_text.setText(LasTimeUtils.convertReminderTagToString(event.getReminder(), activity));
                } else {
                    reminder_layout.setVisibility(View.GONE);
                }
            } else {
                recurrence_layout.setVisibility(View.GONE);
                reminder_layout.setVisibility(View.GONE);
            }

            // Location handling
            EventLocation location = null;
            activity.findViewById(R.id.location_text).setClickable(false);
            if (event.getLocation() != 0 && event.getLocation() != -1) {
                location = activity.getEventDAO().getLocationByEvent(event);
            }
            if (location != null) {
                ((TextView) activity.findViewById(R.id.location_text)).setText(location.getName());
                locationLayout.setVisibility(View.VISIBLE);
            } else {
                locationLayout.setVisibility(View.GONE);
            }

            // Mark handling
            if (LasTimeUtils.getRadioButtonIdForMark(event.getMark()) > -1) {
                ((DeselectableRadioButton) activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(event.getMark()))).setChecked(true);
                // Init mark button
                activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(1)).setAlpha(0.2f);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(2)).setAlpha(0.2f);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(3)).setAlpha(0.2f);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(event.getMark())).setAlpha(1);
                ratingLayout.setVisibility(View.VISIBLE);
            } else {
                ratingLayout.setVisibility(View.GONE);
            }

            // Notes display
            ((EditText) activity.findViewById(R.id.note_text)).setKeyListener(null);
            if (event.getNote() != null && !event.getNote().isEmpty()) {
                noteLayout.setVisibility(View.VISIBLE);
                ((EditText) activity.findViewById(R.id.note_text)).setText(event.getNote());
                activity.findViewById(R.id.note_text).setFocusable(false);
            } else {
                noteLayout.setVisibility(View.GONE);
            }

            // Highlight display
            if (event.getHighlight() != null && !event.getHighlight().isEmpty() && !event.getHighlight().equals(LasTimeUtils.HighlightableFields.NothingToHighLight.getValue())) {
                highlightLayout.setVisibility(View.VISIBLE);
                Spinner spinner = activity.findViewById(R.id.highlight_spinner);
                spinner.setAdapter(new ArrayAdapter<>(activity, R.layout.spinner_layout, Collections.singletonList(event.getHighlight())));
                spinner.setEnabled(false);
                activity.findViewById(R.id.highlight_spinner).setFocusable(false);
            } else {
                highlightLayout.setVisibility(View.GONE);
            }

            // Expenses display
            ((EditText) activity.findViewById(R.id.event_expenses_text)).setKeyListener(null);
            if (event.getExpenses() > 0) {
                expensesLayout.setVisibility(View.VISIBLE);
                Locale locale = LasTimeUtils.getLocale(activity);
                ((EditText) activity.findViewById(R.id.event_expenses_text))
                        .setText(String.format(locale, "%.2f", event.getExpenses())
                                + " " + Currency.getInstance(locale).getCurrencyCode());
                activity.findViewById(R.id.event_expenses_text).setFocusable(false);
            } else {
                expensesLayout.setVisibility(View.GONE);
            }

            // Handling event color
            activity.findViewById(R.id.display_framelayout_title).setBackgroundColor(LasTimeUtils.getColor(days_number_count.getContext(), R.color.ColorPrimary));
            // Background menu image
            ImageView backdrop = ((ImageView) activity.findViewById(R.id.backdrop));

            // Logic on what to draw as background (google map, image,..)
            if (event.getThumbnailPath() != null) {
                //Resize thumbnail
                Display display = activity.getWindowManager().getDefaultDisplay();
                Point size = new Point();
                display.getSize(size);

                int width = size.x;
                int height = ((int) (192 * backdrop.getContext().getResources().getDisplayMetrics().density));

                Picasso.with(activity)
                        .load(Uri.fromFile(new File(event.getThumbnailPath())))
                        .resize(width, height)
                        .centerCrop()
                        .into(backdrop);

                ShapeDrawable shapeDrawable = new ShapeDrawable();
                //shapeDrawable.getPaint().setColor(event.getColor());
                shapeDrawable.setAlpha(100);

                title_layout.setBackground(shapeDrawable);

            } else {
                backdrop.setImageBitmap(null);
                title_layout.setBackground(null);
            }

            activity.setEventDisplayed(event);

            // Display history behaviour
            if (!activity.isHistoryPanelShown()) {
                activity.getHistoryPanel().setVisibility(View.GONE);
                if (activity.getEventDisplayed().getState().equals(OLD) && activity.getMenu() != null)
                    activity.getMenu().findItem(R.id.back_to_active_event).setVisible(true);
            }

            final Animation translateOutLeftAnim = AnimationUtils.loadAnimation(activity, R.anim.slide_out_left);
            final Animation translateInLeftAnim = AnimationUtils.loadAnimation(activity, R.anim.slide_in_left);

            // Button to display history panel
            activity.findViewById(R.id.history_icon).setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (activity.isHistoryPanelShown()) {
                        historyPanel.startAnimation(translateOutLeftAnim);
                        historyPanel.setVisibility(View.GONE);
                        activity.setIsHistoryPanelShown(false);
                        activity.getBackToActiveButton().setVisibility(View.GONE);
                        if (activity.getMenu() != null && activity.getEventDisplayed().getState().equals(OLD))
                            activity.getMenu().findItem(R.id.back_to_active_event).setVisible(true);
                        else if (activity.getMenu() != null)
                            activity.getMenu().findItem(R.id.back_to_active_event).setVisible(false);
                    } else {
                        historyPanel.startAnimation(translateInLeftAnim);
                        historyPanel.setVisibility(View.VISIBLE);
                        activity.setIsHistoryPanelShown(true);
                        displayBackButton(event);
                        if (activity.getMenu() != null)
                            activity.getMenu().findItem(R.id.back_to_active_event).setVisible(false);
                    }
                }
            });

            // Contacts display
            if (event.getContact() != -1) {
                final Contact contact = activity.getEventDAO().getContactById(event.getContact());

                if (activity.hasPermissions(BaseActivity.PERMISSIONS.ReadContactPermissions)) {
                    contact_text.setText(contact.getContactName());
                } else {
                    contact_text.setText(R.string.contact_permissions_needed);
                    contact_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            activity.needReadContactPermissions();
                        }
                    });
                }
                contact_layout.setVisibility(View.VISIBLE);
                activity.findViewById(R.id.contact_call_sms).setVisibility(View.VISIBLE);


                // Check potential desynchros - WRITE PERMISSION NEEDED
                if (activity.hasPermissions(BaseActivity.PERMISSIONS.WriteContactPermissions)) {
                    if (contact.isContactSynchronizedWithPhone(activity)) {
                        // Handle sections visibility
                        activity.findViewById(R.id.desynchronization_section).setVisibility(View.GONE);
                        activity.findViewById(R.id.contact_call_sms).setVisibility(View.VISIBLE);
                        activity.findViewById(R.id.contact_source).setVisibility(View.VISIBLE);
                        if (contact.getPhoneId() != null && !contact.getPhoneId().isEmpty()) {
                            ((TextView) activity.findViewById(R.id.contact_source)).setText("("
                                    + activity.getString(R.string.contact_source) + " : " + activity.getString(R.string.contact_source_phone) + ")");
                        } else {
                            ((TextView) activity.findViewById(R.id.contact_source)).setText("("
                                    + activity.getString(R.string.contact_source) + " : " + activity.getString(R.string.contact_source_application) + ")");
                        }

                    } else {
                        // Handle sections visibility
                        activity.findViewById(R.id.desynchronization_section).setVisibility(View.VISIBLE);
                        activity.findViewById(R.id.contact_call_sms).setVisibility(View.GONE);
                        activity.findViewById(R.id.contact_source).setVisibility(View.GONE);
                        // Prepare action
                        activity.findViewById(R.id.resolve_desynchronization).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                DesynchronizedContactDialogFragment desyncContactPopup = DesynchronizedContactDialogFragment.newInstance(contact, new Contact(contact.getPhoneId(), activity));
                                desyncContactPopup.show(activity.getFragmentManager(), "desyncContactPopup");
                            }
                        });
                        activity.findViewById(R.id.remove_contact).setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                activity.getEventDAO().deleteContact(contact);
                                activity.getEventDAO().updateEventContact(event.getId(), -1);
                                event.setContact(-1);
                                // Refresh display
                                refresh(event);
                                // Notify
                                Snackbar.make(activity.findViewById(R.id.historyPanel), R.string.contact_desync_action_contact_removed, Snackbar.LENGTH_LONG).show();

                            }
                        });
                    }
                } else {
                    contact_text.setText("Write contact permissions needed!");
                    contact_text.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            activity.needWriteContactPermissions();
                        }
                    });
                }
            } else {
                contact_layout.setVisibility(View.GONE);
            }

            displayBackButton(event);

            // Bottom sheet
            bottomSheetLayout.setBackgroundColor(LasTimeUtils.getColor(days_number_count.getContext(), R.color.ColorPrimary));
            BottomSheetBehavior bottomSheetBehavior = BottomSheetBehavior.from(bottomSheetLayout);
            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {
                @Override
                public void onStateChanged(View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_COLLAPSED) {
                        activity.findViewById(R.id.history_section).setVisibility(View.VISIBLE);
                    } else if (newState == BottomSheetBehavior.STATE_EXPANDED) {
                        activity.findViewById(R.id.history_section).setVisibility(View.GONE);
                        ;
                    }
                }

                @Override
                public void onSlide(View bottomSheet, float slideOffset) {
                }
            });

            // Weather handling
            if (LasTimeUtils.getRadioButtonIdForWeather(event.getWeather()) > -1) {
                ((DeselectableRadioButton) activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(event.getWeather()))).setChecked(true);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(1)).setAlpha(0.4f);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(2)).setAlpha(0.4f);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(3)).setAlpha(0.4f);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(4)).setAlpha(0.4f);
                activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(event.getWeather())).setAlpha(1);
                weatherLayout.setVisibility(View.VISIBLE);
            } else {
                weatherLayout.setVisibility(View.GONE);
            }

            updateMenuForNavigationDisplay();

        }
    }

    /**
     * Generates horizontal panel
     */

    protected void displayHorizontalFragment(int eventPosition) {
        FragmentManager fragmentManager = activity.getFragmentManager();
        HorizontalListFragment horizontalListFragment = (HorizontalListFragment) fragmentManager.findFragmentByTag(HorizontalListFragment.DISPLAY_PAGE);
        if (horizontalListFragment == null) {
            horizontalListFragment = new HorizontalListFragment();
            horizontalListFragment.setDisplaySelected(HorizontalListAdapter.Display.DISPLAY_EVENT_DATE);
            horizontalListFragment.setCurrentSelection(eventPosition);
            fragmentManager.beginTransaction()
                    .add(R.id.content, horizontalListFragment, HorizontalListFragment.DISPLAY_PAGE)
                    .commit();
        } else {
            horizontalListFragment = new HorizontalListFragment();
            horizontalListFragment.setDisplaySelected(HorizontalListAdapter.Display.DISPLAY_EVENT_DATE);
            horizontalListFragment.setCurrentSelection(eventPosition);
            fragmentManager.beginTransaction()
                    .replace(R.id.content, horizontalListFragment, HorizontalListFragment.DISPLAY_PAGE)
                    .commit();
        }

        List<Event> events = activity.getEventDAO().findHistoryOfEvent(activity.getEventDisplayed());
        if (events == null || events.isEmpty()) {
            activity.findViewById(R.id.history_section).setVisibility(View.GONE);
        } else {
            activity.findViewById(R.id.history_section).setVisibility(View.VISIBLE);
        }
    }

    protected void updateMenuForNavigationDisplay() {
        if (activity.getMenu() != null) {
            if (activity.getEventDisplayed() != null && activity.getEventDisplayed().getLocation() > -1) {
                activity.getMenu().findItem(R.id.goToLocationMenuItem).setVisible(true);
            } else {
                activity.getMenu().findItem(R.id.goToLocationMenuItem).setVisible(false);
            }
        }
    }

    private void displayBackButton(final Event event) {
        if (event.getState().equals(ACTIVE)) {
            activity.getBackToActiveButton().setVisibility(View.GONE);
        } else if (event.getState().equals(OLD)) {
            activity.getBackToActiveButton().setVisibility(View.VISIBLE);
        }
    }

}
