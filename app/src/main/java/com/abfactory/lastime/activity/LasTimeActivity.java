package com.abfactory.lastime.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.drawable.LayerDrawable;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.component.EventListSectionAdapter;
import com.abfactory.lastime.component.HeaderListView;
import com.abfactory.lastime.component.SectionAdapter;
import com.abfactory.lastime.component.dialogs.StopTutorialFragment;
import com.abfactory.lastime.component.dialogs.TutorialDialogFragment;
import com.abfactory.lastime.component.dialogs.TutorialEndDialogFragment;
import com.abfactory.lastime.component.tutorial.ShowcaseViewFactory;
import com.abfactory.lastime.drawer.DrawerMenu;
import com.abfactory.lastime.list.AlphabeticalOrdering;
import com.abfactory.lastime.list.DetailsOrdering;
import com.abfactory.lastime.list.FrequencyOrdering;
import com.abfactory.lastime.list.Ordering;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.service.LastimeJobService;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.github.amlcurran.showcaseview.ShowcaseView;

import java.util.Collections;
import java.util.List;

import static com.abfactory.lastime.list.Ordering.OrderingType;

/**
 * Main class of APK
 * This class is the principal view where you get all events
 */
public class LasTimeActivity extends BaseActivity {

    //TODO: isolate those declarations
    public static final int EVENT_EDITION = 1;
    public static final int EVENT_DISPLAY = 2;

    public static final int EVENT_CREATED = 1;
    public static final int EVENT_MODIFIED = 2;
    public static final int EVENT_CREATED_FROM_TEMPLATE = 3;

    // Keep event list display scrolling position
    public static final String EVENT_POSITION = "event_list_state";
    private Parcelable listState = null;

    // Search View items
    LinearLayout searchContainer;
    EditText toolbarSearchView;
    ImageButton searchClearButton;
    // DAO object for db access
    IEventDAO eventDAO;
    private BroadcastReceiver receiver;
    private boolean isSearchViewVisible = false;
    // List of events to be displayed
    private List<Event> events;
    private List<Event> filteredEvents;
    // List display handling
    private Ordering orderingHandler;
    private SectionAdapter eventsAdapter;
    private HeaderListView listView;

    /**
     * DrawerPanel used as settings panel
     */
    private DrawerMenu drawerMenu;

    private Menu menu;
    private Toolbar toolbar;
    private FloatingActionButton addBtn;

    private ShowcaseView sv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lastime);

        // Database access
        eventDAO = new EventDAO(this.getApplicationContext());

        // ActionBar
        toolbar = (Toolbar) findViewById(R.id.lastime_toolbar);
        toolbar.inflateMenu(R.menu.menu_last_time);
        setSupportActionBar(toolbar);

        // Drawer
        drawerMenu = new DrawerMenu((DrawerLayout) findViewById(R.id.drawer_layout), this);

        // Nice arrow on top left which spins from the hamburger menu
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerMenu.getDrawerLayout(), toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerMenu.getDrawerLayout().addDrawerListener(drawerToggle);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        drawerToggle.syncState();

        // Handle Add button
        addBtn = (FloatingActionButton) findViewById(R.id.floatingAddButton);
        addBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                // Launching other activity
                Intent intent = new Intent(LasTimeActivity.this, EditEventActivity.class);
                // [TUTORIAL SPECIFIC]
                if (isTutorialModeActivated()) {
                    intent.putExtra(BaseActivity.TOUR_TAG, true);
                }
                // [END TUTORIAL SPECIFIC]
                startActivityForResult(intent, EVENT_EDITION);

            }
        });

        if (getPreferences().isAppToBeProtectedWithPassword()) {
            Intent intent = new Intent(LasTimeActivity.this, LoginActivity.class);
            startActivity(intent);
        }

        // SearchView
        searchContainer = (LinearLayout) findViewById(R.id.search_container);
        toolbarSearchView = (EditText) findViewById(R.id.search_view);
        searchClearButton = (ImageButton) findViewById(R.id.search_clear);
        ImageButton exitSearchButton = (ImageButton) findViewById(R.id.exit_search_btn);
        exitSearchButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Hide search toolbar
                displaySearchView(false);
                refreshListDisplay();
            }
        });

        // Search text changed listener
        toolbarSearchView.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                // Nothing to do...
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (s != null && s.length() > 0) {
                    eventsAdapter.getFilter().filter(s);
                } else {
                    if (events == null)
                        events = eventDAO.getAllActiveEvents();
                    filteredEvents = Collections.unmodifiableList(events);
                }
                refreshListDisplay();
            }

            @Override
            public void afterTextChanged(Editable s) {
                // Nothing to handle yet
            }
        });

        // Clear search text when clear button is tapped
        searchClearButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do job only if useful (to prevent recurrent color updates on UI)
                if (toolbarSearchView.getText() != null && toolbarSearchView.getText().toString().length() > 0) {
                    toolbarSearchView.setText("");
                    filteredEvents = Collections.unmodifiableList(events);
                    refreshListDisplay();
                }
            }
        });

        // Hide the search view
        searchContainer.setVisibility(View.GONE);

        // Handle broadcast for notifs reception
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                // Get the notifications MenuItem and LayerDrawable (layer-list)
                MenuItem item = menu.findItem(R.id.notif);
                LayerDrawable icon = (LayerDrawable) item.getIcon();
                // Update LayerDrawable's BadgeDrawable
                LasTimeUtils.setBadgeCount(getApplication(), icon, eventDAO.getActiveNotificationsCount());
            }
        };

        // Start tutorial if relevant
        if (getPreferences().isTutorialPopupToBeShownAtStartup() || getIntent().getBooleanExtra("FORCE_TUTO", false)) {
            (new TutorialDialogFragment()).show(getFragmentManager(), "tutorialDialog");
        }

        // Handle Rating is to be proposed
        if(getPreferences().getNumberOfConnectionsBeforeAskingForRating()==0){
            startActivity(new Intent(this, RateAppActivity.class));
        } else {
            getPreferences().decrementNumberOfConnectionsBeforeAskingForRating();
        }
    }

    @Override
    protected void onResume() {

        super.onResume();

        if (getPreferences().getInvalidState()) {
            Toast.makeText(LasTimeActivity.this, "Database version used is incorrect! (Downgraded version are not supported)", Toast.LENGTH_LONG).show();
        }

        // Get events from DB
        events = filteredEvents = eventDAO.getAllActiveEvents();

        // List handling
        refreshListDisplay();

        //TODO : Google feature
        // drawerMenu.refreshGoogleAccountDisplay();

        // Create space at the bottom of the list view to easily have access to the last element
        listView.getListView().setClipToPadding(false);
        listView.getListView().setPadding(0, 0, 0, (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 70, getResources().getDisplayMetrics()));

        if (menu != null) {
            // Update notifications count
            MenuItem item = menu.findItem(R.id.notif);
            LayerDrawable icon = (LayerDrawable) item.getIcon();
            // Update LayerDrawable's BadgeDrawable
            LasTimeUtils.setBadgeCount(this, icon, eventDAO.getActiveNotificationsCount());
        }

        drawerMenu.refreshBulletDisplay();

        // Getting last event list state
        if (listState != null)
            listView.getListView().onRestoreInstanceState(listState);
        listState = null;

        // Animation
        this.overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_right);

    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver,
                new IntentFilter(LastimeJobService.NOTIFICATION_RESULT)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    @Override
    public void setTitle(CharSequence title) {
        getActionBar().setTitle(title);
    }

    public void refreshListDisplay() {

        orderingHandler = new DetailsOrdering(this);
        orderingHandler.initFromListOfEvents(filteredEvents);

        // Handle list
        listView = (HeaderListView) findViewById(R.id.event_list_display);
        eventsAdapter = new EventListSectionAdapter(this);
        listView.setAdapter(eventsAdapter);

        /* Display default message if no item to display in the list */
        if (isSearchViewVisible || events == null || events.isEmpty()) {

            if (events == null || events.isEmpty()) {
                (findViewById(R.id.empty_list_section)).setVisibility(View.VISIBLE);
                (findViewById(R.id.event_list_display)).setVisibility(View.GONE);
            }

            if (menu != null) {
                menu.findItem(R.id.search).setVisible(false);
                menu.findItem(R.id.notif).setVisible(false);
            }
        } else {
            (findViewById(R.id.empty_list_section)).setVisibility(View.GONE);
            (findViewById(R.id.event_list_display)).setVisibility(View.VISIBLE);
            if (menu != null) {
                menu.findItem(R.id.search).setVisible(true);
                menu.findItem(R.id.notif).setVisible(true);
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        this.menu = menu;

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_last_time, menu);

        // Get the notifications MenuItem and LayerDrawable (layer-list)
        MenuItem item = menu.findItem(R.id.notif);
        LayerDrawable icon = (LayerDrawable) item.getIcon();
        // Update LayerDrawable's BadgeDrawable
        LasTimeUtils.setBadgeCount(this, icon, eventDAO.getActiveNotificationsCount());

        if (events == null || events.isEmpty()) {
            menu.findItem(R.id.search).setVisible(false);
            menu.findItem(R.id.notif).setVisible(false);
        } else {
            menu.findItem(R.id.search).setVisible(true);
            menu.findItem(R.id.notif).setVisible(true);
        }

        return true;
    }

    @Override
    /**
     * Handles click menu item
     */
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.search) {
            displaySearchView(true);
            return true;
        } else if (id == R.id.notif) {
            DrawerMenu.openActivity(this, MyToDoListActivity.class);
        }
        return super.onOptionsItemSelected(item);
    }

    public void displaySearchView(boolean visible) {

        if (visible) {

            menu.findItem(R.id.search).setVisible(false);
            menu.findItem(R.id.notif).setVisible(false);

            if (getNavButtonView(toolbar) != null)
                getNavButtonView(toolbar).setVisibility(View.GONE);
            searchContainer.setVisibility(View.VISIBLE);

            Window window = getWindow();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(LasTimeUtils.getColor(this, R.color.DarkGrey));
            }

            toolbar.setBackgroundColor(LasTimeUtils.getColor(this, R.color.LightGrey));

            // Shift focus to the search EditText
            toolbarSearchView.requestFocus();

            // Pop up the soft keyboard
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    toolbarSearchView.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_DOWN, 0, 0, 0));
                    toolbarSearchView.dispatchTouchEvent(MotionEvent.obtain(SystemClock.uptimeMillis(), SystemClock.uptimeMillis(), MotionEvent.ACTION_UP, 0, 0, 0));
                }
            }, 200);

            isSearchViewVisible = true;

        } else {
            // Allows user to open drawer again
            menu.findItem(R.id.search).setVisible(true);
            menu.findItem(R.id.notif).setVisible(true);
            if (getNavButtonView(toolbar) != null)
                getNavButtonView(toolbar).setVisibility(View.VISIBLE);
            toolbarSearchView.setText("");
            searchContainer.setVisibility(View.GONE);
            Window window = getWindow();
            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                window.setStatusBarColor(LasTimeUtils.getColor(this, R.color.ColorPrimaryDark));
            }
            toolbar.setBackgroundColor(LasTimeUtils.getColor(this, R.color.ColorPrimary));

            // Hide the keyboard because the search box has been hidden
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(toolbarSearchView.getWindowToken(), 0);

            isSearchViewVisible = false;
        }

    }

    private ImageButton getNavButtonView(Toolbar toolbar) {
        for (int i = 0; i < toolbar.getChildCount(); i++)
            if (toolbar.getChildAt(i) instanceof ImageButton)
                return (ImageButton) toolbar.getChildAt(i);

        return null;
    }

    @Override
    public void onBackPressed() {

        if (isTutorialModeActivated()) {
            (new StopTutorialFragment()).show(getFragmentManager(), "stopTutorialDialog");
        } else {
            if (View.VISIBLE == searchContainer.getVisibility()) {
                // Back should close search view if still there
                displaySearchView(false);
                refreshListDisplay();
            } else {
                // Second one should leave the application as per standard implem
                super.onBackPressed();
            }
        }

    }

    // Once this activity is called back
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Check which request we're responding to
        if (requestCode == EVENT_EDITION) {
            // Make sure the request was successful
            if (resultCode == RESULT_OK) {
                int result = data.getIntExtra("result", -1);
                if (result == EVENT_CREATED) {
                    Snackbar.make(this.addBtn, getString(R.string.event_created), Snackbar.LENGTH_LONG).show();
                }
                if (result == EVENT_MODIFIED) {
                    Snackbar.make(this.addBtn, getString(R.string.event_updated), Snackbar.LENGTH_LONG).show();
                }
                if (isTutorialModeActivated()) {
                    filteredEvents = eventDAO.getAllActiveEvents();
                    listView.getListView().setSelection(0);
                    refreshListDisplay();
                    runTutorialSecondPart();
                }
            } else {
                if (isTutorialModeActivated()) {
                    stopTutorial();
                }
            }
        }
        if (requestCode == EVENT_DISPLAY && resultCode == DisplayEventActivity.DELETE_ALL_OK) {
            Snackbar.make(addBtn, getString(R.string.deleted_event_text), Snackbar.LENGTH_LONG)
                    .show();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        // Always call the superclass so it can save the view hierarchy state
        super.onSaveInstanceState(savedInstanceState);
        // Save the event position selected
        listState = listView.getListView().onSaveInstanceState();
        savedInstanceState.putParcelable(EVENT_POSITION, listState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle state) {
        super.onRestoreInstanceState(state);
        listState = state.getParcelable(EVENT_POSITION);
    }

    // ********************** Tutorial ***********************

    public void startTourGuide() {
        setTutorialModeActivated(true);
        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_GO_TO_EDIT, LasTimeActivity.this);
    }

    public void runTutorialSecondPart() {
        sv.hide();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_MAIN_ACTIVITY_TOUR_EVENT_CREATED, LasTimeActivity.this);
                sv.overrideButtonClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sv.hide();
                        runTutorialLastPart();
                    }
                });
            }
        }, 800);

    }

    public void runTutorialLastPart() {

        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_MAIN_ACTIVITY_TOUR_SEARCH_ACCESS, LasTimeActivity.this);

        sv.overrideButtonClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.hide();
                sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_MAIN_ACTIVITY_TOUR_TODO_ACCESS, LasTimeActivity.this);
                sv.overrideButtonClick(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        sv.hide();
                        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_MAIN_ACTIVITY_TOUR_MENU, LasTimeActivity.this);
                        sv.overrideButtonClick(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                sv.hide();
                                drawerMenu.getDrawerLayout().openDrawer(Gravity.LEFT);
                                (new Handler()).postDelayed(new Runnable() {
                                    @Override
                                    public void run() {
                                        sv.hide();
                                        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_MENU_MAIN_ACTIVITY, LasTimeActivity.this);
                                        sv.overrideButtonClick(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                sv.hide();
                                                sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_MENU_TODO_ACTIVITY, LasTimeActivity.this);
                                                sv.overrideButtonClick(new View.OnClickListener() {
                                                    @Override
                                                    public void onClick(View v) {
                                                        sv.hide();
                                                        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_MENU_WISH_LIST_ACTIVITY, LasTimeActivity.this);
                                                        sv.overrideButtonClick(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(View v) {
                                                                sv.hide();
                                                                sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_MENU_STATISTICS_ACTIVITY, LasTimeActivity.this);
                                                                sv.overrideButtonClick(new View.OnClickListener() {
                                                                    @Override
                                                                    public void onClick(View v) {
                                                                        sv.hide();
                                                                        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_MENU_SETTINGS, LasTimeActivity.this);
                                                                        sv.overrideButtonClick(new View.OnClickListener() {
                                                                            @Override
                                                                            public void onClick(View v) {
                                                                                sv.hide();
                                                                                sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_MENU_TEAM, LasTimeActivity.this);
                                                                                sv.overrideButtonClick(new View.OnClickListener() {
                                                                                    @Override
                                                                                    public void onClick(View v) {
                                                                                        sv.hide();
                                                                                        drawerMenu.getDrawerLayout().closeDrawer(Gravity.LEFT);
                                                                                        setTutorialModeActivated(false);
                                                                                        (new TutorialEndDialogFragment()).show(getFragmentManager(), "tutorialEndDialog");
                                                                                    }
                                                                                });
                                                                            }
                                                                        });
                                                                    }
                                                                });
                                                            }
                                                        });
                                                    }
                                                });
                                            }
                                        });
                                    }
                                }, 500);
                            }
                        });
                    }
                });
            }
        });

    }

    /**
     * Method to call when the tutorial needs to be fully cleaned from the activity
     */
    public void stopTutorial() {
        setTutorialModeActivated(false);
        sv.hide();
        sv = null;
    }

    // ********************** Getter/Setter to share data with other classes ***********************

    public Ordering getOrderingHandler() {
        return this.orderingHandler;
    }

    public void setFilteredEvents(List<Event> filteredEvents) {
        this.filteredEvents = filteredEvents;
    }

    public List<Event> getEvents() {
        return events;
    }

    public void setEvents(List<Event> events) {
        this.events = events;
    }

    public FloatingActionButton getAddBtn() {
        return addBtn;
    }

    public IEventDAO getEventDAO() {
        return eventDAO;
    }

    public Menu getMenu() {
        return menu;
    }

    public void setMenu(Menu menu) {
        this.menu = menu;
    }

    public DrawerMenu getDrawerMenu() {
        return drawerMenu;
    }
}
