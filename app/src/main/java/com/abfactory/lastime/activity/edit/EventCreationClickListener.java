package com.abfactory.lastime.activity.edit;

import android.app.Activity;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LasTimeActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.NotificationUtils;
import com.google.api.client.repackaged.com.google.common.base.Strings;

import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.ACTIVE;

/**
 * On click listener definition for EditEventActivity
 */
public class EventCreationClickListener implements View.OnClickListener {

    private final static String TAG = EventCreationClickListener.class.getName();

    private final EditEventActivity activity;

    public EventCreationClickListener(EditEventActivity activity){
        this.activity = activity;
    }

    public void onClick(View view) {

        EventChecker eventChecker = EventChecker.verify(activity);
        if (eventChecker.isValid()) {
            // Create event
            Date date;
            Calendar calendarDate = Calendar.getInstance(Locale.getDefault());
            Calendar calendarEndDate = null;
            try {
                date = Event.CALENDAR_DISPLAY.parse(((TextView) activity.findViewById(R.id.event_date_text)).getText().toString());
                calendarDate.setTime(date);
                TextView endDateView = (TextView) activity.findViewById(R.id.event_end_date_text);
                if(endDateView.getText() != null && endDateView.getText().length() > 0) {
                    calendarEndDate = Calendar.getInstance(Locale.getDefault());
                    Date endDate = Event.CALENDAR_DISPLAY.parse(endDateView.getText().toString());
                    calendarEndDate.setTime(endDate);
                }
            } catch (ParseException e) {
                Log.e(TAG, "Error occurred creating event: " + e.getMessage(), e);
            }

            // Creating event based on user input
            Object highlight = ((Spinner) activity.findViewById(R.id.highlight_spinner)).getSelectedItem();
            Editable expenses = ((EditText) activity.findViewById(R.id.event_expenses_text)).getText();
            Event newEvent = new Event.EventBuilder(((EditText) activity.findViewById(R.id.event_title)).getText().toString())
                    .addDate(calendarDate.getTime())
                    .addTime(LasTimeUtils.parseTime(((TextView) activity.findViewById(R.id.event_time_text)).getText().toString()))
                    .addMark(activity.getMarkForEvent())
                    .addImagePath(activity.getImageToStore() != null ? activity.getImageToStore().getAbsolutePath() : null)
                    .addThumbnailPath(activity.getThumbnailToStore() != null ? activity.getThumbnailToStore().getAbsolutePath() : null)
                    .addWeight(0)
                    .addReminder(activity.getReminderTag())
                    .addNote(((EditText) activity.findViewById(R.id.note_text)).getText().toString())
                    .addColor(activity.getSelectedColor() != null ? activity.getSelectedColor().getId() : LasTimeUtils.getRandomColor())
                    .addState(ACTIVE)
                    .addCreationDate(new Date())
                    .addRecurrence(activity.getRecurrenceTag())
                    .addEventEndDate(calendarEndDate != null ? calendarEndDate.getTime() : null)
                    .addExpenses(this.isolateExpenses(expenses.toString()))
                    .addHighlight(highlight != null? highlight.toString() : null)
                    .addWeather(activity.getWeatherForEvent())
                    .build();

            // In case of edit mode name could be changed (updating existing one)
            Intent returnIntent = new Intent();
            if (activity.isEditMode() && activity.getEditedEvent() != null) {
                newEvent.setFieldList(activity.getEditedEvent().getFieldList());
                newEvent.setWeight(activity.getCurrentWeight());
                newEvent.setId(activity.getEditedEvent().getId());
                newEvent.setReference(activity.getEditedEvent().getReference());
                // Need to save event only if a modification has been done
                // Use history only if it's an active event
                returnIntent.putExtra("result", LasTimeActivity.EVENT_MODIFIED);
                if (!newEvent.equals(activity.getEditedEvent()) && activity.getEditedEvent().getState().equals(ACTIVE)) {
                    Long id = activity.getEventDAO().modifyEvent(newEvent, activity.getNewLocationActivityResult(), activity.getContact(), false);
                    returnIntent.putExtra("eventId", id);
                } else {
                    // Update event without using history implementation (in case OLD event)
                    activity.getEventDAO().updateEvent(newEvent, activity.getNewLocationActivityResult(), activity.getContact());
                    returnIntent.putExtra("eventId", newEvent.getId());
                }
                // Clean notifications if need be
                NotificationUtils.cleanNotificationsWhenDeletingEvent(activity.getEventDAO(), Integer.parseInt(activity.getEditedEvent().getReference()), activity);
            } else {
                // Store it in DB but check if event already exists (in case of creation)
                Event existingEvent = activity.getEventDAO().findActiveEventByTitle(newEvent.getTitle());
                if (existingEvent != null) {
                    newEvent.setId(existingEvent.getId());
                    newEvent.setWeight(activity.getCurrentWeight());
                    newEvent.setReference(existingEvent.getReference());
                    Long id = activity.getEventDAO().modifyEvent(newEvent, activity.getNewLocationActivityResult(), activity.getContact(), true);
                    returnIntent.putExtra("result", LasTimeActivity.EVENT_MODIFIED);
                    returnIntent.putExtra("eventId", id);
                } else {
                    activity.getEventDAO().addEvent(newEvent, activity.getNewLocationActivityResult(), activity.getContact());
                    if(activity.isCreationFromTemplateMode()) {
                        returnIntent.putExtra("result", LasTimeActivity.EVENT_CREATED_FROM_TEMPLATE);
                        returnIntent.putExtra("templateId", activity.getEventTemplateUsed().getId());
                    } else {
                        returnIntent.putExtra("result", LasTimeActivity.EVENT_CREATED);
                    }
                }
            }

            //
            returnIntent.putExtra("eventPosition", activity.getEventPosition());

            // Stop activity
            activity.setResult(Activity.RESULT_OK, returnIntent);
            activity.finish();

        } else {  // Event not valid
            // Show message
            Snackbar.make(view, eventChecker.getErrorMessage(), Snackbar.LENGTH_LONG).show();
        }
    }

    private float isolateExpenses(String expenses){
        final Locale locale = LasTimeUtils.getLocale(activity);
        if(!Strings.isNullOrEmpty(expenses.toString())){
            if(expenses.contains(Currency.getInstance(locale).getCurrencyCode())){
                expenses = expenses.replace(Currency.getInstance(locale).getCurrencyCode(), "");
            }
            DecimalFormat decimalFormat = (DecimalFormat) DecimalFormat.getInstance(locale);
            try {
                return decimalFormat.parse(expenses.toString()).floatValue();
            } catch (ParseException e) {
                Log.e(EventCreationClickListener.class.getName(), "Unable to convert expenses", e);
            }
        }
        return 0;
    }
}
