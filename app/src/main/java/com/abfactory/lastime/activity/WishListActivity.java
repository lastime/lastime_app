package com.abfactory.lastime.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.component.WishListAdapter;
import com.abfactory.lastime.drawer.DrawerMenu;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventTemplate;
import com.abfactory.lastime.model.IEventDAO;

import java.util.List;

public class WishListActivity extends BaseActivity {

    private DrawerMenu drawerMenu;
    private LinearLayout mainContainer;
    private IEventDAO eventDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wish_list);

        eventDAO = new EventDAO(this.getApplicationContext());

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        mainContainer = (LinearLayout) findViewById(R.id.main_container);

        drawerMenu = new DrawerMenu((DrawerLayout) findViewById(R.id.drawer_layout), this);
        // Nice arrow on top left which spins from the hamburger menu
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerMenu.getDrawerLayout(), toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerMenu.getDrawerLayout().addDrawerListener(drawerToggle);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }

        findViewById(R.id.use_inspire_me_feature_link).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(WishListActivity.this, InspireMeActivity.class));
            }
        });

        drawerToggle.syncState();

        updateDisplay();

    }

    private void updateDisplay() {

        List<EventTemplate> templates = eventDAO.getAllEventTemplates();

        RecyclerView rv = (RecyclerView) findViewById(R.id.rv);
        LinearLayout emptyMessageSection = (LinearLayout) findViewById(R.id.empty_list_section);

        rv.setHasFixedSize(true);
        LinearLayoutManager llm = new LinearLayoutManager(this);
        rv.setLayoutManager(llm);
        WishListAdapter adapter = new WishListAdapter(templates, this);
        rv.setAdapter(adapter);

        if (templates != null && !templates.isEmpty()) {
            rv.setVisibility(View.VISIBLE);
            emptyMessageSection.setVisibility(View.GONE);
        } else {
            rv.setVisibility(View.GONE);
            emptyMessageSection.setVisibility(View.VISIBLE);
        }

        // Update menu bullet display
        drawerMenu.refreshBulletDisplay();
    }

    public void doClikOnCreateEventForWish(EventTemplate wish) {
        Intent intent = new Intent(WishListActivity.this, EditEventActivity.class);
        intent.putExtra("templateId", wish.getId());
        WishListActivity.this.startActivityForResult(intent, 1);
    }

    public void doClikOnDeleteWish(EventTemplate wish) {
        new EventDAO(this.getApplicationContext()).deleteEventTemplate(wish.getId());
        updateDisplay();
        Snackbar.make(getMainContainer(), getResources().getString(R.string.wish_deleted), Snackbar.LENGTH_SHORT).show();
        // Update menu bullet display
        drawerMenu.refreshBulletDisplay();
    }

    /**
     * Get modified event to refresh view
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            int result_type = data.getIntExtra("result", 0);
            if (LasTimeActivity.EVENT_CREATED_FROM_TEMPLATE == result_type) {
                // Delete Template used
                new EventDAO(this.getApplicationContext()).deleteEventTemplate(data.getLongExtra("templateId", 0));
                updateDisplay();
                // Notify user
                Snackbar.make(getMainContainer(), getResources().getString(R.string.new_event_from_template), Snackbar.LENGTH_SHORT).show();
            } else {
                Snackbar.make(getMainContainer(), getResources().getString(R.string.no_new_event), Snackbar.LENGTH_SHORT).show();
            }
        } else if (resultCode == RESULT_CANCELED) {
            // Delete Template used
            new EventDAO(this.getApplicationContext()).deleteEventTemplate(data.getLongExtra("templateId", 0));
            updateDisplay();
            Snackbar.make(getMainContainer(), R.string.event_already_exists, Snackbar.LENGTH_LONG).show();
        } else {
            Snackbar.make(getMainContainer(), getResources().getString(R.string.no_new_event), Snackbar.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMenu.refreshBulletDisplay();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_wish_list, menu);
        MenuItem clearAll = menu.findItem(R.id.clearAll);
        clearAll.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                for (EventTemplate et : eventDAO.getAllEventTemplates()) {
                    eventDAO.deleteEventTemplate(et.getId());
                }
                updateDisplay();
                Snackbar.make(getMainContainer(), R.string.wishlist_cleaned, Snackbar.LENGTH_SHORT).show();
                return true;
            }
        });
        MenuItem goToInspireMe = menu.findItem(R.id.goToInspireMe);
        goToInspireMe.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem item) {
                startActivity(new Intent(WishListActivity.this, InspireMeActivity.class));
                return true;
            }
        });
        //refreshDisplay();
        return true;
    }

    public LinearLayout getMainContainer() {
        return mainContainer;
    }

}
