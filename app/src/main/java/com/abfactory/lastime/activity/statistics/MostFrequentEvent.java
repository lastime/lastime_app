package com.abfactory.lastime.activity.statistics;

import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.StatisticsActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.IEventDAO;

import java.util.List;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.OLD;

public class MostFrequentEvent extends StatisticComputerEventLoop {

    private Event mostFrequentEvent;

    private int maxNumberOfOccurence = 0;

    public MostFrequentEvent(StatisticsActivity activity, IEventDAO eventDAO) {
        super(activity, eventDAO);
    }

    @Override
    public void executeOnEvent(Event event) {
        // Get current event history
        List<Event> eventHistory = eventDAO.getAllEventsByStateAndCommonId(OLD, event.getReference());
        int numberOfOccurrence = eventHistory.size() + 1;
        // If most frequent than reference store it
        if (numberOfOccurrence > maxNumberOfOccurence) {
            maxNumberOfOccurence = numberOfOccurrence;
            mostFrequentEvent = event;
        }
    }

    @Override
    public void execute() {
        if (mostFrequentEvent != null) {
            ((TextView) findViewById(R.id.stats_most_frequent_event_name)).setText(mostFrequentEvent.getTitle());
            int numberOfOccurences = eventDAO.getAllEventsByStateAndCommonId(OLD, mostFrequentEvent.getReference()).size() + 1;
            ((TextView) findViewById(R.id.stats_most_frequent_event_occurences_nbr)).setText(String.valueOf(numberOfOccurences) + "\n" + getString(R.string.times));
        }
    }
}
