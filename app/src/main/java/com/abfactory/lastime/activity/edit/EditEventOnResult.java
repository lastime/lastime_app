package com.abfactory.lastime.activity.edit;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Contact;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import droidninja.filepicker.FilePickerConst;

/**
 * Class that handles all business of on result of EditEventActivity
 */
public class EditEventOnResult {

    private final static String TAG = EditEventOnResult.class.getName();
    private static final int THUMBNAIL_SIZE = 500;
    private final EditEventActivity editEventActivity;

    public EditEventOnResult(EditEventActivity editEventActivity) {
        this.editEventActivity = editEventActivity;
    }

    public void doOnResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            Event editedEvent = editEventActivity.getEditedEvent();
            // Take photo from camera
            switch (requestCode) {
                // Select photo from phone gallery
                case FilePickerConst.REQUEST_CODE_PHOTO:
                    if (resultCode == Activity.RESULT_OK && data != null) {
                        saveImage(data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_MEDIA).get(0));
                    }
                    break;
                // Select photo from existing one or from internet
                case EditEventActivity.BUILTIN_GALLERY:
                    String path = data.getStringExtra("imagePath");
                    Log.d(TAG, "Image selected: " + path);
                    // Update image display
                    saveImageFile(path, editEventActivity);
                    break;
                // Handle logic on location selection
                case EditEventActivity.PICKUP_LOCATION:
                    editEventActivity.setNewLocationActivityResult(data.getParcelableExtra("result"));
                    ((TextView) editEventActivity.findViewById(R.id.location_text)).setText(editEventActivity.getNewLocationActivityResult().getName());
                    break;
                case EditEventActivity.PICK_CONTACT:
                    editEventActivity.setContact(new Contact(data.getData(), editEventActivity));
                    ((TextView) editEventActivity.findViewById(R.id.contact_text)).setText(editEventActivity.getContact().getContactName());
                    editEventActivity.findViewById(R.id.cancelContactButton).setVisibility(View.VISIBLE);
                    break;
                case EditEventActivity.FIELD_LIST_UPDATED:
                    if (editedEvent != null)
                        editedEvent.setFieldList(editEventActivity.getEventDAO().retrieveEventById(editedEvent.getId()).getFieldList());
                    break;
                case EditEventActivity.LOCATION_LIST_SELECTED:
                    if (editedEvent != null) {
                        EventLocation eventLocation = editEventActivity.getEventDAO().getLocationById(data.getLongExtra("locationId", -1));
                        if (eventLocation != null) {
                            editEventActivity.setNewLocationActivityResult(eventLocation);
                            ((TextView) editEventActivity.findViewById(R.id.location_text)).setText(editEventActivity.getNewLocationActivityResult().getName());
                        }
                    }
            }
        }
    }

    /**
     * Method saves full image and thumbnail in dedicated folder
     * then saves paths in database
     */
    public static void saveImage(Bitmap image, String path, EditEventActivity editEventActivity) {
        // Get photo orientation
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            Log.e(TAG, "Error occurred with EXIF image: " + e.getMessage(), e);
        }
        Bitmap bitmap = image;
        if (exif != null) {
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                    ExifInterface.ORIENTATION_UNDEFINED);
            bitmap = LasTimeUtils.rotateBitmap(image, orientation);
        }

        FileOutputStream out = null;

        try {
            // Full size image
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            editEventActivity.setImageToStore(new File(LasTimeUtils.getImageStorageDir(editEventActivity), timeStamp + ".jpg"));
            out = new FileOutputStream(editEventActivity.getImageToStore());
            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, out);
            out.flush();
            out.close();

            // Thumbnail image
            bitmap = LasTimeUtils.cropAndScale(bitmap, editEventActivity.getPicture());
            String thumbnailName = "THUMBNAIL_" + THUMBNAIL_SIZE + "_" + editEventActivity.getImageToStore().getName();
            File imagePath = new File(LasTimeUtils.getImageStorageDir(editEventActivity), thumbnailName);
            editEventActivity.setThumbnailToStore(imagePath);
            editEventActivity.getEditedEvent().setThumbnailPath(imagePath.getPath());
            out = new FileOutputStream(editEventActivity.getThumbnailToStore());

            bitmap.compress(Bitmap.CompressFormat.JPEG, 85, out);
            out.flush();
            out.close();
        } catch (IOException e) {
            Log.e(TAG, "Error occurred with file image: " + e.getMessage(), e);
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (IOException e) {
                    Log.e(TAG, "Error occurred with file image: " + e.getMessage(), e);
                }
        }
    }

    private void saveImage(String picturePath) {
        if (picturePath != null) {
            Bitmap thumbnail = BitmapFactory.decodeFile(picturePath);
            Log.d(TAG, picturePath);
            saveImage(thumbnail, picturePath, this.editEventActivity);
        } else {
            Log.w(TAG, "No image could be retrieved from gallery");
        }
    }

    /**
     * Logic to save image file coming from internet
     *
     * @param path
     */
    public static void saveImageFile(String path, EditEventActivity editEventActivity) {
        // If image is coming from internet
        if (path.startsWith("http://") || path.startsWith("https://")) {
            String filename = path.substring(path.lastIndexOf('/') + 1);
            if (!isImagePathExisting(path, editEventActivity)) {
                // Download file
                new DownloadImageTask(editEventActivity).execute(path, filename);
            }
            File imagePath = new File(LasTimeUtils.getImageStorageDir(editEventActivity), filename);
            editEventActivity.setThumbnailToStore(imagePath);
            editEventActivity.getEditedEvent().setThumbnailPath(imagePath.getPath());
        }
        // If image from sd card
        else {
            File imagePath = new File(path.replace("file:///", ""));
            editEventActivity.setThumbnailToStore(imagePath);
            editEventActivity.getEditedEvent().setThumbnailPath(imagePath.getPath());
        }
    }

    public static boolean isImagePathExisting(String filename, EditEventActivity editEventActivity) {
        String[] imageNames = LasTimeUtils.getImageStorageDir(editEventActivity).list();
        // Check image does not exist
        for (String imageName : imageNames) {
            if (imageName.contains(filename)) {
                return true;
            }
        }
        return false;
    }
}
