package com.abfactory.lastime.activity;

import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.draggablelist.RecyclerListFragment;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;

public class DisplayCustomizationActivity extends BaseActivity {

    private Toolbar toolbar;
    private IEventDAO eventDAO;
    private Event event;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_customization);

        eventDAO = new EventDAO(this);
        event = eventDAO.retrieveEventById(getIntent().getLongExtra("eventId", -1));

        // ActionBar
        toolbar = findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_cancel);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .replace(R.id.content, new RecyclerListFragment())
                    .addToBackStack(null)
                    .commit();
        }

        setResult(RESULT_OK);
    }

    public Event getEvent(){
        return event;
    }

    public IEventDAO getEventDAO(){
        return eventDAO;
    }

    @Override
    public void onBackPressed() {
        finishActivity(RESULT_OK);
    }

}
