package com.abfactory.lastime.activity.display;

import android.content.ActivityNotFoundException;
import android.content.ContentProviderOperation;
import android.content.ContentProviderResult;
import android.content.Intent;
import android.content.OperationApplicationException;
import android.net.Uri;
import android.os.Bundle;
import android.os.RemoteException;
import android.provider.ContactsContract;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.AlphaAnimation;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.component.dialogs.DeleteChoiceDialog;
import com.abfactory.lastime.component.dialogs.GraphSelectionFragment;
import com.abfactory.lastime.component.draggablelist.DraggableListItem;
import com.abfactory.lastime.model.Contact;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.NotificationUtils;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.jjoe64.graphview.GraphView;

import org.acra.ACRA;

import java.util.ArrayList;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.ACTIVE;
import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.OLD;

public class DisplayEventActivity extends BaseActivity implements DeleteChoiceDialog.DeleteChoiceDialogListener,
        AppBarLayout.OnOffsetChangedListener {

    private static final float PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR = 0.9f;
    private static final float PERCENTAGE_TO_HIDE_TITLE_DETAILS = 0.3f;
    private static final int ALPHA_ANIMATIONS_DURATION = 200;
    public static int DELETE_OK = 2;
    public static int DELETE_ALL_OK = 3;
    // Display history panel
    boolean isHistoryPanelShown = false;
    private CollapsingToolbarLayout collapsingToolbar;
    private IEventDAO eventDAO;
    private Event eventDisplayed;
    private Button backToActiveButton;
    private LinearLayout historyPanel;
    private Menu menu;
    // Rollback on history event
    private long eventId;
    private UpdateDisplayEvent updateDisplayEvent;
    // Title frame
    private FrameLayout frameParallax;
    private boolean isTheTitleVisible = false;
    private boolean isTheTitleContainerVisible = true;
    private LinearLayout titleContainer;
    private TextView title;

    private GoogleMap map;
    private EventLocation location;

    // Selected event in panel list
    private int eventPosition;

    // Statistics graph
    GraphView graph;

    private static String TAG = DisplayEventActivity.class.getName();

    public static void startAlphaAnimation(View v, long duration, int visibility) {
        AlphaAnimation alphaAnimation = (visibility == View.VISIBLE)
                ? new AlphaAnimation(0f, 1f)
                : new AlphaAnimation(1f, 0f);

        alphaAnimation.setDuration(duration);
        alphaAnimation.setFillAfter(true);
        v.startAnimation(alphaAnimation);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater mInflater = LayoutInflater.from(this);
        final View contentView = mInflater.inflate(R.layout.activity_display_event, null);
        LinearLayout mainContainer = (LinearLayout) contentView.findViewById(R.id.main_container);

        setContentView(contentView);

        graph = findViewById(R.id.graph);
        graph.setOnClickListener(view -> chooseGraph());

        // Toolbar
        collapsingToolbar = findViewById(R.id.collapsing_toolbar);
        // ActionBar
        Toolbar toolbar = findViewById(R.id.display_event_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_cancel);
        toolbar.setTitle("");
        setSupportActionBar(toolbar);

        AppBarLayout appBarLayout = findViewById(R.id.display_appbar);
        appBarLayout.addOnOffsetChangedListener(this);

        frameParallax = findViewById(R.id.display_framelayout_title);
        title = findViewById(R.id.display_menu_title);
        titleContainer = findViewById(R.id.display_linearlayout_title);

        historyPanel = findViewById(R.id.historyPanel);

        // Display update class
        updateDisplayEvent = new UpdateDisplayEvent(this);

        // EventDAO
        eventDAO = new EventDAO(this.getApplicationContext());

        // Active event button
        backToActiveButton = findViewById(R.id.backToActiveButton);
        backToActiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToActiveEvent(null);
            }
        });

        // Edit button
        FloatingActionButton floatEditButton = (FloatingActionButton) findViewById(R.id.floatEditButton);
        floatEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Calling edit activity
                Intent intent = new Intent(DisplayEventActivity.this, EditEventActivity.class);
                intent.putExtra("eventId", eventDisplayed.getId());
                intent.putExtra("eventPosition", eventPosition);
                DisplayEventActivity.this.startActivityForResult(intent, 1);
            }
        });

        // Refresh button
        FloatingActionButton floatRefreshButton = (FloatingActionButton) findViewById(R.id.floatRefreshButton);
        floatRefreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (eventDisplayed.isActive()) {
                    Long id = eventDAO.refreshEvent(eventDisplayed);
                    updateDisplayEvent.refresh(eventDAO.retrieveEventById(id));
                    // Clean notifications if need be
                    NotificationUtils.cleanNotificationsWhenDeletingEvent(eventDAO, Integer.parseInt(eventDisplayed.getReference()), getApplicationContext());
                    updateDisplayEvent.displayHorizontalFragment(0);
                    // Display rollback option
                    Snackbar.make(historyPanel,
                            getResources().getString(R.string.repeated_event_text), Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(historyPanel, getResources().getString(R.string.please_select_latest_event), Snackbar.LENGTH_LONG).show();
                }
            }
        });

        // View statistics
        TextView statistics = findViewById(R.id.bottomsheet_text);
        statistics.setOnClickListener(v -> {
            RelativeLayout statisticsLayout = findViewById(R.id.design_bottom_sheet);
            BottomSheetBehavior behavior = BottomSheetBehavior.from(statisticsLayout);
            if(behavior.getState() == BottomSheetBehavior.STATE_EXPANDED)
                BottomSheetBehavior.from(statisticsLayout).setState(BottomSheetBehavior.STATE_COLLAPSED);
            else
                BottomSheetBehavior.from(statisticsLayout).setState(BottomSheetBehavior.STATE_EXPANDED);
        });

        // Update all view
        Long eventId = getIntent().getLongExtra("eventId", 0);
        final Event event = eventDAO.retrieveEventById(eventId);
        eventDisplayed = event;

        for (DraggableListItem item : LasTimeUtils.getOrderedSections(eventDisplayed)) {
            mainContainer.addView(getLayoutInflater().inflate(item.getLayoutId(), null));
        }

        if (eventId > 0) {
            updateDisplayEvent.refresh(event);
        }

        // Call contact
        findViewById(R.id.contact_call_textview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasPermissions(PERMISSIONS.CallPermissions)) {
                    callContact();
                } else {
                    needCallPermissions();
                }
            }
        });

        // Send sms
        findViewById(R.id.contact_sms_textview).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (hasPermissions(PERMISSIONS.ReadContactPermissions)) {
                    sendMessageToContact();
                } else {
                    needReadContactPermissions();
                }
            }
        });

        // Horizontal panel display
        updateDisplayEvent.displayHorizontalFragment(0);

        // Parallax feature
        initParallaxValues();
        startAlphaAnimation(title, 0, View.INVISIBLE);
    }

    /**
     * Nested scrolling feature
     **/
    private void initParallaxValues() {
        CollapsingToolbarLayout.LayoutParams frameCollapsing =
                (CollapsingToolbarLayout.LayoutParams) frameParallax.getLayoutParams();
        frameCollapsing.setParallaxMultiplier(0.4f);
        frameParallax.setLayoutParams(frameCollapsing);
    }

    @Override
    /** Nested scrolling feature **/
    public void onOffsetChanged(AppBarLayout appBarLayout, int offset) {
        int maxScroll = appBarLayout.getTotalScrollRange();
        float percentage = (float) Math.abs(offset) / (float) maxScroll;

        handleAlphaOnTitle(percentage);
        handleToolbarTitleVisibility(percentage);
    }

    /**
     * Nested scrolling feature
     **/
    private void handleToolbarTitleVisibility(float percentage) {
        if (percentage >= PERCENTAGE_TO_SHOW_TITLE_AT_TOOLBAR) {

            if (!isTheTitleVisible) {
                startAlphaAnimation(title, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                isTheTitleVisible = true;
            }

        } else {

            if (isTheTitleVisible) {
                startAlphaAnimation(title, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                isTheTitleVisible = false;
            }
        }
    }

    /**
     * Nested scrolling feature
     **/
    private void handleAlphaOnTitle(float percentage) {
        if (percentage >= PERCENTAGE_TO_HIDE_TITLE_DETAILS) {
            if (isTheTitleContainerVisible) {
                startAlphaAnimation(titleContainer, ALPHA_ANIMATIONS_DURATION, View.INVISIBLE);
                isTheTitleContainerVisible = false;
            }

        } else {

            if (!isTheTitleContainerVisible) {
                startAlphaAnimation(titleContainer, ALPHA_ANIMATIONS_DURATION, View.VISIBLE);
                isTheTitleContainerVisible = true;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        updateDisplayEvent.updateMenuForNavigationDisplay();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if ((item.getItemId() == android.R.id.home))
            onBackPressed();
        return true;
    }

    /**
     * Get modified event to refresh view
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (resultCode == RESULT_OK) {
            Long eventId = data.getLongExtra("eventId", 0);
            int eventPosition = data.getIntExtra("eventPosition", 0);
            if (eventId > 0) {
                updateDisplayEvent.refresh(eventDAO.retrieveEventById(eventId));
                // Refresh fragment (by creating it again)
                updateDisplayEvent.displayHorizontalFragment(eventPosition);
            }
        }
        if (resultCode == DELETE_OK) {
            eventId = data.getLongExtra("eventId", 0);
            if (eventId > 0) {
                deleteEvent(eventId);
            }
        }
        if (resultCode == DELETE_ALL_OK) {
            setResult(DisplayEventActivity.DELETE_ALL_OK, null);
            finish();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_display_event, menu);
        updateDisplayEvent.updateMenuForNavigationDisplay();
        return true;
    }

    /**
     * Call delete fragment and set values to fragment
     *
     * @param item
     */
    public void deleteEvent(MenuItem item) {
        if (getEventDisplayed().getState().equals(ACTIVE)) {
            DeleteChoiceDialog deleteChoiceDialog = new DeleteChoiceDialog();
            deleteChoiceDialog.setEventDAO(this.eventDAO);
            deleteChoiceDialog.setEvent(getEventDisplayed());
            // In case no history exists
            if (eventDAO.findHistoryOfEvent(getEventDisplayed()).isEmpty())
                deleteChoiceDialog.forceDeleteChoiceOnly();
            deleteChoiceDialog.show(this.getFragmentManager(), DeleteChoiceDialog.class.getName());
        } else if (getEventDisplayed().getState().equals(OLD)) {
            // Mark event to be deleted in db
            eventId = getEventDisplayed().getId();
            deleteEvent(eventId);
        }
    }

    /**
     * Call go to event location as itinerary
     *
     * @param item
     */
    public void goToLocation(MenuItem item) {
        if (getEventDisplayed().getLocation() != -1) {
            EventLocation location = eventDAO.getLocationByEvent(getEventDisplayed());
            Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                    Uri.parse("http://maps.google.com/maps?daddr=" + location.getLatitude() + "," + location.getLongitude()));
            startActivity(intent);
        } else {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.no_loc_no_nav), Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteEvent(long id) {
        eventDAO.markEventToBeDeleted(id);

        // Display rollback option
        Snackbar snackbar = Snackbar.make(historyPanel,
                getString(R.string.deleted_event_text), Snackbar.LENGTH_LONG);
        snackbar.setAction(R.string.cancel, new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                revertDeleteOld();
                Snackbar.make(historyPanel,
                        getResources().getString(R.string.event_restored),
                        Snackbar.LENGTH_LONG).show();

                // Only need to refresh history panel
                updateDisplayEvent.displayHorizontalFragment(0);
            }
        });
        snackbar.show();

        // Select active event to display
        updateDisplayEvent.refresh(eventDAO.getActiveEvent(getEventDisplayed()));
        updateDisplayEvent.displayHorizontalFragment(0);
    }

    private void revertDeleteOld() {
        // Restore old state without forgetting to update Id
        if (eventId > 0)
            eventDAO.markEventToOld(eventId);
        eventId = -1;
    }

    /**
     * Listener method updated by delete fragment
     */
    @Override
    public void updateActivity(Event event, int referenceToDelete) {
        if (event != null) {
            updateDisplayEvent.refresh(event);
            updateDisplayEvent.displayHorizontalFragment(0);
            Snackbar.make(historyPanel, getString(R.string.deleted_event_text), Snackbar.LENGTH_LONG).show();
            // Clean notifs
            NotificationUtils.cleanNotificationsWhenDeletingEvent(eventDAO, referenceToDelete, this);
        } else {
            setResult(DisplayEventActivity.DELETE_ALL_OK, null);
            // Clean notifs
            NotificationUtils.cleanNotificationsWhenDeletingEvent(eventDAO, referenceToDelete, this);
            // Finish
            this.finish();
        }
    }

    public void goToActiveEvent(MenuItem item) {
        updateDisplayEvent.refresh(eventDAO.findActiveEvent(eventDisplayed));
        updateDisplayEvent.displayHorizontalFragment(0);
        if (menu != null)
            menu.findItem(R.id.back_to_active_event).setVisible(false);
    }

    /**
     * Call contact
     **/
    private void callContact() {
        try {
            if (eventDisplayed.getContact() != -1) {
                Contact contact = eventDAO.getContactById(eventDisplayed.getContact());
                Intent callIntent = new Intent(Intent.ACTION_CALL);
                callIntent.setData(Uri.parse("tel:" + contact.getPhoneNumber()));
                startActivity(callIntent);
            } else {
                // TODO popup
            }
        } catch (ActivityNotFoundException activityException) {
            Log.e(TAG, "Call failed", activityException);
            ACRA.getErrorReporter().handleException(activityException);
        }
    }

    /**
     * Send SMS to contact
     **/
    private void sendMessageToContact() {
        if (eventDisplayed.getContact() != -1) {
            Contact contact = eventDAO.getContactById(eventDisplayed.getContact());
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:" + Uri.encode(contact.getPhoneNumber())));
            startActivity(intent);
        } else {
            // TODO popup
        }

    }

    /** **/
    public void updateContactInfoAgainstPhoneData() {
        // Get data
        Contact appContact = eventDAO.getContactById(eventDisplayed.getContact());
        Contact phoneContact = new Contact(appContact.getPhoneId(), this);
        // Set it
        appContact.setContactName(phoneContact.getContactName());
        appContact.setPhoneNumber(phoneContact.getPhoneNumber());
        // Update DB
        eventDAO.updateContact(appContact);
        // Refresh display
        updateDisplayEvent.refresh(eventDisplayed);
        // Notify
        Snackbar.make(findViewById(R.id.historyPanel), R.string.contact_desync_action_sync_from_phone, Snackbar.LENGTH_LONG).show();
    }

    /** **/
    public void updateContactPhone() {
        Contact appContact = eventDAO.getContactById(eventDisplayed.getContact());

        int id = -1;

        try {
            id = createContact(appContact.getContactName(), appContact.getPhoneNumber());
        } catch (Exception e) {
            Log.e(TAG, "Error occured trying to create contact in phone", e);
            ACRA.getErrorReporter().handleException(e);
        }

        if (id > -1) {
            // Sync it
            appContact.setPhoneId(String.valueOf(id));
            // Update DB
            eventDAO.updateContact(appContact);
            // Refresh display
            updateDisplayEvent.refresh(eventDisplayed);
            // Notify
            Snackbar.make(findViewById(R.id.historyPanel), "Contact added in your phone", Snackbar.LENGTH_LONG).show();
        }
    }

    /**
     * Method to insert contact in address book
     *
     * @param name
     * @param phone
     * @throws RemoteException
     * @throws OperationApplicationException
     */
    private int createContact(String name, String phone) throws RemoteException, OperationApplicationException {
        ArrayList<ContentProviderOperation> ops = new ArrayList<>();
        ops.add(ContentProviderOperation.newInsert(
                ContactsContract.RawContacts.CONTENT_URI)
                .withValue(ContactsContract.RawContacts.ACCOUNT_TYPE, null)
                .withValue(ContactsContract.RawContacts.ACCOUNT_NAME, null)
                .build());
        // Names
        if (name != null) {
            ops.add(ContentProviderOperation.newInsert(
                    ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.StructuredName.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.StructuredName.DISPLAY_NAME, name).build());
        }

        // Mobile Number
        if (phone != null) {
            ops.add(ContentProviderOperation.
                    newInsert(ContactsContract.Data.CONTENT_URI)
                    .withValueBackReference(ContactsContract.Data.RAW_CONTACT_ID, 0)
                    .withValue(ContactsContract.Data.MIMETYPE, ContactsContract.CommonDataKinds.Phone.CONTENT_ITEM_TYPE)
                    .withValue(ContactsContract.CommonDataKinds.Phone.NUMBER, phone)
                    .withValue(ContactsContract.CommonDataKinds.Phone.TYPE, ContactsContract.CommonDataKinds.Phone.TYPE_MOBILE)
                    .build());
        }

        ContentProviderResult[] res = getContentResolver().applyBatch(ContactsContract.AUTHORITY, ops);
        if (res != null && res[0] != null) {
            return Integer.parseInt(String.valueOf(res[0].uri.getLastPathSegment()));
        }
        return -1;
    }

    private void chooseGraph(){
        GraphSelectionFragment picker = new GraphSelectionFragment();
        Bundle bundle = new Bundle();
        //bundle.putString("caller", DatePickerFragment.CALLER.DATE.name());
        //picker.setArguments(bundle);
        picker.show(getFragmentManager(), "chooseGraph");
    }

    // *************************** PERMISSIONS BUSINESS ************************************

    @Override
    public void callPermissionsGranted() {
        callContact();
    }

    @Override
    public void callPermissionsDenied() {
        Snackbar.make(findViewById(R.id.historyPanel), R.string.call_permissions_info_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void readContactPermissionsGranted() {
        updateDisplayEvent.refresh(getEventDisplayed());
    }

    @Override
    public void readContactPermissionsDenied() {
        Snackbar.make(findViewById(R.id.historyPanel), R.string.contact_permissions_info_message, Snackbar.LENGTH_LONG).show();
    }

    // *************************** Getter/setter ********************************


    public Event getEventDisplayed() {
        return eventDisplayed;
    }

    protected void setEventDisplayed(Event event) {
        eventDisplayed = event;
    }

    protected CollapsingToolbarLayout getCollapsingToolbar() {
        return collapsingToolbar;
    }

    protected IEventDAO getEventDAO() {
        return eventDAO;
    }

    protected Menu getMenu() {
        return menu;
    }

    protected boolean isHistoryPanelShown() {
        return isHistoryPanelShown;
    }

    protected void setIsHistoryPanelShown(boolean isHistoryPanelShown) {
        this.isHistoryPanelShown = isHistoryPanelShown;
    }

    public Button getBackToActiveButton() {
        return backToActiveButton;
    }

    public LinearLayout getHistoryPanel() {
        return historyPanel;
    }

    public UpdateDisplayEvent getUpdateDisplayEvent() {
        return updateDisplayEvent;
    }

    public void setEventPosition(int position) {
        eventPosition = position;
    }

    public GraphView getGraph(){
        return this.graph;
    }

}
