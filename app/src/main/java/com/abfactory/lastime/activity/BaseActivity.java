package com.abfactory.lastime.activity;

import android.Manifest;
import android.app.AlertDialog;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.abfactory.lastime.Lastime;
import com.abfactory.lastime.R;
import com.abfactory.lastime.utils.PreferencesHandler;

/**
 * Parent class of all LastTime activities
 */
public abstract class BaseActivity extends AppCompatActivity {

    private static final int READ_PERMISSIONS_REQUEST_CODE = 900;
    private static final int WRITE_PERMISSIONS_REQUEST_CODE = 901;
    private static final int WRITE_CONTACT_PERMISSIONS_REQUEST_CODE = 902;
    private static final int LOCATION_PERMISSIONS_REQUEST_CODE = 903;
    private static final int CAMERA_PERMISSIONS_REQUEST_CODE = 904;
    private static final int CALL_PERMISSIONS_REQUEST_CODE = 905;
    private static final int GET_ACCOUNT_PERMISSIONS_REQUEST_CODE = 906;
    private static final int READ_CONTACT_PERMISSIONS_REQUEST_CODE = 907;
    private static final int NETWORK_PERMISSIONS_REQUEST_CODE = 908;
    private PreferencesHandler preferencesHandler;
    private String TAG;
    private String version;
    // [TUTORIAL SPECIFIC]
    private boolean isTutorialModeActivated = false;
    public static final String TOUR_TAG = "IS_TUTORIAL_MODE";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Init tracker
        Lastime application = (Lastime) getApplication();
        version = "v" + application.getAppVersion();
        preferencesHandler = new PreferencesHandler(this);
        TAG = this.getClass().getName();

        // Set white background app
        getWindow().getDecorView().setBackgroundColor(Color.WHITE);
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    /**
     * get PreferenceHandler
     **/
    public PreferencesHandler getPreferences() {
        return this.preferencesHandler;
    }

    /**
     * get current version of app
     **/
    public String getVersion() {
        return this.version;
    }

    /**
     * get tag for logging
     **/
    public String getTAG() {
        return this.TAG;
    }

    /**
     * Show a dialog box with error information
     *
     * @param message
     */
    public void showErrorMessage(String message) {
        showAlertBox(getString(R.string.error_alert_title), message);
    }

    protected void showAlertBox(String title, String message) {
        AlertDialog.Builder bld = new AlertDialog.Builder(this);
        bld.setTitle(title);
        bld.setMessage(message);
        bld.setNeutralButton("OK", null);
        bld.create().show();
    }

    private void checkPermission(PERMISSIONS permissions) {
        int hasPermissionsRequested = checkSelfPermission(permissions.getPermission());
        // Not always ask user's permissions for now
        if (hasPermissionsRequested != PackageManager.PERMISSION_GRANTED) {
            this.requestPermissions(new String[]{permissions.getPermission()},
                    permissions.getRequestCode());
            return;
        }
        switch (permissions) {
            case WritePermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    writePermissionsDenied();
                } else {
                    writePermissionsGranted();
                }
                break;
            case ReadPermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    readPermissionsDenied();
                } else {
                    readPermissionsGranted();
                }
                break;
            case WriteContactPermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    writeContactPermissionsDenied();
                } else {
                    writeContactPermissionsGranted();
                }
                break;
            case ReadContactPermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    readContactPermissionsDenied();
                } else {
                    readContactPermissionsGranted();
                }
                break;
            case LocationPermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    locationPermissionsDenied();
                } else {
                    locationPermissionsGranted();
                }
                break;
            case CameraPermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    cameraPermissionsDenied();
                } else {
                    cameraPermissionsGranted();
                }
                break;
            case CallPermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    callPermissionsDenied();
                } else {
                    callPermissionsGranted();
                }
                break;
            case GetAccountPermissions:
                if (isAuthorized(hasPermissionsRequested)) {
                    getAccountPermissionsDenied();
                } else {
                    getAccountPermissionsGranted();
                }
                break;
            default:
                break;
        }
    }

    /**
     * Check if write permission has been granted
     * Only Marshmallow release or above
     **/
    public void needWritePermissions() {
        this.checkPermission(PERMISSIONS.WritePermissions);
    }

    /**
     * Check if read permission has been granted
     * Only Marshmallow release or above
     **/
    public void needReadPermissions() {
        this.checkPermission(PERMISSIONS.ReadPermissions);
    }

    /**
     * Check if write contact permission has been granted
     * Only Marshmallow release or above
     **/
    public void needWriteContactPermissions() {
        this.checkPermission(PERMISSIONS.WriteContactPermissions);
    }

    /**
     * Check if read contact permission has been granted
     * Only Marshmallow release or above
     **/
    public void needReadContactPermissions() {
        this.checkPermission(PERMISSIONS.ReadContactPermissions);
    }

    /**
     * Check if location permission has been granted
     * Only Marshmallow release or above
     **/
    public void needLocationPermissions() {
        this.checkPermission(PERMISSIONS.LocationPermissions);
    }

    /**
     * Check if camera permission has been granted
     * Only Marshmallow release or above
     **/
    public void needCameraPermissions() {
        this.checkPermission(PERMISSIONS.CameraPermissions);
    }

    /**
     * Check if call phone permission has been granted
     * Only Marshmallow release or above
     **/
    public void needCallPermissions() {
        this.checkPermission(PERMISSIONS.CallPermissions);
    }

    /**
     * Check if get account permission has been granted
     * Only Marshmallow release or above
     **/
    public void needGetAccountPermissions() {
        this.checkPermission(PERMISSIONS.GetAccountPermissions);
    }

    /**
     * Generic method to only check permissions state
     * Will always return true under SDK 23 build
     */
    public boolean hasPermissions(PERMISSIONS hasPermissionsRequested) {
        return !(Build.VERSION.SDK_INT >= 23 && checkSelfPermission(hasPermissionsRequested.getPermission()) != PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case READ_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    readPermissionsGranted();
                } else {
                    // Permission Denied
                    readPermissionsDenied();
                }
                break;
            case WRITE_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    writePermissionsGranted();
                } else {
                    // Permission Denied
                    writePermissionsDenied();
                }
                break;
            case WRITE_CONTACT_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    writeContactPermissionsGranted();
                } else {
                    // Permission Denied
                    writeContactPermissionsDenied();
                }
                break;
            case READ_CONTACT_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    readContactPermissionsGranted();
                } else {
                    // Permission Denied
                    readContactPermissionsDenied();
                }
                break;
            case LOCATION_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    locationPermissionsGranted();
                } else {
                    // Permission Denied
                    locationPermissionsDenied();
                }
                break;
            case CAMERA_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    cameraPermissionsGranted();
                } else {
                    // Permission Denied
                    cameraPermissionsDenied();
                }
                break;
            case CALL_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    callPermissionsGranted();
                } else {
                    // Permission Denied
                    callPermissionsDenied();
                }
                break;
            case GET_ACCOUNT_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    getAccountPermissionsGranted();
                } else {
                    // Permission Denied
                    getAccountPermissionsDenied();
                }
                break;
            case NETWORK_PERMISSIONS_REQUEST_CODE:
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Permission Granted
                    getNetworkPermissionsGranted();
                } else {
                    // Permission Denied
                    getNetworkPermissionsDenied();
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    /**
     * Method to be overridden
     **/
    public void writePermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void writePermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void readPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void readPermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void writeContactPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void writeContactPermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void readContactPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void readContactPermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void locationPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void locationPermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void cameraPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void cameraPermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void callPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void callPermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void getAccountPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void getAccountPermissionsDenied() {
    }

    /**
     * Method to be overridden
     **/
    public void getNetworkPermissionsGranted() {
    }

    /**
     * Method to be overridden
     **/
    public void getNetworkPermissionsDenied() {
    }

    /**
     * Tutorial data common to all activities - tour guide handler + knowledge of is we ar in tuto mode or not
     */

    public boolean isTutorialModeActivated() {
        return isTutorialModeActivated;
    }

    public boolean isAuthorized(int hasPermissionsRequested) {
        return Build.VERSION.SDK_INT >= 23 && hasPermissionsRequested != PackageManager.PERMISSION_GRANTED;
    }

    public void setTutorialModeActivated(boolean isTutorialModeActivated) {
        this.isTutorialModeActivated = isTutorialModeActivated;
    }

    public enum PERMISSIONS {
        WritePermissions(WRITE_PERMISSIONS_REQUEST_CODE, Manifest.permission.WRITE_EXTERNAL_STORAGE),
        WriteContactPermissions(WRITE_CONTACT_PERMISSIONS_REQUEST_CODE, Manifest.permission.WRITE_CONTACTS),
        ReadContactPermissions(READ_CONTACT_PERMISSIONS_REQUEST_CODE, Manifest.permission.READ_CONTACTS),
        LocationPermissions(LOCATION_PERMISSIONS_REQUEST_CODE, Manifest.permission.ACCESS_FINE_LOCATION),
        CameraPermissions(CAMERA_PERMISSIONS_REQUEST_CODE, Manifest.permission.CAMERA),
        CallPermissions(CALL_PERMISSIONS_REQUEST_CODE, Manifest.permission.CALL_PHONE),
        GetAccountPermissions(GET_ACCOUNT_PERMISSIONS_REQUEST_CODE, Manifest.permission.GET_ACCOUNTS),
        ReadPermissions(READ_PERMISSIONS_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE),
        NetworkPermissions(NETWORK_PERMISSIONS_REQUEST_CODE, Manifest.permission.INTERNET);

        private int requestCode;
        private String permission;

        PERMISSIONS(int requestCode, String permission) {
            this.requestCode = requestCode;
            this.permission = permission;
        }

        public int getRequestCode() {
            return requestCode;
        }

        public String getPermission() {
            return permission;
        }
    }
}
