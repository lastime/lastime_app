package com.abfactory.lastime.activity.edit;

import android.animation.ObjectAnimator;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Parcelable;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.activity.DisplayCustomizationActivity;
import com.abfactory.lastime.activity.LocationsHandlingActivity;
import com.abfactory.lastime.activity.NewLocationActivity;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.component.CustomAutoCompleteView;
import com.abfactory.lastime.component.DeselectableRadioButton;
import com.abfactory.lastime.component.dialogs.ColorPickerSwatch;
import com.abfactory.lastime.component.dialogs.DatePickerFragment;
import com.abfactory.lastime.component.dialogs.DeleteChoiceDialog;
import com.abfactory.lastime.component.dialogs.PhotoActionDialogFragment;
import com.abfactory.lastime.component.dialogs.PickUpColorDialogFragment;
import com.abfactory.lastime.component.dialogs.PickUpLocationDialogFragment;
import com.abfactory.lastime.component.dialogs.RecurrenceDialogFragment;
import com.abfactory.lastime.component.dialogs.ReminderDialogFragment;
import com.abfactory.lastime.component.dialogs.StopTutorialFragment;
import com.abfactory.lastime.component.dialogs.TimePickerFragment;
import com.abfactory.lastime.component.draggablelist.DraggableListItem;
import com.abfactory.lastime.component.gallery.GalleryActivity;
import com.abfactory.lastime.component.tutorial.ShowcaseViewFactory;
import com.abfactory.lastime.model.ColorPaletteElement;
import com.abfactory.lastime.model.Contact;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.model.EventTemplate;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.NotificationUtils;
import com.github.amlcurran.showcaseview.ShowcaseView;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;

import droidninja.filepicker.FilePickerBuilder;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.ACTIVE;
import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.OLD;

/**
 * Class used to add new event
 * Linked to activity_new_event.xml
 */
public class EditEventActivity extends BaseActivity implements DeleteChoiceDialog.DeleteChoiceDialogListener, LocationListener {

    // Menu for camera selection
    protected final static int BUILTIN_GALLERY = 4;
    protected final static int PICKUP_LOCATION = 3;
    //declare globally, this can be any int
    protected static final int PICK_CONTACT = 9999;
    protected static final int FIELD_LIST_UPDATED = 777;
    public static final int LOCATION_LIST_SELECTED = 5;

    private final static String TAG = EditEventActivity.class.getName();
    // Handle response from NewLocationActivity
    EventLocation newLocationActivityResult;
    // Recurrence information
    private String recurrenceTag;
    private String reminderTag;
    private String currentPhotoPath;
    private File thumbnailToStore;
    private File imageToStore;
    private ImageView picture;
    private int currentWeight = 0;
    private boolean isEditMode = false;
    private boolean isCreationFromTemplateMode = false;
    private Event editedEvent;
    private EventTemplate eventTemplateUsed;
    //private int selectedRadioButton = -1;
    // Handle keyboard display
    private boolean actionButtonsDisplayed = true;
    private Contact contact;
    // Color selection
    private ColorPaletteElement selectedColor = null;
    // db handler
    private IEventDAO eventDAO;
    // Class that handle update of all fields
    private UpdateEditEvent updateEditEventActivity;
    // Menu
    private Menu menu;
    private ShowcaseView sv;

    private LinearLayout shown_sections;
    private LinearLayout hidden_sections;

    //GPS stuff
    private LocationManager locationManager;
    private boolean isGPS = false;
    private boolean isNetwork = false;
    private boolean canGetLocation = true;
    private Location currentLocation;

    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10;
    private static final long MIN_TIME_BW_UPDATES = 1000 * 30;

    private int eventPosition;


    private static Comparator<EventLocation> createComparator(EventLocation referencePoint) {
        final EventLocation finalP = new EventLocation(null, referencePoint.getLatitude(), referencePoint.getLongitude(), null, null);
        return new Comparator<EventLocation>() {
            @Override
            public int compare(EventLocation p0, EventLocation p1) {
                double ds0 = LasTimeUtils.gps2m(p0, finalP);
                double ds1 = LasTimeUtils.gps2m(p1, finalP);
                return Double.compare(ds0, ds1);
            }

        };
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (savedInstanceState != null) {
            currentPhotoPath = savedInstanceState.getString("currentPhotoPath");
        }

        LayoutInflater mInflater = LayoutInflater.from(this);
        final View contentView = mInflater.inflate(R.layout.activity_edit_event, null);
        setContentView(contentView);

        // Picture panel
        picture = findViewById(R.id.display_picture);

        // Add dynamically layouts
        shown_sections = contentView.findViewById(R.id.shown_sections);
        hidden_sections = contentView.findViewById(R.id.hidden_sections);

        // Database access
        eventDAO = new EventDAO(this.getApplicationContext());

        // Update class
        updateEditEventActivity = new UpdateEditEvent(this);

        // If coming from DisplayEventActivity or WishList (template creation mode)
        Long eventId = getIntent().getLongExtra("eventId", 0);
        eventPosition = getIntent().getIntExtra("eventPosition", 0);

        if (eventId > 0) {
            editedEvent = eventDAO.retrieveEventById(eventId);
            contentView.findViewById(R.id.more_options).setVisibility(View.VISIBLE);
        } else {
            contentView.findViewById(R.id.more_options).setVisibility(View.GONE);
        }

        // Display fields selection
        contentView.findViewById(R.id.more_options).setOnClickListener(v -> {
            Intent intent = new Intent(this, DisplayCustomizationActivity.class);
            intent.putExtra("eventId", editedEvent.getId());
            startActivityForResult(intent, FIELD_LIST_UPDATED);
        });

        // [TUTORIAL SPECIFIC]
        setTutorialModeActivated(getIntent().getBooleanExtra(BaseActivity.TOUR_TAG, false));
        if (isTutorialModeActivated()) {
            findViewById(R.id.floatingConfirmButton).setEnabled(false);
            (findViewById(R.id.event_title).getViewTreeObserver()).addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    findViewById(R.id.event_title).getViewTreeObserver().removeOnGlobalLayoutListener(this);
                    runTourGuide();
                }
            });
        }
        // [END TUTORIAL SPECIFIC]

        // Animation
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

        // ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.edit_event_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_cancel);
        setSupportActionBar(toolbar);

        // Handle Create button
        final FloatingActionButton createBtn = findViewById(R.id.floatingConfirmButton);
        createBtn.setOnClickListener(new EventCreationClickListener(this));

        // GPS Location
        locationManager = (LocationManager) getSystemService(Service.LOCATION_SERVICE);
        isGPS = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetwork = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
    }

    @Override
    protected void onResume() {
        super.onResume();
        displayFields();
        updateFieldsValues();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // All result business is handle in this class
        new EditEventOnResult(this).doOnResult(requestCode, resultCode, data);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        this.menu = menu;
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_new_event, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public void onBackPressed() {
        if (isTutorialModeActivated()) {
            (new StopTutorialFragment()).show(getFragmentManager(), "stopTutorialDialog");
        } else {
            super.onBackPressed();
        }
    }

    /**
     * Dialog box to get image from camera or gallery
     */
    public void selectImage(View view) {
        if (thumbnailToStore == null && imageToStore == null) {
            PhotoActionDialogFragment.newInstance(PhotoActionDialogFragment.PhotoActionDialogFragmentType.FROM_SCRATCH).show(getFragmentManager(), "photoActionDialog");
        } else {
            PhotoActionDialogFragment.newInstance(PhotoActionDialogFragment.PhotoActionDialogFragmentType.WITH_EXISTING).show(getFragmentManager(), "photoActionDialog");
        }
    }

    public void doPerformSearchOnPhone() {
        needReadPermissions();
    }

    /**
     * Build In photo gallery
     **/
    public void doPerformClickBuiltInPhotos() {
        Intent intent = new Intent(this, GalleryActivity.class);
        startActivityForResult(intent, BUILTIN_GALLERY);
    }

    public void doPerformClickDeletePhoto() {
        // Reset photo
        imageToStore = null;
        thumbnailToStore = null;
        // Update Display
        ((ImageView) findViewById(R.id.display_picture)).setImageDrawable(null);
    }

    /**
     * Open date picker fragment
     */
    public void pickUpDate() {
        DatePickerFragment picker = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("caller", DatePickerFragment.CALLER.DATE.name());
        picker.setArguments(bundle);
        picker.show(getFragmentManager(), "datePicker");
    }

    /**
     * Open end date picker fragment
     */
    public void pickUpEndDate(String initialDate, String minDate) {
        DatePickerFragment picker = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("caller", DatePickerFragment.CALLER.END_DATE.name());
        bundle.putString("initialDate", initialDate);
        bundle.putBoolean("forceNoLimit", true);
        bundle.putString("minDate", minDate);
        picker.setArguments(bundle);
        picker.show(getFragmentManager(), "datePicker");
    }

    /**
     * Open time picker fragment
     */
    public void pickUpTime(String initialTime) {
        TimePickerFragment picker = new TimePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("initialTime", initialTime);
        picker.setArguments(bundle);
        picker.show(getFragmentManager(), "timePicker");
    }

    /**
     * Open time picker fragment
     */
    public void pickUpLocation() {
        needLocationPermissions();

        EventLocation referenceLocation = null;

        final List<EventLocation> locationsList = eventDAO.getAllEventLocations();

        if(currentLocation != null) {
            referenceLocation = EventLocation.parseCurrentLocation(currentLocation);
            // Order by distance to current location
            Collections.sort(locationsList, createComparator(referenceLocation));
        }

        // Add to result
        int numberOfElementsToConsider = 3;
        List<EventLocation> result = new ArrayList<>();
        int i = 0;
        while ((i < numberOfElementsToConsider) && i < locationsList.size()) {
            result.add(locationsList.get(i));
            i++;
        }

        PickUpLocationDialogFragment pickUpLocationPopup = PickUpLocationDialogFragment.newInstance(result, referenceLocation);
        pickUpLocationPopup.show(getFragmentManager(), "pickUpLocation");

    }

    public void useLocationForEvent(EventLocation location) {
        newLocationActivityResult = location;
        ((TextView) findViewById(R.id.location_text)).setText(newLocationActivityResult.getName());
    }

    /**
     * Open activity to choose location
     **/
    public void chooseLocation() {
        Intent intent = new Intent(EditEventActivity.this, NewLocationActivity.class);
        if (currentLocation != null) {
            intent.putExtra("edit_location", (Parcelable) EventLocation.parseCurrentLocation(currentLocation));
        }
        startActivityForResult(intent, PICKUP_LOCATION);
    }

    public void chooseLocationFromList() {
        Intent intent = new Intent(EditEventActivity.this, LocationsHandlingActivity.class);
        intent.putExtra("activateSelection", true);
        startActivityForResult(intent, LOCATION_LIST_SELECTED);
    }

    // ******************* Menu buttons methods ***********************

    /**
     * Call delete fragment and set values to fragment
     *
     * @param item is not used
     */
    public void deleteEvent(MenuItem item) {
        if (editedEvent.getState().equals(ACTIVE)) {
            DeleteChoiceDialog deleteChoiceDialog = new DeleteChoiceDialog();
            deleteChoiceDialog.setEventDAO(this.eventDAO);
            deleteChoiceDialog.setEvent(editedEvent);
            deleteChoiceDialog.show(this.getFragmentManager(), DeleteChoiceDialog.class.getName());
        } else if (editedEvent.getState().equals(OLD)) {
            Intent returnIntent = new Intent();
            returnIntent.putExtra("eventId", editedEvent.getId());
            setResult(DisplayEventActivity.DELETE_OK, returnIntent);
            finish();
        }
    }

    /**
     * Clean all fields
     *
     * @param item is not used
     */
    public void cleanAllFields(MenuItem item) {
        this.updateEditEventActivity.cleanAllFields();
        /* No need to hide it anymore
        if (menu != null)
            menu.findItem(R.id.undo_prefill).setVisible(false);*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        if (id == android.R.id.home) {
            finish();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void doPerformRecurrenceUpdate(String recurrenceTag) {
        if (LasTimeUtils.isReminderActivated(reminderTag)
                && LasTimeUtils.isRecurrenceActivated(recurrenceTag)
                && LasTimeUtils.areValidsRecurrenceAndReminder(recurrenceTag, reminderTag, (getEditedEvent() == null ? new Date() : getEditedEvent().getDate()))) {
            Snackbar.make(findViewById(R.id.floatingConfirmButton), getResources().getString(R.string.rec_greater_reminder), Snackbar.LENGTH_SHORT).show();
        } else {
            this.recurrenceTag = recurrenceTag;
            ((TextView) findViewById(R.id.event_recurrence)).setText(LasTimeUtils.convertRecurrenceTagToString(recurrenceTag, this));
        }
    }

    public void doPerformReminderUpdate(String reminderTag) {
        if (LasTimeUtils.isReminderActivated(reminderTag)
                && LasTimeUtils.isRecurrenceActivated(recurrenceTag)
                && LasTimeUtils.areValidsRecurrenceAndReminder(recurrenceTag, reminderTag, (getEditedEvent() == null ? new Date() : getEditedEvent().getDate()))) {
            Snackbar.make(findViewById(R.id.floatingConfirmButton), getResources().getString(R.string.reminder_smaller_rec), Snackbar.LENGTH_SHORT).show();
        } else {
            this.reminderTag = reminderTag;
            ((TextView) findViewById(R.id.event_remind)).setText(LasTimeUtils.convertReminderTagToString(reminderTag, this));
        }
    }

    public void doPerformClickDeleteLocation() {
        newLocationActivityResult = null;
        (findViewById(R.id.map_layout)).setVisibility(View.GONE);
        ((TextView) findViewById(R.id.location_text)).setText(null);
    }

    @Override
    public void updateActivity(Event event, int referenceToDeleteForNotif) {
        setResult(DisplayEventActivity.DELETE_ALL_OK, null);
        // Clean notifs
        NotificationUtils.cleanNotificationsWhenDeletingEvent(eventDAO, referenceToDeleteForNotif, this);
        // Finish
        finish();
    }

    public void handleColorPanelDisplay() {
        if (selectedColor != null) {
            findViewById(R.id.edit_event_toolbar).setBackgroundColor(selectedColor.getId());
            ((ImageView) findViewById(R.id.img_color)).setColorFilter(selectedColor.getId());
            ((TextView) findViewById(R.id.event_color)).setText(selectedColor.getDisplayableName());
        } else {
            ((ImageView) findViewById(R.id.img_color)).setColorFilter(Color.parseColor(ColorPaletteElement.DEFAULT_COLOR));
            ((TextView) findViewById(R.id.event_color)).setText("");
        }
    }

    // *************************** PERMISSIONS BUSINESS ************************************

    @Override
    public void locationPermissionsGranted() {
        getUserLocation();
    }

    @Override
    public void locationPermissionsDenied() {
        Snackbar.make(findViewById(R.id.floatingConfirmButton), R.string.location_permissions_info_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void writeContactPermissionsGranted() {
        Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.CommonDataKinds.Phone.CONTENT_URI);
        startActivityForResult(i, PICK_CONTACT);
    }

    @Override
    public void writeContactPermissionsDenied() {
        Snackbar.make(findViewById(R.id.floatingConfirmButton), R.string.contact_permissions_info_message, Snackbar.LENGTH_LONG).show();
    }

    @Override
    public void readPermissionsGranted() {
        FilePickerBuilder.getInstance().setMaxCount(1)
                .setActivityTheme(R.style.AppTheme)
                .pickPhoto(this);
    }

    @Override
    public void readPermissionsDenied() {
        Snackbar.make(findViewById(R.id.floatingConfirmButton), R.string.read_permissions_required, Snackbar.LENGTH_LONG).show();
    }

    // ********************** Tutorial ***********************

    private void runTourGuide() {

        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_EVENT_NAME, this);

        (findViewById(R.id.event_title)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {

                ((InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE)).hideSoftInputFromWindow((findViewById(R.id.event_title)).getWindowToken(), 0);

                //SIMULATE WRITING
                ((CustomAutoCompleteView) findViewById(R.id.event_title)).animateText(getString(R.string.tour_fake_event_name), 100);

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sv.hide();
                        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_EVENT_RECURRENCE, EditEventActivity.this);
                    }
                }, 3000);

            }
        });

        ((TextView) findViewById(R.id.event_recurrence)).addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                sv.hide();
                sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_EVENT_MORE_PARAMS, EditEventActivity.this);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        (findViewById(R.id.more_options)).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.hide();

                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        findViewById(R.id.hidden_sections).setVisibility(View.VISIBLE);
                        findViewById(R.id.more_options).setVisibility(View.GONE);
                    }
                }, 200);

                findViewById(R.id.scrollViewEditEvent).post(new Runnable() {
                    @Override
                    public void run() {
                        ObjectAnimator.ofInt(((ScrollView) findViewById(R.id.scrollViewEditEvent)), "scrollY", ((ScrollView) findViewById(R.id.scrollViewEditEvent)).getHeight()).setDuration(1500).start();
                    }
                });

                (new Handler()).postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_EVENT_COLOR, EditEventActivity.this);
                    }
                }, 1200);
            }
        });

        ((TextView) findViewById(R.id.event_color)).addTextChangedListener(new TextWatcher() {

            @Override
            public void afterTextChanged(Editable s) {
                findViewById(R.id.floatingConfirmButton).setEnabled(true);
                sv.hide();
                sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_CREATE_EVENT_TOUR_EVENT_CREATION_OK, EditEventActivity.this);
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

    }

    //---------------------- Getter/setter to access activity members -------------------------

    public boolean isEditMode() {
        return this.isEditMode;
    }

    public boolean isCreationFromTemplateMode() {
        return this.isCreationFromTemplateMode;
    }

    public EventTemplate getEventTemplateUsed() {
        return this.eventTemplateUsed;
    }

    public Event getEditedEvent() {
        return this.editedEvent;
    }

    public IEventDAO getEventDAO() {
        return this.eventDAO;
    }

    public ImageView getPicture() {
        return picture;
    }

    public File getThumbnailToStore() {
        return thumbnailToStore;
    }

    // In case of prefilled event, see update in LastTimeUtils
    public void setThumbnailToStore(File thumbnailToStore) {
        this.thumbnailToStore = thumbnailToStore;
    }

    public File getImageToStore() {
        return imageToStore;
    }

    // In case of prefilled event, see update in LastTimeUtils
    protected void setImageToStore(File imageToStore) {
        this.imageToStore = imageToStore;
    }

    public String getCurrentPhotoPath() {
        return currentPhotoPath;
    }

    public void setCurrentPhotoPath(String currentPhotoPath) {
        this.currentPhotoPath = currentPhotoPath;
    }

    /*
     * Determine mark for event (-1 means unset)
     */
    public int getMarkForEvent() {
        int mark = -1;
        switch (((RadioGroup) findViewById(R.id.radioFeeling)).getCheckedRadioButtonId()) {
            case R.id.radiogood:
                mark = 1;
                break;
            case R.id.radioaverage:
                mark = 2;
                break;
            case R.id.radiobad:
                mark = 3;
                break;
            default:
                break;
        }
        return mark;
    }

    /*
 * Determine weather for event (-1 means unset)
 */
    public int getWeatherForEvent() {
        int weather = -1;
        switch (((RadioGroup) findViewById(R.id.radioWeather)).getCheckedRadioButtonId()) {
            case R.id.radioSunny:
                weather = 1;
                break;
            case R.id.radioCloudy:
                weather = 2;
                break;
            case R.id.radioWindy:
                weather = 3;
                break;
            case R.id.radioRaining:
                weather = 4;
                break;
            default:
                break;
        }
        return weather;
    }

    @Override
    public void onSaveInstanceState(Bundle bundle) {
        super.onSaveInstanceState(bundle);
        bundle.putString("currentPhotoPath", currentPhotoPath);
    }

    public Contact getContact() {
        return contact;
    }

    public void setContact(Contact contact) {
        this.contact = contact;
    }

    public String getRecurrenceTag() {
        return recurrenceTag;
    }

    public String getReminderTag() {
        return reminderTag;
    }

    public ColorPaletteElement getSelectedColor() {
        return selectedColor;
    }

    public int getCurrentWeight() {
        return currentWeight;
    }

    /**
     * Called by fragment only
     **/
    public void setCurrentWeight(int currentWeight) {
        this.currentWeight = currentWeight;
    }

    public EventLocation getNewLocationActivityResult() {
        return newLocationActivityResult;
    }

    public void setNewLocationActivityResult(EventLocation newLocationActivityResult) {
        this.newLocationActivityResult = newLocationActivityResult;
    }

    public Menu getMenu() {
        return menu;
    }

    public int getEventPosition() {
        return this.eventPosition;
    }

    private void displayFields() {
        shown_sections.removeAllViews();
        // Handle items shown by default
        List<DraggableListItem> itemsToBeDisplayed = LasTimeUtils.getOrderedSectionsToBeDisplayed(editedEvent);
        Iterator<DraggableListItem> shownItemsIterator = itemsToBeDisplayed.iterator();
        while (shownItemsIterator.hasNext()) {
            shown_sections.addView(getLayoutInflater().inflate(shownItemsIterator.next().getLayoutId(), null));
            if (shownItemsIterator.hasNext()) {
                shown_sections.addView(getLayoutInflater().inflate(R.layout.section_separator, null));
            }
        }

        hidden_sections.removeAllViews();
        List<DraggableListItem> itemsToBeHidden = LasTimeUtils.getOrderedSectionsToBeHidden(editedEvent);
        if (itemsToBeHidden != null && !itemsToBeHidden.isEmpty()) {
            // Handle items hidden by default
            Iterator<DraggableListItem> hiddenItemsIterator = itemsToBeHidden.iterator();
            hidden_sections.addView(getLayoutInflater().inflate(R.layout.section_separator, null));
            int layoutId;
            while (hiddenItemsIterator.hasNext()) {
                layoutId = hiddenItemsIterator.next().getLayoutId();
                hidden_sections.addView(getLayoutInflater().inflate(layoutId, null));
                if (hiddenItemsIterator.hasNext()) {
                    hidden_sections.addView(getLayoutInflater().inflate(R.layout.section_separator, null));
                }
            }
        }

        applyLogicOnFiels();
    }

    private void applyLogicOnFiels() {
        // Fragment call on time and date
        findViewById(R.id.event_date_text).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                LasTimeUtils.hideKeyboard(EditEventActivity.this);
                EditEventActivity.this.pickUpDate();
            }
        });

        // Date handling
        updateEditEventActivity.updateDateDisplay(Calendar.getInstance(Locale.getDefault()).getTime());

        //Mark button
        CompoundButton.OnCheckedChangeListener onCheckedChangeListener = new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                if (b)
                    compoundButton.setAlpha(1);
                else
                    compoundButton.setAlpha(0.2f);
            }
        };

        DeselectableRadioButton radioGood = (DeselectableRadioButton) findViewById(R.id.radiogood);
        radioGood.setOnCheckedChangeListener(onCheckedChangeListener);
        DeselectableRadioButton radioAverage = (DeselectableRadioButton) findViewById(R.id.radioaverage);
        radioAverage.setOnCheckedChangeListener(onCheckedChangeListener);
        DeselectableRadioButton radioBad = (DeselectableRadioButton) findViewById(R.id.radiobad);
        radioBad.setOnCheckedChangeListener(onCheckedChangeListener);

        findViewById(R.id.event_time_text).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                LasTimeUtils.hideKeyboard(EditEventActivity.this);
                EditEventActivity.this.pickUpTime(((TextView) findViewById(R.id.event_time_text)).getText().toString());
            }
        });

        // Fragment call on end date
        findViewById(R.id.event_end_date_text).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                LasTimeUtils.hideKeyboard(EditEventActivity.this);
                if (((TextView) EditEventActivity.this.findViewById(R.id.event_end_date_text)).getText() != null
                        && ((TextView) EditEventActivity.this.findViewById(R.id.event_end_date_text)).getText().length() > 0)
                    EditEventActivity.this.pickUpEndDate(((TextView) findViewById(R.id.event_end_date_text)).getText().toString(),
                            ((TextView) findViewById(R.id.event_date_text)).getText().toString());
                else
                    EditEventActivity.this.pickUpEndDate(((TextView) findViewById(R.id.event_date_text)).getText().toString(),
                            ((TextView) findViewById(R.id.event_date_text)).getText().toString());
            }
        });

        findViewById(R.id.event_recurrence).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                RecurrenceDialogFragment.newInstance(recurrenceTag).show(getFragmentManager(), "recurrencePicker");
            }
        });

        findViewById(R.id.event_remind).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!LasTimeUtils.isRecurrenceActivated(recurrenceTag)) {
                    Snackbar.make(EditEventActivity.this.findViewById(R.id.floatingConfirmButton), EditEventActivity.this.getResources().getString(R.string.recc_mandatory_before_reminder), Snackbar.LENGTH_SHORT).show();
                } else {
                    ReminderDialogFragment.newInstance(reminderTag).show(EditEventActivity.this.getFragmentManager(), "reminderPicker");
                }
            }
        });

        // Implement listener to get selected color value
        final ColorPickerSwatch.OnColorSelectedListener colorCalendarListener;
        colorCalendarListener = new ColorPickerSwatch.OnColorSelectedListener() {
            @Override
            public void onColorSelected(int color) {
                selectedColor = ColorPaletteElement.getColorPaletteElementFromId(color);
                EditEventActivity.this.handleColorPanelDisplay();
            }
        };

        // Change color
        findViewById(R.id.event_color).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                PickUpColorDialogFragment colorCalendar = PickUpColorDialogFragment.newInstance(LasTimeUtils.colorChoice(),
                        (selectedColor != null ? Color.parseColor(selectedColor.getCode()) : -1), 4,
                        PickUpColorDialogFragment.SIZE_SMALL);

                colorCalendar.setOnColorSelectedListener(colorCalendarListener);
                colorCalendar.show(EditEventActivity.this.getFragmentManager(), "cal");

            }
        });

        // Delete contact
        findViewById(R.id.cancelContactButton).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                contact = null;
                ((TextView) EditEventActivity.this.findViewById(R.id.contact_text)).setText("");
                EditEventActivity.this.findViewById(R.id.cancelContactButton).setVisibility(View.GONE);
            }
        });

        // Note field - Add scroll feature
        ((EditText) findViewById(R.id.note_text)).setMaxLines(7);
        findViewById(R.id.note_text).setOnTouchListener(new View.OnTouchListener() {
            public boolean onTouch(final View v, final MotionEvent motionEvent) {
                if (v.getId() == R.id.note_text) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        // Contact
        findViewById(R.id.contact_text).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EditEventActivity.this.needWriteContactPermissions();
            }
        });

        // Button to open locationPicker
        findViewById(R.id.location_text).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                EditEventActivity.this.pickUpLocation();
            }
        });

        // Weather
        CompoundButton.OnCheckedChangeListener onCheckedChangeListenerForWeather = (compoundButton, b) -> {
            if (b)
                compoundButton.setAlpha(1);
            else
                compoundButton.setAlpha(0.4f);
        };
        ((DeselectableRadioButton) findViewById(R.id.radioSunny)).setOnCheckedChangeListener(onCheckedChangeListenerForWeather);
        ((DeselectableRadioButton) findViewById(R.id.radioCloudy)).setOnCheckedChangeListener(onCheckedChangeListenerForWeather);
        ((DeselectableRadioButton) findViewById(R.id.radioWindy)).setOnCheckedChangeListener(onCheckedChangeListenerForWeather);
        ((DeselectableRadioButton) findViewById(R.id.radioRaining)).setOnCheckedChangeListener(onCheckedChangeListenerForWeather);
    }

    private void updateFieldsValues() {
        // It means event is displayed to be modified
        if (editedEvent != null) {
            isEditMode = true;
            updateEditEventActivity.update(editedEvent);
            if (getSupportActionBar() != null)
                getSupportActionBar().setTitle(getString(R.string.edit_your_event));
            if (editedEvent.getLocation() != -1 && editedEvent.getLocation() != 0 && newLocationActivityResult == null) {
                newLocationActivityResult = eventDAO.getLocationByEvent(editedEvent);
            }
            recurrenceTag = editedEvent.getRecurrence();
            reminderTag = editedEvent.getReminder();
            selectedColor = ColorPaletteElement.getColorPaletteElementFromId(editedEvent.getColor());
        }
    }

    /* GPS part */

    @Override
    public void onLocationChanged(Location location) {
        Log.d(TAG, "onLocationChanged");
        currentLocation = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {}

    @Override
    public void onProviderEnabled(String s) {
        getLocation();
    }

    @Override
    public void onProviderDisabled(String s) {
        if (locationManager != null) {
            locationManager.removeUpdates(this);
        }
    }

    private void getUserLocation() {
        if (!isGPS && !isNetwork) {
            Log.d(TAG, "Connection off");
            currentLocation = getLastLocation();
        } else {
            Log.d(TAG, "Connection on");
            currentLocation = getLocation();
        }
    }

    private Location getLocation() {
        try {
            if (canGetLocation) {
                Log.d(TAG, "Can get location");
                if (isGPS) {
                    // from GPS
                    Log.d(TAG, "GPS on");
                    locationManager.requestLocationUpdates(
                            LocationManager.GPS_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        return locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
                    }
                } else if (isNetwork) {
                    // from Network Provider
                    Log.d(TAG, "NETWORK_PROVIDER on");
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    if (locationManager != null) {
                        return locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                    }
                }
            } else {
                Log.d(TAG, "Can't get location");
            }
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Location getLastLocation() {
        Location location = null;
        try {
            Criteria criteria = new Criteria();
            String provider = locationManager.getBestProvider(criteria, false);
            location = locationManager.getLastKnownLocation(provider);
            Log.d(TAG, provider);
            Log.d(TAG, location == null ? "NO LastLocation" : location.toString());
        } catch (SecurityException e) {
            e.printStackTrace();
        }
        return location;
    }

}