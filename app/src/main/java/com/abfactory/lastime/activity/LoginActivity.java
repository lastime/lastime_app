package com.abfactory.lastime.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnTouchListener;
import android.widget.Button;
import android.widget.EditText;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.dialogs.BasicDialogFragment;
import com.abfactory.lastime.component.dialogs.ConfirmPwdResetDialogFragment;
import com.abfactory.lastime.mail.BackgroundMail;
import com.abfactory.lastime.mail.Utils;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.PreferencesHandler;

public class LoginActivity extends AppCompatActivity {

    private final static String LASTIME_CONTACT_MAIL = "lastimeapp.contact@gmail.com";
    private final static String LASTIME_CONTACT_PWD = "n5h8H3XVLMaX4X2jGFCzdg==";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        // Access edit text
        final EditText pwd = (EditText) findViewById(R.id.pwd_input);
        pwd.setOnTouchListener(new OnTouchListener() {
            public boolean onTouch(View v, MotionEvent event) {
                // Do nothing to prevent from showing the keyboard
                return true; // the listener has consumed the event
            }
        });

        // Handle Unlock button
        Button unlockBtn = (Button) findViewById(R.id.unlock_btn);
        unlockBtn.setEnabled(false);
        unlockBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                if (isPasswordValid()) {
                    finish();
                } else {
                    // Show message
                    BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.INVALID_PASSWORD, null).show(getFragmentManager(), "invalidPassword");
                }
            }
        });

        // Handle Unlock button
        Button lostPwdBtn = (Button) findViewById(R.id.lostPwd);
        lostPwdBtn.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                new ConfirmPwdResetDialogFragment().show(getFragmentManager(), "confirmPwdResetDialogFragment");
            }
        });
    }

    private boolean isPasswordValid() {
        // Access Preferences
        PreferencesHandler ph = new PreferencesHandler(getApplicationContext());
        // Access pwd edit text
        String pwd = ((EditText) findViewById(R.id.pwd_input)).getText().toString();

        return pwd.equals(ph.getPasswordForApp());
    }

    public void upDatePwdInputOnButtonClicked(View v) {

        EditText pwd = (EditText) findViewById(R.id.pwd_input);

        if (v.getId() != R.id.unlock_btn) {
            pwd.getText().append(((Button) v).getText());
        }

        Button unlockBtn = (Button) findViewById(R.id.unlock_btn);
        if (pwd.getText().length() > 0) {
            unlockBtn.setEnabled(true);
        } else {
            unlockBtn.setEnabled(false);
        }
    }

    public void clearEditText(View v) {
        clearPwd();
    }

    public void clearPwd() {
        EditText pwd = (EditText) findViewById(R.id.pwd_input);
        pwd.setText("");
        Button unlockBtn = (Button) findViewById(R.id.unlock_btn);
        unlockBtn.setEnabled(false);
    }

    public void doPerformClickResetPwd() {

        PreferencesHandler ph = new PreferencesHandler(getApplicationContext());

        //New pwd
        String resetPwd = LasTimeUtils.generateRandom4DigitsCode();

        // Send email
        BackgroundMail bm = new BackgroundMail(LoginActivity.this);
        bm.setGmailUserName(LASTIME_CONTACT_MAIL);
        bm.setGmailPassword(Utils.decryptIt(LASTIME_CONTACT_PWD));
        bm.setMailTo(ph.getBackUpEmailForApp());
        bm.setFormSubject("[LasTime App] " + getResources().getString(R.string.pwd_reset_email_title));
        bm.setFormBody(getString(R.string.pwd_reset_email_content_part1) + " " + resetPwd + getString(R.string.pwd_reset_email_content_part2));
        bm.setProcessVisibility(false);
        bm.send();

        // Update pwd
        ph.setPassword(resetPwd);

        // Notify user
        BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.PWD_RESET, null).show(getFragmentManager(), "passwordResetDialog");

    }

}
