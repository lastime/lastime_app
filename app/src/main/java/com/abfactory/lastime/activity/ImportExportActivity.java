package com.abfactory.lastime.activity;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.*;

import com.abfactory.lastime.R;
import com.abfactory.lastime.utils.IOEventTask;
import com.github.angads25.filepicker.controller.DialogSelectionListener;
import com.github.angads25.filepicker.model.DialogConfigs;
import com.github.angads25.filepicker.model.DialogProperties;
import com.github.angads25.filepicker.view.FilePickerDialog;

import java.io.File;
import java.io.IOException;

/**
 * Import or export all event content
 */
public class ImportExportActivity extends BaseActivity {

    private final static String TAG = ImportExportActivity.class.getName();
    private TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_io);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_cancel);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        output = (TextView) findViewById(R.id.import_export_output);

        // Select folder + buttons
        Button buttonFolder = (Button) findViewById(R.id.folder_selection_button);
        buttonFolder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                needWritePermissions();
            }
        });

        final Button buttonExport = (Button) findViewById(R.id.export_button);
        buttonExport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File folder = new File(getPreferences().getIOFolderPath());
                if (folder.exists()) {
                    IOEventTask ioEventTask = new IOEventTask(ImportExportActivity.this, IOEventTask.IOTask.EXPORT);
                    ioEventTask.execute(folder);
                } else {
                    Snackbar.make(buttonExport, "Folder is not valid", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        final Button buttonImport = (Button) findViewById(R.id.import_button);
        buttonImport.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                File folder = new File(getPreferences().getIOFolderPath());
                if (folder.exists()) {
                    IOEventTask ioEventTask = new IOEventTask(ImportExportActivity.this, IOEventTask.IOTask.IMPORT);
                    ioEventTask.execute(folder);
                } else {
                    Snackbar.make(buttonExport, "Folder is not valid", Snackbar.LENGTH_LONG).show();
                }
            }
        });

        final Switch switchButton = (Switch) findViewById(R.id.switchAutoSave);
        if (getPreferences().isAutoSaveActivated())
            switchButton.setChecked(true);
        switchButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                if (isChecked) {
                    if (new File(getPreferences().getIOFolderPath()).exists()) {
                        getPreferences().setAutoSaveActivated(true);
                    } else {
                        Snackbar.make(buttonExport, "Folder is not valid", Snackbar.LENGTH_LONG).show();
                        switchButton.setChecked(false);
                    }
                } else {
                    getPreferences().setAutoSaveActivated(false);
                }
            }
        });

        ((TextView) findViewById(R.id.export_path)).setText(getPreferences().getIOFolderPath());

        EditText EtOne = (EditText) findViewById(R.id.import_export_output);
        EtOne.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.import_export_output) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        // Animation
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    public void writePermissionsGranted() {
        DialogProperties properties = new DialogProperties();
        properties.selection_mode = DialogConfigs.SINGLE_MODE;
        properties.selection_type = DialogConfigs.DIR_SELECT;
        properties.root = new File(DialogConfigs.DEFAULT_DIR);
        properties.error_dir = new File(DialogConfigs.DEFAULT_DIR);
        properties.offset = new File(DialogConfigs.DEFAULT_DIR);
        properties.extensions = null;

        FilePickerDialog dialog = new FilePickerDialog(ImportExportActivity.this, properties);
        dialog.setTitle("Select a folder");

        dialog.setDialogSelectionListener(new DialogSelectionListener() {
            @Override
            public void onSelectedFilePaths(String[] files) {
                if (files.length > 0) {
                    try {
                        File fileCreated = new File(files[0], IOEventTask.IO_FILE);
                        if (!fileCreated.exists())
                            fileCreated.createNewFile();
                        getPreferences().setIOFolderPath(files[0].toString());
                        ((TextView) findViewById(R.id.export_path)).setText(files[0].toString());
                    } catch (IOException e) {
                        Log.e(ImportExportActivity.class.getName(), "Error occurred on using file: " + files[0], e);
                        ((TextView) findViewById(R.id.export_path)).setText("Error occurred with selected folder");
                    }
                }
            }
        });
        dialog.show();
    }

    @Override
    public void writePermissionsDenied() {
        ((TextView) findViewById(R.id.export_path)).setText("Write permissions required!!");
    }

    public TextView getOutput() {
        return this.output;
    }

}
