package com.abfactory.lastime.activity.edit;

import android.text.TextUtils;
import android.util.Log;
import android.widget.EditText;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.clearable.ClearableEditText;
import com.abfactory.lastime.model.Event;

import java.io.File;
import java.text.ParseException;
import java.util.Date;

/**
 * Class to check event or modification of event
 */
public class EventChecker {

    private String errorMessage;
    private boolean valid;

    public EventChecker(String errorMessage){
        this.errorMessage = errorMessage;
        this.valid = false;
    }

    public EventChecker(String errorMessage, boolean valid){
        this.errorMessage = errorMessage;
        this.valid = valid;
    }

    public boolean isValid(){
        return valid;
    }

    public String getErrorMessage(){ return errorMessage;}

    public static EventChecker verify(EditEventActivity activity){
        EditText title = (EditText) activity.findViewById(R.id.event_title);
        TextView date = (TextView) activity.findViewById(R.id.event_date_text);
        TextView endDate = (TextView) activity.findViewById(R.id.event_end_date_text);
        TextView time = (TextView) activity.findViewById(R.id.event_time_text);
        ClearableEditText expenses = (ClearableEditText) activity.findViewById(R.id.event_expenses_text);

        // Title must be provided (not empty)
        if(title==null || title.getText()==null || title.getText().toString().length()<1){
            return new EventChecker(activity.getString(R.string.event_title_error));
        }

        // Date must be valid (not empty)
        if(date==null || date.getText()==null || date.getText().toString().length()<1){
            return new EventChecker(activity.getString(R.string.event_date_error));
        }

        // End date should be always after event date
        if(endDate != null && endDate.getText().length() > 0){
            try {
                Date eventStartDate = Event.CALENDAR_DISPLAY.parse(date.getText().toString());
                Date eventEndDate = Event.CALENDAR_DISPLAY.parse(endDate.getText().toString());
                if(eventEndDate.before(eventStartDate))
                    return new EventChecker(activity.getString(R.string.event_end_date_error));
            } catch (ParseException e) {
                Log.e(EventChecker.class.getName(), "Error parsing dates");
            }
        }

        // Check if event already exists (in case of edit mode, avoid doubloon)
        if(activity.isEditMode() && !title.getText().toString().equalsIgnoreCase(activity.getEditedEvent().getTitle())){
            Long existingEventId = activity.getEventDAO().findActiveEventIdByTitle(title.getText().toString());
            if(existingEventId != null) {
                return new EventChecker(activity.getString(R.string.event_title_exists_error));
            }
        }

        if(activity.getThumbnailToStore() != null && !activity.getThumbnailToStore().exists())
            return new EventChecker("Image has been deleted, please select new one!");

        return new EventChecker("Everything fine", true);
    }
}
