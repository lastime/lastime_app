package com.abfactory.lastime.activity;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.calendar.GoogleCalendarActivity;
import com.abfactory.lastime.drawer.DrawerMenu;
import com.abfactory.lastime.mail.Utils;
import com.abfactory.lastime.mail.mail.GmailSender;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.model.Notification;
import com.abfactory.lastime.utils.AssetsPropertiesReader;
import com.abfactory.lastime.utils.NotificationUtils;
import com.abfactory.lastime.utils.PreferencesHandler;

import org.acra.ACRA;

import java.util.Date;
import java.util.Properties;

/**
 * Display several dev options
 */
public class DeveloperActivity extends BaseActivity {

    private DrawerMenu drawerMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_developer);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Drawer
        drawerMenu = new DrawerMenu((DrawerLayout) findViewById(R.id.drawer_layout), this);

        // Nice arrow on top left which spins from the hamburger menu
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerMenu.getDrawerLayout(), toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerMenu.getDrawerLayout().addDrawerListener(drawerToggle);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        drawerToggle.syncState();

        final IEventDAO eventDAO = new EventDAO(this.getApplicationContext());

        // Service execution time
        ((TextView) findViewById(R.id.last_service_execution)).setText(getPreferences().getLastNotificationExecution());

        // Force notification service to run
        //Button clean duplicated events
        final Button executeNotification = (Button) findViewById(R.id.executeNotificationButton);
        executeNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                NotificationUtils.raiseAllRelevantNotifications(DeveloperActivity.this);
                Snackbar.make(executeNotification, "Notification service run", Snackbar.LENGTH_SHORT).show();
            }
        });

        final Switch switchACRAButton = (Switch) findViewById(R.id.switchACRAButton);
        switchACRAButton.setChecked(getPreferences().isACRAActivated());
        switchACRAButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getPreferences().setACRAActivated(isChecked);
                ACRA.getErrorReporter().setEnabled(isChecked);
            }
        });

        final Switch switchGoogleAnalyticButton = (Switch) findViewById(R.id.switchGoogleAnalyticButton);
        switchGoogleAnalyticButton.setChecked(getPreferences().isGoogleAnalyticActivated());
        switchGoogleAnalyticButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                getPreferences().setGoogleAnalyticActivated(isChecked);
            }
        });

        //Button clean duplicated events
        final Button buttonDuplicated = (Button) findViewById(R.id.duplicated_button);
        buttonDuplicated.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventDAO.cleanActiveEventDuplicated();
                Snackbar.make(buttonDuplicated, "Cleanup done", Snackbar.LENGTH_SHORT).show();
            }
        });

        //Button clean notifications
        final Button buttonCleanNotification = (Button) findViewById(R.id.clean_notification_button);
        buttonCleanNotification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Notification notif : eventDAO.getAllNotifications()) {
                    eventDAO.deleteNotification(notif.getNotificationId());
                }
                Snackbar.make(buttonCleanNotification, "Notifications cleaned", Snackbar.LENGTH_SHORT).show();
            }
        });

        //Button clean notifications
        final Button buttonFixContacts = (Button) findViewById(R.id.fix_contacts);
        buttonFixContacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventDAO.migrateContactsToNewFormat(getApplicationContext());
                Snackbar.make(buttonFixContacts, "Contacts fixed !", Snackbar.LENGTH_SHORT).show();
            }
        });

        //Button clean notifications
        final Button buttonResetTuto = (Button) findViewById(R.id.reset_tuto);
        buttonResetTuto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getPreferences().setTutorialPopupToBeShownAtStartup(true);
                Snackbar.make(buttonResetTuto, "Tuto reset done !", Snackbar.LENGTH_SHORT).show();
            }
        });

        //Button clean events defined in futur
        final Button buttonShootNotification = findViewById(R.id.shoot_notif_btn);
        buttonShootNotification.setOnClickListener(v -> {
            Event event = new Event.EventBuilder("Test")
                    .addDate(new Date())
                    .addRecurrence("2_WEEKS")
                    .addReference("123")
                    .build();
            NotificationUtils.createNotification(event, getApplicationContext());
            Snackbar.make(buttonShootNotification, "Notification created", Snackbar.LENGTH_SHORT).show();
        });

        //Button erase all events! Could not be undone!!
        final Button buttonCleanAllEvents = (Button) findViewById(R.id.clean_all_events);
        buttonCleanAllEvents.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                eventDAO.eraseAllEvents();
                Snackbar.make(buttonCleanAllEvents, "All events erased", Snackbar.LENGTH_SHORT).show();
            }
        });

        // Button to reset anonymous login choice pref
        findViewById(R.id.reset_anonymous_login_choice_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (new PreferencesHandler(getApplicationContext())).setAnonymousLoginChosen(false);
                Snackbar.make(buttonCleanAllEvents, "Reset done !", Snackbar.LENGTH_SHORT).show();
            }
        });

        // Button to launch Google calendar dev activity
        findViewById(R.id.launch_google_calendar_dev_activity_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(DeveloperActivity.this, GoogleCalendarActivity.class));
            }
        });

        // Button to send a test mail
        findViewById(R.id.send_test_mail_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AsyncTask<Void, Void, Void>() {
                    @Override
                    protected Void doInBackground(Void... voids) {
                        AssetsPropertiesReader assetsPropertiesReader = new AssetsPropertiesReader(DeveloperActivity.this);
                        Properties properties = assetsPropertiesReader.getProperties();
                        String mailTo = properties.getProperty("lastime_mail");
                        String lastimeContactMail = properties.getProperty("lastime_mail");
                        String lastimeContactPwd = properties.getProperty("lastime_mail_password");
                        GmailSender sender = new GmailSender(lastimeContactMail, Utils.decryptIt(lastimeContactPwd));
                        try {
                            sender.sendMail("Lastime test message", "TEST", null, mailTo);
                        } catch (Exception e) {
                            Log.e(DeveloperActivity.class.getName(), "Cannot send mail!", e);
                        }
                        return null;
                    }
                }.execute();
            }
        });
    }

    public void raiseException(View view) {
        ACRA.getErrorReporter().handleException(new Exception("This is an exception test raised with ACRA"));
    }


}
