package com.abfactory.lastime.activity;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.statistics.LastEventCreated;
import com.abfactory.lastime.activity.statistics.MostFrequentEvent;
import com.abfactory.lastime.activity.statistics.NumberOfAllEvents;
import com.abfactory.lastime.activity.statistics.NumberOfDistinctEvents;
import com.abfactory.lastime.activity.statistics.OldestEvent;
import com.abfactory.lastime.activity.statistics.StatisticComputer;
import com.abfactory.lastime.activity.statistics.StatisticComputerEventLoop;
import com.abfactory.lastime.drawer.DrawerMenu;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.hookedonplay.decoviewlib.DecoView;
import com.hookedonplay.decoviewlib.charts.SeriesItem;
import com.hookedonplay.decoviewlib.events.DecoEvent;

import java.util.ArrayList;
import java.util.List;

import static com.abfactory.lastime.model.IEventDAO.EVENT_STATE.OLD;
import static org.apache.commons.lang3.reflect.TypeUtils.isInstance;

/**
 * Display quick card statistics
 */
public class StatisticsActivity extends BaseActivity {

    private DrawerMenu drawerMenu;
    private IEventDAO eventDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        LayoutInflater mInflater = LayoutInflater.from(this);
        final View contentView = mInflater.inflate(R.layout.activity_statistics, null);
        setContentView(contentView);

        // ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar_statistics);
        setSupportActionBar(toolbar);

        drawerMenu = new DrawerMenu((DrawerLayout) findViewById(R.id.drawer_layout), this);
        // Nice arrow on top left which spins from the hamburger menu
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerMenu.getDrawerLayout(), toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerMenu.getDrawerLayout().addDrawerListener(drawerToggle);
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        drawerToggle.syncState();

        eventDAO = new EventDAO(this);

        refreshDisplay();

        /*FragmentManager fragmentManager = getFragmentManager();
        HorizontalListFragment horizontalListFragment = (HorizontalListFragment) fragmentManager.findFragmentByTag(HorizontalListFragment.EDIT_PAGE);
        if (horizontalListFragment == null) {
            horizontalListFragment = new HorizontalListFragment();
            horizontalListFragment.setDisplaySelected(HorizontalListAdapter.Display.DISPLAY_TITLE);
            fragmentManager.beginTransaction()
                    .add(R.id.content, horizontalListFragment, HorizontalListFragment.EDIT_PAGE)
                    .commit();
        }

        List<Event> events = eventDAO.getTopEvents(HorizontalListFragment.MAX);
        if(events==null || events.isEmpty()){
            findViewById(R.id.content).setVisibility(View.GONE);
            findViewById(R.id.message_no_event_horizontal_scroll).setVisibility(View.VISIBLE);
        } else {
            findViewById(R.id.content).setVisibility(View.VISIBLE);
            findViewById(R.id.message_no_event_horizontal_scroll).setVisibility(View.GONE);
        }


        // TODO: remove this, just for test purpose
        StringBuilder stringBuilder = new StringBuilder();

        // TODO: find a way to specifiy event we want
        List<Event> eventList = eventDAO.getAllActiveEvents();
        stringBuilder.append("All instances of event  :" + eventDAO.getAllEventInstanceNumber(eventList.get(0)) + "\n");
        stringBuilder.append("All event number " + eventDAO.getAllActiveEventNumber() + "\n");
        stringBuilder.append("Interval min/max in days: " + eventDAO.computeIntervalsTimeInDays(eventList.get(0)) + "\n");
        stringBuilder.append("Interval min/max based on all events in days: " + eventDAO.computeIntervalsTimeInDays(eventList) + "\n");

        // TODO: May be think about a way to retrieve all db information async
        ((TextView)findViewById(R.id.testOutput)).setText(stringBuilder);*/

        // Animation
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    @Override
    protected void onResume() {
        super.onResume();
        drawerMenu.refreshBulletDisplay();
        refreshDisplay();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_statistics, menu);
//        MenuItem switchItem = menu.findItem(R.id.myswitch);
//        switchItem.setActionView(R.layout.switch_layout);
        return true;
    }

    public void refreshDisplay() {
        if ((eventDAO.getAllActiveEvents() == null) || (eventDAO.getAllActiveEvents().isEmpty())) {
            findViewById(R.id.statistics_not_available_section).setVisibility(View.VISIBLE);
            findViewById(R.id.statistics_section).setVisibility(View.GONE);
        } else {
            findViewById(R.id.statistics_not_available_section).setVisibility(View.GONE);
            findViewById(R.id.statistics_section).setVisibility(View.VISIBLE);

            int percentageDone = Math.abs(100 - (100 * eventDAO.getAllActiveNotifications().size() / eventDAO.getAllActiveEventNumber()));
            ((TextView) findViewById(R.id.percentage_events_uptodate)).setText(percentageDone + "%");

            // Create pie for number of events up to date
            DecoView arcView = (DecoView) findViewById(R.id.dynamicArcView);
            arcView.addSeries(new SeriesItem.Builder(Color.argb(255, 218, 218, 218))
                    .setRange(0, 100, 100)
                    .setInitialVisibility(false)
                    .setLineWidth(100f)
                    .build());
            SeriesItem seriesItem1 = new SeriesItem.Builder(Color.argb(255, 64, 196, 0))
                    .setRange(0, 100, 0)
                    .setInitialVisibility(false)
                    .setLineWidth(100f)
                    .build();
            int series1Index = arcView.addSeries(seriesItem1);
            arcView.addEvent(new DecoEvent.Builder(DecoEvent.EventType.EVENT_SHOW, true)
                    .setDelay(500)
                    .setDuration(500)
                    .build());
            arcView.addEvent(new DecoEvent.Builder(percentageDone).setIndex(series1Index).setDelay(1500).build());

            List<StatisticComputer> statisticComputers = new ArrayList<>();
            List<StatisticComputerEventLoop> statisticComputersEventLoop = new ArrayList<>();

            addStatisticComputer(new LastEventCreated(this, eventDAO), statisticComputers, statisticComputersEventLoop);
            addStatisticComputer(new NumberOfDistinctEvents(this, eventDAO), statisticComputers, statisticComputersEventLoop);
            addStatisticComputer(new NumberOfAllEvents(this, eventDAO), statisticComputers, statisticComputersEventLoop);
            addStatisticComputer(new OldestEvent(this, eventDAO), statisticComputers, statisticComputersEventLoop);
            addStatisticComputer(new MostFrequentEvent(this, eventDAO), statisticComputers, statisticComputersEventLoop);

            loopOnEvents(statisticComputersEventLoop);
            runStatisticComputers(statisticComputers);
        }
    }

    private void addStatisticComputer(StatisticComputer computer, List<StatisticComputer> statisticComputerList, List<StatisticComputerEventLoop> statisticComputerEventLoopList) {
        statisticComputerList.add(computer);
        if (isInstance(computer, StatisticComputerEventLoop.class)) {
            statisticComputerEventLoopList.add((StatisticComputerEventLoop) computer);
        }
    }

    private void loopOnEvents(List<StatisticComputerEventLoop> statisticComputersEventLoop) {
        List<Event> allActiveEvents = eventDAO.getAllActiveEvents();
        for (Event e : allActiveEvents) {
            for (StatisticComputerEventLoop computer : statisticComputersEventLoop) {
                computer.executeOnEvent(e);
            }
        }
    }

    private void runStatisticComputers(List<StatisticComputer> computers) {
        for (StatisticComputer computer : computers) {
            computer.execute();
        }
    }
}
