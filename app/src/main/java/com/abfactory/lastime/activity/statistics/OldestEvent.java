package com.abfactory.lastime.activity.statistics;

import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.StatisticsActivity;
import com.abfactory.lastime.model.IEventDAO;

public class OldestEvent extends StatisticComputer {

    public OldestEvent(StatisticsActivity activity, IEventDAO eventDAO) {
        super(activity, eventDAO);
    }

    @Override
    public void execute() {
        ((TextView) findViewById(R.id.stats_oldest_event)).setText(eventDAO.getOldestEvent().getTitle());
    }
}
