package com.abfactory.lastime.activity;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.dialogs.BasicDialogFragment;

public class RateAppActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_rateapp);
    }

    public void onRateNowPressed(View view) {
        Uri uri = Uri.parse("market://details?id=" + getPackageName());
        Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
        try {
            getPreferences().cancelAskingForRating();
            startActivity(myAppLinkToMarket);
            finish();
        } catch (ActivityNotFoundException e) {
            getPreferences().resetNumberOfConnectionsBeforeAskingForRating();
            showErrorMessage(getString(R.string.donation_playstore_trouble));
        }
    }

    public void onRemindLaterPressed(View view) {
        getPreferences().resetNumberOfConnectionsBeforeAskingForRating();
        finish();
    }

    public void onNoRatingThanksPressed(View view) {
        getPreferences().cancelAskingForRating();
        BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.RATE_FROM_WHO_WE_ARE_SECTION, null)
                .show(this.getFragmentManager(), "RateFromWhoWeAreSection");
        finish();
    }

}
