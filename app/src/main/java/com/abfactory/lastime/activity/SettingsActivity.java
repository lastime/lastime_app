package com.abfactory.lastime.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Switch;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.dialogs.BasicDialogFragment;
import com.abfactory.lastime.component.dialogs.PasswordDialogFragment;
import com.abfactory.lastime.component.dialogs.ProvideBackUpEmailDialogFragment;
import com.abfactory.lastime.drawer.DrawerMenu;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.NotificationUtils;

public class SettingsActivity extends BaseActivity {

    private ScrollView scrollView;
    private RadioGroup displayModePreferenceRadioGroup;
    private Switch pwdActivationSwitch;
    private Switch notificationActivationSwitch;
    private TextView pwdField;
    private TextView emailField;
    /* TODO Google feature
    private TextView googleAccountStatus;
    private TextView googleAccountStatusText;
    private SignInButton googleAccountBtnSignIn;
    private Button googleAccountBtnRevoke;
    private CircularProgressView progressBar;
    private static final int RC_SIGN_IN = 0;
    */

    /**
     * TODO : Google feature
    Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    private ACCOUNT_STATUS_DETERMINATION accountStatusDetermination = ACCOUNT_STATUS_DETERMINATION.UNDETERMINED;
    private boolean mSignInClicked;
    private ConnectionResult mConnectionResult;
    // A flag indicating that a PendingIntent is in progress and prevents us from starting further intents.
    private boolean mIntentInProgress;
     **/

    private static final String INCORRECT_PASS_NO_CHANGE = "incorrectPwdNoChange";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Drawer
        DrawerMenu drawerMenu = new DrawerMenu((DrawerLayout) findViewById(R.id.drawer_layout), this);

        // Nice arrow on top left which spins from the hamburger menu
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerMenu.getDrawerLayout(), toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerMenu.getDrawerLayout().addDrawerListener(drawerToggle);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        drawerToggle.syncState();

        // Get UI elements
        scrollView = (ScrollView) findViewById(R.id.scrollViewEditEvent);
        LinearLayout passwordSection = (LinearLayout) findViewById(R.id.password_section);
        LinearLayout emailSection = (LinearLayout) findViewById(R.id.backup_email_definition_section);
        displayModePreferenceRadioGroup = (RadioGroup) findViewById(R.id.ordering_display_radio_group);
        pwdActivationSwitch = (Switch) findViewById(R.id.pwd_activation_switch);
        notificationActivationSwitch = (Switch) findViewById(R.id.notifs_activation_switch);
        pwdField = (TextView) findViewById(R.id.password_field);
        emailField = (TextView) findViewById(R.id.email_field);

        /**
         * TODO: Google feature
        googleAccountStatus = (TextView) findViewById(R.id.google_account_status);
        googleAccountStatusText = (TextView) findViewById(R.id.google_account_status_text);
        googleAccountBtnSignIn = (SignInButton) findViewById(R.id.btn_google_sign_in);
        googleAccountBtnRevoke = (Button) findViewById(R.id.revoke_google_btn);
        progressBar = (CircularProgressView) findViewById(R.id.progressBar);
         **/

        // Update
        updateSettingDisplay();

        // Setting mode preference
        displayModePreferenceRadioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // Save change
                getPreferences().saveDisplayPreference(getPreferences().getDisplayModePreferenceFromRadioButtonId(checkedId));
                // Notify
                if (getPreferences().isDisplayModeSetByTimeLine()) {
                    Snackbar.make(scrollView, getString(R.string.ordering_by_details), Snackbar.LENGTH_SHORT).show();
                } else if (getPreferences().isDisplayModeSetByFrequency()) {
                    Snackbar.make(scrollView, getString(R.string.ordering_by_frequency), Snackbar.LENGTH_SHORT).show();
                } else {
                    Snackbar.make(scrollView, getString(R.string.ordering_alphabetically), Snackbar.LENGTH_SHORT).show();
                }
            }
        });

        // Activating or deactivating password
        pwdActivationSwitch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // If password exists and needs to be updated
                if (LasTimeUtils.isPasswordValid(getPreferences().getPasswordForApp())) {
                    PasswordDialogFragment.newInstance(PasswordDialogFragment.PasswordDialogFragmentType.CANCEL_PWD)
                            .show(getSupportFragmentManager(), "cancelPasswordDialogFragment");
                    // If no password exists
                } else {
                    PasswordDialogFragment.newInstance(PasswordDialogFragment.PasswordDialogFragmentType.CREATE_PWD)
                            .show(getSupportFragmentManager(), "createPasswordDialogFragment");
                }
            }
        });

        // Setting password
        passwordSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pwdActivationSwitch.isChecked()){
                    PasswordDialogFragment.newInstance(PasswordDialogFragment.PasswordDialogFragmentType.MODIFY_PWD)
                            .show(getSupportFragmentManager(), "modifyPasswordDialogFragment");
                }
            }
        });

        // Setting password
        emailSection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(pwdActivationSwitch.isChecked()) {
                    ProvideBackUpEmailDialogFragment.newInstance(getPreferences().getPasswordForApp()).show(getFragmentManager(), "provideBackUpEmailDialogFragment");
                }
            }
        });

        // Activating or deactivating notifs
        notificationActivationSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    // Handle activation
                    NotificationUtils.activateNotifications(getApplicationContext());
                } else {
                    // Handle deactivation
                    NotificationUtils.deactivateNotifications(getApplicationContext());
                }
                getPreferences().saveNotificationsSetting(isChecked);
            }
        });

        /**
         * TODO: Google feature
        (findViewById(R.id.btn_google_sign_in)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                signInWithGplus();
            }
        });

        (findViewById(R.id.revoke_google_btn)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                revokeGplusAccess();
            }
        });
         **/

        findViewById(R.id.start_tutorial_section).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), LasTimeActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.putExtra("FORCE_TUTO", true);
                startActivity(intent);
            }
        });

        // Animation
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);

    }

    /** HANDLE PASSWORD CREATION ACTIONS **/
    public void doPositiveClicPwdCreationOperation(String newPassword, String confPassword){
        if(LasTimeUtils.isValidPasswordCreation(newPassword, confPassword)){
            ProvideBackUpEmailDialogFragment.newInstance(newPassword).show(getFragmentManager(), "provideBackUpEmailDialogFragment");
        } else {
            BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.PWD_DONT_MATCH, null)
                    .show(getFragmentManager(), INCORRECT_PASS_NO_CHANGE);
            getPreferences().resetPwdPreference();
        }
        updateSettingDisplay();
    }

    public void doNegativeClicPwdCreationOperation(){
        updateSettingDisplay();
        showNoChangePerformedMessage();
    }

    /** HANDLE PASSWORD MODIFICATION ACTIONS **/

    public void doPositiveClicPwdModificationOperation(String oldPassword, String newPassword, String confPassword){
        if (LasTimeUtils.isPasswordCorrect(getPreferences(), oldPassword)) {
            if(LasTimeUtils.isValidPasswordCreation(newPassword, confPassword)){
                getPreferences().setPassword(newPassword);
                Snackbar.make(scrollView, getResources().getString(R.string.pwd_succesfully_modified), Snackbar.LENGTH_SHORT).show();
            } else {
                BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.PWD_DONT_MATCH, null)
                        .show(getFragmentManager(), INCORRECT_PASS_NO_CHANGE);
            }
        } else {
            BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.INCORRECT_PWD, null)
                    .show(getFragmentManager(), "incorrectPwd");
        }
        // Update display
        updateSettingDisplay();
    }

    public void doNegativeClicPwdModificationOperation(){
        updateSettingDisplay();
        showNoChangePerformedMessage();
    }

    /** HANDLE PASSWORD CANCELLATION ACTIONS **/

    public void doPositiveClicPwdDeactivationOperation(String pwd){
        if (LasTimeUtils.isPasswordCorrect(getPreferences(), pwd)) {
            getPreferences().resetPwdPreference();
            Snackbar.make(scrollView, getResources().getString(R.string.pwd_succesfully_canceled), Snackbar.LENGTH_SHORT).show();
        } else {
            BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.INCORRECT_PWD_NO_CHANGE, null)
                    .show(getFragmentManager(), INCORRECT_PASS_NO_CHANGE);
            getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        }
        updateSettingDisplay();
    }

    public void doNegativeClicPwdDeactivationOperation(){
        // Force to keep checked
        pwdActivationSwitch.setChecked(true);
        showNoChangePerformedMessage();
    }

    // Update settings display
    private void updateSettingDisplay(){

        // display mode preference
        displayModePreferenceRadioGroup.check(getPreferences().getRadioButtonIdToCheck());

        // pwd display
        pwdActivationSwitch.setChecked(getPreferences().isAppToBeProtectedWithPassword());
        if (getPreferences().isAppToBeProtectedWithPassword() && getPreferences().getPasswordForApp() != null && !getPreferences().getPasswordForApp().isEmpty()) {
            pwdField.setText(R.string.pwd_defined_text);
        } else {
            pwdField.setText(R.string.no_pwd_defined_text);
        }

        // BackupEmail field
        if (LasTimeUtils.isValidEmail(getPreferences().getBackUpEmailForApp())) {
            emailField.setText(getPreferences().getBackUpEmailForApp());
        } else {
            emailField.setText(R.string.no_backup_email_set);
        }

        // Notifications display
        notificationActivationSwitch.setChecked(getPreferences().isNotificationsActivated());

        // Accounts display
        // TODO : Google feature
        // handleAccountsConnectionDisplay();
    }

    public void doPositiveSetEmailBackup(String pwd, String email) {
        if(LasTimeUtils.isValidEmail(email)){
            getPreferences().setPasswordAndBackup(pwd, email);
            Snackbar.make(scrollView, getResources().getString(R.string.pwd_activation_successful), Snackbar.LENGTH_LONG).show();
            updateSettingDisplay();
        } else {
            BasicDialogFragment invalidBackUpEmailPopup = BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.INVALID_BACKUP_EMAIL, pwd);
            invalidBackUpEmailPopup.show(getFragmentManager(), "invalidBackUpEmail");
        }
    }

    public void showNoChangePerformedMessage() {
        Snackbar.make(scrollView, getResources().getString(R.string.no_change_performed), Snackbar.LENGTH_LONG).show();
    }

    /**
     *
     * TODO : Google feature
     *
    @Override
    public void onConnectionSuspended(int arg0) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnected(Bundle arg0) {
        accountStatusDetermination = ACCOUNT_STATUS_DETERMINATION.CONNECTED;
        handleAccountsConnectionDisplay();
    }

    @Override
    public void onConnectionFailed(ConnectionResult result) {
        accountStatusDetermination = ACCOUNT_STATUS_DETERMINATION.NOT_CONNECTED;
        handleAccountsConnectionDisplay();

        if (!result.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(result.getErrorCode(), this,
                    0).show();
            return;
        }
        if (!mIntentInProgress) {
            // Store the ConnectionResult for later usage
            mConnectionResult = result;

            if (mSignInClicked) {
                // The user has already clicked 'sign-in' so we attempt to
                // resolve all
                // errors until the user is signed in, or they cancel.
                resolveSignInError();
            }
        }
    }

    public void handleAccountsConnectionDisplay(){
        // Google play services
        if(!LasTimeUtils.isGooglePlayServicesAvailable(this)) {
            googleAccountStatus.setText("Google play services required!");
            googleAccountBtnSignIn.setVisibility(View.GONE);
            googleAccountBtnRevoke.setVisibility(View.GONE);
            progressBar.setVisibility(View.GONE);
            googleAccountStatusText.setVisibility(View.GONE);
            Snackbar.make(this.scrollView, "Google play services required!", Snackbar.LENGTH_LONG).show();
            return;
        }

        //Google account
        switch (accountStatusDetermination) {
            case CONNECTED:
                needGetAccountPermissions();
                googleAccountStatus.setText(getResources().getString(R.string.account_status_connected));
                googleAccountStatusText.setText(getResources().getString(R.string.google_account_text_connected));
                googleAccountBtnSignIn.setVisibility(View.GONE);
                googleAccountBtnRevoke.setVisibility(View.VISIBLE);
                progressBar.setVisibility(View.GONE);
                break;
            case NOT_CONNECTED:
                googleAccountStatus.setText(getResources().getString(R.string.account_status_not_connected));
                googleAccountStatusText.setText(getResources().getString(R.string.google_account_text_not_connected));
                googleAccountBtnSignIn.setVisibility(View.VISIBLE);
                googleAccountBtnRevoke.setVisibility(View.GONE);
                progressBar.setVisibility(View.GONE);
                LasTimeUtils.resetGoogleAccountLogin(this);
                break;
            default:
                googleAccountStatus.setText(getResources().getString(R.string.account_status_undetermined));
                googleAccountStatusText.setText(getResources().getString(R.string.google_account_undetermined));
                googleAccountBtnSignIn.setVisibility(View.GONE);
                googleAccountBtnRevoke.setVisibility(View.GONE);
                progressBar.setVisibility(View.VISIBLE);
                break;
        }
    }
    **/

    /**
     * TODO Google feature
     * Sign-in into google
    private void signInWithGplus() {

        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            resolveSignInError();
        }
        handleAccountsConnectionDisplay();
    }

    /**
     * TODO Google feature
     * Method to resolve any signin errors
     *
    private void resolveSignInError() {
        if (mConnectionResult != null && mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                Log.w(SettingsActivity.class.getName(), e);
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        } else {
            mGoogleApiClient.connect();
        }
        handleAccountsConnectionDisplay();
    }*/

    /**
     * TODO: Google feature
     * Revoking access from google
     *
    private void revokeGplusAccess()  {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);
            Plus.AccountApi.revokeAccessAndDisconnect(mGoogleApiClient)
                    .setResultCallback(new ResultCallback<Status>() {
                        @Override
                        public void onResult(Status arg0) {
                            Snackbar.make(scrollView, "User access revoked!", Snackbar.LENGTH_SHORT).show();
                            accountStatusDetermination = ACCOUNT_STATUS_DETERMINATION.NOT_CONNECTED;
                            handleAccountsConnectionDisplay();
                            mGoogleApiClient = new GoogleApiClient.Builder(SettingsActivity.this)
                                    .addConnectionCallbacks(SettingsActivity.this)
                                    .addOnConnectionFailedListener(SettingsActivity.this).addApi(Plus.API)
                                    .addScope(Plus.SCOPE_PLUS_LOGIN)
                                    .addScope(Plus.SCOPE_PLUS_PROFILE).build();
                        }

                    });
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int responseCode, Intent intent) {
        if (requestCode == RC_SIGN_IN) {
            if (responseCode != RESULT_OK) {
                mSignInClicked = false;
            }
            mIntentInProgress = false;
            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
        handleAccountsConnectionDisplay();
    }

    @Override
    protected void onResume() {
        super.onResume();
        // Initialize google API for login calls
        if (LasTimeUtils.isGooglePlayServicesAvailable(this)) {
            buildGoogleApiClient();
        }
    }

    protected synchronized void buildGoogleApiClient() {
        // Initializing google plus api client
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Plus.API)
                .addScope(Plus.SCOPE_PLUS_PROFILE)
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
        mGoogleApiClient.connect();
    }

    @Override
    public void getAccountPermissionsGranted(){
        // Regenerate Google token id
        new GetGoogleIdTokenTask(this, mGoogleApiClient).execute();
        // Sync user info
        LasTimeUtils.syncGoogleAccountInfo(this, mGoogleApiClient);
    }

    @Override
    public void getAccountPermissionsDenied() {
        revokeGplusAccess();
        LasTimeUtils.resetGoogleAccountLogin(this);
        Snackbar.make(scrollView, "Read Account permissions required to use Google sign-in", Snackbar.LENGTH_LONG).show();
    }

    private enum ACCOUNT_STATUS_DETERMINATION {UNDETERMINED, CONNECTED, NOT_CONNECTED}
    */
}
