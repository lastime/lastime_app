package com.abfactory.lastime.activity.edit;

import android.graphics.Color;
import android.graphics.Point;
import android.net.Uri;
import android.view.Display;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.component.DeselectableRadioButton;
import com.abfactory.lastime.component.clearable.ClearableEditText;
import com.abfactory.lastime.model.ColorPaletteElement;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;

/**
 * Display update handled in that class
 */
public class UpdateEditEvent {

    private final EditEventActivity activity;

    public UpdateEditEvent(EditEventActivity activity) {
        this.activity = activity;
    }

    /**
     * Update all fields of EditEventActivity
     */
    public void update(final Event event) {

        // Title
        AutoCompleteTextView autoCompleteTextView = activity.findViewById(R.id.event_title);
        autoCompleteTextView.setText(event.getTitle());
        autoCompleteTextView.dismissDropDown();

        // Color
        if (ColorPaletteElement.getColorPaletteElementFromId(event.getColor()) != null) {
            activity.findViewById(R.id.edit_event_toolbar).setBackgroundColor(event.getColor());
            ((ImageView) activity.findViewById(R.id.img_color)).setColorFilter(event.getColor());
            ((TextView) activity.findViewById(R.id.event_color)).setText(ColorPaletteElement.getColorPaletteElementFromId(event.getColor()).getDisplayableName());
            activity.findViewById(R.id.edit_event_toolbar).setBackgroundColor(event.getColor());
        } else {
            ((ImageView) activity.findViewById(R.id.img_color)).setColorFilter(Color.parseColor(ColorPaletteElement.DEFAULT_COLOR));
            ((TextView) activity.findViewById(R.id.event_color)).setText("");
        }

        // Image preview
        activity.findViewById(R.id.edit_event_toolbar).setBackgroundColor(LasTimeUtils.getColor(activity, R.color.ColorPrimary));
        if (event.getThumbnailPath() != null) {
            // Full image is not mandatory
            if (event.getImagePath() != null)
                activity.setImageToStore(new File(event.getImagePath()));
            activity.setThumbnailToStore(new File(event.getThumbnailPath()));
            refreshImageDisplay(event.getThumbnailPath(), true);
        } else {
            ((ImageView) activity.findViewById(R.id.display_picture)).setImageDrawable(null);
            ((ImageView) activity.findViewById(R.id.img_color)).setColorFilter(LasTimeUtils.getColor(activity, R.color.ColorPrimary));
            activity.findViewById(R.id.display_picture_layout).setBackgroundColor(LasTimeUtils.getColor(activity, R.color.ColorPrimary));
        }

        // Weight
        activity.setCurrentWeight(event.getWeight());

        // Date of event
        if (event.getDate() != null) {
            TextView dateInfo = (TextView) activity.findViewById(R.id.event_date_text);
            TextView endDateInfo = (TextView) activity.findViewById(R.id.event_end_date_text);
            TextView timeInfo = (TextView) activity.findViewById(R.id.event_time_text);
            dateInfo.setText(event.getDateAsString());
            if (event.getEndDateAsString() != null)
                endDateInfo.setText(event.getEndDateAsString());
            if (event.getTimeOfEvent() != null && event.getTimeOfEvent() > 0)
                timeInfo.setText(LasTimeUtils.displayTime(event.getTimeOfEvent()));
        }

        // Mark
        if (LasTimeUtils.getRadioButtonIdForMark(event.getMark()) > -1) {
            ((DeselectableRadioButton) activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(event.getMark()))).setChecked(true);
            activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(event.getMark())).setAlpha(1);
        }

        // Weather
        if (LasTimeUtils.getRadioButtonIdForWeather(event.getWeather()) > -1) {
            ((DeselectableRadioButton) activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(event.getWeather()))).setChecked(true);
            activity.findViewById(LasTimeUtils.getRadioButtonIdForWeather(event.getWeather())).setAlpha(1);
        }

        // Database access
        EventDAO eventDAO = new EventDAO(activity.getApplicationContext());
        EventLocation location = null;

        // Location update
        if (activity.getNewLocationActivityResult() != null) {
            location = activity.getNewLocationActivityResult();
        } else if (event.getLocation() != 0 && event.getLocation() != -1) {
            location = eventDAO.getLocationByEvent(event);
        }
        if (location != null) {
            ((TextView) activity.findViewById(R.id.location_text)).setText(location.getName());
        } else {
            ((TextView) activity.findViewById(R.id.location_text)).setText("");
        }

        // Note
        if (event.getNote() != null) {
            ((ClearableEditText) activity.findViewById(R.id.note_text)).setText(event.getNote());
        }

        // Highlight
        ArrayAdapter<String> spinnerAdapter = new ArrayAdapter<>(activity, R.layout.spinner_layout, LasTimeUtils.getFieldsToHighlight());
        if (event.getHighlight() != null) {
            Spinner spinner = activity.findViewById(R.id.highlight_spinner);
            spinner.setAdapter(spinnerAdapter);
            spinner.setSelection(spinnerAdapter.getPosition(event.getHighlight()));
        }

        // Expenses
        if (event.getExpenses() > 0) {
            final Locale locale = LasTimeUtils.getLocale(activity);
            final ClearableEditText clearableEditText = ((ClearableEditText) activity.findViewById(R.id.event_expenses_text));
            clearableEditText.setText(String.format(locale, "%.2f", event.getExpenses())
                    + " " + Currency.getInstance(locale).getCurrencyCode());
            clearableEditText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View v, boolean hasFocus) {
                    if (hasFocus) {
                        clearableEditText.setText(String.format(locale, "%.2f", event.getExpenses()));
                    }
                }
            });
        }

        // Recurrence
        if (LasTimeUtils.isRecurrenceActivated(event.getRecurrence())) {
            ((TextView) activity.findViewById(R.id.event_recurrence)).setText(LasTimeUtils.convertRecurrenceTagToString(event.getRecurrence(), activity));
            if (LasTimeUtils.isReminderActivated(event.getReminder())) {
                ((TextView) activity.findViewById(R.id.event_remind)).setText(LasTimeUtils.convertReminderTagToString(event.getReminder(), activity));
            }
        }

        // Contact
        if (event.getContact() != -1) {
            TextView contact_text = ((TextView) activity.findViewById(R.id.contact_text));
            if (activity.hasPermissions(BaseActivity.PERMISSIONS.WriteContactPermissions)) {
                contact_text.setText(eventDAO.getContactById(event.getContact()).getContactName());
            } else {
                contact_text.setText(R.string.contact_permissions_needed);
                contact_text.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        activity.needWriteContactPermissions();
                    }
                });
            }
            activity.findViewById(R.id.cancelContactButton).setVisibility(View.VISIBLE);
        } else {
            activity.findViewById(R.id.cancelContactButton).setVisibility(View.GONE);
        }

    }

    public void updateDateDisplay(Date date) {
        TextView dateInfo = (TextView) activity.findViewById(R.id.event_date_text);
        dateInfo.setText(Event.CALENDAR_DISPLAY.format(date.getTime()));
        // Time display should be optional
        // TextView timeInfo = (TextView) editEventActivity.findViewById(R.id.event_time_text);
        // timeInfo.setText(Event.TIME_DISPLAY.format(date.getTime()));
    }

    protected void refreshImageDisplay(String path, boolean isFromSD) {
        Display display = activity.getWindowManager().getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);

        int width = size.x;
        int height = ((int) (172 * activity.getPicture().getContext().getResources().getDisplayMetrics().density));

        if (isFromSD) {
            Picasso.with(activity)
                    .load(Uri.fromFile(new File(path)))
                    .resize(width, height)
                    .centerCrop()
                    .into(activity.getPicture());
        } else {
            Picasso.with(activity)
                    .load(path)
                    .resize(width, height)
                    .centerCrop()
                    .into(activity.getPicture());
        }
    }

    protected void cleanAllFields() {

        // Title
        ((AutoCompleteTextView) activity.findViewById(R.id.event_title)).setText("");
        // Color
        ((ImageView) activity.findViewById(R.id.img_color)).setColorFilter(Color.parseColor(ColorPaletteElement.DEFAULT_COLOR));
        ((TextView) activity.findViewById(R.id.event_color)).setText("");
        // Image preview
        //((ImageView) activity.findViewById(R.id.display_picture)).setImageDrawable(null);
        // Weight
        activity.setCurrentWeight(0);
        // Date of event
        ((TextView) activity.findViewById(R.id.event_date_text)).setText(Event.CALENDAR_DISPLAY.format(new Date()));
        ((TextView) activity.findViewById(R.id.event_end_date_text)).setText("");
        ((TextView) activity.findViewById(R.id.event_time_text)).setText("");
        // Mark
        activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(1)).setAlpha(0.2f);
        activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(2)).setAlpha(0.2f);
        activity.findViewById(LasTimeUtils.getRadioButtonIdForMark(3)).setAlpha(0.2f);
        // Location update
        ((TextView) activity.findViewById(R.id.location_text)).setText("");
        activity.findViewById(R.id.map_layout).setVisibility(View.GONE);
        // Note
        ((TextView) activity.findViewById(R.id.note_text)).setText("");
        // Highlight
        ((TextView) activity.findViewById(R.id.highlight_spinner)).setText("");
        // Expenses
        ((TextView) activity.findViewById(R.id.event_expenses_text)).setText("");
        // Recurrence
        ((TextView) activity.findViewById(R.id.event_recurrence)).setText("");
        ((TextView) activity.findViewById(R.id.event_remind)).setText("");
        // Contact
        activity.findViewById(R.id.cancelContactButton).setVisibility(View.GONE);

    }

}
