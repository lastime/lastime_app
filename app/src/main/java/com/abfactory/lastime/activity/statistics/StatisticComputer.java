package com.abfactory.lastime.activity.statistics;

import android.support.annotation.IdRes;
import android.support.annotation.NonNull;
import android.support.annotation.StringRes;
import android.view.View;

import com.abfactory.lastime.activity.StatisticsActivity;
import com.abfactory.lastime.model.IEventDAO;

/**
 * Classes in charge of computing a particular statistic extends this class.
 */
public abstract class StatisticComputer {

    private StatisticsActivity activity;
    protected IEventDAO eventDAO;

    public StatisticComputer(StatisticsActivity activity, IEventDAO eventDAO) {
        this.activity = activity;
        this.eventDAO = eventDAO;
    }

    public abstract void execute();

    protected View findViewById(@IdRes int id) {
        return activity.findViewById(id);
    }

    public final String getString(@StringRes int resId) {
        return activity.getString(resId);
    }

}
