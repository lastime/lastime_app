package com.abfactory.lastime.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.dialogs.BasicDialogFragment;
import com.abfactory.lastime.component.yesnocards.CardsDataAdapter;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventTemplate;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.DownloadImageAndUpdateTemplateTask;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.wenchao.cardstack.CardStack;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class InspireMeActivity extends BaseActivity {

    List<EventTemplate> referenceTemplatesList = new ArrayList<>();
    private CardStack mCardStack;
    private CardsDataAdapter mCardAdapter;
    private List<EventTemplate> templatesToWishList = new ArrayList<>();
    private TextView counter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pick_up_for_wish_list);

        // Reference list of event templates to be proposed (to take from backend in the long run)
        referenceTemplatesList = new ArrayList<>();
        referenceTemplatesList.add(new EventTemplate(getString(R.string.template_haidresser), "http://lastime.moninstantbeaute.biz/gallery/coiffeur_2_1024.jpg", getString(R.string.template_haidresser_text), "#4CAF50"));
        referenceTemplatesList.add(new EventTemplate(getString(R.string.template_car), "http://lastime.moninstantbeaute.biz/gallery/revision_1024.jpg", getString(R.string.template_car_text), "#2196F3"));
        referenceTemplatesList.add(new EventTemplate(getString(R.string.template_cinema), "http://lastime.moninstantbeaute.biz/gallery/cinema_2_1024.jpg", getString(R.string.template_cinema_text), "#F44336"));
        referenceTemplatesList.add(new EventTemplate(getString(R.string.template_restaurant), "http://lastime.moninstantbeaute.biz/gallery/breakfast_table_1024.jpg", getString(R.string.template_restaurant_text), "#009688"));
        referenceTemplatesList.add(new EventTemplate(getString(R.string.template_cleaning), "http://lastime.moninstantbeaute.biz/gallery/menage_1024.jpg", getString(R.string.template_cleaning_text), "#3F51B5"));

        // Toolbar first to be set
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_cancel);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        // Handle card display
        mCardStack = (CardStack) findViewById(R.id.container);
        mCardStack.setContentResource(R.layout.std_card_inner);

        mCardAdapter = new CardsDataAdapter(getApplicationContext());
        if (referenceTemplatesList != null) {
            for (EventTemplate template : referenceTemplatesList) {
                mCardAdapter.add(template);
            }
        }
        mCardStack.setAdapter(mCardAdapter);

        mCardStack.setListener(new CardsDataListener());

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_pick_up_for_wish_list, menu);
        RelativeLayout badgeLayout = (RelativeLayout) menu.findItem(R.id.badge).getActionView();
        counter = (TextView) badgeLayout.findViewById(R.id.counter);
        return true;
    }

    /**
     * Define action when menu item to cancelLast selection is clicked
     */
    public void cancelLast(MenuItem item) {
        // To implement
    }

    public class CardsDataListener implements CardStack.CardEventListener {

        int THRESHOLD_TO_TRIGGER_DISCARD = 500;
        int THRESHOLD_TO_DISPLAY_TOOLTIP = 200;

        //the direction indicate swipe direction
        //there are four directions
        //  0  |  1
        // ----------
        //  2  |  3

        @Override
        public boolean swipeEnd(int direction, float distance) {
            //if "return true" the dismiss animation will be triggered
            //if false, the card will move back to stack
            //distance is finger swipe distance in dp
            return (distance > THRESHOLD_TO_TRIGGER_DISCARD) ? true : false;
        }

        @Override
        public boolean swipeStart(int direction, float distance) {
            return true;
        }

        @Override
        public boolean swipeContinue(int direction, float distanceX, float distanceY) {
            if (distanceX > THRESHOLD_TO_DISPLAY_TOOLTIP) {
                if (direction == 1 || direction == 3) {
                    mCardStack.getTopView().findViewById(R.id.keep_info).setVisibility(View.VISIBLE);
                    mCardStack.getTopView().findViewById(R.id.discard_info).setVisibility(View.GONE);
                } else {
                    mCardStack.getTopView().findViewById(R.id.discard_info).setVisibility(View.VISIBLE);
                    mCardStack.getTopView().findViewById(R.id.keep_info).setVisibility(View.GONE);
                }
            } else {
                mCardStack.getTopView().findViewById(R.id.keep_info).setVisibility(View.GONE);
                mCardStack.getTopView().findViewById(R.id.discard_info).setVisibility(View.GONE);
            }
            return true;
        }

        @Override
        public void discarded(int id, int direction) {
            // If discarded to keep
            if (direction == 1 || direction == 3) {
                // Add item to list to be handled at the end of the process
                templatesToWishList.add(mCardAdapter.getItem(id - 1));
                // Counter display update
                counter.setText(Integer.toString(templatesToWishList.size()));
                // If discarded to pass
            } else {
                // Do nothing
            }
            if (mCardAdapter.getCount() == id) {
                processWhenAllCardsHandled();
            }
        }

        @Override
        public void topCardTapped() {
            //this callback invoked when a top card is tapped by user.
        }

        private void processWhenAllCardsHandled() {
            IEventDAO eventDAO = new EventDAO(InspireMeActivity.this.getApplicationContext());
            Date creationDate = new Date();
            for (EventTemplate et : templatesToWishList) {
                // Set same creation date to all templates
                et.setCreationDate(creationDate);
                // Create event template and get ID
                long id = eventDAO.createEventTemplateWithoutImage(et);
                // Start image download and when done put image in event template thx to ID
                if (et.getImagePath().startsWith("http://") || et.getImagePath().startsWith("https://")) {
                    String filename = et.getImagePath().substring(et.getImagePath().lastIndexOf('/') + 1);
                    String[] imageNames = LasTimeUtils.getImageStorageDir(InspireMeActivity.this).list();
                    // Check image does not exist
                    for (String imageName : imageNames) {
                        if (imageName.equals(filename)) {
                            break;
                        }
                    }
                    new DownloadImageAndUpdateTemplateTask(InspireMeActivity.this, id).execute(et.getImagePath(), filename);
                }
            }

            // Show finalization message and hide actions
            BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.INSPIRE_ME_COMPLETE, null).show(getFragmentManager(), "passwordResetDialog");

            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    final Intent mainIntent = new Intent(InspireMeActivity.this, WishListActivity.class);
                    InspireMeActivity.this.startActivity(mainIntent);
                    InspireMeActivity.this.finish();
                }
            }, 2000);
        }
    }

}
