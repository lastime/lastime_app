package com.abfactory.lastime.activity.statistics;

import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.StatisticsActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.IEventDAO;

public class NumberOfAllEvents extends StatisticComputerEventLoop {

    public NumberOfAllEvents(StatisticsActivity activity, IEventDAO eventDAO) {
        super(activity, eventDAO);
    }

    @Override
    public void executeOnEvent(Event event) {

    }

    @Override
    public void execute() {
        ((TextView) findViewById(R.id.stats_number_of_all_events)).setText(String.valueOf(eventDAO.getEventsCount()));
    }

}
