package com.abfactory.lastime.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.EditText;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.EventResource;

import org.restlet.Component;
import org.restlet.data.Protocol;
import org.restlet.routing.Router;

public class ServerActivity extends AppCompatActivity {

    private final static String TAG = ServerActivity.class.getName();

    public TextView output;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_server);

        output = (EditText)findViewById(R.id.server_output);

        Component serverComponent = new Component();
        serverComponent.getServers().add(Protocol.HTTP, 8080);
        final Router router = new Router(serverComponent.getContext().createChildContext());
        router.attach("/events/{id}", EventResource.class);
        serverComponent.getDefaultHost().attach(router);
        try {
            // TODO: use button start and stop
            serverComponent.start();
            writeToConsole("Server started!");
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void writeToConsole(String line){
        output.append(line + "\n");
    }
}
