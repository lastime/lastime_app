package com.abfactory.lastime.activity.calendar;

import android.os.AsyncTask;
import android.text.TextUtils;

import com.abfactory.lastime.utils.LasTimeUtils;
import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.client.util.DateTime;
import com.google.api.services.calendar.model.Event;
import com.google.api.services.calendar.model.Events;

import java.io.IOException;
import java.util.*;

public class RetrieveEventsTask extends AsyncTask<String, Void, List<com.abfactory.lastime.model.Event>> {

    private GoogleCalendarActivity activity;

    private com.google.api.services.calendar.Calendar calendarService = null;
    private Exception mLastError = null;

    public RetrieveEventsTask(GoogleCalendarActivity activity) {
        this.activity = activity;

        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        calendarService = new com.google.api.services.calendar.Calendar.Builder(
                transport, jsonFactory, activity.getmCredential())
                .setApplicationName("LasTime")
                .build();
    }

    /**
     * Background task to call Google Calendar API.
     *
     * @param params no parameters needed for this task.
     */
    @Override
    protected List<com.abfactory.lastime.model.Event> doInBackground(String... params) {
        try {
            return getDataFromApi(params[0]);
        } catch (Exception e) {
            mLastError = e;
            cancel(true);
            return null;
        }
    }

    /**
     * Fetch a list of the next n events from selected calendar.
     *
     * @return Lastime events.
     * @throws IOException
     */
    private List<com.abfactory.lastime.model.Event> getDataFromApi(String calendarId) throws IOException {
        DateTime now = new DateTime(System.currentTimeMillis());
        List<com.abfactory.lastime.model.Event> eventStrings = new ArrayList<>();
        Set<String> existingTitles = new HashSet<>();

        Events events = calendarService.events().list(calendarId)
                .setMaxResults(50)
                .setTimeMax(now)
                .setOrderBy("startTime")
                .setSingleEvents(true)
                .execute();
        List<Event> items = events.getItems();

        for (Event event : items) {
            //Filter duplicates by title
            if (!existingTitles.contains(event.getSummary())) {
                existingTitles.add(event.getSummary());
                eventStrings.add(parseEvent(event));
            }
        }
        return eventStrings;
    }


    @Override
    protected void onPreExecute() {
        activity.getmOutputText().setText("");
        activity.getmProgress().show();
    }

    @Override
    protected void onPostExecute(List<com.abfactory.lastime.model.Event> output) {
        activity.getmProgress().hide();
        if (output == null || output.size() == 0) {
            activity.getmOutputText().setText("No results returned.");
        } else {
            activity.getmOutputText().setText(TextUtils.join("\n", output));
        }
    }

    @Override
    protected void onCancelled() {
        activity.getmProgress().hide();
        if (mLastError != null) {
            if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                activity.showGooglePlayServicesAvailabilityErrorDialog(
                        ((GooglePlayServicesAvailabilityIOException) mLastError)
                                .getConnectionStatusCode());
            } else if (mLastError instanceof UserRecoverableAuthIOException) {
                activity.startActivityForResult(
                        ((UserRecoverableAuthIOException) mLastError).getIntent(),
                        GoogleCalendarActivity.REQUEST_AUTHORIZATION);
            } else {
                activity.getmOutputText().setText("The following error occurred:\n"
                        + mLastError.getMessage());
            }
        } else {
            activity.getmOutputText().setText("Request cancelled.");
        }
    }

    /**
     * Transform Google event into LasTime event
     **/
    private com.abfactory.lastime.model.Event parseEvent(Event event) {
        Date eventDate = null;
        int eventTime = 0;

        if (event.getStart().getDate() != null) {
            eventDate = new Date(event.getStart().getDate().getValue());
            // If it's not an all day event, adding the time
        } else if (event.getStart().getDateTime() != null) {
            eventDate = new Date(event.getStart().getDateTime().getValue());
            eventTime = LasTimeUtils.parseTime(com.abfactory.lastime.model.Event.TIME_DISPLAY.format(eventDate));
        }

        if (eventDate == null)
            throw new IllegalArgumentException("Event could not have an empty date!!!");

        return new com.abfactory.lastime.model.Event.EventBuilder(event.getSummary())
                .addDate(eventDate)
                .addTime(eventTime)
                .addNote(event.getDescription())
                .addColor(LasTimeUtils.getRandomColor())
                .addCreationDate(new Date())
                .build();
    }
}
