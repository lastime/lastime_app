package com.abfactory.lastime.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.design.widget.Snackbar;
import android.support.v4.content.ContextCompat;
import android.support.v4.graphics.drawable.DrawableCompat;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.Button;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.PlaceAutocompleteAdapter;
import com.abfactory.lastime.component.clearable.ClearableAutoCompleteTextView;
import com.abfactory.lastime.component.dialogs.*;
import com.abfactory.lastime.component.tutorial.ShowcaseViewFactory;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.GoogleApiClient.ConnectionCallbacks;
import com.google.android.gms.common.api.GoogleApiClient.OnConnectionFailedListener;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.*;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;

public class NewLocationActivity extends BaseActivity implements LocationListener,
        OnConnectionFailedListener, ConnectionCallbacks, OnMapReadyCallback {

    private static final int ZOOM_LEVEL = 13;
    private static final double DISTANCE_THRESHOLD = 2000; // Threshold in meters to consider possible duplicate locations
    protected GoogleApiClient mGoogleApiClient;
    GoogleMap map;
    Button renameBtn;
    /**
     * Selected place
     */
    private EventLocation result;
    private Location loc;
    private Location lastKnownLocation;
    private PlaceAutocompleteAdapter mAdapter;
    private AutoCompleteTextView mAutocompleteView;

    private ShowcaseView sv;

    /**
     * Callback for results from a Places Geo Data API query that shows the first place result in
     * the details view on screen.
     */
    private ResultCallback<PlaceBuffer> mUpdatePlaceDetailsCallback
            = new ResultCallback<PlaceBuffer>() {
        @Override
        public void onResult(PlaceBuffer places) {
            if (!places.getStatus().isSuccess()) {
                // Request did not complete successfully
                places.release();
                return;
            }
            // Get the Place object from the buffer.
            final Place place = places.get(0);
            // Save it for future use, notably when exiting the activity
            result = new EventLocation();
            result.setName(place.getName().toString());
            result.setLatitude(place.getLatLng().latitude);
            result.setLongitude(place.getLatLng().longitude);
            result.setGooglePlacesId(place.getId());

            // Add corresponding marker and zoom to place
            addMarkerToMap(place.getLatLng(), place.getName().toString());
            zoomToLocation(place.getLatLng());
            // Set tag to notify position was already found via edit text or gps auto location

            // When autocomplete proposal is selected, hide keyboard
            AutoCompleteTextView myEditText = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
            InputMethodManager imm = (InputMethodManager) getSystemService(
                    Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(myEditText.getWindowToken(), 0);
            // Put cursor at the beginning of the edit text to display the beginning of the text
            myEditText.setSelection(0);

            // Show Rebane Btn for google places
            showRenameButton();

            // Release places utils
            places.release();
        }
    };
    /**
     * Listener that handles selections from suggestions from the AutoCompleteTextView that
     * displays Place suggestions.
     * Gets the place id of the selected item and issues a request to the Places Geo Data API
     * to retrieve more details about the place.
     *
     * @see com.google.android.gms.location.places.GeoDataApi#getPlaceById(com.google.android.gms.common.api.GoogleApiClient,
     * String...)
     */
    private AdapterView.OnItemClickListener mAutocompleteClickListener
            = new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            /*
             Retrieve the place ID of the selected item from the Adapter.
             The adapter stores each Place suggestion in a PlaceAutocomplete object from which we
             read the place ID.
              */
            final PlaceAutocompleteAdapter.PlaceAutocomplete item = mAdapter.getItem(position);
            final String placeId = String.valueOf(item.placeId);

            /*
             Issue a request to the Places Geo Data API to retrieve a Place object with additional
              details about the place.
              */
            PendingResult<PlaceBuffer> placeResult = Places.GeoDataApi
                    .getPlaceById(mGoogleApiClient, placeId);
            placeResult.setResultCallback(mUpdatePlaceDetailsCallback);

        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_location);

        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (!locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            Snackbar.make(findViewById(R.id.autocomplete_places), R.string.gps_not_activated_err_msg, Snackbar.LENGTH_LONG).show();
        }

        // Handle map display
        if (LasTimeUtils.isGooglePlayServiceUpToDate(this)) {
            SupportMapFragment supportMapFragment;
            supportMapFragment = (SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map);
            supportMapFragment.getMapAsync(this);
        }

        // Animation
        this.overridePendingTransition(R.anim.abc_fade_in, R.anim.abc_fade_out);

        // ActionBar
        Toolbar toolbar = (Toolbar) findViewById(R.id.lastime_toolbar);
        toolbar.setNavigationIcon(R.drawable.ic_action_cancel);
        if (result != null) {
            toolbar.setTitle(getString(R.string.edit_location_title));
        }
        setSupportActionBar(toolbar);

        /* Set AutoComplete edit text for places */
        mAutocompleteView = (AutoCompleteTextView) findViewById(R.id.autocomplete_places);
        mAutocompleteView.setOnItemClickListener(mAutocompleteClickListener);
        useMoreRelevantLocationForPlacesSuggestions();

        // Define rename button
        renameBtn = (Button) findViewById(R.id.rename_btn);
    }

    @Override
    public void onMapReady(GoogleMap map) {
        //DO WHATEVER YOU WANT WITH GOOGLEMAP
        map.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        map.setMyLocationEnabled(true);
        map.getUiSettings().setZoomControlsEnabled(true);
        map.setOnMarkerDragListener(new MapDragListener());

        this.map = map;

        // Detect Edit Mode
        result = getIntent().getParcelableExtra("edit_location");
        if (result != null) {
            ((AutoCompleteTextView) findViewById(R.id.autocomplete_places)).setText(result.getName());
            addMarkerToMap(new LatLng(result.getLatitude(), result.getLongitude()), result.getName());
            zoomToLocation(new LatLng(result.getLatitude(), result.getLongitude()));
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        mGoogleApiClient = null;
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Location handling
        // Initialize google API for Places calls
        if (LasTimeUtils.isGooglePlayServicesAvailable(this)) {
            buildGoogleApiClient();
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void onBackPressed() {

        if (isTutorialModeActivated()) {
            (new StopTutorialFragment()).show(getFragmentManager(), "stopTutorialDialog");
        } else {
            // Second one should leave the application as per standard implem
            super.onBackPressed();
        }

    }

    /*
        Add marker at given set of coordinates and assign name to tooltip
     */
    private void addMarkerToMap(LatLng coordinates, String name) {
            map.clear();
            map.addMarker(new MarkerOptions()
                    .position(coordinates)
                    .title(name).draggable(true));
    }

    /*
        Zoom google camera focus to given set of coordinates
     */
    private void zoomToLocation(LatLng coordinates) {
        CameraUpdate goToLocation = CameraUpdateFactory.newLatLngZoom(coordinates, ZOOM_LEVEL);
        map.animateCamera(goToLocation);
    }

    /**
     * Called when the Activity could not connect to Google Play services and the auto manager
     * could resolve the error automatically.
     * In this case the API is not available and notify the user.
     *
     * @param connectionResult can be inspected to determine the cause of the failure
     */
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        Snackbar.make(findViewById(R.id.autocomplete_places), "Could not connect to Google Play service", Snackbar.LENGTH_LONG).show();
        Log.e(this.getClass().getSimpleName(), "Could not connect to Google Play service: " + connectionResult.getErrorCode());
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu items for use in the action bar
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_new_location, menu);
        // Tint icons
        Drawable drawable = menu.findItem(R.id.help).getIcon();
        drawable = DrawableCompat.wrap(drawable);
        DrawableCompat.setTint(drawable, ContextCompat.getColor(this, R.color.white_theme));
        menu.findItem(R.id.help).setIcon(drawable);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            // To prevent clean on edit event when getting back to this screen
            finish();
        }
        return true;
    }

    /**
     * Define action when menu item to use currently defined location is clicked
     */
    public void useMarkedLocation(MenuItem item) {
        if (result != null) {
            EventLocation closestLocation = findClosestLocationAlreadyExisting(result);
            if (closestLocation != null) {
                // Show popup
                ReuseClosestLocationDialogFragment.newInstance(closestLocation).show(getFragmentManager(), "reuseClosestLocation");
            } else {
                returnActivityResult();
            }
        } else {
            BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.NO_LOC_IDENTIFIED_FOR_BAD_CONNECTION, null).show(getFragmentManager(), "noLocIdentifiedForBadConnection");
        }

    }

    /**
     * Define action when menu item to run location handling tutorial is clicked
     */
    public void runHelpTutorial(MenuItem item) {
        (new LocationTutorialDialogFragment()).show(getFragmentManager(), "locationtutorialDialog");
    }

    public void doPositiveReuseLocationOperation(EventLocation location) {
        ((AutoCompleteTextView) findViewById(R.id.autocomplete_places)).setText(location.getName());
        result = location;
        returnActivityResult();
    }

    public void doNegativeCreateNewLocationOperation() {
        returnActivityResult();
    }

    private void returnActivityResult() {
        String name = ((AutoCompleteTextView) findViewById(R.id.autocomplete_places)).getText().toString();
        if (name != null && !name.isEmpty()) {
            Intent returnIntent = new Intent();
            result.setName(((AutoCompleteTextView) findViewById(R.id.autocomplete_places)).getText().toString());
            returnIntent.putExtra("result", (Parcelable) result);
            setResult(RESULT_OK, returnIntent);
            mGoogleApiClient.disconnect();
            finish();
        } else {
            BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.NAME_MANADATORY_FOR_LOCATION, null).show(getFragmentManager(), "nameManadatoryForLoc");
        }
    }

    private EventLocation findClosestLocationAlreadyExisting(EventLocation tentativeNewLocation) {
        // Get DB access
        EventDAO eventDAO = new EventDAO(this.getApplicationContext());
        // get existing locations
        List<EventLocation> existingLocations = eventDAO.getAllEventLocations();
        // Closest location result
        EventLocation eventLocation = null;
        double distance;

        // Identify closest location to current tentatively created location
        if (existingLocations != null && !existingLocations.isEmpty()) {
            eventLocation = existingLocations.get(0);
            distance = LasTimeUtils.gps2m(tentativeNewLocation, existingLocations.get(0));

            for (EventLocation existingLocation : existingLocations) {
                if (LasTimeUtils.gps2m(tentativeNewLocation, existingLocation) < distance) {
                    eventLocation = existingLocation;
                    distance = LasTimeUtils.gps2m(tentativeNewLocation, existingLocation);
                }
            }
        }

        // Check if identified location relevant in terms of threshold
        if (eventLocation != null && LasTimeUtils.gps2m(tentativeNewLocation, eventLocation) > DISTANCE_THRESHOLD) {
            eventLocation = null;
        }

        return eventLocation;
    }

    public void renameGooglePlaceLocation(View v) {
        RenameLocationDialogFragment renameLocationPopup = RenameLocationDialogFragment.newInstance(mAutocompleteView.getText().toString());
        renameLocationPopup.show(getFragmentManager(), "renameLocationPopup");
    }

    /*
     * Allows user to set pin to current location
     */
    public void setPinToCurrentLocation(View v) {
        // First trust localisation API
        if (loc != null) {
            addMarkerToMap(new LatLng(loc.getLatitude(), loc.getLongitude()), getString(R.string.new_place));
            Snackbar.make(findViewById(R.id.autocomplete_places), getString(R.string.pin_set_notif_text), Snackbar.LENGTH_SHORT).show();
            initSearchAndLocation(loc);
            // If not available trust phone knowledge of last know position
        } else if (lastKnownLocation != null) {
            addMarkerToMap(new LatLng(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude()), getString(R.string.new_place));
            Snackbar.make(findViewById(R.id.autocomplete_places), getString(R.string.pin_set_notif_text), Snackbar.LENGTH_SHORT).show();
            initSearchAndLocation(lastKnownLocation);
            // Otherwise just notify user to wait
        } else {
            Snackbar.make(findViewById(R.id.autocomplete_places), getString(R.string.loc_name_manadatory_content), Snackbar.LENGTH_SHORT).show();
        }
    }

    // Initialize searchbar and zoom to defined location
    private void initSearchAndLocation(Location l) {
        // Handle searchbar
        hideRenameButton();
        ((ClearableAutoCompleteTextView) findViewById(R.id.autocomplete_places)).setText("");
        //Zoom to location
        zoomToLocation(new LatLng(l.getLatitude(), l.getLongitude()));
    }

    private void hideRenameButton() {
        renameBtn.setVisibility(View.GONE);
    }

    private void showRenameButton() {
        renameBtn.setVisibility(View.VISIBLE);
    }

    public void doPositiveRenameLocationOperation(String newName) {
        result.setName(newName);
        mAutocompleteView.setText(newName);
        mAutocompleteView.requestFocus();
        mAutocompleteView.dismissDropDown();
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .build();
    }

    @Override
    public void onConnectionSuspended(int i) {
        /** Not used yet **/
    }

    @Override
    public void onConnected(Bundle connectionHint) {
        // Get last known location
        lastKnownLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        useMoreRelevantLocationForPlacesSuggestions();
        // Start location request
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, getLocationRequest(), this);
    }

    private LocationRequest getLocationRequest() {
        LocationRequest mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(10000);
        mLocationRequest.setFastestInterval(5000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        return mLocationRequest;
    }

    @Override
    public void onLocationChanged(Location location) {

        // Save current location
        loc = location;

        // In case no location was found before through GPS or via edit text selection
        if (result == null) {
            // Update location variable with new Location
            result = new EventLocation();
            result.setLatitude(location.getLatitude());
            result.setLongitude(location.getLongitude());
            // Add corresponding marker and zoom to place
            addMarkerToMap(new LatLng(location.getLatitude(), location.getLongitude()), getString(R.string.new_place));
            zoomToLocation(new LatLng(location.getLatitude(), location.getLongitude()));

            useMoreRelevantLocationForPlacesSuggestions();
        }
    }

    private void useMoreRelevantLocationForPlacesSuggestions() {

        double extension = 0.3;
        LatLngBounds adapterBounds = new LatLngBounds(new LatLng(-extension, -extension), new LatLng(extension, extension));

        if (loc != null) {
            adapterBounds = new LatLngBounds(
                    new LatLng(loc.getLatitude() - extension, loc.getLongitude() - extension),
                    new LatLng(loc.getLatitude() + extension, loc.getLongitude() + extension)
            );
        } else if (lastKnownLocation != null) {
            adapterBounds = new LatLngBounds(
                    new LatLng(lastKnownLocation.getLatitude() - extension, lastKnownLocation.getLongitude() - extension),
                    new LatLng(lastKnownLocation.getLatitude() + extension, lastKnownLocation.getLongitude() + extension)
            );
        }

        mAdapter = new PlaceAutocompleteAdapter(this, R.layout.custom_autocomplete_adapter, mGoogleApiClient, adapterBounds, null);
        mAutocompleteView.setAdapter(mAdapter);
    }

    public void startTourGuide() {
        setTutorialModeActivated(true);
        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_LOCATION_SEARCH_BAR, NewLocationActivity.this);
        sv.overrideButtonClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.hide();
                showTourMapFeatures();
            }
        });
    }

    // ********************** Tutorial ***********************

    public void showTourMapFeatures() {
        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_LOCATION_MAP_FEATURES, NewLocationActivity.this);
        sv.overrideButtonClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.hide();
                showLocationCreation();
            }
        });
    }

    public void showLocationCreation() {
        sv = ShowcaseViewFactory.get(ShowcaseViewFactory.ShowcaseViewType.SHOWCASE_LOCATION_SET_LOCATION, NewLocationActivity.this);
        sv.overrideButtonClick(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sv.hide();
                setTutorialModeActivated(false);
                (new LocationTutorialEndDialogFragment()).show(getFragmentManager(), "locationtutorialendDialog");
            }
        });
    }

    /**
     * Method to call when the tutorial needs to be fully cleaned from the activity
     */
    public void stopTutorial() {
        setTutorialModeActivated(false);
        sv.hide();
        sv = null;
    }

    /**
     * Drag listener for markers on the google map
     */
    public class MapDragListener implements GoogleMap.OnMarkerDragListener {

        private double startingLat;
        private double startingLong;

        @Override
        public void onMarkerDragStart(Marker marker) {
            // Record initial latitudes when marker is dragged
            startingLat = marker.getPosition().latitude;
            startingLong = marker.getPosition().longitude;
        }

        @Override
        public void onMarkerDrag(Marker marker) {
            /** Not used yet **/
        }

        @Override
        public void onMarkerDragEnd(Marker marker) {
            if (marker.getPosition().latitude != startingLat || marker.getPosition().longitude != startingLong) {
                // Clean edit text
                mAutocompleteView.setText("");
                // New Result object
                result = new EventLocation();
                result.setLatitude(marker.getPosition().latitude);
                result.setLongitude(marker.getPosition().longitude);
                // Standardize marker name
                marker.setTitle(getResources().getString(R.string.new_location));
                // Hide Rename Button since custom place
                hideRenameButton();
            }
        }

    }

}


