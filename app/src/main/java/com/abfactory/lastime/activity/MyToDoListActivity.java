package com.abfactory.lastime.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.StaggeredGridLayoutManager;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.TodoViewAdapter;
import com.abfactory.lastime.drawer.DrawerMenu;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.service.LastimeJobService;
import com.abfactory.lastime.utils.PreferencesHandler;

import java.util.Date;

/**
 * Display the changelog of the application
 */
public class MyToDoListActivity extends BaseActivity {

    IEventDAO eventDAO;
    private RecyclerView recList;
    private BroadcastReceiver receiver;
    private DrawerMenu drawerMenu;
    private TodoViewAdapter todoViewAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notifications);
        Toolbar toolbar = (Toolbar) findViewById(R.id.display_event_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        drawerMenu = new DrawerMenu((DrawerLayout) findViewById(R.id.drawer_layout), this);
        // Nice arrow on top left which spins from the hamburger menu
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(
                this, drawerMenu.getDrawerLayout(), toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close
        );
        drawerMenu.getDrawerLayout().addDrawerListener(drawerToggle);
        if(getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
        }
        drawerToggle.syncState();

        // Database access
        eventDAO = new EventDAO(this.getApplicationContext());

        //CardView
        recList = (RecyclerView) findViewById(R.id.cardList);
        recList.setHasFixedSize(true);
        StaggeredGridLayoutManager llm = new StaggeredGridLayoutManager(2, StaggeredGridLayoutManager.VERTICAL);
        recList.setLayoutManager(llm);

        // Handle list display
        todoViewAdapter = new TodoViewAdapter(this);
        recList.setAdapter(todoViewAdapter);

        // Handle broadcast for notifs reception
        receiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                refreshDisplay();
            }
        };

        // Today header
        ((TextView) findViewById(R.id.today_textview)).setText(Event.CALENDAR_DISPLAY.format(new Date()));
    }

    @Override
    protected void onResume() {
        super.onResume();
        refreshDisplay();
        todoViewAdapter.refresh();
        drawerMenu.refreshBulletDisplay();
    }

    @Override
    protected void onStart() {
        super.onStart();
        LocalBroadcastManager.getInstance(this).registerReceiver((receiver),
                new IntentFilter(LastimeJobService.NOTIFICATION_RESULT)
        );
    }

    @Override
    protected void onStop() {
        LocalBroadcastManager.getInstance(this).unregisterReceiver(receiver);
        super.onStop();
    }

    public void refreshDisplay() {

        // If no notification to be shown
        if(eventDAO.getAllActiveNotifications()==null || eventDAO.getAllActiveNotifications().isEmpty()){
            // Hide/Show layouts
            if ((new PreferencesHandler(this)).isNotificationsActivated()) {
                findViewById(R.id.no_notif_panel).setVisibility(View.VISIBLE);
                findViewById(R.id.notif_deactivated).setVisibility(View.GONE);
            } else {
                findViewById(R.id.no_notif_panel).setVisibility(View.GONE);
                findViewById(R.id.notif_deactivated).setVisibility(View.VISIBLE);
            }
            recList.setVisibility(View.GONE);
        } else {
            // If notification raised
            // Hide/Show layouts
            findViewById(R.id.no_notif_panel).setVisibility(View.GONE);
            findViewById(R.id.notif_deactivated).setVisibility(View.GONE);
            recList.setVisibility(View.VISIBLE);
            todoViewAdapter.refresh();
        }

        // Animation
        this.overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_left);
    }

    public View getMainContainer(){
        return findViewById(R.id.main_container);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_todo_list, menu);
        refreshDisplay();
        return true;
    }

    /**
     * Call main activity, try to resume it
     * @param item is not used
     */
    public void goBackToAllEvents(MenuItem item) {
        DrawerMenu.openActivity(this, LasTimeActivity.class);
    }

    public DrawerMenu getDrawerMenu() {
        return drawerMenu;
    }
}
