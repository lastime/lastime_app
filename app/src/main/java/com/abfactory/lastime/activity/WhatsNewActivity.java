package com.abfactory.lastime.activity;

import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.abfactory.lastime.R;
import com.abfactory.lastime.component.WhatsNewPagerAdapter;
import com.abfactory.lastime.component.whatsnew.CirclePageIndicator;

public class WhatsNewActivity extends BaseActivity {


    /** Called when the activity is first created. */
    @Override

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_whats_new);

        WhatsNewPagerAdapter adapter = new WhatsNewPagerAdapter();
        ViewPager myPager = (ViewPager) findViewById(R.id.pager);
        myPager.setAdapter(adapter);
        myPager.setCurrentItem(0);
        CirclePageIndicator indicator = (CirclePageIndicator)findViewById(R.id.indicator);
        indicator.setViewPager(myPager);
    }
}
