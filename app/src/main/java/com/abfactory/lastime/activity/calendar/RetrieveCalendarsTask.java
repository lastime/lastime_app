package com.abfactory.lastime.activity.calendar;

import android.os.AsyncTask;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.google.api.client.extensions.android.http.AndroidHttp;
import com.google.api.client.googleapis.extensions.android.gms.auth.GooglePlayServicesAvailabilityIOException;
import com.google.api.client.googleapis.extensions.android.gms.auth.UserRecoverableAuthIOException;
import com.google.api.client.http.HttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.calendar.model.CalendarList;
import com.google.api.services.calendar.model.CalendarListEntry;

import java.io.IOException;
import java.util.List;

public class RetrieveCalendarsTask extends AsyncTask<String, Void, List<CalendarListEntry>> {

    private GoogleCalendarActivity activity;

    private com.google.api.services.calendar.Calendar calendarService = null;
    private Exception mLastError = null;

    public RetrieveCalendarsTask(GoogleCalendarActivity activity) {

        this.activity = activity;

        HttpTransport transport = AndroidHttp.newCompatibleTransport();
        JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
        calendarService = new com.google.api.services.calendar.Calendar.Builder(
                transport, jsonFactory, activity.getmCredential())
                .setApplicationName("LasTime")
                .build();
    }

    /**
     * Background task to call Google Calendar API.
     *
     * @param params no parameters needed for this task.
     */
    @Override
    protected List<CalendarListEntry> doInBackground(String... params) {
        try {
            return getDataFromApi();
        } catch (Exception e) {
            mLastError = e;
            cancel(true);
            return null;
        }
    }

    /**
     * Fetch a list of calendars.
     *
     * @return List of Strings describing returned events.
     * @throws IOException
     */
    private List<CalendarListEntry> getDataFromApi() throws IOException {

        CalendarList calendarList = calendarService.calendarList().list().execute();
        List<CalendarListEntry> calendarItems = calendarList.getItems();

        return calendarItems;
    }


    @Override
    protected void onPreExecute() {
        activity.getmOutputText().setText("");
        activity.getmProgress().show();
    }

    @Override
    protected void onPostExecute(List<CalendarListEntry> output) {
        activity.getmProgress().hide();
        activity.getButtonLayout().removeAllViews();
        if (output == null || output.size() == 0) {
            activity.getmOutputText().setText("No results returned.");
        } else {
            ViewGroup.LayoutParams tlp = new ViewGroup.LayoutParams(
                    ViewGroup.LayoutParams.WRAP_CONTENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT);
            // Create button for each calendar
            for (final CalendarListEntry entry : output) {
                Button button = new Button(activity);
                button.setText(entry.getSummary());
                button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        new RetrieveEventsTask(activity).execute(entry.getId());
                    }
                });
                activity.getButtonLayout().addView(button, tlp);
            }
        }
    }

    @Override
    protected void onCancelled() {
        activity.getmProgress().hide();
        if (mLastError != null) {
            if (mLastError instanceof GooglePlayServicesAvailabilityIOException) {
                activity.showGooglePlayServicesAvailabilityErrorDialog(
                        ((GooglePlayServicesAvailabilityIOException) mLastError)
                                .getConnectionStatusCode());
            } else if (mLastError instanceof UserRecoverableAuthIOException) {
                activity.startActivityForResult(
                        ((UserRecoverableAuthIOException) mLastError).getIntent(),
                        GoogleCalendarActivity.REQUEST_AUTHORIZATION);
            } else {
                activity.getmOutputText().setText("The following error occurred:\n"
                        + mLastError.getMessage());
            }
        } else {
            activity.getmOutputText().setText("Request cancelled.");
        }
    }
}
