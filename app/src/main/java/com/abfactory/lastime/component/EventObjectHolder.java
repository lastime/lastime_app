package com.abfactory.lastime.component;

import android.widget.RelativeLayout;

public class EventObjectHolder {
    public RelativeLayout mainView;
    public RelativeLayout rightView;
    public RelativeLayout leftView;
}
