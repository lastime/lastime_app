package com.abfactory.lastime.component;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LasTimeActivity;
import com.abfactory.lastime.activity.MyToDoListActivity;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.Notification;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.NotificationUtils;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.joda.time.DateTime;

import java.io.File;
import java.util.Date;
import java.util.List;

public class TodoViewAdapter extends RecyclerView.Adapter<TodoViewAdapter.TodoViewHolder> {

    private List<Notification> notifications;
    private EventDAO eventDAO;
    private MyToDoListActivity activity;

    public TodoViewAdapter(MyToDoListActivity activity) {
        this.eventDAO = new EventDAO(activity);
        notifications = eventDAO.getAllActiveNotifications();
        this.activity = activity;
    }

    public void refresh() {
        notifications = eventDAO.getAllActiveNotifications();
        this.notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return notifications.size();
    }

    @Override
    public void onBindViewHolder(TodoViewHolder todoViewHolder, int i) {
        final Date today = new Date();
        final Notification notification = notifications.get(i);
        final Event event = eventDAO.retrieveEventById(notification.getEventId());

        // In case notification points on a deleted event
        if (event == null) {
            eventDAO.deleteNotification(notification.getNotificationId());
            return;
        }

        todoViewHolder.eventName.setText(event.getTitle());

        // Display last date event has been done
        todoViewHolder.eventDate.setText(Event.CALENDAR_DISPLAY_NOTIF.format(event.getDate()));

        int days = LasTimeUtils.getDaysDifference(event.getDate(), today);
        if (days > 0) {
            todoViewHolder.eventDays.setText(days + " " + activity.getString(R.string.days));
            todoViewHolder.eventDays.setVisibility(View.VISIBLE);
        } else {
            todoViewHolder.eventDays.setVisibility(View.GONE);
        }

        if (event.getThumbnailPath() != null) {
            todoViewHolder.eventLayout.setBackgroundColor(LasTimeUtils.getColor(activity.getApplicationContext(), R.color.transparent_black));
            // Display event picture if defined
            Picasso.with(activity).load(Uri.fromFile(new File(event.getThumbnailPath())))
                    .resize(LasTimeUtils.getWidthScreen(activity.getApplicationContext()), (int) LasTimeUtils.convertDpToPixel(100, activity))
                    .centerCrop()
                    .into(new Target() {
                        @Override
                        public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                            todoViewHolder.cardView.setBackground(new BitmapDrawable(activity.getApplicationContext().getResources(), bitmap));
                        }

                        @Override
                        public void onBitmapFailed(final Drawable errorDrawable) {
                            Log.d("TodoViewAdapter", "FAILED");
                        }

                        @Override
                        public void onPrepareLoad(final Drawable placeHolderDrawable) {
                            Log.d("TodoViewAdapter", "Prepare Load");
                        }
                    });
        } else {
            todoViewHolder.eventLayout.setBackgroundColor(event.getColor());
            todoViewHolder.cardView.setBackground(null);
        }

        todoViewHolder.cardView.setContentPadding(0, 0, 0, 0);

        // View event
        todoViewHolder.cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Pass event to DisplayEventActivity
                Intent intent = new Intent(activity, DisplayEventActivity.class);
                intent.putExtra("eventId", notification.getEventId());
                activity.startActivityForResult(intent, LasTimeActivity.EVENT_DISPLAY);
            }
        });

        // Button to ignore event notif
        todoViewHolder.closeCard.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // Set notif to old and refresh list.
                (new EventDAO(activity)).setNotificationToOld(notification.getNotificationId());
                NotificationUtils.cleanPhoneNotificationForEvent(activity, (new EventDAO(activity)).retrieveEventById(notification.getEventId()));
                notifications.remove(notification);
                TodoViewAdapter.this.notifyDataSetChanged();

                // Update menu bullet display
                activity.getDrawerMenu().refreshBulletDisplay();
            }
        });
    }

    @Override
    public TodoViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.todo_cards_list, viewGroup, false);

        return new TodoViewHolder(itemView);
    }

    public class TodoViewHolder extends RecyclerView.ViewHolder {

        protected TextView eventName;
        protected TextView eventDate;
        protected TextView eventDays;
        protected LinearLayout eventLayout;
        protected ImageView closeCard;
        protected RelativeLayout recurrencePanel;
        protected CardView cardView;

        public TodoViewHolder(View itemView) {
            super(itemView);
            eventName = itemView.findViewById(R.id.todo_event_name);
            eventDate = itemView.findViewById(R.id.todo_event_date);
            eventDays = itemView.findViewById(R.id.todo_event_days);
            closeCard = itemView.findViewById(R.id.close_cards);
            eventLayout = itemView.findViewById(R.id.todo_event_layout);
            recurrencePanel = itemView.findViewById(R.id.todo_recurrence_panel);
            cardView = itemView.findViewById(R.id.card_view);
        }
    }
}
