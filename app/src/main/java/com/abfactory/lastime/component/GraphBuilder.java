package com.abfactory.lastime.component;

import com.abfactory.lastime.Lastime;
import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.LegendRenderer;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import org.joda.time.DateTime;
import org.joda.time.DateTimeUtils;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

public class GraphBuilder {

    public final static String[] YEAR_ARRAY = new String[]{"Ja", "Fe", "Mar", "Av", "Mai", "Ju", "Jui", "Au", "Se", "Oc", "No", "De"};
    public final static String[] WEEK_ARRAY = new String[]{"Lun", "Mar", "Mer", "Jeu", "Ven", "Sam", "Dim"};


    public static void createEventOccurrenceGraphPerYear(GraphView graph, Event event) {
        IEventDAO eventDao = new EventDAO(Lastime.getContext());

        int currentYear = DateTime.now().getYear();

        // Graph config
        graph.removeAllSeries();
        graph.getLegendRenderer().resetStyles();
        graph.getViewport().setMinX(0);
        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(YEAR_ARRAY);
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph.setTitleColor(LasTimeUtils.getColor(Lastime.getContext(), R.color.white_theme));
        graph.setTitle("Nombre d'occurances par an / Total: " + eventDao.getAllEventHistory(event).size());
        graph.getLegendRenderer().setVisible(true);
        graph.getLegendRenderer().setAlign(LegendRenderer.LegendAlign.TOP);

        // Lines
        LineGraphSeries<DataPoint> currentYearLine = new LineGraphSeries<>();
        currentYearLine.setColor(LasTimeUtils.getColor(Lastime.getContext(), R.color.ColorExtra));
        LineGraphSeries<DataPoint> currentYearLineM1 = new LineGraphSeries<>();
        currentYearLineM1.setColor(LasTimeUtils.getColor(Lastime.getContext(), R.color.ColorSecondary));
        LineGraphSeries<DataPoint> currentYearLineM2 = new LineGraphSeries<>();
        currentYearLineM2.setColor(LasTimeUtils.getColor(Lastime.getContext(), R.color.ColorPrimaryDark));

        // Only on 3 last years
        for (int i = 0; i < 3; i++) {
            int[] eventByMonths = eventDao.getEventsByMonths(event, currentYear - i);
            int monthCounter = 0;
            int eventCounter = 0;
            if (currentYear - i == currentYear) {
                for (int number : eventByMonths) {
                    currentYearLine.appendData(new DataPoint(monthCounter + 1, number), false, 1000);
                    monthCounter++;
                    eventCounter += number;
                }
                currentYearLine.setTitle(Integer.toString(currentYear - i) + " - " + eventCounter);
            }
            if (currentYear - i == currentYear - 1) {
                for (int number : eventByMonths) {
                    currentYearLineM1.appendData(new DataPoint(monthCounter + 1, number), false, 1000);
                    monthCounter++;
                    eventCounter += number;
                }
                currentYearLineM1.setTitle(Integer.toString(currentYear - i) + " - " + eventCounter);
            }
            if (currentYear - i == currentYear - 2) {
                for (int number : eventByMonths) {
                    currentYearLineM2.appendData(new DataPoint(monthCounter + 1, number), false, 1000);
                    monthCounter++;
                    eventCounter += number;
                }
                currentYearLineM2.setTitle(Integer.toString(currentYear - i) + " - " + eventCounter);
            }
        }

        // Add them to chart
        graph.addSeries(currentYearLine);
        graph.addSeries(currentYearLineM1);
        graph.addSeries(currentYearLineM2);
    }

    public static void createEventOccurrenceGraphPerMonth(GraphView graph, Event event, int month, int year) {
        IEventDAO eventDao = new EventDAO(Lastime.getContext());

        DateTime datetime = DateTime.now();
        datetime.withMonthOfYear(month);
        DateTimeFormatter formatter = DateTimeFormat.forPattern("MMMMM");

        // Graph config
        graph.removeAllSeries();
        graph.getLegendRenderer().resetStyles();
        graph.getLegendRenderer().setVisible(false);
        graph.getViewport().setMinX(0);
        // use static labels for horizontal and vertical labels
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph);
        staticLabelsFormatter.setHorizontalLabels(WEEK_ARRAY);
        graph.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph.setTitleColor(LasTimeUtils.getColor(Lastime.getContext(), R.color.white_theme));

        // Bar
        BarGraphSeries<DataPoint> currentBar = new BarGraphSeries<>();
        currentBar.setColor(LasTimeUtils.getColor(Lastime.getContext(), R.color.ColorSecondary));
        currentBar.setSpacing(50);
        currentBar.setDrawValuesOnTop(true);

        int[] eventByDays = eventDao.getEventsByDays(event, month, year);
        int dayCounter = 0;
        int eventCounter = 0;
        for (int number : eventByDays) {
            currentBar.appendData(new DataPoint(dayCounter + 1, number), false, 1000);
            dayCounter++;
            eventCounter += number;
        }

        // Add them to chart
        graph.addSeries(currentBar);

        graph.setTitle("Nombre d'occurances pour " + formatter.print(datetime)
                + " / Total: " + eventCounter);
    }
}
