package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.NewLocationActivity;
import com.abfactory.lastime.activity.WishListActivity;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.model.EventTemplate;
import com.abfactory.lastime.utils.LasTimeUtils;

public class WishListActionsDialogFragment extends DialogFragment {

    private static final String ARG_CLICKED_WISH = "clicked_wish";

    public WishListActionsDialogFragment(){
        super();
    }

    public static WishListActionsDialogFragment newInstance(EventTemplate clickedWish) {
        WishListActionsDialogFragment f = new WishListActionsDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable(ARG_CLICKED_WISH, clickedWish);
        f.setArguments(args);

        return f;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final EventTemplate wish = getArguments().getParcelable(ARG_CLICKED_WISH);

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        String[] actions;
        actions = new String[] {this.getString(R.string.create_from_template), this.getString(R.string.delete_template)};

        dialog.setTitle(wish.getTitle());
        dialog.setItems(actions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (i == 0) {
                    ((WishListActivity) getActivity()).doClikOnCreateEventForWish(wish);
                    dismiss();
                } else {
                    ((WishListActivity) getActivity()).doClikOnDeleteWish(wish);
                    dismiss();
                }
            }
        });

        return dialog.create();

    }

}
