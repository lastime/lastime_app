package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.SettingsActivity;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.PreferencesHandler;

public class ProvideBackUpEmailDialogFragment extends DialogFragment {

    private static final String PWD_SET = "pwd";
    private PreferencesHandler ph;
    private String passwordToUse;

    public ProvideBackUpEmailDialogFragment(){
        super();
    }

    public static ProvideBackUpEmailDialogFragment newInstance(String pwd) {
        ProvideBackUpEmailDialogFragment f = new ProvideBackUpEmailDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(PWD_SET, pwd);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        passwordToUse = getArguments().getString(PWD_SET);
        ph = new PreferencesHandler(getActivity().getApplicationContext());

        final View content = getActivity().getLayoutInflater().inflate(R.layout.provide_backup_email_dialog, null);

        // Update case
        if(LasTimeUtils.isValidEmail(ph.getBackUpEmailForApp())){
            ((TextView)content.findViewById(R.id.title)).setText(R.string.update_backup_email_title);
            ((EditText) content.findViewById(R.id.backup_email)).setText(ph.getBackUpEmailForApp());
        // Creation from scratch case
        } else {
            ((TextView)content.findViewById(R.id.title)).setText(R.string.set_backup_email_title);
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(content)
                .setPositiveButton(R.string.validate, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((SettingsActivity) getActivity()).doPositiveSetEmailBackup(passwordToUse, ((EditText) content.findViewById(R.id.backup_email)).getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((SettingsActivity) getActivity()).showNoChangePerformedMessage();
                    }
                });

        return builder.create();

    }

}
