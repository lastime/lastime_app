package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.utils.LasTimeUtils;

public class ReminderDialogFragment extends DialogFragment {

    private static final String REC_TAG = "tag";
    private String reminderTag;

    public ReminderDialogFragment(){
        super();
    }

    public static ReminderDialogFragment newInstance(String recurrenceTag) {

        ReminderDialogFragment f = new ReminderDialogFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(REC_TAG, recurrenceTag);
        f.setArguments(args);
        return f;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        reminderTag = getArguments().getString(REC_TAG);

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        String[] itemsToBeDisplayed;
        if(isCustomReminder()){
            itemsToBeDisplayed = new String[] {LasTimeUtils.convertReminderTagToString(reminderTag, getActivity()),
                    getActivity().getString(R.string.no_reminder),
                    getActivity().getString(R.string.one_day_before),
                    getActivity().getString(R.string.one_week_before),
                    getActivity().getString(R.string.one_month_before)};
        } else {
            itemsToBeDisplayed = new String[] {getActivity().getString(R.string.no_reminder),
                    getActivity().getString(R.string.one_day_before),
                    getActivity().getString(R.string.one_week_before),
                    getActivity().getString(R.string.one_month_before)};
        }

        dialog.setPositiveButton(getActivity().getString(R.string.customize_recc_or_reminder), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                ReminderDialogFragmentAdvanced.newInstance(reminderTag).show(getFragmentManager(), "recurrencePicker");
            }
        });

        final ListAdapter adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.custom_single_choice_dialog_item, android.R.id.text1,itemsToBeDisplayed);

        dialog.setSingleChoiceItems(adapter, getSelectedPosition(), selectItemListener);

        return dialog.create();

    }

    private boolean isCustomReminder() {
        return reminderTag !=null
                &&!reminderTag.equals("1_DAYS")
                &&!reminderTag.equals("1_WEEKS")
                &&!reminderTag.equals("1_MONTHS")
                &&!reminderTag.equals("NONE");
    }

    private int getSelectedPosition(){
        if(reminderTag !=null){
            if(isCustomReminder()){
                return 0;
            } else {
                switch(reminderTag){
                    case "1_DAYS":
                        return 1;
                    case "1_WEEKS":
                        return 2;
                    case "1_MONTHS":
                        return 3;
                    case "NONE":
                        return 0;
                    default:
                        return 0;
                }
            }
        } else {
            return 0;
        }
    }

    private String getTagFromPosition(int position){
        int re_evaluated_position = position;
        if(isCustomReminder()){
            re_evaluated_position--;
        }
        switch(re_evaluated_position){
            case 1:
                return "1_DAYS";
            case 2:
                return "1_WEEKS";
            case 3:
                return "1_MONTHS";
            default:
                return "NONE";
        }
    }

    DialogInterface.OnClickListener selectItemListener = new DialogInterface.OnClickListener()
    {

        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            ((EditEventActivity) getActivity()).doPerformReminderUpdate(getTagFromPosition(which));
            dialog.dismiss();
        }

    };

}
