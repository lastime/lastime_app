package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LoginActivity;

public class ConfirmPwdResetDialogFragment extends DialogFragment {

    public ConfirmPwdResetDialogFragment(){
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.basic_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(R.string.pwd_reset_title);
        ((TextView)content.findViewById(R.id.error_msg)).setText(R.string.pwd_reset_content);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setPositiveButton(getResources().getString(R.string.proceed_reset),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((LoginActivity) getActivity()).doPerformClickResetPwd();
                            }
                        }
                )
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*Do nothing*/
                    }
                });

        return  builder.create();

    }

}
