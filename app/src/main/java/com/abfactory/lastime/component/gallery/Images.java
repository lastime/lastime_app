package com.abfactory.lastime.component.gallery;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * Piwigo model to map what we need in image
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class Images {

    // URL of original image
    private String element_url;
    // Name of file
    private String file;

    public String getElement_url() {
        return element_url;
    }

    public void setElement_url(String element_url) {
        this.element_url = element_url;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }
}
