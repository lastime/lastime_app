package com.abfactory.lastime.component.twowayview;

import android.app.Fragment;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.StatisticsActivity;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.List;

/**
 * Fragment class display a horizontal list
 * of events
 */
public class HorizontalListFragment extends Fragment {

    // Max events proposed
    public static final String MAX = "40";
    public static String EDIT_PAGE ="HorizontalListFragment_Edit_Page";
    public static String DISPLAY_PAGE ="HorizontalListFragment_Display_Page";
    private List<Event> events;
    private HorizontalListAdapter.Display displaySelected;
    private int eventSelection = 0;

    public void setDisplaySelected(HorizontalListAdapter.Display displaySelected){
        this.displaySelected = displaySelected;
    }

    public void setCurrentSelection(int eventPosition){
        eventSelection = eventPosition;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.horizontal_list, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        TwoWayView mRecyclerView = (TwoWayView) view.findViewById(R.id.horizontal_event_list);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLongClickable(true);

        final ItemClickSupport itemClick = ItemClickSupport.addTo(mRecyclerView);

        final HorizontalListAdapter horizontalListAdapter = new HorizontalListAdapter(getActivity(), displaySelected);

        final Drawable divider = LasTimeUtils.getDrawable(getActivity(), R.drawable.two_way_view_divider);
        mRecyclerView.addItemDecoration(new DividerItemDecoration(divider));

        final IEventDAO eventDAO = new EventDAO(getActivity().getApplicationContext());

        // BAD design!! - Hack of event counter
        int counter = 0;

        if (getActivity() instanceof StatisticsActivity) {
            final StatisticsActivity activity = (StatisticsActivity) getActivity();
            itemClick.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClick(RecyclerView parent, View child, int position, long id) {
                    // Tracking button
//                    activity.reportButtonEvent(BaseActivity.Actions.Click_on_event_prefill_item);
//                    Event event = events.get(position);
//                    new UpdateEditEvent(activity).update(event);
//                    activity.getMenu().findItem(R.id.undo_prefill).setVisible(true);
//                    LasTimeUtils.hideKeyboard(activity);
                    //FIXME: implement click
                }
            });

            // Create list with filtered events based on frequency
            events = eventDAO.getTopEvents(MAX);
        }

        if (getActivity() instanceof DisplayEventActivity) {
            final DisplayEventActivity activity = (DisplayEventActivity) getActivity();

            // Color enhancement
            horizontalListAdapter.setMainColor(LasTimeUtils.getColor(activity, R.color.ColorPrimaryDark));

            // Add a blank event used as padding
            horizontalListAdapter.addItem(0, null);
            // Set counter to one to add padding event
            counter = 1;

            itemClick.setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
                @Override
                public void onItemClick(RecyclerView parent, View child, int position, long id) {
                    // Do not handle click on fake(padding) event
                    if(position == 0 && horizontalListAdapter.isItemNull(position))
                        return;

                    // Set current selection for drawing
                    int oldSelection = horizontalListAdapter.getCurrentSelection();
                    horizontalListAdapter.setCurrentSelection(position);
                    horizontalListAdapter.notifyItemChanged(oldSelection);
                    horizontalListAdapter.notifyItemChanged(position);

                    // Update display
                    activity.getUpdateDisplayEvent().refresh(events.get(position - 1));
                    activity.setEventPosition(position);
                }
            });

            // Create list with filtered events based on history
            events = eventDAO.findHistoryOfEvent(activity.getEventDisplayed());
            events.add(0, eventDAO.getActiveEvent(activity.getEventDisplayed()));
            if(eventSelection > 0){
                horizontalListAdapter.setCurrentSelection(eventSelection);
            }else {
                horizontalListAdapter.setCurrentSelection(1);
            }
        }

        // Add all events to be displayed
        for (int i = counter; i < events.size() + counter; i++) {
            horizontalListAdapter.addItem(i, events.get(i - counter));
        }

        mRecyclerView.setAdapter(horizontalListAdapter);
    }

}
