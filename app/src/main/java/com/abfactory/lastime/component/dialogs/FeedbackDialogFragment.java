package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.abfactory.lastime.R;
import com.abfactory.lastime.mail.BackgroundMail;
import com.abfactory.lastime.mail.Utils;
import com.abfactory.lastime.utils.LasTimeUtils;

public class FeedbackDialogFragment extends DialogFragment{

    private final static String LASTIME_CONTACT_MAIL = "lastimeapp.contact@gmail.com";
    private final static String LASTIME_CONTACT_PWD = "n5h8H3XVLMaX4X2jGFCzdg==";

    public FeedbackDialogFragment() {
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final View content = getActivity().getLayoutInflater().inflate(R.layout.feedback_dialog, null);

        ((TextView)content.findViewById(R.id.title)).setText(R.string.feedback_popup_title);
        ((TextView) content.findViewById(R.id.feedback_title)).setText(
                getString(R.string.your_feedback) + " (0/" + getResources().getInteger(R.integer.max_feedback_length) + ")"
        );

        ((EditText)content.findViewById(R.id.team)).addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                /*Do nothing*/
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                ((TextView) content.findViewById(R.id.feedback_title)).setText(
                        getString(R.string.your_feedback) + " (" + s.length() + "/" + getResources().getInteger(R.integer.max_feedback_length) + ")"
                );
            }

            @Override
            public void afterTextChanged(Editable s) {
                /*Do nothing*/
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setPositiveButton(R.string.ok,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                if (LasTimeUtils.isDeviceOnline(getActivity())) {
                                    sendFeedbackEmail(((EditText) content.findViewById(R.id.team)).getText().toString(),
                                            (String) ((Spinner) content.findViewById(R.id.feedback_type)).getSelectedItem(),
                                            ((EditText) content.findViewById(R.id.email_contact)).getText().toString());
                                    if (LasTimeUtils.isValidEmail(((EditText) content.findViewById(R.id.email_contact)).getText().toString())) {
                                        (BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.FEEDBACK_SENT, null)).show(getFragmentManager(), "feedbackSent");
                                    } else {
                                        (BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.FEEDBACK_SENT_NO_EMAIL, null)).show(getFragmentManager(), "feedbackSentNoEmail");
                                    }
                                } else {
                                    Toast.makeText(getActivity(), "Your device is not connected to network!", Toast.LENGTH_LONG).show();
                                }
                            }
                        }
                )
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                // Do nothing
                            }
                        }
                );

        this.setCancelable(true);

        final Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);

        return dialog;
    }

    private void sendFeedbackEmail(String feedback, String type, String contactEmail) {
            BackgroundMail bm = new BackgroundMail(getActivity());
            bm.setGmailUserName(LASTIME_CONTACT_MAIL);
            bm.setGmailPassword(Utils.decryptIt(LASTIME_CONTACT_PWD));
            bm.setMailTo(LASTIME_CONTACT_MAIL);
            bm.setFormSubject("[" + type.toUpperCase() + "] LasTime app");
            bm.setFormBody("Email: " + contactEmail + "\n\n" + feedback);
            bm.setProcessVisibility(false);
            bm.send();
    }

}
