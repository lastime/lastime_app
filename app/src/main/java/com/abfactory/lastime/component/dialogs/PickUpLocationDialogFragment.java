package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LocationsHandlingActivity;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.ArrayList;
import java.util.List;

public class PickUpLocationDialogFragment  extends DialogFragment {

    private static final String EVENTS_TO_DISPLAY_TYPE = "type";
    private static final String REFERENCE_LOCATION_TYPE = "ref";

    public PickUpLocationDialogFragment(){
        super();
    }

    public static PickUpLocationDialogFragment newInstance(List<EventLocation> locationsToDisplay, EventLocation referenceLocation) {
        PickUpLocationDialogFragment f = new PickUpLocationDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelableArrayList(EVENTS_TO_DISPLAY_TYPE, new ArrayList<EventLocation>(locationsToDisplay));
        args.putParcelable(REFERENCE_LOCATION_TYPE, referenceLocation);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final List<EventLocation> locationsToDisplay = getArguments().getParcelableArrayList(EVENTS_TO_DISPLAY_TYPE);
        final EventLocation referenceLocation = getArguments().getParcelable(REFERENCE_LOCATION_TYPE);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.pick_up_location_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(R.string.pick_up_location_title);

        switch(locationsToDisplay.size()){
            case 3:
                (content.findViewById(R.id.location_three_layout)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((EditEventActivity) getActivity()).useLocationForEvent(locationsToDisplay.get(2));
                        dismiss();
                    }
                });
                ((TextView) content.findViewById(R.id.location_three_name)).setText(locationsToDisplay.get(2).getName());
                if (referenceLocation != null)
                    ((TextView) content.findViewById(R.id.location_three_distance)).setText("(" + LasTimeUtils.getDisplayableDistanceBetweenTwoLocations(locationsToDisplay.get(2), referenceLocation) + ")");
            case 2:
                (content.findViewById(R.id.location_two_layout)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((EditEventActivity) getActivity()).useLocationForEvent(locationsToDisplay.get(1));
                        dismiss();
                    }
                });
                ((TextView) content.findViewById(R.id.location_two_name)).setText(locationsToDisplay.get(1).getName());
                if (referenceLocation != null)
                    ((TextView) content.findViewById(R.id.location_two_distance)).setText("(" + LasTimeUtils.getDisplayableDistanceBetweenTwoLocations(locationsToDisplay.get(1), referenceLocation) + ")");
            case 1:
                (content.findViewById(R.id.location_one_layout)).setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ((EditEventActivity) getActivity()).useLocationForEvent(locationsToDisplay.get(0));
                        dismiss();
                    }
                });
                ((TextView) content.findViewById(R.id.location_one_name)).setText(locationsToDisplay.get(0).getName());
                if (referenceLocation != null)
                    ((TextView) content.findViewById(R.id.location_one_distance)).setText("(" + LasTimeUtils.getDisplayableDistanceBetweenTwoLocations(locationsToDisplay.get(0), referenceLocation) + ")");
                // Handle sections gone
                if(locationsToDisplay.size()<3)
                    content.findViewById(R.id.location_three_layout).setVisibility(View.GONE);
                if(locationsToDisplay.size()<2)
                    content.findViewById(R.id.location_two_layout).setVisibility(View.GONE);
                break;
            case 0:
                content.findViewById(R.id.location_one_layout).setVisibility(View.GONE);
                content.findViewById(R.id.location_two_layout).setVisibility(View.GONE);
                content.findViewById(R.id.location_three_layout).setVisibility(View.GONE);
                ((TextView) content.findViewById(R.id.closest_location_text)).setText(getResources().getString(R.string.incentive_when_no_loc));
                break;
            default:
                Log.e("PICK_UP_LOC", "We should not reach this pickup creation with more than 3 places");
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content);

        builder.setNeutralButton(R.string.choose_in_list,(dialogInterface, i) -> {
            ((EditEventActivity)getActivity()).chooseLocationFromList();
        });

        if(locationsToDisplay.size()==0){
            builder.setPositiveButton(R.string.add_new_location_text_when_none, (dialogInterface, i) -> ((EditEventActivity) getActivity()).chooseLocation());
        } else {
            builder.setPositiveButton(R.string.add_new_location_text, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    ((EditEventActivity) getActivity()).chooseLocation();
                }
            });
        }

        return builder.create();
    }

}
