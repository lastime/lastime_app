package com.abfactory.lastime.component;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.abfactory.lastime.R;

public class WhatsNewPagerAdapter extends PagerAdapter {

    public int getCount() {
        return 3;
    }

    public Object instantiateItem(View collection, int position) {

        LayoutInflater inflater = (LayoutInflater) collection.getContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.layout_whats_new_pager_text_mode, null);

        ((ViewPager) collection).addView(view, 0);
        switch (position) {
            case 0:
                ((TextView)view.findViewById(R.id.text)).setText("Awesome Feature #1");
                ((ImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.screenshot1);
                ((TextView)view.findViewById(R.id.description)).setText("Swiper un événement afin de le répéter facilement à la date du jour !");
                break;
            case 1:
                ((TextView)view.findViewById(R.id.text)).setText("Awesome Feature #2");
                ((ImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.screenshot2);
                ((TextView)view.findViewById(R.id.description)).setText("Choisis les données que tu veux voir apparaître lors de la création ou la modification d'un événement.");
                break;
            case 2:
                ((TextView)view.findViewById(R.id.text)).setText("Awesome Feature #3");
                ((ImageView)view.findViewById(R.id.img)).setImageResource(R.drawable.screenshot3);
                ((TextView)view.findViewById(R.id.description)).setText("Tout est configurable dans LasTime !");
                break;
        }
        return view;
    }

    public boolean isViewFromObject(View arg0, Object arg1) {
        return arg0 == ((View) arg1);
    }

    @Override
    public void destroyItem(View container, int position, Object object) {
        ((ViewPager) container).removeView((View) object);
    }

}
