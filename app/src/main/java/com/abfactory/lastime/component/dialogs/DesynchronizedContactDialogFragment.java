package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.model.Contact;

public class DesynchronizedContactDialogFragment extends DialogFragment {

    private static final String APP_CONTACT = "app_contact";
    private static final String PHONE_CONTACT = "phone_contact";

    private Contact appContact;
    private Contact phoneContact;

    public DesynchronizedContactDialogFragment(){
        super();
    }

    public static DesynchronizedContactDialogFragment newInstance(Contact appContact, Contact phoneContact) {
        DesynchronizedContactDialogFragment f = new DesynchronizedContactDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(APP_CONTACT, appContact);
        args.putSerializable(PHONE_CONTACT, phoneContact);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        appContact = (Contact) getArguments().get(APP_CONTACT);
        phoneContact = (Contact) getArguments().get(PHONE_CONTACT);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.desync_contact_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(R.string.contact_desync_dialog_title);
        if(appContact.getContactName().equals(phoneContact.getContactName())){
            ((TextView)content.findViewById(R.id.name_comparison)).setText(R.string.contact_desync_dialog_identical_name_information);

        } else {
            ((TextView)content.findViewById(R.id.name_comparison)).setText(appContact.getContactName() + " / " +
                    (phoneContact.getContactName() != null ? phoneContact.getContactName() : "Not defined"));
        }

        if(appContact.getPhoneNumber().equals(phoneContact.getPhoneNumber())){
            ((TextView)content.findViewById(R.id.telephone_comparison)).setText(R.string.contact_desync_dialog_identical_phone_information);

        } else {
            ((TextView)content.findViewById(R.id.telephone_comparison)).setText(appContact.getPhoneNumber() + " / " +
                    (phoneContact.getPhoneNumber() != null ? phoneContact.getPhoneNumber() : "Not defined"));
        }

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setPositiveButton(R.string.contact_desync_dialog_trust_phone_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((DisplayEventActivity) getActivity()).updateContactInfoAgainstPhoneData();
                        dismiss();
                    }
                })
                .setNegativeButton(R.string.contact_desync_dialog_trust_app_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        ((DisplayEventActivity) getActivity()).updateContactPhone();
                        dismiss();
                    }
                });

        this.setCancelable(true);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(true);

        return dialog;

    }

}
