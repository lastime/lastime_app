package com.abfactory.lastime.component.gallery;

/**
 * Rest response of Piwigo gallery
 */
public class PiwigoResponse {
    private String stat;
    private PiwigoResult result;

    public String getStat() {
        return stat;
    }

    public void setStat(String stat) {
        this.stat = stat;
    }

    public PiwigoResult getResult() {
        return result;
    }

    public void setResult(PiwigoResult result) {
        this.result = result;
    }
}
