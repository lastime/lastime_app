package com.abfactory.lastime.component.gallery;

public class GalleryRestItem {

    private String category;
    private String url;

    public String getCategory() {
        return category;
    }

    public String getUrl() {
        return url;
    }
}
