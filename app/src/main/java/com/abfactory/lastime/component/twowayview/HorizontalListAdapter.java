package com.abfactory.lastime.component.twowayview;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.RippleDrawable;
import android.graphics.drawable.ShapeDrawable;
import android.graphics.drawable.shapes.RectShape;
import android.media.ThumbnailUtils;
import android.os.Build;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class HorizontalListAdapter extends RecyclerView.Adapter<HorizontalListAdapter.SimpleViewHolder> {

    private final static int SQUARE = 300;
    private final Context mContext;
    private final List<Event> mItems;
    private int color = 000000;
    private List<Integer> colors;
    private int currentSelection = -1;
    private Display displaySelected;

    public HorizontalListAdapter(Context context, Display displaySelected) {
        mContext = context;
        mItems = new ArrayList<>();
        this.displaySelected = displaySelected;
        this.colors = new ArrayList<>();
        colors.add(color);

    }

    public void addItem(int position, Event event) {
        mItems.add(position, event);
        notifyItemInserted(position);
        colors = LasTimeUtils.generateGradient(color, mItems.size());
    }

    public void removeItem(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
        colors = LasTimeUtils.generateGradient(color, mItems.size());
    }

    @Override
    public SimpleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = null;
        switch (displaySelected) {
            case DISPLAY_CREATION_TIME:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_time, parent, false);
                break;
            case DISPLAY_TITLE:
                view = LayoutInflater.from(mContext).inflate(R.layout.item, parent, false);
                break;
            case DISPLAY_EVENT_DATE:
                view = LayoutInflater.from(mContext).inflate(R.layout.item_time, parent, false);
                break;
        }
        return new SimpleViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SimpleViewHolder holder, int position) {
        // Display title, date of event or creation time
        if (mItems.get(position) != null) {
            switch (displaySelected) {
                case DISPLAY_CREATION_TIME:
                    holder.description.setText(mItems.get(position).getCreationTimeAsString());
                    break;
                case DISPLAY_TITLE:
                    holder.description.setText(mItems.get(position).getTitle());
                    break;
                case DISPLAY_EVENT_DATE:
                    if (LasTimeUtils.areInSameYear(mItems.get(position).getDate(), new Date())
                            && mItems.get(position).getTimeOfEvent() != null
                            && mItems.get(position).getTimeOfEvent() > -1) {
                        holder.description.setText(LasTimeUtils.displayTime(mItems.get(position).getTimeOfEvent()));
                        holder.description.setVisibility(View.VISIBLE);
                    } else {
                        holder.description.setVisibility(View.GONE);
                    }
                    holder.itemView.setAlpha(0.85f);
                    break;
            }

            if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                final RectShape rectShape = new RectShape();
                rectShape.resize(SQUARE, SQUARE);
                Drawable shapeDrawable2 = new ShapeDrawable(rectShape);
                Drawable shapeDrawable1;
                boolean isBitmap = false;

                // Only on creation mode and if image set
                // TO be removed????
                if (displaySelected.equals(Display.DISPLAY_TITLE) && mItems.get(position).getThumbnailPath() != null) {
                    Bitmap bitmap = BitmapFactory.decodeFile(mItems.get(position).getThumbnailPath());
                    bitmap = ThumbnailUtils.extractThumbnail(bitmap, SQUARE, SQUARE);
                    shapeDrawable1 = new BitmapDrawable(holder.picture.getResources(), bitmap);
                    holder.alpha.setText("");
                } else {
                    if(LasTimeUtils.isNotEmpty(mItems.get(position).getThumbnailPath())) {
                        Bitmap bitmap = BitmapFactory.decodeFile(mItems.get(position).getThumbnailPath());
                        bitmap = ThumbnailUtils.extractThumbnail(bitmap, SQUARE, SQUARE);
                        shapeDrawable1 = new BitmapDrawable(holder.picture.getResources(), bitmap);
                        isBitmap = true;
                    }else{
                        shapeDrawable1 = new ShapeDrawable(rectShape);
                    }
                    if (displaySelected.equals(Display.DISPLAY_TITLE)) {
                        if(!isBitmap) {
                            ((ShapeDrawable) shapeDrawable1).getPaint().setColor(mItems.get(position).getColor());
                        }
                        holder.alpha.setText("" + Character.toUpperCase(mItems.get(position).getTitle().charAt(0)));
                    } else if (displaySelected.equals(Display.DISPLAY_CREATION_TIME)) {
                        holder.alpha.setText(mItems.get(position).getCreationAsString().replaceAll(" ", "\n"));
                        if(!isBitmap){
                            ((ShapeDrawable) shapeDrawable1).getPaint().setColor(colors.get(position));
                        }
                    } else if (displaySelected.equals(Display.DISPLAY_EVENT_DATE)) {
                        if (LasTimeUtils.areInSameYear(mItems.get(position).getDate(), new Date()))
                            holder.alpha.setText(mItems.get(position).getDateAsStringNoYear().replaceAll(" ", "\n"));
                        else
                            holder.alpha.setText(mItems.get(position).getDateAsString().replaceAll(" ", "\n"));
                        if(!isBitmap){
                            ((ShapeDrawable) shapeDrawable1).getPaint().setColor(colors.get(position));
                        }
                    }

                }

                final RippleDrawable ripple = new RippleDrawable(
                        ColorStateList.valueOf(holder.picture.getResources().getColor(R.color.accent_material_dark)),
                        shapeDrawable1,
                        shapeDrawable2);
                holder.picture.setBackground(ripple);

            } else {
                // Rollback if not compatible with RippleDrawable (Lollipop)
                if (displaySelected.equals(Display.DISPLAY_TITLE) && mItems.get(position).getThumbnailPath() != null) {
                    Bitmap bitmap = BitmapFactory.decodeFile(mItems.get(position).getThumbnailPath());
                    bitmap = ThumbnailUtils.extractThumbnail(bitmap, SQUARE, SQUARE);
                    holder.picture.setImageBitmap(bitmap);
                    holder.alpha.setText("");
                } else {
                    holder.picture.setBackgroundColor(mItems.get(position).getColor());
                    holder.picture.setImageBitmap(null);
                    if (displaySelected.equals(Display.DISPLAY_TITLE)) {
                        holder.alpha.setText("" + Character.toUpperCase(mItems.get(position).getTitle().charAt(0)));
                    } else if (displaySelected.equals(Display.DISPLAY_CREATION_TIME)) {
                        holder.alpha.setText(mItems.get(position).getCreationAsString().replaceAll(" ", "\n"));
                    } else if (displaySelected.equals(Display.DISPLAY_EVENT_DATE)) {
                        holder.picture.setBackgroundColor(colors.get(position));
                        if (LasTimeUtils.areInSameYear(mItems.get(position).getDate(), new Date()))
                            holder.alpha.setText(mItems.get(position).getDateAsStringNoYear().replaceAll(" ", "\n"));
                        else
                            holder.alpha.setText(mItems.get(position).getDateAsString().replaceAll(" ", "\n"));
                    }
                }
            }

            if (position == currentSelection) {
                holder.relativeLayout.setPadding(10, 10, 10, 10);
                holder.relativeLayout.setBackground(mContext.getResources().getDrawable(R.drawable.border_selection));
            } else {
                holder.relativeLayout.setPadding(0, 0, 0, 0);
                holder.relativeLayout.setBackground(null);
            }

        } else {
            holder.description.setVisibility(View.GONE);
            holder.picture.setBackgroundColor(color);
            holder.alpha.setText("");
            holder.itemView.setAlpha(0.5f);
        }
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    protected boolean isItemNull(int position) {
        return mItems.get(position) == null;
    }

    protected void setMainColor(int color){
        this.color = color;
    }

    protected int getCurrentSelection() {
        return currentSelection;
    }

    protected void setCurrentSelection(int position){
        this.currentSelection = position;
    }

    public enum Display {DISPLAY_TITLE, DISPLAY_CREATION_TIME, DISPLAY_EVENT_DATE}

    public class SimpleViewHolder extends RecyclerView.ViewHolder {
        public final TextView description;
        public final ImageView picture;
        public final TextView alpha;
        public final RelativeLayout relativeLayout;

        public SimpleViewHolder(View view) {
            super(view);
            picture = (ImageView) view.findViewById(R.id.thumbnailPicture);
            description = (TextView) view.findViewById(R.id.thumbnailText);
            alpha = (TextView) view.findViewById(R.id.alphaText);
            relativeLayout = (RelativeLayout) view.findViewById(R.id.item_layout);
        }
    }
}
