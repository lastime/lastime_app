package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;

public class PhotoActionDialogFragment  extends DialogFragment {

    private static final String ARG_TYPE = "type";

    public PhotoActionDialogFragment(){
        super();
    }

    public static PhotoActionDialogFragment newInstance(PhotoActionDialogFragmentType type) {
        PhotoActionDialogFragment f = new PhotoActionDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        PhotoActionDialogFragmentType type = (PhotoActionDialogFragmentType) getArguments().get(ARG_TYPE);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.photo_action_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(getResources().getString(R.string.choose_picture));

        if(PhotoActionDialogFragmentType.FROM_SCRATCH.equals(type)){
            content.findViewById(R.id.delete_photo_btn).setVisibility(View.GONE);
        };

        content.findViewById(R.id.take_photo_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditEventActivity) getActivity()).doPerformSearchOnPhone();
                dismiss();
            }
        });

        content.findViewById(R.id.select_builtin_gallery_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditEventActivity) getActivity()).doPerformClickBuiltInPhotos();
                dismiss();
            }
        });

        content.findViewById(R.id.delete_photo_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditEventActivity) getActivity()).doPerformClickDeletePhoto();
                dismiss();
            }
        });

        content.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(content);

        return builder.show();
    }

    public enum PhotoActionDialogFragmentType {
        FROM_SCRATCH, WITH_EXISTING;
    }
}
