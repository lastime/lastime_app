package com.abfactory.lastime.component.gallery;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class GalleryActivity extends BaseActivity {
    private static final String TAG = GalleryActivity.class.getSimpleName();

    private ProgressBar mProgressBar;

    private GridView mGridView;
    private GridViewAdapter mGridAdapter;
    private ArrayList<GridItem> mGridData;
    private ArrayList<GridItem> selectedItems;

    private Menu menu;

    private IEventDAO eventDAO;

    protected static boolean isRemoteImage(GridItem item) {
        if (item == null || item.getImage() == null)
            return false;
        return item.getImage().startsWith("http://") ||
                item.getImage().startsWith("https://");
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gallery);

        Toolbar toolbar = (Toolbar) findViewById(R.id.gallery_toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar != null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
        }

        eventDAO = new EventDAO(this);

        mProgressBar = (ProgressBar) findViewById(R.id.progressBar);
        mProgressBar.setVisibility(View.VISIBLE);

        selectedItems = new ArrayList<>();

        mGridView = (GridView) findViewById(R.id.gridView);
        mGridView.setChoiceMode(GridView.CHOICE_MODE_MULTIPLE);
        //Initialize with existing images
        final File imageDirectory = getDir(LasTimeUtils.IMAGE_FOLDER_NAME, Context.MODE_PRIVATE);
        //TODO : Logic to get all images, for now only work with thumbnails
        mGridData = new ArrayList<>();
        File[] images = imageDirectory.listFiles();
        Uri uri;
        for (int i = 0; i < images.length; i++) {
            // Do not use image > 1 Mo
            if (images[i].length() / 1024 / 1024 < 1) {
                GridItem gridItem = new GridItem();
                gridItem.setTitle(images[i].getName());
                uri = Uri.fromFile(images[i]);
                gridItem.setImage(uri.toString());
                mGridData.add(gridItem);
            }
        }

        mGridAdapter = new GridViewAdapter(this, R.layout.grid_item_layout, mGridData);
        mGridView.setAdapter(mGridAdapter);

        //Grid view click event
        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
                //Get item at position
                GridItem item = (GridItem) parent.getItemAtPosition(position);

                ImageView imageView = (ImageView) v.findViewById(R.id.grid_item_image);

                int[] screenLocation = new int[2];
                imageView.getLocationOnScreen(screenLocation);

                //Create intent and finish activity
                Intent intent = new Intent();
                intent.putExtra("imagePath", item.getImage());
                setResult(Activity.RESULT_OK, intent);
                finish();
            }
        });

        //Grid view Long click event
        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            public boolean onItemLongClick(AdapterView<?> parent, View v, int position, long id) {
                //Get item at position
                GridItem item = (GridItem) parent.getItemAtPosition(position);

                ImageView imageView = (ImageView) v.findViewById(R.id.grid_item_image);

                int[] screenLocation = new int[2];
                imageView.getLocationOnScreen(screenLocation);

                // Selection feature only for local images
                if(!isRemoteImage(item)) {
                    if (selectedItems.contains(item)) {
                        selectedItems.remove(item);
                        mGridView.setItemChecked(position, false);
                    } else {
                        selectedItems.add(item);
                        mGridView.setItemChecked(position, true);
                    }
                    updateMenu();
                }

                return true;
            }
        });

        // DECONNECTING IT DUE TO JSOUP ERROR WITH  JACK COMPILER :(
        // SEE: https://code.google.com/p/android/issues/detail?id=224898&thanks=224898&ts=1476133081
        //Start download
        // new AsyncHttpTask(this).execute();
        mProgressBar.setVisibility(View.GONE); // TO BE REMOVED AS WELL!!
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_gallery, menu);
        this.menu = menu;
        //Logic to delete selected images on trash click
        menu.findItem(R.id.delete_photo_menu).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                File file;
                for (GridItem selectedItem : selectedItems) {
                    if(!isRemoteImage(selectedItem)) {
                        String imagePath = selectedItem.getImage().replace("file://","");
                        List<Event> events = eventDAO.retrieveAllEvents();
                        boolean exists = false;
                        for (Event event : events) {
                            if(imagePath.equals(event.getThumbnailPath())) {
                                exists = true;
                            }
                        }
                        if(!exists) {
                            Log.d(TAG, "Picture to be deleted: " + selectedItem.getTitle());
                            file = new File(imagePath);
                            if(file.delete())
                                mGridAdapter.remove(selectedItem);
                        }else {
                            Snackbar.make(mGridView, getString(R.string.not_able_to_delete_image), Snackbar.LENGTH_SHORT).show();
                        }
                    }
                }

                // Reset selections
                selectedItems.clear();
                mGridView.clearChoices();
                return true;
            }
        });
        // Clean up unused images
        menu.findItem(R.id.cleanup_gallery_menu).setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                int deletedImages = LasTimeUtils.cleanStorageFolder(eventDAO, getDir(LasTimeUtils.IMAGE_FOLDER_NAME, Context.MODE_PRIVATE));
                Snackbar.make(mGridView, GalleryActivity.this.getString(R.string.storage_folder_done, deletedImages), Snackbar.LENGTH_SHORT).show();
                return true;
            }
        });
        updateMenu();
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == android.R.id.home)
            onBackPressed();
        return true;
    }

    private void updateMenu(){
        if (menu != null) {
            //Trash display
            if(!selectedItems.isEmpty()) {
                menu.findItem(R.id.delete_photo_menu).setVisible(true);
            }
            else{
                menu.findItem(R.id.delete_photo_menu).setVisible(false);
            }
        }
    }

    // *************************** Getter/setter ********************************

    protected ProgressBar getmProgressBar() {
        return mProgressBar;
    }

    protected GridViewAdapter getmGridAdapter() {
        return mGridAdapter;
    }

    protected ArrayList<GridItem> getmGridData() {
        return mGridData;
    }

    public GridView getmGridView() {
        return mGridView;
    }
}