package com.abfactory.lastime.component.tutorial;

import android.app.Activity;
import android.support.v7.widget.Toolbar;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.abfactory.lastime.R;
import com.github.amlcurran.showcaseview.ShowcaseView;
import com.github.amlcurran.showcaseview.targets.ViewTarget;

public class ShowcaseViewFactory {

    public static ShowcaseView get(ShowcaseViewType type, Activity activity){

        ShowcaseView sv = null;

        int margin = ((Number) (activity.getResources().getDisplayMetrics().density * 12)).intValue();

        RelativeLayout.LayoutParams lps_bottom_right = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps_bottom_right.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        lps_bottom_right.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        lps_bottom_right.setMargins(margin, margin, margin, margin);

        RelativeLayout.LayoutParams lps_center = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lps_center.addRule(RelativeLayout.ALIGN_PARENT_TOP);
        lps_center.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
        lps_center.setMargins(margin, margin+150, margin, margin);

        switch (type){
            case SHOWCASE_CREATE_EVENT_TOUR_GO_TO_EDIT:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.floatingAddButton, activity))
                        .setContentTitle(R.string.tour_guide_start_title)
                        .setContentText(R.string.tour_guide_start_details)
                        .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(activity.findViewById(R.id.floatingAddButton).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .build();
                sv.hideButton();
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_EVENT_NAME:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.event_title, activity))
                        .setContentTitle(R.string.tour_guide_event_name_title)
                        .setContentText(R.string.tour_guide_event_name_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.event_title).getWidth(),
                                activity.findViewById(R.id.event_title).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .build();
                sv.hideButton();
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_EVENT_RECURRENCE:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.recurrence_panel, activity))
                        .setContentTitle(R.string.tour_guide_recurrence_title)
                        .setContentText(R.string.tour_guide_recurrence_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.recurrence_panel).getWidth(),
                                activity.findViewById(R.id.recurrence_panel).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .build();
                sv.hideButton();
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_EVENT_MORE_PARAMS:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.more_options, activity))
                        .setContentTitle(R.string.tour_guide_more_params_title)
                        .setContentText(R.string.tour_guide_more_params_details)
                        .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(activity.findViewById(R.id.more_options).getWidth()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .build();
                sv.hideButton();
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_EVENT_COLOR:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.color, activity))
                        .setContentTitle(R.string.tour_guide_color_title)
                        .setContentText(R.string.tour_guide_color_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.color).getWidth(),
                                activity.findViewById(R.id.color).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .build();
                sv.hideButton();
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_EVENT_CREATION_OK:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.floatingConfirmButton, activity))
                        .setContentTitle(R.string.tour_guide_event_creation_title)
                        .setContentText(R.string.tour_guide_event_creation_details)
                        .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(activity.findViewById(R.id.floatingConfirmButton).getWidth()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .build();
                sv.hideButton();
                return sv;
            case SHOWCASE_MAIN_ACTIVITY_TOUR_TODO_ACCESS:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ToolbarActionItemTarget((Toolbar) activity.findViewById(R.id.lastime_toolbar), R.id.notif))
                        .setContentTitle(R.string.tour_guide_todo_menu_title)
                        .setContentText(R.string.tour_guide_todo_menu_details)
                        .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(150))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_MAIN_ACTIVITY_TOUR_SEARCH_ACCESS:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ToolbarActionItemTarget((Toolbar) activity.findViewById(R.id.lastime_toolbar), R.id.search))
                        .setContentTitle(R.string.tour_guide_search_menu_title)
                        .setContentText(R.string.tour_guide_search_menu_details)
                        .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(150))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_MAIN_ACTIVITY_TOUR_MENU:
                try {
                    sv =  new ShowcaseView.Builder(activity)
                            .setTarget(ViewTargets.navigationButtonViewTarget((Toolbar) activity.findViewById(R.id.lastime_toolbar)))
                            .setContentTitle(R.string.tour_guide_main_menu_title)
                            .setContentText(R.string.tour_guide_main_menu_details)
                            .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(150))
                            .setStyle(R.style.CustomShowcaseTheme)
                            .replaceEndButton(R.layout.view_custom_button)
                            .blockAllTouches()
                            .build();
                    sv.setButtonPosition(lps_bottom_right);
                    sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                } catch (ViewTargets.MissingViewException e) {
                    e.printStackTrace();
                }
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_MENU_MAIN_ACTIVITY:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.my_events, activity))
                        .setContentTitle(R.string.tour_guide_all_events_title)
                        .setContentText(R.string.tour_guide_all_events_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.my_events).getWidth(),
                                activity.findViewById(R.id.my_events).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_MENU_TODO_ACTIVITY:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.todo, activity))
                        .setContentTitle(R.string.tour_guide_todo_from_main_menu_title)
                        .setContentText(R.string.tour_guide_todo_from_main_menu_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.todo).getWidth(),
                                activity.findViewById(R.id.todo).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_MENU_WISH_LIST_ACTIVITY:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.wish_list, activity))
                        .setContentTitle(R.string.tour_guide_wish_list_title)
                        .setContentText(R.string.tour_guide_wish_list_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.wish_list).getWidth(),
                                activity.findViewById(R.id.wish_list).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_MENU_STATISTICS_ACTIVITY:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.statistics, activity))
                        .setContentTitle(R.string.tour_guide_statistics_title)
                        .setContentText(R.string.tour_guide_statistics_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.statistics).getWidth(),
                                activity.findViewById(R.id.statistics).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_MENU_SETTINGS:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.settings, activity))
                        .setContentTitle(R.string.tour_guide_settings_title)
                        .setContentText(R.string.tour_guide_settings_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.settings).getWidth(),
                                activity.findViewById(R.id.settings).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_center);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_CREATE_EVENT_TOUR_MENU_TEAM:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.team, activity))
                        .setContentTitle(R.string.tour_guide_feedback_title)
                        .setContentText(R.string.tour_guide_feedback_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.team).getWidth(),
                                activity.findViewById(R.id.team).getHeight()))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_center);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_MAIN_ACTIVITY_TOUR_EVENT_CREATED:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.floatingAddButton, activity))
                        .setContentTitle(R.string.tour_guide_intermediary_step_title)
                        .setContentText(R.string.tour_guide_intermediary_step_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(0,0))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_LOCATION_SEARCH_BAR:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.autocomplete_places, activity))
                        .setContentTitle(R.string.location_tutorial_step_searchbar_title)
                        .setContentText(R.string.location_tutorial_step_searchbar_details)
                        .setShowcaseDrawer(new MaterialRectangleShowcaseDrawer(
                                activity.findViewById(R.id.autocomplete_places).getWidth(),
                                activity.findViewById(R.id.autocomplete_places).getHeight()
                        ))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_LOCATION_MAP_FEATURES:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ViewTarget(R.id.setPinToCurrentLocationButton, activity))
                        .setContentTitle(R.string.location_tutorial_step_mapsfeature_title)
                        .setContentText(R.string.location_tutorial_step_mapsfeature_details)
                        .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(
                                activity.findViewById(R.id.setPinToCurrentLocationButton).getWidth()+10
                        ))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            case SHOWCASE_LOCATION_SET_LOCATION:
                sv =  new ShowcaseView.Builder(activity)
                        .setTarget(new ToolbarActionItemTarget((Toolbar) activity.findViewById(R.id.lastime_toolbar), R.id.save))
                        .setContentTitle(R.string.location_tutorial_step_setlocation_title)
                        .setContentText(R.string.location_tutorial_step_setlocation_details)
                        .setShowcaseDrawer(new MaterialCircleShowcaseDrawer(150))
                        .setStyle(R.style.CustomShowcaseTheme)
                        .replaceEndButton(R.layout.view_custom_button)
                        .blockAllTouches()
                        .build();
                sv.setButtonPosition(lps_bottom_right);
                sv.setButtonText(activity.getString(R.string.next_step_in_tour));
                return sv;
            default:
                return null;
        }
    }

    public enum ShowcaseViewType {
        SHOWCASE_CREATE_EVENT_TOUR_GO_TO_EDIT,
        SHOWCASE_CREATE_EVENT_TOUR_EVENT_NAME,
        SHOWCASE_CREATE_EVENT_TOUR_EVENT_RECURRENCE,
        SHOWCASE_CREATE_EVENT_TOUR_EVENT_MORE_PARAMS,
        SHOWCASE_CREATE_EVENT_TOUR_EVENT_COLOR,
        SHOWCASE_CREATE_EVENT_TOUR_EVENT_CREATION_OK,
        SHOWCASE_MAIN_ACTIVITY_TOUR_EVENT_CREATED,
        SHOWCASE_MAIN_ACTIVITY_TOUR_TODO_ACCESS,
        SHOWCASE_MAIN_ACTIVITY_TOUR_SEARCH_ACCESS,
        SHOWCASE_MAIN_ACTIVITY_TOUR_MENU,
        SHOWCASE_CREATE_EVENT_TOUR_MENU_MAIN_ACTIVITY,
        SHOWCASE_CREATE_EVENT_TOUR_MENU_TODO_ACTIVITY,
        SHOWCASE_CREATE_EVENT_TOUR_MENU_WISH_LIST_ACTIVITY,
        SHOWCASE_CREATE_EVENT_TOUR_MENU_STATISTICS_ACTIVITY,
        SHOWCASE_CREATE_EVENT_TOUR_MENU_SETTINGS,
        SHOWCASE_CREATE_EVENT_TOUR_MENU_TEAM,
        SHOWCASE_LOCATION_SEARCH_BAR,
        SHOWCASE_LOCATION_MAP_FEATURES,
        SHOWCASE_LOCATION_SET_LOCATION
    }
}
