package com.abfactory.lastime.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Filter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LasTimeActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.search.EventSearch;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.google.common.base.Strings;

import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.Set;

/**
 * Section adapter implementation dedicated to event list
 */
public class EventListSectionAdapter extends SectionAdapter {

    private EventsFilter eventsFilter;
    private LasTimeActivity lasTimeActivity;

    public EventListSectionAdapter(LasTimeActivity lasTimeActivity) {
        this.lasTimeActivity = lasTimeActivity;
    }

    @Override
    public int numberOfSections() {
        return lasTimeActivity.getOrderingHandler().getNumberOfSections();
    }

    @Override
    public int numberOfRows(int section) {
        return lasTimeActivity.getOrderingHandler().getNumberOfRowsForCategory(section);
    }

    @Override
    public Object getRowItem(int section, int row) {
        return lasTimeActivity.getOrderingHandler().getEvent(section, row);
    }

    @Override
    public boolean hasSectionHeaderView(int section) {
        return lasTimeActivity.getOrderingHandler().getNumberOfRowsForCategory(section) != 0;
    }

    @Override
    public View getRowView(final int section, final int row, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) lasTimeActivity.getSystemService
                    (Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(lasTimeActivity.getResources().getLayout(R.layout.events_list_item), null);
        }

        Event event = lasTimeActivity.getOrderingHandler().getEvent(section, row);

        // Event title
        ((TextView) convertView.findViewById(R.id.titleText)).setText(event.getTitle());

        // Event highlight
        if(event.getHighlight() != null && !Strings.isNullOrEmpty(LasTimeUtils.getHighlightValue(event))
                && !event.getHighlight().equals(LasTimeUtils.HighlightableFields.NothingToHighLight.getValue())) {
            ((TextView) convertView.findViewById(R.id.highlightText)).setText(LasTimeUtils.getHighlightValue(event));
            convertView.findViewById(R.id.highlightText).setVisibility(View.VISIBLE);
        }else{
            convertView.findViewById(R.id.highlightText).setVisibility(View.GONE);
        }

        // Display of date of event
        boolean areInSameYear = LasTimeUtils.areInSameYear(event.getDate(), new Date());
        boolean timeIsSet = event.getTimeOfEvent() != null && event.getTimeOfEvent() > 0;

        if (areInSameYear && timeIsSet)
            ((TextView) convertView.findViewById(R.id.occurrenceText)).setText(event.getDateAsStringNoYearAndTime());
        else if (areInSameYear)
            ((TextView) convertView.findViewById(R.id.occurrenceText)).setText(event.getDateAsStringNoYear());
        else if (timeIsSet) {
            ((TextView) convertView.findViewById(R.id.occurrenceText)).setText(event.getDateAsStringAndTime());
        } else {
            ((TextView) convertView.findViewById(R.id.occurrenceText)).setText(event.getDateAsString());
        }

        // Display event duration
        TextView event_last_days_view = (TextView) convertView.findViewById(R.id.event_last_days_view);
        if (event.getEndDate() != null && event.getDate().before(event.getEndDate())) {
            event_last_days_view.setText((LasTimeUtils.computeDaysDifference(event.getDate(), event.getEndDate())+1) + " " + lasTimeActivity.getString(R.string.days));
            event_last_days_view.setVisibility(View.VISIBLE);
        }else{
            event_last_days_view.setVisibility(View.GONE);
        }

        if (lasTimeActivity.getOrderingHandler().getEvent(section, row).getThumbnailPath() != null) {
            File imgFile = new File(event.getThumbnailPath());
            if (imgFile.exists()) {
                Bitmap myBitmap = BitmapFactory.decodeFile(imgFile.getAbsolutePath());
                myBitmap = ThumbnailUtils.extractThumbnail(myBitmap, 128, 128);
                ((CircularImageView) convertView.findViewById(R.id.circular_image_view)).setImageBitmap(myBitmap);
            } else {
                ((CircularImageView) convertView.findViewById(R.id.circular_image_view)).setImageBitmap(LasTimeUtils.createCircleImage(event));
                ((CircularImageView) convertView.findViewById(R.id.circular_image_view)).setImageAlpha(50);
            }
        } else {
            ((CircularImageView) convertView.findViewById(R.id.circular_image_view)).setImageBitmap(LasTimeUtils.createCircleImage(event));
            ((CircularImageView) convertView.findViewById(R.id.circular_image_view)).setImageAlpha(50);
        }

        // Display number of days
        // Only if date > 1 week
        int numberOfDays;
        Date today = new Date();
        // Logic if event lasts several days, take the last day
        if(event.getEndDate() != null){
            if(today.after(event.getEndDate())) {
                numberOfDays = LasTimeUtils.getDaysDifference(event.getEndDate(), today);
            }else{
                numberOfDays = 0;
            }
        }else {
            numberOfDays = LasTimeUtils.getDaysDifference(event.getDate(), today);
        }

        if (numberOfDays > 6) {
            ((TextView) convertView.findViewById(R.id.days_number_count))
                    .setText(numberOfDays + " " + lasTimeActivity.getString(R.string.days));
            convertView.findViewById(R.id.days_number_count).setVisibility(View.VISIBLE);
        } else {
            convertView.findViewById(R.id.days_number_count).setVisibility(View.GONE);
        }

        final EventObjectHolder holder = getEventObjectHolder(convertView);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) holder.mainView.getLayoutParams();
        params.rightMargin = 0;
        params.leftMargin = 0;
        holder.mainView.setLayoutParams(params);
        convertView.setOnTouchListener(new SwipeDetector(holder, section, row, lasTimeActivity));

        return convertView;
    }

    private EventObjectHolder getEventObjectHolder(View workingView) {
        Object tag = workingView.getTag();
        EventObjectHolder holder;

        if (tag == null || !(tag instanceof EventObjectHolder)) {
            holder = new EventObjectHolder();
            holder.mainView = (RelativeLayout) workingView.findViewById(R.id.event_object_mainview);
            holder.rightView = (RelativeLayout) workingView.findViewById(R.id.event_object_create_notif);
            holder.leftView = (RelativeLayout) workingView.findViewById(R.id.event_object_repeatview);


            /* initialize other views here */

            workingView.setTag(holder);
        } else {
            holder = (EventObjectHolder) tag;
        }

        return holder;
    }

    @Override
    public int getSectionHeaderViewTypeCount() {
        return 2;
    }

    @Override
    public int getSectionHeaderItemViewType(int section) {
        return section % 2;
    }

    @Override
    public View getSectionHeaderView(int section, View convertView, ViewGroup parent) {

        if (convertView == null) {
            convertView = lasTimeActivity.getLayoutInflater().inflate(lasTimeActivity.getResources().getLayout(R.layout.events_list_section), null);
        }

        Context context = convertView.getContext();
        ((TextView) convertView.findViewById(R.id.section_title)).setText(lasTimeActivity.getOrderingHandler().getLabelForSection(context, section));

        return convertView;
    }

    @Override
    public void onRowItemClick(AdapterView<?> parent, View view, int section, int row, long id) {
        super.onRowItemClick(parent, view, section, row, id);
    }

    @Override
    public Filter getFilter() {
        if (eventsFilter == null) {
            eventsFilter = new EventsFilter();
        }
        return eventsFilter;
    }

    /**
     * Custom filter for friend list
     * Filter content in friend list according to the search text
     */
    class EventsFilter extends Filter {

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults filterResults = new FilterResults();
            if (constraint != null && constraint.length() > 0) {

                // Search engine
                EventSearch eventSearch = EventSearch.getInstance();
                eventSearch.setEventList(lasTimeActivity.getEvents());

                Set<Event> tempList = eventSearch.searchInEvents(constraint.toString());

                filterResults.count = tempList.size();
                filterResults.values = new ArrayList<>(tempList);
            } else {
                filterResults.count = lasTimeActivity.getEvents().size();
                filterResults.values = lasTimeActivity.getEvents();
            }

            return filterResults;
        }

        /**
         * Notify about filtered list to ui
         *
         * @param constraint text
         * @param results    filtered result
         */
        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            lasTimeActivity.setFilteredEvents((ArrayList<Event>) results.values);
            notifyDataSetChanged();
        }
    }
}

