package com.abfactory.lastime.component.tabs;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LocationsHandlingActivity;
import com.abfactory.lastime.component.LocationsListAdapter;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventLocation;

import java.util.List;


public class ListLocHandlingTab extends Fragment {

    RecyclerView listRecyclerView;
    LinearLayout noLocationInAppInfo;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.list_loc_handling_tab, container, false);

        listRecyclerView = (RecyclerView) v.findViewById(R.id.list_recycler_view);
        noLocationInAppInfo = (LinearLayout) v.findViewById(R.id.no_location_in_app_display);

        updateDisplay();

        return v;
    }

    public void updateDisplay() {

        // Get all locations present inapp
        List<EventLocation> locationsInApp = new EventDAO(getContext()).getAllEventLocations();

        // Handle empty case, otherwise display list of locations
        if (locationsInApp == null || locationsInApp.isEmpty()) {

            // Handle corresponding visibilitity
            listRecyclerView.setVisibility(View.GONE);
            noLocationInAppInfo.setVisibility(View.VISIBLE);

        } else {

            // Handle corresponding visibilitity
            listRecyclerView.setVisibility(View.VISIBLE);
            noLocationInAppInfo.setVisibility(View.GONE);

            // use this setting to improve performance if you know that changes
            // in content do not change the layout size of the RecyclerView
            listRecyclerView.setHasFixedSize(true);

            // use a linear layout manager
            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getContext());
            listRecyclerView.setLayoutManager(linearLayoutManager);

            // specify an adapter (see also next example)
            LocationsHandlingActivity activity = (LocationsHandlingActivity) getActivity();
            LocationHandlingListAdapter locationHandlingListAdapter = new LocationHandlingListAdapter(locationsInApp, this, activity, activity.hasSelectionActivated());
            listRecyclerView.setAdapter(locationHandlingListAdapter);

        }

    }
}