package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.activity.LoginActivity;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.PreferencesHandler;

public class BasicDialogFragment extends DialogFragment {

    private static final String ARG_TYPE = "type";
    private static final String ADD_DATA = "add_data";

    private BasicDialogFragmentType type;
    private String additionalData;

    public BasicDialogFragment(){
        super();
    }

    public static BasicDialogFragment newInstance(BasicDialogFragmentType type, String additionalData) {
        BasicDialogFragment f = new BasicDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        args.putSerializable(ADD_DATA, additionalData);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        type = (BasicDialogFragmentType) getArguments().get(ARG_TYPE);
        additionalData = getArguments().getString(ADD_DATA);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.basic_dialog, null);
        /*if(BasicDialogFragmentType.INSPIRE_ME_COMPLETE.equals(type)){
            content.findViewById(R.id.title).setVisibility(View.GONE);
            content.findViewById(R.id.progressBar).setVisibility(View.VISIBLE);
        }*/
        ((TextView)content.findViewById(R.id.title)).setText(type.getTitleStrinRessource());
        ((TextView)content.findViewById(R.id.error_msg)).setText(type.getMessageStrinRessource());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                 .setView(content);

        if(!BasicDialogFragmentType.INSPIRE_ME_COMPLETE.equals(type)){
            builder.setPositiveButton(getResources().getString(R.string.positive_action_label),
                    new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int whichButton) {
                            if (BasicDialogFragmentType.INVALID_PASSWORD.equals(type)) {
                                ((LoginActivity) getActivity()).clearPwd();
                            }
                            dismiss();
                        }
                    }
            );
        }

        this.setCancelable(true);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;

    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        if(BasicDialogFragmentType.INVALID_BACKUP_EMAIL.equals(type)){
            PreferencesHandler ph = new PreferencesHandler(getActivity().getApplicationContext());
            if(!LasTimeUtils.isValidEmail(ph.getBackUpEmailForApp())){
                ProvideBackUpEmailDialogFragment.newInstance(additionalData).show(getFragmentManager(), "provideBackUpEmailDialogFragment");
            }
        }
    }

    public enum BasicDialogFragmentType {

        PWD_DONT_MATCH(R.string.passwords_dont_match, R.string.passwords_dont_match_desc),
        INCORRECT_PWD(R.string.current_password_incorrect, R.string.current_password_incorrect_desc),
        INCORRECT_PWD_NO_CHANGE(R.string.incorrect_pwd_title, R.string.invalid_password_nothing_changed),
        MISSING_MAND_INFO(R.string.missing_mandatory_information, R.string.fill_all_mandatory_fields),
        FEEDBACK_SENT(R.string.feedback_sent_title, R.string.feedback_sent_content),
        FEEDBACK_SENT_NO_EMAIL(R.string.feedback_sent_title, R.string.feedback_sent_no_email_content),
        INVALID_BACKUP_EMAIL(R.string.invalid_backup_title, R.string.invalid_backup_content),
        PWD_RESET(R.string.conf_reset_title, R.string.conf_reset_content),
        INVALID_PASSWORD(R.string.incorrect_pwd_title, R.string.incorrect_pwd_text),
        NAME_MANADATORY_FOR_LOCATION(R.string.loc_name_manadatory_title, R.string.loc_name_manadatory_content),
        NO_LOC_IDENTIFIED_FOR_BAD_CONNECTION(R.string.no_loc_bad_connection_title, R.string.no_loc_bad_connection_content),
        INSPIRE_ME_EXPLANATIONS(R.string.inspire_me_explanation_title,R.string.inspire_me_explanation_details),
        INSPIRE_ME_COMPLETE(R.string.inspire_me_complete_title,R.string.inspire_me_complete_content),
        IMPOSSIBLE_TO_DELETE_USED_LOCATION(R.string.impossible_to_delete_loc, R.string.impossible_to_delete_loc_desc),
        RATE_FROM_WHO_WE_ARE_SECTION(R.string.rate_us_later_title, R.string.rate_us_later_desc);

        int titleStringRessource;
        int messageStringRessource;

        BasicDialogFragmentType(int titleStringRessource, int messageStringRessource) {
            this.titleStringRessource = titleStringRessource;
            this.messageStringRessource = messageStringRessource;
        }

        public int getTitleStrinRessource(){
            return titleStringRessource;
        }

        public int getMessageStrinRessource(){
            return messageStringRessource;
        }
    }

}
