package com.abfactory.lastime.component.twowayview;

import android.content.Context;
import android.support.v7.widget.RecyclerView.Recycler;
import android.support.v7.widget.RecyclerView.State;
import android.util.AttributeSet;

public class HorizontalListManager extends BaseLayoutManager {
    private static final String LOGTAG = "HorizontalListManager";

    public HorizontalListManager(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public HorizontalListManager(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public HorizontalListManager(Context context, Orientation orientation) {
        super(orientation);
    }

    @Override
    int getLaneCount() {
        return 1;
    }

    @Override
    void getLaneForPosition(Lanes.LaneInfo outInfo, int position, Direction direction) {
        outInfo.set(0, 0);
    }

    @Override
    void moveLayoutToPosition(int position, int offset, Recycler recycler, State state) {
        getLanes().reset(offset);
    }
}
