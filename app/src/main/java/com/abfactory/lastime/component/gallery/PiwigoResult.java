package com.abfactory.lastime.component.gallery;

/**
 * Piwigo result model
 */
public class PiwigoResult {

    private Paging paging;
    private Images[] images;

    public Paging getPaging() {
        return paging;
    }

    public void setPaging(Paging paging) {
        this.paging = paging;
    }

    public Images[] getImages() {
        return images;
    }

    public void setImages(Images[] images) {
        this.images = images;
    }
}
