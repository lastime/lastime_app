package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.ArrayList;
import java.util.List;

public class ReminderDialogFragmentAdvanced extends DialogFragment {

    private static final String REC_TAG = "tag";

    private String reminderTag;

    public ReminderDialogFragmentAdvanced(){
        super();
    }

    public static ReminderDialogFragmentAdvanced newInstance(String recurrenceTag) {
        ReminderDialogFragmentAdvanced f = new ReminderDialogFragmentAdvanced();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(REC_TAG, recurrenceTag);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        reminderTag = getArguments().getString(REC_TAG);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.reminder_dialog, null);

        // Spinner set-up
        Spinner spinner = (Spinner) content.findViewById(R.id.spinner);
        List<String> list = new ArrayList<String>();
        list.add(getResources().getString(R.string.days_before));
        list.add(getResources().getString(R.string.weeks_before));
        list.add(getResources().getString(R.string.months_before));
        ArrayAdapter<String> dataAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, list);
        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(dataAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                ((TextView) parent.getChildAt(0)).setTextColor(Color.WHITE);
                switch (position) {
                    case 0:
                        ((TextView) content.findViewById(R.id.duration_text)).setText(" " + getResources().getString(R.string.duration_days_can_be_plural));
                        break;
                    case 1:
                        ((TextView) content.findViewById(R.id.duration_text)).setText(" " + getResources().getString(R.string.duration_weeks_can_be_plural));
                        break;
                    case 2:
                        ((TextView) content.findViewById(R.id.duration_text)).setText(" " + getResources().getString(R.string.duration_months_can_be_plural));
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setPositiveButton(getResources().getString(R.string.positive_action_label),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((EditEventActivity) getActivity()).doPerformReminderUpdate(
                                        LasTimeUtils.generateTagFromRecurrencePopupSelection(
                                                 ((EditText) content.findViewById(R.id.number_duration)).getText().toString(),
                                                ((Spinner) content.findViewById(R.id.spinner)).getSelectedItemPosition()
                                        )
                                );
                            }
                        }
                );

        return builder.create();
    }

}
