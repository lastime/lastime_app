package com.abfactory.lastime.component;

import android.content.Context;
import android.support.v4.widget.NestedScrollView;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * No touch event handled by children
 */
public class NestedScrollViewNoChild extends NestedScrollView {

    public NestedScrollViewNoChild(Context context) {
        super(context);
    }

    public NestedScrollViewNoChild(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public NestedScrollViewNoChild(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        return false;
    }
}
