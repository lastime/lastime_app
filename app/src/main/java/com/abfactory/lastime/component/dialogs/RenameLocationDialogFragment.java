package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.NewLocationActivity;

public class RenameLocationDialogFragment extends DialogFragment {

    private static final String ARG_CURRENT_NAME = "current_name";

    public RenameLocationDialogFragment(){
        super();
    }

    public static RenameLocationDialogFragment newInstance(String currentName) {
        RenameLocationDialogFragment f = new RenameLocationDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putString(ARG_CURRENT_NAME, currentName);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        String currentName = getArguments().getString(ARG_CURRENT_NAME);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.rename_loc_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(R.string.rename_location_title);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setCancelable(false)
                .setView(content)
                .setPositiveButton(R.string.validate, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((NewLocationActivity) getActivity()).doPositiveRenameLocationOperation(((EditText) content.findViewById(R.id.location_name)).getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        // Do nothing
                    }
                });

        return builder.create();

    }

}
