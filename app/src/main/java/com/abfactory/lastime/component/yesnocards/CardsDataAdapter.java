package com.abfactory.lastime.component.yesnocards;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.EventTemplate;
import com.abfactory.lastime.utils.DisplayImageFromServerTask;

public class CardsDataAdapter extends ArrayAdapter<EventTemplate> {

    public CardsDataAdapter(Context context) {
        super(context, R.layout.std_card_inner);
    }

    @Override
    public View getView(int position, final View contentView, ViewGroup parent){

        new DisplayImageFromServerTask(getContext().getResources(), (ImageView) contentView.findViewById(R.id.image)).execute(getItem(position).getImagePath());
        ((TextView) contentView.findViewById(R.id.title)).setText(getItem(position).getTitle());
        contentView.findViewById(R.id.title).setBackgroundColor(Color.parseColor(getItem(position).getColor()));
        ((TextView) contentView.findViewById(R.id.description)).setText(getItem(position).getDescription());
        contentView.findViewById(R.id.description).setBackgroundColor(Color.parseColor("#CC"+getItem(position).getColor().substring(1,7)));
        ((TextView) contentView.findViewById(R.id.number)).setText((position + 1) + "/" + getCount());
        contentView.findViewById(R.id.number).setBackgroundColor(Color.parseColor(getItem(position).getColor()));
        contentView.findViewById(R.id.image_container).setBackgroundColor(Color.parseColor("#33"+getItem(position).getColor().substring(1,7)));

        return contentView;
    }
}
