package com.abfactory.lastime.component.gallery;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.graphics.ColorMatrix;
import android.graphics.ColorMatrixColorFilter;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.squareup.picasso.Picasso;

public class GridViewAdapter extends ArrayAdapter<GridItem> {

    //private final ColorMatrixColorFilter grayscale Filter;
    private Context mContext;
    private int layoutResourceId;
    private ArrayList<GridItem> mGridData = new ArrayList<GridItem>();

    public GridViewAdapter(Context mContext, int layoutResourceId, ArrayList<GridItem> mGridData) {
        super(mContext, layoutResourceId, mGridData);
        this.layoutResourceId = layoutResourceId;
        this.mContext = mContext;
        this.mGridData = mGridData;
    }

    /**
     * Updates grid data and refresh grid items.
     *
     * @param mGridData
     */
    public void setGridData(ArrayList<GridItem> mGridData) {
        this.mGridData = mGridData;
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View row = convertView;
        ViewHolder holder;

        if (row == null) {
            LayoutInflater inflater = ((Activity) mContext).getLayoutInflater();
            row = inflater.inflate(layoutResourceId, parent, false);
            holder = new ViewHolder();
            holder.titleTextView = (TextView) row.findViewById(R.id.grid_item_title);
            holder.imageView = (ImageView) row.findViewById(R.id.grid_item_image);
            holder.imageType = (ImageView) row.findViewById(R.id.grid_item_type);
            row.setTag(holder);
        } else {
            holder = (ViewHolder) row.getTag();
        }

        GridItem item = mGridData.get(position);
        holder.titleTextView.setText(Html.fromHtml(item.getTitle()));

        Drawable drawable;
        if(GalleryActivity.isRemoteImage(item)){
            drawable = mContext.getResources().getDrawable(R.drawable.ic_get_app_black_36dp);
            drawable.setColorFilter(mContext.getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
            holder.imageType.setImageDrawable(drawable);
        } else {
            drawable = mContext.getResources().getDrawable(R.drawable.ic_check_circle_black_36dp);
            drawable.setColorFilter(mContext.getResources().getColor(R.color.ColorPrimaryDark), PorterDuff.Mode.SRC_IN);
            holder.imageType.setImageDrawable(drawable);
        }

        Picasso.with(mContext)
                .load(item.getImage())
                .resize(100,100)
                .centerCrop()
                .into(holder.imageView);
        return row;
    }

    static class ViewHolder {
        TextView titleTextView;
        ImageView imageView;
        ImageView imageType;
    }
}