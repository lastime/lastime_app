package com.abfactory.lastime.component;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.EventLocation;
import java.util.ArrayList;
import java.util.List;

/**
 * SAdapter implementation dedicated to location list
 */
public class LocationsListAdapter extends ArrayAdapter<EventLocation> {


    public LocationsListAdapter(Context context, List<EventLocation> locations) {
        super(context, 0, locations);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        EventLocation location = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.locations_list_item, parent, false);
        }

        ((TextView)convertView.findViewById(R.id.locationTitle)).setText(location.getName());

        // Return the completed view to render on screen
        return convertView;
    }

}

