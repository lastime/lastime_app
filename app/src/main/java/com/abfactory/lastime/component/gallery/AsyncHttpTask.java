package com.abfactory.lastime.component.gallery;

import android.os.AsyncTask;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.ImmutableList;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.List;

import static android.support.design.widget.Snackbar.LENGTH_LONG;

/**
 * Class handles call to retrieve list of image urls to be proposed
 */

public class AsyncHttpTask extends AsyncTask<String, Void, Integer> {

    private static final String TAG = AsyncHttpTask.class.getName();
    private GalleryActivity activity;

    private static final String IMAGES_URL = "http://lastime.moninstantbeaute.biz/gallery";

    public AsyncHttpTask(GalleryActivity activity) {
        this.activity = activity;
    }

    @Override
    protected Integer doInBackground(String... params) {
        try {
            // Targeting temp server
            Document doc = Jsoup.connect(IMAGES_URL).get();
            for (Element file : doc.select("li a")) {
                String imagePath = (file.attr("href"));
                if(!imagePath.equals("/")) {
                    Log.d(TAG, "image found in remote folder: " + imagePath);
                    GridItem item = new GridItem();
                    item.setImage(IMAGES_URL + "/" + imagePath);
                    item.setTitle(imagePath.substring(imagePath.lastIndexOf('/') + 1));
                    activity.getmGridData().add(item);
                }
            }
        } catch (Exception e) {
            Log.d(TAG, "Error retrieving photos list " + e.getLocalizedMessage(), e);
            return 0;
        }

        return 1;
    }

    @Override
    protected void onPostExecute(Integer result) {
        // Download complete. Lets update UI

        if (result == 1) {
            activity.getmGridAdapter().setGridData(activity.getmGridData());
        } else {
            activity.showErrorMessage("Failed to contact LasTime server");
        }

        //Hide progressbar
        activity.getmProgressBar().setVisibility(View.GONE);
    }
}
