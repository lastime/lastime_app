package com.abfactory.lastime.component;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.drawable.BitmapDrawable;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;

public class FrequencyIndicatorImageView extends ImageView {

    private int borderWidth = 0;
    private int viewWidth;
    private int viewHeight;
    private Bitmap image;
    private Paint paint;
    private Paint paintBorder;
    private BitmapShader shader;

    public FrequencyIndicatorImageView(Context context) {
        super(context);
        setup();
    }

    public FrequencyIndicatorImageView(Context context, AttributeSet attrs) {
        super(context, attrs);
        setup();
    }

    public FrequencyIndicatorImageView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setup ();
    }

    private void setup()
    {
        // init paint
        paint = new Paint();
        paint.setStyle(Paint.Style.FILL);
        paint.setAntiAlias(true);

    }

    public void setBorderWidth(int borderWidth)
    {
        this.borderWidth = borderWidth;
        this.invalidate();
    }

    public void setBorderColor(int borderColor)
    {
        if(paintBorder != null)
            paintBorder.setColor(borderColor);

        this.invalidate();
    }

    private void loadBitmap()
    {
        BitmapDrawable bitmapDrawable = (BitmapDrawable) this.getDrawable();

        if(bitmapDrawable != null)
            image = bitmapDrawable.getBitmap();
    }

    @SuppressLint("DrawAllocation")
    @Override
    public void onDraw(Canvas canvas)
    {


        paint.setColor(Color.BLACK);

        Path path = new Path();

        path.reset();

        int measuredWidth = getMeasuredWidth();
        int measuredHeight = getMeasuredHeight();
        int x = (measuredWidth/2)  ;
        int y = (measuredHeight/2) ;
        int outerRadius = Math.min(x,y) ;
        int innerRadius = outerRadius/2 ;

        int points = 5;

        if (points < 3) return;
        float fillPercent = 1f;

        float a = (float) (Math.PI * 2) / (points*2);
        int workingRadius = outerRadius;
        path.reset();

        canvas.save();
        canvas.translate(x, y);
        for(int j = 0; j < ((fillPercent < 1) ? 2 : 1) ; j++){
            path.moveTo(workingRadius,0);
            for (int i = 1; i < points*2; i++) {
                workingRadius = (workingRadius == outerRadius) ? innerRadius : outerRadius;
                float xPt = (float) (workingRadius * Math.cos(a*i));
                float yPt = (float) (workingRadius * Math.sin(a*i));
                path.lineTo(xPt, yPt);
            }
            path.close();
            outerRadius -= outerRadius * fillPercent;
            innerRadius = outerRadius/2 ;
            a = -a;
        }


        canvas.rotate(-90);
        canvas.drawPath(path, paint);

        // Draw text

        canvas.rotate(90);

        Paint paintText = new Paint();
        paintText.setColor(Color.RED);
        paintText.setTextSize(28);

        String numberToDisplay = "333";
        // measure text width
        float textWidth = paintText.measureText(numberToDisplay, 0, numberToDisplay.length());
        // measure text height
        float textHeight = paintText.descent() - paintText.ascent();

        Log.i("TEST_STRING", "textWidth = " + textWidth + " ; textHeight = " + textHeight);
        int xPos = measuredWidth/2-(int)textWidth;
        int yPos = 0;
        /*int xPos = (getWidth() / 2);
        int yPos = (int) ((getHeight() / 2) /*- ((paintText.descent() + paintText.ascent()) / 2)) ;*/
        canvas.drawText(numberToDisplay, xPos, yPos, paintText);

        canvas.restore();

        super.onDraw(canvas);

    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec)
    {
        int width = measureWidth(widthMeasureSpec);
        int height = measureHeight(heightMeasureSpec, widthMeasureSpec);

        viewWidth = width - (borderWidth *2);
        viewHeight = height - (borderWidth*2);

        setMeasuredDimension(width, height);
    }

    private int measureWidth(int measureSpec)
    {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        } else {
            // Measure the text
            result = viewWidth;

        }

        return result;
    }

    private int measureHeight(int measureSpecHeight, int measureSpecWidth) {
        int result = 0;
        int specMode = MeasureSpec.getMode(measureSpecHeight);
        int specSize = MeasureSpec.getSize(measureSpecHeight);

        if (specMode == MeasureSpec.EXACTLY) {
            // We were told how big to be
            result = specSize;
        } else {
            // Measure the text (beware: ascent is a negative number)
            result = viewHeight;
        }
        return result;
    }
}
