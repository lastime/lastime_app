package com.abfactory.lastime.component.dialogs;

import android.app.Dialog;
import android.app.DialogFragment;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.TimePicker;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.MyToDoListActivity;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.Calendar;

/**
 * Class to use time picker based on DialogFragment
 * (instead of using deprecated Dialog)
 */
public class TimePickerFragment extends DialogFragment
        implements TimePickerDialog.OnTimeSetListener {

    private boolean ignoreResult = false;

    public TimePickerFragment(){
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int hour;
        int minute;

        Calendar c = Calendar.getInstance();

        // Use the date set in the class first or current time as the default time in the picker
        String initialTime = getArguments().getString("initialTime");
        if(initialTime!=null && initialTime.length()>0){
            int minutes = LasTimeUtils.parseTime(initialTime);
            hour = minutes / 60;
            minute = minutes % 60;
        }else{
            hour = c.get(Calendar.HOUR_OF_DAY);
            minute = c.get(Calendar.MINUTE);
        }

        // Create a new instance of DatePickerDialog and return it
        final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(), R.style.myDialogTheme, this, hour, minute, true);
        timePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getResources().getString(R.string.remove_time), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                resetField();
                ignoreResult = true;
            }
        });
        return timePickerDialog;
    }

    @Override
    public void onTimeSet(TimePicker view, int hour, int minute) {
        int minutes = hour * 60 + minute;

        if(ignoreResult) {
            resetField();
            return;
        }

        if (getActivity() instanceof EditEventActivity)
            ((TextView) this.getActivity().findViewById(R.id.event_time_text)).setText(LasTimeUtils.displayTime(minutes));

        if (getActivity() instanceof MyToDoListActivity) {
            DateTimePickerFragment fragment = (DateTimePickerFragment) (getActivity()).getFragmentManager().findFragmentByTag("dateTimePicker");
            ((TextView) fragment.getContent().findViewById(R.id.event_time_text)).setText(LasTimeUtils.displayTime(minutes));
        }

    }

    private void resetField() {
        if (getActivity() instanceof EditEventActivity)
            ((TextView) getActivity().findViewById(R.id.event_time_text)).setText(null);

        if (getActivity() instanceof MyToDoListActivity) {
            DateTimePickerFragment fragment = (DateTimePickerFragment) (getActivity()).getFragmentManager().findFragmentByTag("dateTimePicker");
            ((TextView) fragment.getContent().findViewById(R.id.event_time_text)).setText(null);
        }
    }
}
