package com.abfactory.lastime.component;

import android.app.Activity;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.WishListActivity;
import com.abfactory.lastime.component.dialogs.RecurrenceDialogFragment;
import com.abfactory.lastime.component.dialogs.WishListActionsDialogFragment;
import com.abfactory.lastime.model.EventTemplate;
import com.abfactory.lastime.utils.DisplayImageFromServerTask;

import java.util.Date;
import java.util.List;

public class WishListAdapter extends RecyclerView.Adapter<WishListAdapter.WishViewHolder>{

    private List<EventTemplate> wishes;
    private Date lastDateWishesWereAdded;
    private Activity activity;

    public static class WishViewHolder extends RecyclerView.ViewHolder {
        CardView mainContainer;
        RelativeLayout imageContainer;
        RelativeLayout titleContainer;
        ImageView wishImage;
        TextView titleText;
        ImageView imageNewItem;
        TextView descriptionText;

        WishViewHolder(View itemView) {
            super(itemView);
            mainContainer = (CardView)itemView.findViewById(R.id.cv);
            imageContainer = (RelativeLayout)itemView.findViewById(R.id.image_container);
            titleContainer = (RelativeLayout)itemView.findViewById(R.id.title_container);
            wishImage = (ImageView)itemView.findViewById(R.id.image);
            titleText = (TextView)itemView.findViewById(R.id.title);
            imageNewItem = (ImageView) itemView.findViewById(R.id.image_new_item);
            descriptionText = (TextView)itemView.findViewById(R.id.description);
        }
    }

    public WishListAdapter(List<EventTemplate> wishes, Activity activity){
        this.wishes = wishes;
        lastDateWishesWereAdded = extractLastDateFromItems();
        this.activity = activity;
    }

    private Date extractLastDateFromItems() {
        Date result = null;

        if(wishes!=null && !wishes.isEmpty()){
            result = wishes.get(0).getCreationDate();
            for(EventTemplate et: wishes){
                if(result.before(et.getCreationDate()) && et.getCreationDate()!=null){
                    result = et.getCreationDate();
                }
            }
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return wishes.size();
    }

    @Override
    public WishViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.wish_card_view, viewGroup, false);
        WishViewHolder wvh = new WishViewHolder(v);
        return wvh;
    }

    @Override
    public void onBindViewHolder(WishViewHolder wishViewHolder, final int i) {
        wishViewHolder.wishImage.setImageBitmap(BitmapFactory.decodeFile(wishes.get(i).getImagePath()));
        wishViewHolder.titleText.setText(wishes.get(i).getTitle());
        wishViewHolder.titleContainer.setBackgroundColor(Color.parseColor(wishes.get(i).getColor()));
        wishViewHolder.descriptionText.setText(wishes.get(i).getDescription());
        wishViewHolder.descriptionText.setBackgroundColor(Color.parseColor(wishes.get(i).getColor()));
        wishViewHolder.imageContainer.setBackgroundColor(Color.parseColor("#33" + wishes.get(i).getColor().substring(1, 7)));
        if(lastDateWishesWereAdded.equals(wishes.get(i).getCreationDate())){
            wishViewHolder.imageNewItem.setVisibility(View.VISIBLE);
        } else {
            wishViewHolder.imageNewItem.setVisibility(View.GONE);
        }
        wishViewHolder.mainContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                (WishListActionsDialogFragment.newInstance(wishes.get(i))).show(activity.getFragmentManager(), "recurrencePicker");
            }
        });
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

}
