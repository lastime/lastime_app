package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;

public class ModifyLocationDialogFragment extends DialogFragment {

    public ModifyLocationDialogFragment(){
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final View content = getActivity().getLayoutInflater().inflate(R.layout.modify_location_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(getResources().getString(R.string.modify_location_title));

        content.findViewById(R.id.modify_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditEventActivity) getActivity()).chooseLocation();
                dismiss();
            }
        });

        content.findViewById(R.id.delete_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ((EditEventActivity) getActivity()).doPerformClickDeleteLocation();
                dismiss();
            }
        });

        content.findViewById(R.id.cancel_btn).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Do nothing
                dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(content);

        return builder.create();
    }
}
