package com.abfactory.lastime.component;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Rect;
import android.graphics.drawable.Drawable;

import com.abfactory.lastime.R;

public class HistoryDrawable extends Drawable {

    private Paint mBorderPaint;
    private Paint mButtonPaint;
    private Bitmap icon;

    public HistoryDrawable(Context context) {
        icon = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_view_carousel_black_48dp);

        mButtonPaint = new Paint();
        mButtonPaint.setColor(context.getResources().getColor(R.color.green_dialog_theme));
        mButtonPaint.setAntiAlias(true);
        mButtonPaint.setStyle(Paint.Style.FILL);

        mBorderPaint = new Paint();
        mBorderPaint.setColor(context.getResources().getColor(R.color.green_theme));
        mBorderPaint.setAntiAlias(true);
        mBorderPaint.setStyle(Paint.Style.FILL);

    }

    @Override
    public void draw(Canvas canvas) {

        Rect bounds = getBounds();
        float width = bounds.right - bounds.left;
        float height = bounds.bottom - bounds.top;

        // Position the badge in the top-right quadrant of the LASTIME_ICON.
        float radius = Math.min(width, height);
        float centerX = width;
        float centerY = height;

        // Draw badge circle.
        canvas.drawCircle(centerX, centerY, radius, mBorderPaint);
        canvas.drawCircle(centerX, centerY, radius-30, mButtonPaint);

        // Draw image
        Paint p=new Paint();
        p.setColor(Color.WHITE);
        canvas.drawBitmap(icon, width/4+icon.getWidth()/4, height/4+icon.getHeight()/4, p);

        invalidateSelf();
    }

    @Override
    public void setAlpha(int alpha) {
        // do nothing
    }

    @Override
    public void setColorFilter(ColorFilter cf) {
        // do nothing
    }

    @Override
    public int getOpacity() {
        return PixelFormat.UNKNOWN;
    }
}
