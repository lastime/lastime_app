package com.abfactory.lastime.component;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.abfactory.lastime.Lastime;
import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.StatisticsCalculator;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.OptionalDouble;
import java.util.OptionalInt;

import static com.abfactory.lastime.utils.StatisticsCalculator.STATS_TYPE.MONTH;
import static com.abfactory.lastime.utils.StatisticsCalculator.STATS_TYPE.TOTAL;
import static com.abfactory.lastime.utils.StatisticsCalculator.STATS_TYPE.WEEK;
import static com.abfactory.lastime.utils.StatisticsCalculator.STATS_TYPE.YEAR;

public class StatsViewAdapter extends RecyclerView.Adapter<StatsViewAdapter.StatsViewHolder> {

    private List<STATS_THEME> statsThemeList;
    private IEventDAO eventDAO;
    private static NumberFormat nf;
    private final List<Event> eventList;

    private enum STATS_NAME {
        WEEK_OCC(Lastime.getContext().getString(R.string.week_occurrences)),
        MONTH_OCC(Lastime.getContext().getString(R.string.month_occurrences)),
        YEAR_OCC(Lastime.getContext().getString(R.string.year_occurrences)),
        TOTAL_OCC(Lastime.getContext().getString(R.string.total_occurrences)),

        WEEK_EXP(Lastime.getContext().getString(R.string.week_expenses)),
        MONTH_EXP(Lastime.getContext().getString(R.string.month_expenses)),
        YEAR_EXP(Lastime.getContext().getString(R.string.year_expenses)),
        TOTAL_EXP(Lastime.getContext().getString(R.string.total_expenses)),

        WEEK_BIGGEST_DIFF(Lastime.getContext().getString(R.string.biggest_difference_in_week)),
        MONTH_BIGGEST_DIFF(Lastime.getContext().getString(R.string.biggest_difference_in_month)),
        YEAR_BIGGEST_DIFF(Lastime.getContext().getString(R.string.biggest_difference_in_year)),
        TOTAL_BIGGEST_DIFF(Lastime.getContext().getString(R.string.biggest_difference_in_total)),

        WEEK_SMALLEST_DIFF(Lastime.getContext().getString(R.string.smallest_difference_in_week)),
        MONTH_SMALLEST_DIFF(Lastime.getContext().getString(R.string.smallest_difference_in_month)),
        YEAR_SMALLEST_DIFF(Lastime.getContext().getString(R.string.smallest_difference_in_year)),
        TOTAL_SMALLEST_DIFF(Lastime.getContext().getString(R.string.smallest_difference_in_total));

        private String name;

        STATS_NAME(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }
    }

    private enum STATS_THEME {
        OCCURRENCES, EXPENSES, SMALLEST, BIGGEST;
    }

    public StatsViewAdapter(DisplayEventActivity activity) {
        this.eventDAO = new EventDAO(activity);
        eventList = eventDAO.getAllEventHistory(activity.getEventDisplayed());
        initStatListToDisplay();
        nf = NumberFormat.getInstance();
        nf.setMaximumFractionDigits(2);
    }

    public void initStatListToDisplay() {
        statsThemeList = new ArrayList<>();
        statsThemeList.add(STATS_THEME.OCCURRENCES);
        statsThemeList.add(STATS_THEME.SMALLEST);
        statsThemeList.add(STATS_THEME.BIGGEST);
        statsThemeList.add(STATS_THEME.EXPENSES);
    }

    @Override
    public int getItemCount() {
        return statsThemeList.size();
    }

    @Override
    public void onBindViewHolder(StatsViewHolder statsViewHolder, int i) {;
        switch (i) {
            // Occurrences
            case 0:
                statsViewHolder.addStats(new Statistic(STATS_NAME.WEEK_OCC, StatisticsCalculator.getTotalInstanceNumber(eventList,WEEK)));
                statsViewHolder.addStats(new Statistic(STATS_NAME.MONTH_OCC, StatisticsCalculator.getTotalInstanceNumber(eventList, MONTH)));
                statsViewHolder.addStats(new Statistic(STATS_NAME.YEAR_OCC, StatisticsCalculator.getTotalInstanceNumber(eventList, YEAR)));
                statsViewHolder.addStats(new Statistic(STATS_NAME.TOTAL_OCC, StatisticsCalculator.getTotalInstanceNumber(eventList, TOTAL)));
                break;
            // Biggest difference between events
            case 1:
                statsViewHolder.addStats(STATS_NAME.WEEK_BIGGEST_DIFF, StatisticsCalculator.getBiggestDiffBetweenEvents(eventList, WEEK));
                statsViewHolder.addStats(STATS_NAME.MONTH_BIGGEST_DIFF, StatisticsCalculator.getBiggestDiffBetweenEvents(eventList, MONTH));
                statsViewHolder.addStats(STATS_NAME.YEAR_BIGGEST_DIFF, StatisticsCalculator.getBiggestDiffBetweenEvents(eventList, YEAR));
                statsViewHolder.addStats(STATS_NAME.TOTAL_BIGGEST_DIFF, StatisticsCalculator.getBiggestDiffBetweenEvents(eventList, TOTAL));
                break;
            // Smallest difference between events
            case 2:
                statsViewHolder.addStats(STATS_NAME.WEEK_SMALLEST_DIFF, StatisticsCalculator.getSmallestDiffBetweenEvents(eventList, WEEK));
                statsViewHolder.addStats(STATS_NAME.MONTH_SMALLEST_DIFF, StatisticsCalculator.getSmallestDiffBetweenEvents(eventList, MONTH));
                statsViewHolder.addStats(STATS_NAME.YEAR_SMALLEST_DIFF, StatisticsCalculator.getSmallestDiffBetweenEvents(eventList, YEAR));
                statsViewHolder.addStats(STATS_NAME.TOTAL_SMALLEST_DIFF, StatisticsCalculator.getSmallestDiffBetweenEvents(eventList, TOTAL));
                break;
            // Expenses
            case 3:
                statsViewHolder.addStats(STATS_NAME.WEEK_EXP, StatisticsCalculator.getTotalExpenses(eventList, WEEK));
                statsViewHolder.addStats(STATS_NAME.MONTH_EXP, StatisticsCalculator.getTotalExpenses(eventList, MONTH));
                statsViewHolder.addStats(STATS_NAME.YEAR_EXP, StatisticsCalculator.getTotalExpenses(eventList, YEAR));
                statsViewHolder.addStats(STATS_NAME.TOTAL_EXP, StatisticsCalculator.getTotalExpenses(eventList, TOTAL));
                break;
            default:
                break;
        }

        statsViewHolder.displayValue(0);
    }

    @Override
    public StatsViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View itemView = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.stats_cards_list, viewGroup, false);

        final StatsViewHolder statsViewHolder = new StatsViewHolder(itemView);

        itemView.setOnClickListener(view -> {
            statsViewHolder.switchValue();
        });

        return statsViewHolder;
    }

    public class StatsViewHolder extends RecyclerView.ViewHolder {

        private List<Statistic> statisticList;
        private int currentStatSelection;
        protected TextView statsTop;
        protected TextView statsBottom;
        protected TextView statsResult;

        public StatsViewHolder(View itemView) {
            super(itemView);
            statsTop = itemView.findViewById(R.id.stats_top);
            statsBottom = itemView.findViewById(R.id.stats_bottom);
            statsResult = itemView.findViewById(R.id.stats_result);
            statisticList = new ArrayList<>();
            currentStatSelection = 0;
        }

        public void addStats(Statistic statistic) {
            if (statistic.getResult() > 0)
                statisticList.add(statistic);
        }

        public void addStats(STATS_NAME name, OptionalDouble result) {
            if (result.isPresent() && result.getAsDouble() > 0d)
                statisticList.add(new Statistic(name, result.getAsDouble()));
        }

        public void addStats(STATS_NAME name, OptionalInt result) {
            if (result.isPresent() && result.getAsInt() > 0)
                statisticList.add(new Statistic(name, result.getAsInt()));
        }

        public void switchValue() {
            if (currentStatSelection < statisticList.size() - 1) {
                currentStatSelection++;
            } else {
                currentStatSelection = 0;
            }
            displayValue(currentStatSelection);
        }

        public void displayValue(int selection) {
            if (!statisticList.isEmpty()) {
                statsTop.setText(statisticList.get(selection).getName());
                statsBottom.setText(statisticList.get(selection).getName());
                statsResult.setText(nf.format(statisticList.get(selection).getResult()));
                itemView.findViewById(R.id.stats_notfound_img).setVisibility(View.GONE);
                statsTop.setVisibility(View.VISIBLE);
                statsBottom.setVisibility(View.VISIBLE);
                statsResult.setVisibility(View.VISIBLE);
            } else {
                itemView.findViewById(R.id.stats_notfound_img).setVisibility(View.VISIBLE);
                statsTop.setVisibility(View.GONE);
                statsBottom.setVisibility(View.GONE);
                statsResult.setVisibility(View.GONE);
            }
        }
    }

    public class Statistic {
        private String name;
        private double result;

        public Statistic(STATS_NAME name, double result) {
            this.name = name.getName();
            this.result = result;
        }

        public String getName() {
            return name;
        }

        public double getResult() {
            return result;
        }
    }
}
