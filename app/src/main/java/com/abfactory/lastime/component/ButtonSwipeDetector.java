package com.abfactory.lastime.component;

import android.app.Activity;
import android.support.design.widget.Snackbar;
import android.view.MotionEvent;
import android.view.View;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LasTimeActivity;

/**
 * Class to handle swiping from a button
 */
public class ButtonSwipeDetector implements View.OnTouchListener {

    private static final int MIN_DISTANCE = 300;
    private static final int MIN_LOCK_DISTANCE = 30; // disallow motion intercept
    private static final int MAX_LOCK_DISTANCE = 600;
    private boolean motionInterceptDisallowed = false;
    private float downX, upX, downY, upY;
    private boolean isSwipe;
    private Activity activity;

    public ButtonSwipeDetector(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                isSwipe = false;
                downX = event.getX();
                downY = event.getY();
                return true; // allow other events like Click to be processed
            }

            case MotionEvent.ACTION_MOVE: {
                upX = event.getX();
                upY = event.getY();
                float deltaX = downX - upX;

                // If swipe to left to show delete option on right side of the list item
                if (Math.abs(deltaX) > MIN_LOCK_DISTANCE && v.getParent() != null && !motionInterceptDisallowed) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    motionInterceptDisallowed = true;
                    isSwipe = true;
                }

                if (deltaX < 0) {
//                    holder.rightView.setVisibility(View.GONE);
//                    holder.leftView.setVisibility(View.VISIBLE);
                } else {
                    // if first swiped left and then swiped right
//                    holder.rightView.setVisibility(View.VISIBLE);
//                    holder.leftView.setVisibility(View.GONE);
                }
                if (isSwipe && Math.abs(deltaX) < MAX_LOCK_DISTANCE) {
                    swipe(-(int) deltaX);
                }

                return true;
            }

            case MotionEvent.ACTION_UP:
                upX = event.getX();
                upY = event.getY();

                float deltaX = upX - downX;
                float deltaY = upY - downY;

                // Handle click case
                if (!isSwipe) {
                    // Pass event to DisplayEventActivity
//                    Intent intent = new Intent(activity, DisplayEventActivity.class);
//                    intent.putExtra("eventId", activity.getOrderingHandler().getEvent(section, row).getId());
//                    activity.startActivityForResult(intent, LastTimeActivity.EVENT_DISPLAY);
                }
                // Handle swipe cases
                else {

                    if (Math.abs(deltaX) < 50 && Math.abs(deltaY) < 50 && !isSwipe) {
                        swipeBack();
                    } else {
                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            if (deltaX < 0) {
                                swipeFromRight();
                            } else {
                                swipeFromLeft();
                            }
                        } else {
                            swipeBack();
                        }

                        if (v.getParent() != null) {
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            motionInterceptDisallowed = false;
                        }

//                        holder.rightView.setVisibility(View.VISIBLE);
                    }
                }

                return true;

            case MotionEvent.ACTION_CANCEL:
                swipeBack();
                return false;
        }

        return true;
    }

    private void swipe(int distance) {
//        View animationView = holder.mainView;
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) animationView.getLayoutParams();
//        params.rightMargin = -distance;
//        params.leftMargin = distance;
//        animationView.setLayoutParams(params);
    }

    private void swipeBack() {
//        View animationView = holder.mainView;
//
//        TranslateAnimation translate = new TranslateAnimation(animationView.getLeft(), 0f, 0f, 0f);
//
//        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) animationView.getLayoutParams();
//        params.rightMargin = 0;
//        params.leftMargin = 0;
//        animationView.setLayoutParams(params);
//
//        translate.setDuration(1000);
//        animationView.setAnimation(translate);
    }

    // Used to change event color randomly
    private void swipeFromRight() {
//        Event e = this.activity.getOrderingHandler().getEvent(section, row);
//        Integer color = activity.getEventDAO().changeEventColor(e);
//        if (color != null)
//            e.setColor(color);
//        swipeBack();
//        this.activity.refreshListDisplay();
        Snackbar.make(((LasTimeActivity)ButtonSwipeDetector.this.activity).getAddBtn(),
                activity.getResources().getString(R.string.color_changed), Snackbar.LENGTH_LONG).show();
    }

    // Used to refresh event
    private void swipeFromLeft() {
//        Event e = this.activity.getOrderingHandler().getEvent(section, row);
//
//        // Save safe mode data
//        activity.setSafeModifyEventOld((Event) e.clone());
//
//        // Update DB
//        activity.getEventDAO().refreshEvent(e);
//
//        // Perform modif for display
//        e.setDate(Calendar.getInstance().getTime());
//        e.setWeight(e.getWeight() + 1);
//
//        // Redisplay
//        swipeBack();
//
//        this.activity.refreshListDisplay();

        // Display rollback option
        Snackbar.make(((LasTimeActivity)ButtonSwipeDetector.this.activity).getAddBtn(),
                ButtonSwipeDetector.this.activity.getString(R.string.repeated_event_text), Snackbar.LENGTH_LONG).show();
    }
}