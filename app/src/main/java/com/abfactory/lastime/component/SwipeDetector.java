package com.abfactory.lastime.component;

import android.content.Intent;
import android.graphics.drawable.LayerDrawable;
import android.support.design.widget.Snackbar;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.TranslateAnimation;
import android.widget.RelativeLayout;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LasTimeActivity;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.model.Notification;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.NotificationUtils;

import java.util.List;

public class SwipeDetector implements View.OnTouchListener {

    private static final int MIN_DISTANCE = 300;
    private static final int MIN_LOCK_DISTANCE = 30; // disallow motion intercept
    private static final int MAX_LOCK_DISTANCE = 600;
    private boolean motionInterceptDisallowed = false;
    private float downX, upX, downY, upY;
    private EventObjectHolder holder;
    private int section;
    private int row;
    private boolean isSwipe;
    private LasTimeActivity lasTimeActivity;
    private EventDAO eventDAO;

    public SwipeDetector(EventObjectHolder h, int s, int r, LasTimeActivity lasTimeActivity) {
        holder = h;
        section = s;
        row = r;
        this.lasTimeActivity = lasTimeActivity;
        eventDAO = new EventDAO(this.lasTimeActivity.getApplicationContext());
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN: {
                isSwipe = false;
                downX = event.getX();
                downY = event.getY();
                return true; // allow other events like Click to be processed
            }

            case MotionEvent.ACTION_MOVE: {
                upX = event.getX();
                upY = event.getY();
                float deltaX = downX - upX;

                // If swipe to left to show delete option on right side of the list item
                if (Math.abs(deltaX) > MIN_LOCK_DISTANCE && v.getParent() != null && !motionInterceptDisallowed) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    motionInterceptDisallowed = true;
                    isSwipe = true;
                }

                if (deltaX < 0) {
                    holder.rightView.setVisibility(View.GONE);
                    holder.leftView.setVisibility(View.VISIBLE);
                } else {
                    // if first swiped left and then swiped right
                    holder.rightView.setVisibility(View.VISIBLE);
                    holder.leftView.setVisibility(View.GONE);
                }
                if (isSwipe && Math.abs(deltaX) < MAX_LOCK_DISTANCE) {
                    swipe(-(int) deltaX);
                }

                return true;
            }

            case MotionEvent.ACTION_UP:
                upX = event.getX();
                upY = event.getY();

                float deltaX = upX - downX;
                float deltaY = upY - downY;

                // Handle click case
                if(!isSwipe){
                    // Pass event to DisplayEventActivity
                    Intent intent = new Intent(lasTimeActivity, DisplayEventActivity.class);
                    intent.putExtra("eventId", lasTimeActivity.getOrderingHandler().getEvent(section, row).getId());
                    lasTimeActivity.startActivityForResult(intent, LasTimeActivity.EVENT_DISPLAY);
                }
                // Handle swipe cases
                else {

                    if (Math.abs(deltaX) < 50 && Math.abs(deltaY) < 50 && !isSwipe) {
                        swipeBack();
                    } else {
                        if (Math.abs(deltaX) > MIN_DISTANCE) {
                            if (deltaX < 0) {
                                swipeFromRight();
                            } else {
                                swipeFromLeft();
                            }
                        } else {
                            swipeBack();
                        }

                        if (v.getParent() != null) {
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            motionInterceptDisallowed = false;
                        }

                        holder.rightView.setVisibility(View.VISIBLE);
                    }
                }

                return true;

            case MotionEvent.ACTION_CANCEL:
                swipeBack();
                return false;
        }

        return true;
    }

    private void swipe(int distance) {
        View animationView = holder.mainView;
        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) animationView.getLayoutParams();
        params.rightMargin = -distance;
        params.leftMargin = distance;
        animationView.setLayoutParams(params);
    }

    private void swipeBack() {
        View animationView = holder.mainView;

        TranslateAnimation translate = new TranslateAnimation(animationView.getLeft(), 0f, 0f, 0f);

        RelativeLayout.LayoutParams params = (RelativeLayout.LayoutParams) animationView.getLayoutParams();
        params.rightMargin = 0;
        params.leftMargin = 0;
        animationView.setLayoutParams(params);

        translate.setDuration(250);
        animationView.setAnimation(translate);
    }

    /**
     * Used to create a now entry in TODO list
     **/
    private void swipeFromRight() {

        Event e = this.lasTimeActivity.getOrderingHandler().getEvent(section, row);
        eventDAO.createNotification(new Notification(e.getId(), IEventDAO.NOTIFICATION_STATE.ACTIVE));
        swipeBack();
        this.lasTimeActivity.refreshListDisplay();

        // Update badge count
        if(lasTimeActivity.getMenu()!=null){
            // Update notifications count
            MenuItem item = lasTimeActivity.getMenu().findItem(R.id.notif);
            LayerDrawable icon = (LayerDrawable) item.getIcon();
            // Update LayerDrawable's BadgeDrawable
            LasTimeUtils.setBadgeCount(lasTimeActivity, icon, lasTimeActivity.getEventDAO().getActiveNotificationsCount());
        }
        
        // Update menu bullet display
        lasTimeActivity.getDrawerMenu().refreshBulletDisplay();

        Snackbar.make(SwipeDetector.this.lasTimeActivity.getAddBtn(),
                lasTimeActivity.getResources().getString(R.string.event_added_in_todo_list), Snackbar.LENGTH_LONG).show();
    }

    // Used to refresh event
    private void swipeFromLeft() {

        Event e = this.lasTimeActivity.getOrderingHandler().getEvent(section, row);

        // Update DB
        lasTimeActivity.getEventDAO().refreshEvent(e);

        // Redisplay
        swipeBack();

        // Clean notifications if need be
        NotificationUtils.cleanNotificationsWhenDeletingEvent(new EventDAO(this.lasTimeActivity.getApplicationContext()), Integer.parseInt(e.getReference()), this.lasTimeActivity);

        // Refresh list
        // Get events from DB
        List<Event> events = lasTimeActivity.getEventDAO().getAllActiveEvents();
        lasTimeActivity.setEvents(events);
        lasTimeActivity.setFilteredEvents(events);
        this.lasTimeActivity.refreshListDisplay();

        // Update badge count
        if(lasTimeActivity.getMenu()!=null){
            // Update notifications count
            MenuItem item = lasTimeActivity.getMenu().findItem(R.id.notif);
            LayerDrawable icon = (LayerDrawable) item.getIcon();
            // Update LayerDrawable's BadgeDrawable
            LasTimeUtils.setBadgeCount(lasTimeActivity, icon, lasTimeActivity.getEventDAO().getActiveNotificationsCount());
        }

        // Update menu bullet display
        lasTimeActivity.getDrawerMenu().refreshBulletDisplay();

        // Display rollback option
        Snackbar.make(SwipeDetector.this.lasTimeActivity.getAddBtn(),
                SwipeDetector.this.lasTimeActivity.getString(R.string.repeated_event_text), Snackbar.LENGTH_LONG).show();
    }
}
