package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.LasTimeActivity;
import com.abfactory.lastime.activity.NewLocationActivity;
import com.abfactory.lastime.activity.edit.EditEventActivity;

public class StopTutorialFragment extends DialogFragment {

    public StopTutorialFragment(){
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.basic_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(R.string.stop_tuto_dialog_title);
        ((TextView)content.findViewById(R.id.error_msg)).setText(R.string.stop_tuto_dialog_details);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setNegativeButton(R.string.cancel_maj, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dismiss();
                    }
                })
                .setPositiveButton(R.string.stop_tuto_dialog_stop_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(getActivity() instanceof EditEventActivity){
                            getActivity().finish();
                        } else if(getActivity() instanceof LasTimeActivity){
                            ((LasTimeActivity)getActivity()).stopTutorial();
                        } else if(getActivity() instanceof NewLocationActivity){
                            ((NewLocationActivity)getActivity()).stopTutorial();
                        }
                        dismiss();
                    }
                });

        this.setCancelable(true);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;

    }

}
