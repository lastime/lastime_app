package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.design.widget.Snackbar;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.MyToDoListActivity;
import com.abfactory.lastime.model.Contact;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.utils.LasTimeUtils;

import org.joda.time.DateTime;

import java.text.ParseException;
import java.util.Date;

public class DateTimePickerFragment extends DialogFragment {

    private static final int CALENDAR_REQUEST = 100;

    private View content;
    private TextView date;
    private TextView time;

    private EventDAO eventDAO;
    private long eventId;
    private long notificationId;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        // Use the date set in the class first or current date as the default date in the picker
        final String initDate = getArguments().getString("initialDate");
        final String initTime = getArguments().getString("initialTime");
        notificationId = getArguments().getLong("notificationId");
        eventId = getArguments().getLong("eventId");

        // DAO
        eventDAO = new EventDAO(getActivity());

        // Use the Builder class for convenient dialog construction
        Builder builder = new AlertDialog.Builder(getActivity());

        // Set the layout for the dialog
        content = getActivity().getLayoutInflater().inflate(R.layout.date_time_picker_layout, null);
        date = (TextView) content.findViewById(R.id.event_date_text);
        date.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickUpDate(initDate);
            }
        });
        time = (TextView) content.findViewById(R.id.event_time_text);
        time.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                pickUpTime(initTime);
            }
        });

        if (initDate != null) {
            date.setText(initDate);
        }
        if (initTime != null) {
            time.setText(initTime);
        }

        // Push event to calendar
        content.findViewById(R.id.push_event_to_calendar).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    addToCalendar();
                } catch (ParseException e) {
                    Log.e(DateTimePickerFragment.class.getName(), "Error parsing date", e);
                }
            }
        });

        builder.setView(content);


        // Set title of dialog
        // Set Ok button
        builder.setPositiveButton(R.string.ok,
                new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        saveData();
                    }
                })
                        // Set Cancel button
                .setNegativeButton(R.string.cancel,
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // User cancelled the dialog
                                DateTimePickerFragment.this.getDialog().cancel();
                            }
                        });

        // Create the AlertDialog object and return it
        return builder.create();
    }

    /**
     * Open date picker fragment
     */
    private void pickUpDate(String initialDate) {
        DatePickerFragment picker = new DatePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("initialDate", initialDate);
        bundle.putBoolean("forceNoLimit", true);
        picker.setArguments(bundle);
        picker.show(getFragmentManager(), "datePicker");
    }

    /**
     * Open time picker fragment
     */
    private void pickUpTime(String initialTime) {
        TimePickerFragment picker = new TimePickerFragment();
        Bundle bundle = new Bundle();
        bundle.putString("initialTime", initialTime);
        picker.setArguments(bundle);
        picker.show(getFragmentManager(), "timePicker");
    }

    /**
     * Add event to user's calendar (Android one) based on previous input
     **/
    private void addToCalendar() throws ParseException {
        if (date.getText() == null || date.getText().toString().length() < 1) {
            Snackbar.make(((MyToDoListActivity) getActivity()).getMainContainer(), R.string.date_mandatory, Snackbar.LENGTH_LONG).show();
            return;
        }
        Event event = eventDAO.retrieveEventById(eventId);
        DateTime appointment = new DateTime(Event.CALENDAR_DISPLAY.parse(date.getText().toString()));
        DateTime endTime = null;
        if (time.getText() != null && time.getText().toString().length() > 0) {
            int minutes = LasTimeUtils.parseTime(time.getText().toString());
            int hour = minutes / 60;
            int minute = minutes % 60;
            appointment = appointment.plusHours(hour);
            appointment = appointment.plusMinutes(minute);

            // Set endTime one hour later by default
            // TODO: Think a better way to handle it, could last more...
            endTime = appointment.plusHours(1);
        }
        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.Events.TITLE, event.getTitle())
                .putExtra(CalendarContract.Events.DESCRIPTION, event.getNote()
                        + (event.getContact() != -1 ? "\n\n Contact: "
                        + eventDAO.getContactById(event.getContact()).getContactName() : ""))
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY)
                .putExtra(CalendarContract.Events.EVENT_COLOR, event.getColor())
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, appointment.getMillis());
        if (endTime != null) {
            intent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getMillis());
        } else {
            intent.putExtra(CalendarContract.Events.ALL_DAY, true);
        }
        if (event.getLocation() > 0)
            intent.putExtra(CalendarContract.Events.EVENT_LOCATION, eventDAO.getLocationById(event.getLocation()).getName());
        startActivityForResult(intent, CALENDAR_REQUEST);
    }

    public View getContent() {
        return content;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CALENDAR_REQUEST) {
            Log.i(DateTimePickerFragment.class.getName(), "Result code: " + resultCode);
            saveData();
            this.dismiss();
        }
    }

    private void saveData() {
        try {
            Date d = null;
            if (date.getText() != null && date.getText().length() > 0)
                d = Event.CALENDAR_DISPLAY.parse(date.getText().toString());
            eventDAO.setNotificationEventPlannedDate(notificationId, d);
        } catch (ParseException e) {
            Log.e(DateTimePickerFragment.class.getName(), "Error parsing date", e);
        }
        int t = LasTimeUtils.parseTime(time.getText().toString());
        eventDAO.setNotificationEventPlannedTime(notificationId, t);
        if (getActivity() instanceof MyToDoListActivity)
            ((MyToDoListActivity) getActivity()).refreshDisplay();
    }
}
