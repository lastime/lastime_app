package com.abfactory.lastime.component.dialogs;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.util.Log;
import android.widget.DatePicker;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.MyToDoListActivity;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.model.Event;

import java.text.ParseException;
import java.util.Calendar;
import java.util.Date;

/**
 * Class to use date picker based on DialogFragment
 * (instead of using deprecated Dialog)
 */

public class DatePickerFragment extends DialogFragment
        implements DatePickerDialog.OnDateSetListener {

    private boolean ignoreResult = false;
    private CALLER caller;

    public enum CALLER {
        DATE, END_DATE
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        int year;
        int month;
        int day;

        Calendar c = Calendar.getInstance();

        // Use the date set in the class first or current date as the default date in the picker
        if (getArguments().getString("caller") != null)
            caller = CALLER.valueOf(getArguments().getString("caller"));
        String initDate = getArguments().getString("initialDate");
        String minDate = getArguments().getString("minDate");
        boolean forceNoLimit = getArguments().getBoolean("forceNoLimit");

        if(minDate != null)
            c = this.getCalendar(minDate);
        if (initDate != null) {
            c = this.getCalendar(initDate);
        }

        year = c.get(Calendar.YEAR);
        month = c.get(Calendar.MONTH);
        day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(), R.style.myDialogTheme, this, year, month, day);
        Calendar limit = Calendar.getInstance();
        limit.set(Calendar.HOUR_OF_DAY, 23);
        if (!forceNoLimit)
            datePickerDialog.getDatePicker().setMaxDate(limit.getTimeInMillis());
        else if(caller != null && caller.equals(CALLER.END_DATE)) {
            limit = this.getCalendar(minDate);
            datePickerDialog.getDatePicker().setMinDate(limit.getTimeInMillis());
        }

        if (getActivity() instanceof MyToDoListActivity || caller != null && caller.equals(CALLER.END_DATE)) {
            datePickerDialog.setButton(DialogInterface.BUTTON_NEUTRAL, getResources().getString(R.string.remove_time), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    resetField();
                    ignoreResult = true;
                }
            });
        }
        return datePickerDialog;
    }

    @Override
    public void onDateSet(DatePicker view, int year, int month, int day) {
        Calendar c = Calendar.getInstance();
        c.set(year, month, day);

        if (ignoreResult) {
            resetField();
            return;
        }

        if (getActivity() instanceof EditEventActivity) {
            if (caller.equals(CALLER.DATE)) {
                ((TextView) this.getActivity().findViewById(R.id.event_date_text)).setText(Event.CALENDAR_DISPLAY.format(c.getTime()));
                // Check end_date, it could not be before event date
                TextView endDate = ((TextView) this.getActivity().findViewById(R.id.event_end_date_text));
                if(endDate.getText() != null && endDate.getText().length() > 0){
                    try {
                        Date date = Event.CALENDAR_DISPLAY.parse(endDate.getText().toString());
                        if(date.before(c.getTime())){
                            // Remove end date in that case
                            endDate.setText(null);
                        }
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
            else if (caller.equals(CALLER.END_DATE))
                ((TextView) this.getActivity().findViewById(R.id.event_end_date_text)).setText(Event.CALENDAR_DISPLAY.format(c.getTime()));
        }

        if (getActivity() instanceof MyToDoListActivity) {
            DateTimePickerFragment fragment = (DateTimePickerFragment) (getActivity()).getFragmentManager().findFragmentByTag("dateTimePicker");
            ((TextView) fragment.getContent().findViewById(R.id.event_date_text)).setText(Event.CALENDAR_DISPLAY.format(c.getTime()));
        }
    }

    private Calendar getCalendar(String toParse) {
        final Calendar result = Calendar.getInstance();
        try {
            result.setTime(Event.CALENDAR_DISPLAY.parse(toParse));
        } catch (ParseException e) {
            Log.e(DateTimePickerFragment.class.getName(), "Error occurred parsing date", e);
        }
        return result;
    }

    private void resetField() {
        DateTimePickerFragment fragment = (DateTimePickerFragment) (getActivity()).getFragmentManager().findFragmentByTag("dateTimePicker");
        if (getActivity() instanceof MyToDoListActivity) {
            ((TextView) fragment.getContent().findViewById(R.id.event_date_text)).setText(null);
        }
        if (caller != null && caller.equals(CALLER.END_DATE)) {
            ((TextView) this.getActivity().findViewById(R.id.event_end_date_text)).setText(null);
        }
    }
}
