package com.abfactory.lastime.component.draggablelist;

import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.support.v4.view.MotionEventCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.DisplayCustomizationActivity;
import com.abfactory.lastime.component.draggablelist.helper.ItemTouchHelperAdapter;
import com.abfactory.lastime.component.draggablelist.helper.ItemTouchHelperViewHolder;
import com.abfactory.lastime.component.draggablelist.helper.OnStartDragListener;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ItemViewHolder>
        implements ItemTouchHelperAdapter {

    private List<DraggableListItem> mItems = new ArrayList<>();
    private DisplayCustomizationActivity ctx;

    private final OnStartDragListener mDragStartListener;

    public RecyclerListAdapter(DisplayCustomizationActivity context, OnStartDragListener dragStartListener) {
        mDragStartListener = dragStartListener;
        ctx = context;
        mItems = LasTimeUtils.getDisplayPreferences(ctx.getEvent());
    }

    @Override
    public ItemViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.draggable_element_list_item, parent, false);
        ItemViewHolder itemViewHolder = new ItemViewHolder(view);
        return itemViewHolder;
    }

    @Override
    public void onBindViewHolder(final ItemViewHolder holder, int position) {
        DraggableListItem item = mItems.get(position);
        holder.textView.setText(ctx.getString(item.getElementname()));
        if (!item.isHiddenItemSeparator() && !item.isDisplayedItemSeparator()) {
            holder.icon.setVisibility(View.VISIBLE);
            Drawable d = LasTimeUtils.getDrawable(ctx, item.getResourceId());
            d.setTint(Color.GRAY);
            holder.icon.setBackground(d);
            holder.reorderButtonView.setVisibility(View.VISIBLE);
            // Start a drag whenever the handle view it touched
            holder.reorderButtonView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View v, MotionEvent event) {
                    if (MotionEventCompat.getActionMasked(event) == MotionEvent.ACTION_DOWN) {
                        mDragStartListener.onStartDrag(holder);
                    }
                    return false;
                }
            });
        } else {
            holder.mainContainer.setBackgroundColor(Color.parseColor("#B3E5FC"));
            holder.reorderButtonView.setVisibility(View.GONE);
            holder.icon.setVisibility(View.GONE);
        }
    }

    @Override
    public void onItemDismiss(int position) {
        mItems.remove(position);
        notifyItemRemoved(position);
    }

    @Override
    public boolean onItemMove(int fromPosition, int toPosition) {
        // To prevent we swipe with the first list item which is the divider for elements shown by default
        if (toPosition == 0) {
            toPosition = 1;
        }
        // Swap business
        Collections.swap(mItems, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
        // Save settings
        ctx.getEvent().setFieldList(LasTimeUtils.formatDisplayPreferences(mItems));
        ctx.getEventDAO().refreshFieldEvent(ctx.getEvent());
        return true;
    }

    @Override
    public int getItemCount() {
        return mItems.size();
    }

    /**
     * Simple example of a view holder that implements {@link ItemTouchHelperViewHolder} and has a
     * "handle" view that initiates a drag event when touched.
     */
    public static class ItemViewHolder extends RecyclerView.ViewHolder implements
            ItemTouchHelperViewHolder {

        public final RelativeLayout mainContainer;
        public final ImageView icon;
        public final TextView textView;
        public final ImageView reorderButtonView;

        public ItemViewHolder(View itemView) {
            super(itemView);
            mainContainer = itemView.findViewById(R.id.main_container);
            icon = itemView.findViewById(R.id.icon);
            textView = itemView.findViewById(R.id.item_name);
            reorderButtonView = itemView.findViewById(R.id.reorder_button);
        }

        @Override
        public void onItemSelected() {
            itemView.setBackgroundColor(Color.parseColor("#ffebeff1"));
        }

        @Override
        public void onItemClear() {
            itemView.setBackgroundColor(Color.parseColor("#ffebeff1"));
        }
    }
}