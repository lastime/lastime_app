package com.abfactory.lastime.component.dialogs;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.IEventDAO;

/**
 * Class displays fragment to user to choice which kind of delete he wants to execute
 */
public class DeleteChoiceDialog extends DialogFragment{
    public DeleteChoiceDialog(){
        super();
    }

    private IEventDAO eventDAO = null;
    private Event event = null;
    private boolean isOnlySimpleDelete = false;

    public interface DeleteChoiceDialogListener{
        void updateActivity(Event event, int reference);
    }

    private DeleteChoiceDialogListener listener;

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.delete_choice_dialog, null);

        LinearLayout deleteAll = (LinearLayout) content.findViewById(R.id.deleteAll);
        if(!isOnlySimpleDelete) {
            deleteAll.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (eventDAO != null && event != null)
                        eventDAO.deleteEventAndHistory(event.getReference());
                    notifyActivity(null, Integer.parseInt(event.getReference()));
                }
            });
            deleteAll.setVisibility(View.VISIBLE);
            content.findViewById(R.id.deleteCurrentDescription).setVisibility(View.VISIBLE);
        }else{
            deleteAll.setVisibility(View.GONE);
            content.findViewById(R.id.deleteCurrentDescription).setVisibility(View.GONE);
        }

        LinearLayout deleteCurrent = (LinearLayout) content.findViewById(R.id.deleteCurrent);
        deleteCurrent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(eventDAO != null && event != null) {
                    Event eventActivated = eventDAO.deleteEvent(event);
                    notifyActivity(eventActivated, Integer.parseInt(event.getReference()));
                }
                dismiss();
            }
        });

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setNegativeButton(getResources().getString(R.string.cancel), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        /*Do nothing*/
                    }
                });

        return  builder.create();

    }

    @Override
    public void onAttach(Activity activity){
        super.onAttach(activity);
        this.listener = (DeleteChoiceDialogListener) activity;
    }

    private void notifyActivity(Event event, int referenceToDelete){
        if(this.listener != null)
            listener.updateActivity(event, referenceToDelete);
    }

    public void forceDeleteChoiceOnly(){
        isOnlySimpleDelete = true;
    }

    public void setEventDAO(IEventDAO eventDAO){
        this.eventDAO = eventDAO;
    }

    public void setEvent(Event event){
        this.event = event;
    }
}
