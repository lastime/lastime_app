package com.abfactory.lastime.component.draggablelist;


import com.abfactory.lastime.R;

import java.util.ArrayList;
import java.util.List;

public class DraggableListItem {

    private int id;
    private int elementName;
    private int resourceId;
    private int layoutId;
    private boolean isDisplayedItemSeparator;
    private boolean isHiddenItemSeparator;

    public static final String FIELDS_DEFAULT =  "1;4;7;8;2;3;5;6;9;10;11;12;DIVIDER";

    public static List<DraggableListItem> viewItems = new ArrayList<>();
    static {
        viewItems.add(new DraggableListItem(1, R.string.draggable_list_item_event_date, R.drawable.ic_event_black_48dp, R.layout.section_date));
        viewItems.add(new DraggableListItem(2, R.string.draggable_list_item_event_hour, R.drawable.ic_query_builder_grey600_24dp, R.layout.section_hour));
        viewItems.add(new DraggableListItem(3, R.string.draggable_list_item_event_location, R.drawable.ic_map_black_48dp, R.layout.section_map));
        viewItems.add(new DraggableListItem(4, R.string.draggable_list_item_event_highlight, R.drawable.ic_lightbulb_outline_black_48dp, R.layout.section_highlight));
        viewItems.add(new DraggableListItem(5, R.string.draggable_list_item_event_recurrence, R.drawable.ic_replay_black_48dp, R.layout.section_recurrence));
        viewItems.add(new DraggableListItem(6, R.string.draggable_list_item_event_reminder, R.drawable.ic_notifications_active_black_48dp, R.layout.section_reminder));
        viewItems.add(new DraggableListItem(7, R.string.draggable_list_item_event_note, R.drawable.ic_subject_grey600_24dp, R.layout.section_note));
        viewItems.add(new DraggableListItem(8, R.string.draggable_list_item_event_expenses, R.drawable.ic_attach_money_black_24dp, R.layout.section_expenses));
        viewItems.add(new DraggableListItem(9, R.string.draggable_list_item_event_color, R.drawable.ic_color_lens_black_48dp, R.layout.section_color));
        viewItems.add(new DraggableListItem(10, R.string.draggable_list_item_event_rate, R.drawable.ic_favorite_border_black_48dp, R.layout.section_mark));
        viewItems.add(new DraggableListItem(11, R.string.draggable_list_item_event_contact, R.drawable.ic_face_black_48dp, R.layout.section_contact));
        viewItems.add(new DraggableListItem(12, R.string.draggable_list_item_event_weather, R.drawable.icons8_cloud, R.layout.section_weather));
    }

    public DraggableListItem(int id, int elementName, int ressourceId, int layoutId){
        this.id = id;
        this.elementName = elementName;
        this.resourceId = ressourceId;
        this.layoutId = layoutId;
        this.isDisplayedItemSeparator = false;
        this.isHiddenItemSeparator = false;
    }
    public DraggableListItem(int elementName,boolean isDisplayedItemSeparator, boolean isHiddenItemSeparator){
        this.id = -1;
        this.elementName = elementName;
        this.resourceId = -1;
        this.isDisplayedItemSeparator = isDisplayedItemSeparator;
        this.isHiddenItemSeparator = isHiddenItemSeparator;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getElementname() {
        return elementName;
    }

    public void setElementname(int elementname) {
        this.elementName = elementname;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }

    public int getLayoutId() {
        return layoutId;
    }

    public void setLayoutId(int layoutId) {
        this.layoutId = layoutId;
    }

    public boolean isDisplayedItemSeparator() {
        return isDisplayedItemSeparator;
    }

    public void setIsDisplayedItemSeparator(boolean isDisplayedItemSeparator) {
        this.isDisplayedItemSeparator = isDisplayedItemSeparator;
    }

    public boolean isHiddenItemSeparator() {
        return isHiddenItemSeparator;
    }

    public void setIsHiddenItemSeparator(boolean isHiddenItemSeparator) {
        this.isHiddenItemSeparator = isHiddenItemSeparator;
    }

    public static DraggableListItem getDraggableListItemForId(int id){
        for(DraggableListItem item: viewItems){
            if(item.getId() == id){
                return item;
            }
        }
        return null;
    }

}
