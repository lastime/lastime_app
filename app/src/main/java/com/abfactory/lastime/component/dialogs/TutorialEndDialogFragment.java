package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.activity.LasTimeActivity;

public class TutorialEndDialogFragment extends DialogFragment {

    public TutorialEndDialogFragment(){
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.tutorial_end_dialog, null);
        ((CheckBox)content.findViewById(R.id.do_not_show_again)).setChecked(!((BaseActivity)getActivity()).getPreferences().isTutorialPopupToBeShownAtStartup());

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setPositiveButton(R.string.tour_guide_start_using_btn, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Take checkbox into consideration
                        ((BaseActivity) getActivity()).getPreferences().setTutorialPopupToBeShownAtStartup(!((CheckBox) content.findViewById(R.id.do_not_show_again)).isChecked());
                        // End tutorial mode
                        ((LasTimeActivity) getActivity()).setTutorialModeActivated(false);
                        // Dismiss popup
                        dismiss();
                    }
                });

        this.setCancelable(true);

        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;

    }

}
