package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.NewLocationActivity;
import com.abfactory.lastime.model.EventLocation;

public class ReuseClosestLocationDialogFragment extends DialogFragment {

    private static final String ARG_CLOSEST_LOCATION = "closest_location";

    public ReuseClosestLocationDialogFragment(){
        super();
    }

    public static ReuseClosestLocationDialogFragment newInstance(EventLocation closestLocation) {
        ReuseClosestLocationDialogFragment f = new ReuseClosestLocationDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putParcelable(ARG_CLOSEST_LOCATION, closestLocation);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final EventLocation referenceLocation = getArguments().getParcelable(ARG_CLOSEST_LOCATION);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.reuse_closest_location_dialog, null);
        ((TextView)content.findViewById(R.id.title)).setText(getResources().getString(R.string.reuse_closest_title));
        ((TextView)content.findViewById(R.id.error_msg)).setText(getResources().getString(R.string.reuse_closest_title_part_1) + referenceLocation.getName()
                + getResources().getString(R.string.reuse_closest_title_part_2) );

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                 .setPositiveButton(getResources().getString(R.string.reuse),
                         new DialogInterface.OnClickListener() {
                             public void onClick(DialogInterface dialog, int whichButton) {
                                 ((NewLocationActivity) getActivity()).doPositiveReuseLocationOperation(referenceLocation);
                             }
                         }
                 ).setNegativeButton(getResources().getString(R.string.create_new),
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int whichButton) {
                                ((NewLocationActivity) getActivity()).doNegativeCreateNewLocationOperation();
                            }
                        }
                );

        return builder.create();

    }

}
