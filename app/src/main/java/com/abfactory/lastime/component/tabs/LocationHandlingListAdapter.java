package com.abfactory.lastime.component.tabs;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.support.design.widget.Snackbar;
import android.support.v4.app.FragmentActivity;
import android.support.v7.widget.RecyclerView;
import android.transition.TransitionManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.component.dialogs.BasicDialogFragment;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventLocation;
import com.abfactory.lastime.utils.DisplayImageFromServerTask;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.List;

public class LocationHandlingListAdapter extends RecyclerView.Adapter<LocationHandlingListAdapter.ViewHolder> {

    private List<EventLocation> locations;
    private int mExpandedPosition = -1;
    private FragmentActivity activity;
    private ListLocHandlingTab tabFragment;
    private boolean isSelectionActivated;

    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public static class ViewHolder extends RecyclerView.ViewHolder {
        // each data item is just a string in this case
        public TextView locationTitleText;
        public TextView locationLatText;
        public TextView locationLngText;
        public LinearLayout detailsSection;
        public Button deleteLocationButton;
        public TextView listEventsWhereLocationUsedText;
        public TextView isGoogleLocationText;
        public ImageView mapView;
        public LinearLayout fullContainer;
        public LinearLayout locationSelection;
        public ImageView locationSelectionImage;

        public ViewHolder(View v) {
            super(v);
            locationTitleText = (TextView) v.findViewById(R.id.locationTitle);
            locationLatText = (TextView) v.findViewById(R.id.locationLat);
            locationLngText = (TextView) v.findViewById(R.id.locationLng);
            detailsSection = (LinearLayout) v.findViewById(R.id.details);
            deleteLocationButton = (Button) v.findViewById(R.id.delete_loc_btn);
            listEventsWhereLocationUsedText = (TextView) v.findViewById(R.id.listEventsWhereLocationUsedText);
            isGoogleLocationText = (TextView) v.findViewById(R.id.isGoogleLocationText);
            mapView = (ImageView) v.findViewById(R.id.mapView);
            fullContainer = (LinearLayout) v.findViewById(R.id.full_container);
            locationSelection = v.findViewById(R.id.location_selection);
            locationSelectionImage = v.findViewById(R.id.location_selection_img);
        }
    }

    // Provide a suitable constructor (depends on the kind of dataset)
    public LocationHandlingListAdapter(List<EventLocation> locs, ListLocHandlingTab t, FragmentActivity act, boolean isSelectionActivated) {
        locations = locs;
        tabFragment = t;
        activity = act;
        this.isSelectionActivated = isSelectionActivated;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public LocationHandlingListAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        ViewHolder vh = new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.locations_list_item, parent, false));
        return vh;
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        // Selection handling
        if (isSelectionActivated) {
            holder.locationSelection.setVisibility(View.VISIBLE);
        } else {
            holder.locationSelection.setVisibility(View.GONE);
        }

        holder.locationSelectionImage.setOnClickListener(v -> {
            Intent intent = new Intent();
            intent.putExtra("locationId", locations.get(position).getLocationId());
            activity.setResult(Activity.RESULT_OK, intent);
            activity.finish();
        });


        // Handle color
        if ((position % 2) == 0) {
            holder.fullContainer.setBackgroundColor(Color.parseColor("#1565C0"));
            holder.locationTitleText.setTextColor(Color.parseColor("#ffffff"));
        } else {
            holder.fullContainer.setBackgroundColor(Color.parseColor("#BBDEFB"));
            holder.locationTitleText.setTextColor(Color.parseColor("#1565C0"));
        }

        // Handle text displays
        holder.locationTitleText.setText(locations.get(position).getName());
        holder.locationLatText.setText(activity.getString(R.string.latitude) + ": " + locations.get(position).getLatitude());
        holder.locationLngText.setText(activity.getString(R.string.longitude) + ": " + locations.get(position).getLongitude());
        holder.isGoogleLocationText.setText(locations.get(position).getGooglePlacesId() == null ? activity.getString(R.string.location_is_user_location) : activity.getString(R.string.location_is_from_google_places));

        // Get events where a given location is used
        EventDAO eventDAO = new EventDAO(activity);
        List<Event> eventsWhereLocationIsUsed = eventDAO.getAllEventsForLocation(locations.get(position));
        if (eventsWhereLocationIsUsed.isEmpty()) {
            holder.listEventsWhereLocationUsedText.setText(activity.getString(R.string.location_not_used));
        } else {
            holder.listEventsWhereLocationUsedText.setText(activity.getString(R.string.location_used_in_following_events)
                    + ": " + LasTimeUtils.listASeriesOfEvents(eventsWhereLocationIsUsed));
        }

        // Map handling
        String url = "http://maps.google.com/maps/api/staticmap?center="
                + locations.get(position).getLatitude() + ","
                + locations.get(position).getLongitude() + "&zoom=10"
                + "&size=" + 300 + "x" + 150
                + "&markers=color:red%7Clabel:P%7C" + locations.get(position).getLatitude() + "," + locations.get(position).getLongitude()
                + "&sensor=false";
        new DisplayImageFromServerTask(activity.getResources(), holder.mapView).execute(url);

        // Go to Google map
        holder.mapView.setOnClickListener(v -> {
                Intent intent = new Intent(android.content.Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?daddr=" + locations.get(position).getLatitude() + "," + locations.get(position).getLongitude()));
                activity.startActivity(intent);
        });

        // Expand / collapse handling
        final boolean isExpanded = position == mExpandedPosition;
        holder.detailsSection.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
        holder.itemView.setActivated(isExpanded);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mExpandedPosition = isExpanded ? -1 : position;
                TransitionManager.beginDelayedTransition(tabFragment.listRecyclerView);
                notifyDataSetChanged();
            }
        });

        // Location deletion handling
        holder.deleteLocationButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EventDAO eventDAO = new EventDAO(activity);
                // If location is linked to no event, deletion is allowed
                if (eventDAO.getAllEventsForLocation(locations.get(position)).isEmpty()) {
                    // Delete location
                    eventDAO.deleteLocationById(locations.get(position).getLocationId());
                    // Update activity Display
                    //recyclerView.setAdapter(new LocationHandlingListAdapter(eventDAO.getAllEventLocations(), recyclerView, activity));
                    //recyclerView.invalidate();
                    tabFragment.updateDisplay();
                    // Notify user
                    Snackbar.make(activity.findViewById(R.id.tabs), activity.getString(R.string.location_successfully_deleted), Snackbar.LENGTH_SHORT).show();
                } else {
                    // Otherwise, raise popup warning
                    BasicDialogFragment.newInstance(BasicDialogFragment.BasicDialogFragmentType.IMPOSSIBLE_TO_DELETE_USED_LOCATION, null)
                            .show(activity.getFragmentManager(), "ImpossibleToDeleteUsedLocation");
                }

            }
        });
    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount() {
        return locations.size();
    }

}
