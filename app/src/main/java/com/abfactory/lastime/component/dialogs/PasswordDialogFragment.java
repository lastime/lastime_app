package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.SettingsActivity;

public class PasswordDialogFragment extends DialogFragment {

    private static final String ARG_TYPE = "type";

    public PasswordDialogFragment() {
        super();
    }

    public static PasswordDialogFragment newInstance(PasswordDialogFragmentType type) {
        PasswordDialogFragment f = new PasswordDialogFragment();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(ARG_TYPE, type);
        f.setArguments(args);

        return f;
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        PasswordDialogFragmentType type = (PasswordDialogFragmentType) getArguments().get(ARG_TYPE);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.pwd_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        switch(type){
            case CREATE_PWD:
                /*** Prepare specific layout ***/
                content.findViewById(R.id.old_pwd).setVisibility(View.GONE);
                content.findViewById(R.id.new_pwd).setVisibility(View.VISIBLE);
                content.findViewById(R.id.conf_pwd).setVisibility(View.VISIBLE);
                /*** Handle specific builder ***/
                ((TextView)content.findViewById(R.id.title)).setText(R.string.set_password);
                ((TextView) content.findViewById(R.id.pwd_action_text)).setText(R.string.new_password_text);
                builder.setPositiveButton(R.string.validate, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((SettingsActivity) getActivity()).doPositiveClicPwdCreationOperation(
                                ((EditText) content.findViewById(R.id.new_pwd)).getText().toString(),
                                ((EditText) content.findViewById(R.id.conf_pwd)).getText().toString()
                        );
                    }
                })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ((SettingsActivity)getActivity()).doNegativeClicPwdCreationOperation();
                            }
                        });

                break;
            case MODIFY_PWD:
                /*** Prepare specific layout ***/
                content.findViewById(R.id.old_pwd).setVisibility(View.VISIBLE);
                content.findViewById(R.id.new_pwd).setVisibility(View.VISIBLE);
                content.findViewById(R.id.conf_pwd).setVisibility(View.VISIBLE);
                /*** Handle specific builder ***/
                ((TextView)content.findViewById(R.id.title)).setText(R.string.modify_password);
                ((TextView) content.findViewById(R.id.pwd_action_text)).setText(R.string.modify_password_text);
                builder.setPositiveButton(R.string.validate, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((SettingsActivity) getActivity()).doPositiveClicPwdModificationOperation(
                                ((EditText) content.findViewById(R.id.old_pwd)).getText().toString(),
                                ((EditText) content.findViewById(R.id.new_pwd)).getText().toString(),
                                ((EditText) content.findViewById(R.id.conf_pwd)).getText().toString()
                        );
                    }
                })
                        .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                ((SettingsActivity)getActivity()).doNegativeClicPwdModificationOperation();
                            }
                        });
                break;
            case CANCEL_PWD:
                /*** Prepare specific layout ***/
                content.findViewById(R.id.old_pwd).setVisibility(View.VISIBLE);
                content.findViewById(R.id.new_pwd).setVisibility(View.GONE);
                content.findViewById(R.id.conf_pwd).setVisibility(View.GONE);
                ((TextView) content.findViewById(R.id.pwd_action_text)).setText(R.string.password_mandatory_to_deactivate);
                /*** Handle specific builder ***/
                ((TextView)content.findViewById(R.id.title)).setText(R.string.protected_operation);
                builder.setPositiveButton(R.string.unblock, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((SettingsActivity) getActivity()).doPositiveClicPwdDeactivationOperation(((EditText) content.findViewById(R.id.old_pwd)).getText().toString());
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ((SettingsActivity)getActivity()).doNegativeClicPwdDeactivationOperation();
                    }
                });
                break;
        }

        // Handle common builder part to all dialogs
        builder.setCancelable(false)
                .setView(content);
        return builder.create();

    }

    public enum PasswordDialogFragmentType {
        CREATE_PWD,
        MODIFY_PWD,
        CANCEL_PWD
    }

}
