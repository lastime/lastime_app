package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.widget.ArrayAdapter;
import android.widget.ListAdapter;
import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.utils.LasTimeUtils;

public class RecurrenceDialogFragment extends DialogFragment {

    private static final String REC_TAG = "tag";
    private String recurrenceTag;

    public RecurrenceDialogFragment(){
        super();
    }

    public static RecurrenceDialogFragment newInstance(String recurrenceTag) {

        RecurrenceDialogFragment f = new RecurrenceDialogFragment();
        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putSerializable(REC_TAG, recurrenceTag);
        f.setArguments(args);
        return f;

    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        recurrenceTag = getArguments().getString(REC_TAG);

        AlertDialog.Builder dialog = new AlertDialog.Builder(getActivity());

        String[] itemsToBeDisplayed;
        if(isCustomRecurrence()){
            itemsToBeDisplayed = new String[] {LasTimeUtils.convertRecurrenceTagToString(recurrenceTag, getActivity()),
                    getActivity().getString(R.string.add_recurrence),
                    getActivity().getString(R.string.every_day),
                    getActivity().getString(R.string.every_week),
                    getActivity().getString(R.string.every_month),
                    getActivity().getString(R.string.every_year)};
        } else {
            itemsToBeDisplayed = new String[] {getActivity().getString(R.string.add_recurrence),
                    getActivity().getString(R.string.every_day),
                    getActivity().getString(R.string.every_week),
                    getActivity().getString(R.string.every_month),
                    getActivity().getString(R.string.every_year)};
        }

        dialog.setPositiveButton(getActivity().getString(R.string.customize_recc_or_reminder), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                RecurrenceDialogFragmentAdvanced.newInstance(recurrenceTag).show(getFragmentManager(), "recurrencePicker");
            }
        });

        final ListAdapter adapter = new ArrayAdapter<String>(getActivity(),
                R.layout.custom_single_choice_dialog_item, android.R.id.text1,itemsToBeDisplayed);

        dialog.setSingleChoiceItems(adapter, getSelectedPosition(), selectItemListener);

        return dialog.create();

    }

    private boolean isCustomRecurrence() {
        return recurrenceTag!=null
                &&!recurrenceTag.equals("1_DAYS")
                &&!recurrenceTag.equals("1_WEEKS")
                &&!recurrenceTag.equals("1_MONTHS")
                &&!recurrenceTag.equals("1_YEARS")
                &&!recurrenceTag.equals("NONE");
    }

    private int getSelectedPosition(){
        if(recurrenceTag!=null){
            if(isCustomRecurrence()){
                return 0;
            } else {
                switch(recurrenceTag){
                    case "1_DAYS":
                        return 1;
                    case "1_WEEKS":
                        return 2;
                    case "1_MONTHS":
                        return 3;
                    case "1_YEARS":
                        return 4;
                    case "NONE":
                        return 0;
                    default:
                        return 0;
                }
            }
        } else {
            return 0;
        }
    }

    private String getTagFromPosition(int position){
        int re_evaluated_position = position;
        if(isCustomRecurrence()){
            re_evaluated_position--;
        }
        switch(re_evaluated_position){
            case 1:
                return "1_DAYS";
            case 2:
                return "1_WEEKS";
            case 3:
                return "1_MONTHS";
            case 4:
                return "1_YEARS";
            default:
                return "NONE";
        }
    }

    DialogInterface.OnClickListener selectItemListener = new DialogInterface.OnClickListener()
    {

        @Override
        public void onClick(DialogInterface dialog, int which)
        {
            ((EditEventActivity) getActivity()).doPerformRecurrenceUpdate(getTagFromPosition(which));
            dialog.dismiss();
        }

    };

}
