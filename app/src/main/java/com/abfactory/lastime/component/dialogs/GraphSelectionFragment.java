package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.display.DisplayEventActivity;
import com.abfactory.lastime.component.GraphBuilder;
import com.abfactory.lastime.utils.LasTimeUtils;

import org.joda.time.DateTime;

public class GraphSelectionFragment extends DialogFragment {

    public static String[] MONTH_NAMES = new String[]{"Janvier", "Février", "Mars", "Avril", "Mai", "Juin", "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"};

    public GraphSelectionFragment() {
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final DisplayEventActivity activity = (DisplayEventActivity) getActivity();

        final View content = getActivity().getLayoutInflater().inflate(R.layout.graph_selection_dialog, null);

        int currentYear = DateTime.now().getYear();
        int currentMonth = DateTime.now().getMonthOfYear();

        TextView occurencesPerYear = content.findViewById(R.id.occurences_per_year);
        occurencesPerYear.setOnClickListener(v -> {
            GraphBuilder.createEventOccurrenceGraphPerYear(activity.getGraph(), activity.getEventDisplayed());
            GraphSelectionFragment.this.dismiss();
        });

        Spinner spinnerYear = content.findViewById(R.id.year_spinner);
        ArrayAdapter<String> spinnerYearAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_layout,
                new String[]{Integer.toString(currentYear), Integer.toString(currentYear - 1), Integer.toString(currentYear - 2)});
        spinnerYear.setAdapter(spinnerYearAdapter);
        spinnerYear.setSelection(0);


        Spinner spinnerMonth = content.findViewById(R.id.month_spinner);
        ArrayAdapter<String> spinnerMonthAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_layout, MONTH_NAMES);
        spinnerMonth.setAdapter(spinnerMonthAdapter);
        spinnerMonth.setSelection(currentMonth - 1);

        Button occurencesPerMonthValidateButton = content.findViewById(R.id.validate_occurences_month_button);
        occurencesPerMonthValidateButton.setOnClickListener(v -> {
            GraphBuilder.createEventOccurrenceGraphPerMonth(activity.getGraph(), activity.getEventDisplayed(),
                    spinnerMonth.getSelectedItemPosition() + 1, Integer.valueOf((String) spinnerYear.getSelectedItem()));
            GraphSelectionFragment.this.dismiss();
        });

        builder.setNegativeButton(R.string.cancel, (dialogInterface, i) -> GraphSelectionFragment.this.dismiss());

        builder.setView(content);

        return builder.show();
    }
}
