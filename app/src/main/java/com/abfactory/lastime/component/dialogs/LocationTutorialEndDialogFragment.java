package com.abfactory.lastime.component.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.NewLocationActivity;

public class LocationTutorialEndDialogFragment extends DialogFragment {

    public LocationTutorialEndDialogFragment(){
        super();
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        final View content = getActivity().getLayoutInflater().inflate(R.layout.location_tutorial_end_dialog, null);

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity())
                .setView(content)
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        // Go back to activity, tour is over
                        dismiss();
                    }
                });

        this.setCancelable(true);
        Dialog dialog = builder.create();
        dialog.setCanceledOnTouchOutside(false);

        return dialog;

    }

}
