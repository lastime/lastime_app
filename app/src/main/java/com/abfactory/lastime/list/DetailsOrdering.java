package com.abfactory.lastime.list;

import android.content.Context;
import android.util.Log;
import android.util.SparseArray;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.utils.LasTimeUtils;

import java.util.*;

public class DetailsOrdering extends Ordering {

    private static final String TAG = DetailsOrdering.class.getName();
    private final Context context;
    private SparseArray<List<Event>> filteredEvents;
    private SparseArray<String> tempLabels;
    private SparseArray<String> finalLabels;

    public DetailsOrdering(Context context) {
        filteredEvents = new SparseArray<>();
        tempLabels = new SparseArray<>();
        finalLabels = new SparseArray<>();
        this.context = context;
    }

    @Override
    public void addElement(Event e) {
        // Add element never user as everything calculated in Init method
    }

    @Override
    public void initFromListOfEvents(List<Event> listOfEvents) {
        final Date referenceDate = new Date();

        // Logic to make sections by timeline
        int numberOfDays;
        for (Event event : listOfEvents) {
            try {
                numberOfDays = 0;
                // Use end date first if it exists
                if (event.getEndDate() == null) {
                    numberOfDays = LasTimeUtils.computeDaysDifference(event.getDate(), referenceDate);

                } else if (event.getEndDate().before(referenceDate)) {
                    numberOfDays = LasTimeUtils.computeDaysDifference(event.getEndDate(), referenceDate);
                }

                if (filteredEvents.get(numberOfDays) == null) {
                    // Use end date first if it exists
                    if (event.getEndDate() == null) {
                        tempLabels.put(numberOfDays, LasTimeUtils.computeTimelineOutput(context, event.getDate(), referenceDate, numberOfDays));
                    } else if (event.getEndDate().before(referenceDate)) {
                        tempLabels.put(numberOfDays, LasTimeUtils.computeTimelineOutput(context, event.getEndDate(), referenceDate, numberOfDays));
                    } else {
                        tempLabels.put(numberOfDays, context.getString(R.string.today));
                    }
                    List<Event> events = new ArrayList<>();
                    events.add(event);
                    filteredEvents.put(numberOfDays, events);
                } else {
                    filteredEvents.get(numberOfDays).add(event);
                }
            } catch (IllegalArgumentException e) {
                Log.e(TAG, "Error occurred while computing days difference, event ignored: " + event.getTitle(), e);
            }
        }

        // Create section and labels
        for (int i = 0; i < filteredEvents.size(); i++) {
            int key = filteredEvents.keyAt(i);
            finalLabels.put(i, tempLabels.get(key));
            mapOfEvents.put(i, filteredEvents.get(key));
        }

        // Date sorting
        for (Map.Entry<Integer, List<Event>> entry : mapOfEvents.entrySet()) {
            Collections.sort(entry.getValue(), Event.DateEventComparator);
            Collections.reverse(entry.getValue());
        }
    }

    @Override
    public int getNumberOfSections() {
        // Hack due to bad design in ordering constructor!! :/
        if (filteredEvents != null)
            return filteredEvents.size();
        else return 0;
    }

    @Override
    public String getLabelForSection(Context context, int section) {
        return finalLabels.get(section);
    }
}
