package com.abfactory.lastime.list;

import android.content.Context;

import com.abfactory.lastime.model.Event;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public abstract class Ordering {

    public enum OrderingType {
        ALPHABETICAL_ORDER,
        FREQUENCY,
        DETAILS
    }

    Map<Integer, List<Event>> mapOfEvents = new HashMap<Integer, List<Event>>();

    public Ordering(){
        mapOfEvents = new HashMap<Integer, List<Event>>();
        for(int i=0; i<getNumberOfSections(); i++){
            mapOfEvents.put(i, new ArrayList<Event>());
        }
    }
    /*
    * Init with list of event
    */
    public void initFromListOfEvents(List<Event> listOfEvents){

        for(Event e: listOfEvents){
            addElement(e);
        }

        if (this instanceof FrequencyOrdering){
            // Order by date and for equivalent date, by fequency
            for(Integer i: mapOfEvents.keySet()){
                Collections.sort(mapOfEvents.get(i), Event.FrequencyEventComparator);
            }
        } else {
            // Order by date and for equivalent dates, by fequency
            for(Integer i: mapOfEvents.keySet()){
                Collections.sort(mapOfEvents.get(i));
            }
        }

    }

    /*
     * Add an element to the ordering holder
     */
    public abstract void addElement(Event e);

    /*
     * Get the number of sections/categories to be used for this ordering display
     */
    public abstract int getNumberOfSections();

    /*
     * Get label for section. The context is necessary for localization reasons.
     */
    public abstract String getLabelForSection(Context context, int section);

    /*
     * Get the number of rows for a given category section
     */
    public int getNumberOfRowsForCategory(int section){
        if(mapOfEvents.get(section)== null) {
            return 0;
        }
        return mapOfEvents.get(section).size();
    }

    /*
     * Get event for row and section
     */
    public Event getEvent(int section, int row){
        return mapOfEvents.get(section).get(row);
    }

    public int getEventPosition(int section, int row){
        int total = row;
        for (int i = 0; i < section; i++) {
            total += getNumberOfRowsForCategory(i);
        }
        return total;
    }

}
