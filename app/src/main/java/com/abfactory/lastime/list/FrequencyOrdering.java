package com.abfactory.lastime.list;

import android.content.Context;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Event;

public class FrequencyOrdering extends Ordering {

    @Override
    public void addElement(Event e) {
        mapOfEvents.get(1).add(e);
    }

    @Override
    public int getNumberOfSections() {
        return 2;
    }

    @Override
    public String getLabelForSection(Context context, int section) {
        return context.getString(R.string.most_frequent_to_least_frequent);
    }
}
