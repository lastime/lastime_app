package com.abfactory.lastime.list;

import android.content.Context;
import android.util.Log;

import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.utils.LasTimeUtils;

public class AlphabeticalOrdering extends Ordering {

    @Override
    public void addElement(Event e) {
        if(e.getTitle()==null || e.getTitle().length() <= 0){
            Log.i("ALPHA_ORDERING", "We should not never have to add events with no title or empty title [event_id=" + e.getId() + "]" );
        } else {
            mapOfEvents.get("0ABCDEFGHIJKLMNOPQRSTUVWXYZ".indexOf(LasTimeUtils.flattenToAscii(e.getTitle()).toUpperCase().charAt(0))).add(e);
        }
    }

    @Override
    public int getNumberOfSections() {
        return 27;
    }

    @Override
    public String getLabelForSection(Context context, int section) {
        return String.valueOf((char)(section +1 + 'A' -2));
    }
}
