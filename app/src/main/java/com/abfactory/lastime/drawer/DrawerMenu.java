package com.abfactory.lastime.drawer;

import android.content.Context;
import android.content.Intent;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.abfactory.lastime.Lastime;
import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.*;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.EventTemplate;
import com.abfactory.lastime.model.Notification;
import com.abfactory.lastime.utils.PreferencesHandler;

import java.util.List;

/**
 * Decorator of DrawerLayout
 */
public class DrawerMenu {

    /**
     * Method preparing drawer display
     **/

    private DrawerLayout drawerLayout;
    private PreferencesHandler ph;
    private EventDAO eventDAO;

    private int nbOfClickDone = 0;
    private Toast toast;

    public DrawerMenu(final DrawerLayout drawerLayout, final BaseActivity activity) {
        this.drawerLayout = drawerLayout;
        final View drawerMenuLayout = drawerLayout.findViewById(R.id.drawer_menu_layout);
        final Context context = drawerLayout.getContext();
        this.ph = activity.getPreferences();
        eventDAO = new EventDAO(context);

        /** TODO : Google feature (remove this) **/
        drawerLayout.findViewById(R.id.account_section).setVisibility(View.GONE);

        // Handle display
        ((ImageView) drawerLayout.findViewById(R.id.my_events_img)).setColorFilter(ContextCompat.getColor(context, R.color.menu_blue));
        ((ImageView) drawerLayout.findViewById(R.id.todo_img)).setColorFilter(ContextCompat.getColor(context, R.color.menu_yellow));
        ((ImageView) drawerLayout.findViewById(R.id.wish_list_img)).setColorFilter(ContextCompat.getColor(context, R.color.menu_green));
        ((ImageView) drawerLayout.findViewById(R.id.statistics_img)).setColorFilter(ContextCompat.getColor(context, R.color.menu_red));
        ((TextView) drawerLayout.findViewById(R.id.version)).setText("VERSION " + ((Lastime) context.getApplicationContext()).getAppVersion());

        // Display bullet with numbers
        refreshBulletDisplay();

        if (activity instanceof LasTimeActivity) {
            (drawerLayout.findViewById(R.id.my_events)).setBackgroundColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.item_selection_in_menu));
            ((TextView) drawerLayout.findViewById(R.id.my_events_txt)).setTextColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.menu_blue));
        } else if (activity instanceof MyToDoListActivity) {
            (drawerLayout.findViewById(R.id.todo)).setBackgroundColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.item_selection_in_menu));
            ((TextView) drawerLayout.findViewById(R.id.todo_txt)).setTextColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.menu_yellow));
        } else if (activity instanceof WishListActivity) {
            (drawerLayout.findViewById(R.id.wish_list)).setBackgroundColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.item_selection_in_menu));
            ((TextView) drawerLayout.findViewById(R.id.wish_list_txt)).setTextColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.menu_green));
        } else if (activity instanceof StatisticsActivity) {
            (drawerLayout.findViewById(R.id.statistics)).setBackgroundColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.item_selection_in_menu));
            ((TextView) drawerLayout.findViewById(R.id.statistics_txt)).setTextColor(ContextCompat.getColor(drawerLayout.getContext(), R.color.menu_red));
        }

        // Till it's not really ready for the moment
        (drawerLayout.findViewById(R.id.wish_list)).setVisibility(View.GONE);

        // Display dev options
        refreshDisplayDevOptions();

        // Handle listeners

        (drawerLayout.findViewById(R.id.my_events)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof LasTimeActivity)) {
                    openActivity(activity, LasTimeActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.todo)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof MyToDoListActivity)) {
                    openActivity(activity, MyToDoListActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.settings)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof SettingsActivity)) {
                    openActivity(activity, SettingsActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.developper_options)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof DeveloperActivity)) {
                    openActivity(activity, DeveloperActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.wish_list)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof WishListActivity)) {
                    openActivity(activity, WishListActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.statistics)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof StatisticsActivity)) {
                    openActivity(activity, StatisticsActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.team)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof DonateActivity)) {
                    openActivity(activity, DonateActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.import_export)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof ImportExportActivity)) {
                    openActivity(activity, ImportExportActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.location_handling)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof LocationsHandlingActivity)) {
                    openActivity(activity, LocationsHandlingActivity.class);
                }
            }
        });

        (drawerLayout.findViewById(R.id.customisation)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                drawerLayout.closeDrawer(drawerMenuLayout);
                if (!(activity instanceof DisplayCustomizationActivity)) {
                    openActivity(activity, DisplayCustomizationActivity.class);
                }
            }
        });

        // TODO: Google feature
        // refreshGoogleAccountDisplay();

        // Fun to get developer options
        TextView version = (TextView) drawerLayout.findViewById(R.id.version);
        version.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if(ph.getGoogleAccountName() != null && !ph.getGoogleAccountName().isEmpty()) {
                if (toast == null)
                    toast = Toast.makeText(context, "", Toast.LENGTH_SHORT);
                if (nbOfClickDone == 10) {
                    ph.saveDevOptionsDisplaySetting(true);
                    refreshDisplayDevOptions();
//                        SimpleMailSender simpleMailSender = new SimpleMailSender(ph.getGoogleAccountName(), context);
//                        simpleMailSender.execute();
                } else {
                    if (nbOfClickDone > 5) {
                        toast.setText(10 - nbOfClickDone + " way to go!");
                        toast.show();
                    }
                    nbOfClickDone++;
                }
//                }
            }
        });
        // Remove dev options access
        version.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                ph.saveDevOptionsDisplaySetting(false);
                nbOfClickDone = 0;
                refreshDisplayDevOptions();
                return true;
            }
        });

    }

    /**
     * Methods to avoid recreating activity each time!!
     *
     * @param activity
     */
    public static void openActivity(BaseActivity activity, Class activityToOpen) {
        Intent openActivity = new Intent(activity, activityToOpen);
        openActivity.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        activity.startActivity(openActivity);
    }

    /**
     * TODO : Google feature
     * public void refreshGoogleAccountDisplay() {
     * // Account section
     * if (ph.getGoogleUserName() != null && !ph.getGoogleUserName().isEmpty()) {
     * ((TextView) drawerLayout.findViewById(R.id.google_username)).setText(ph.getGoogleUserName());
     * drawerLayout.findViewById(R.id.google_username).setVisibility(View.VISIBLE);
     * drawerLayout.findViewById(R.id.not_connected).setVisibility(View.GONE);
     * } else {
     * drawerLayout.findViewById(R.id.google_username).setVisibility(View.GONE);
     * drawerLayout.findViewById(R.id.not_connected).setVisibility(View.VISIBLE);
     * }
     * if (ph.getGoogleAccountName() != null && !ph.getGoogleAccountName().isEmpty()) {
     * ((TextView) drawerLayout.findViewById(R.id.google_account_name)).setText(ph.getGoogleAccountName());
     * drawerLayout.findViewById(R.id.google_account_name).setVisibility(View.VISIBLE);
     * } else {
     * drawerLayout.findViewById(R.id.google_account_name).setVisibility(View.GONE);
     * }
     * ImageView accountImage = (ImageView) drawerLayout.findViewById(R.id.account_img);
     * if (ph.hasGoogleAccountImage()) {
     * File path = new File(LasTimeUtils.getImageStorageDir(drawerLayout.getContext()) + "/googleAccountImage.jpg");
     * if (path.exists() && path.canRead()) {
     * if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
     * accountImage.setImageTintList(null);
     * }
     * Picasso.with(drawerLayout.getContext())
     * .load(Uri.fromFile(path))
     * .resize(35, 35)
     * .centerCrop()
     * .transform(new CircleTransform())
     * .into(accountImage);
     * }
     * }
     * if (ph.hasGoogleAccountCoverImage()) {
     * File path = new File(LasTimeUtils.getImageStorageDir(drawerLayout.getContext()) + "/googleAccountCoverImage.jpg");
     * if (path.exists() && path.canRead()) {
     * Picasso.with(drawerLayout.getContext())
     * .load(Uri.fromFile(path))
     * .into(new Target() {
     *
     * @Override public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
     * drawerLayout.findViewById(R.id.back_img_drawer).setBackground(new BitmapDrawable(drawerLayout.getResources(), bitmap));
     * }
     * @Override public void onBitmapFailed(final Drawable errorDrawable) {
     * Log.d(DrawerMenu.class.getName(), "Image could not be loaded");
     * }
     * @Override public void onPrepareLoad(final Drawable placeHolderDrawable) {
     * Log.d(DrawerMenu.class.getName(), "Prepare image load");
     * }
     * });
     * }
     * }
     * }
     **/

    private void refreshDisplayDevOptions() {
        if (!ph.hasToDisplayDevOptions()) {
            (drawerLayout.findViewById(R.id.developper_options)).setVisibility(View.GONE);
        } else {
            (drawerLayout.findViewById(R.id.developper_options)).setVisibility(View.VISIBLE);
        }
    }

    public void refreshBulletDisplay() {
        List<Notification> notifications = eventDAO.getAllActiveNotifications();
        if (notifications != null && !notifications.isEmpty()) {
            (drawerLayout.findViewById(R.id.todos_remaining)).setVisibility(View.VISIBLE);
            ((TextView) drawerLayout.findViewById(R.id.number_of_todo)).setText("" + notifications.size());
        } else {
            (drawerLayout.findViewById(R.id.todos_remaining)).setVisibility(View.GONE);
        }

        List<EventTemplate> wishes = eventDAO.getAllEventTemplates();
        if (wishes != null && !wishes.isEmpty()) {
            (drawerLayout.findViewById(R.id.wish_list_items)).setVisibility(View.VISIBLE);
            ((TextView) drawerLayout.findViewById(R.id.number_of_wish_list_items)).setText("" + wishes.size());
        } else {
            (drawerLayout.findViewById(R.id.wish_list_items)).setVisibility(View.GONE);
        }
    }

    public DrawerLayout getDrawerLayout() {
        return drawerLayout;
    }
}
