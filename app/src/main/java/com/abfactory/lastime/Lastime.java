package com.abfactory.lastime;

import android.Manifest;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.os.Build;
import android.preference.PreferenceManager;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.abfactory.lastime.mail.LastimeReportSenderFactory;
import com.abfactory.lastime.model.DatabaseHandler;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.PreferencesHandler;

import net.danlew.android.joda.JodaTimeAndroid;

import org.acra.ACRA;
import org.acra.ReportingInteractionMode;
import org.acra.annotation.ReportsCrashes;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

/**
 * Class to control global state of Lastime application
 */
@ReportsCrashes(
        mode = ReportingInteractionMode.DIALOG,
        mailTo = "lastimeapp.contact@gmail.com",
        resDialogText = R.string.crash_dialog_text,
        resDialogTitle = R.string.crash_dialog_title, // optional. default is your application name
        resDialogIcon = R.drawable.ic_action_feedback,
        resDialogCommentPrompt = R.string.crash_dialog_comment_prompt, // optional. When defined, adds a user text field input with this text resource as a label
        resDialogEmailPrompt = R.string.crash_user_email_label, // optional. When defined, adds a user email text entry with this text resource as label. The email address will be populated from SharedPreferences and will be provided as an ACRA field if configured.
        reportSenderFactoryClasses = {LastimeReportSenderFactory.class}
)
public class Lastime extends MultiDexApplication {

    // App version
    private String version;

    // Permissions
    private List<String> permissions;

    private static Context context;

    @Override
    public void onCreate() {

        // Context
        context = getApplicationContext();

        // Preferences
        PreferencesHandler preferencesHandler = new PreferencesHandler(context);

        // ACRA initialization
        ACRA.init(this);

        // Verify if reporting is activated in preferences
        ACRA.getErrorReporter().setEnabled(preferencesHandler.isACRAActivated());

        // Joda init (manipulating dates)
        JodaTimeAndroid.init(this);

        LasTimeUtils.scheduleJob(this);

        // Store app launch date
        preferencesHandler.setLastAppExecution(Event.DATE_FOR_EXPORT.format(new Date()));

        super.onCreate();
    }

    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    synchronized public String getAppVersion() {
        if (version == null) {
            // Set version code
            try {
                PackageInfo pInfo = this.getPackageManager().getPackageInfo(getPackageName(), 0);
                version = "" + ((float) pInfo.versionCode) / 100;
            } catch (PackageManager.NameNotFoundException e) {
                Log.e(this.getClass().getName(), "Error occurred to retrieve app version", e);
                return "Error";
            }
        }
        return version;
    }

    public static Context getContext() {
        return context;
    }
}
