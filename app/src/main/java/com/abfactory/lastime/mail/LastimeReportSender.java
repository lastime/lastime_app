package com.abfactory.lastime.mail;

import android.content.Context;
import android.util.Log;

import com.abfactory.lastime.mail.mail.GmailSender;
import com.abfactory.lastime.utils.AssetsPropertiesReader;

import org.acra.ReportField;
import org.acra.collector.CrashReportData;
import org.acra.sender.ReportSender;
import org.acra.sender.ReportSenderException;

import java.util.Properties;

/**
 * Based on GmailSender to send error report
 */
public class LastimeReportSender implements ReportSender {

    private final String mailTo;
    private final String lastimeContactMail;
    private final String lastimeContactPwd;

    private static final char EOL = '\n';
    private static final long BYTES_PER_MEGABYTE = 1024 * 1024;

    public LastimeReportSender(Context context){
        AssetsPropertiesReader assetsPropertiesReader = new AssetsPropertiesReader(context);
        Properties properties = assetsPropertiesReader.getProperties();
        mailTo = properties.getProperty("lastime_mail");
        lastimeContactMail = properties.getProperty("lastime_mail");
        lastimeContactPwd = properties.getProperty("lastime_mail_password");
    }

    @Override
    public void send(Context context, CrashReportData errorContent) throws ReportSenderException {
        try {
            GmailSender sender = new GmailSender(lastimeContactMail, Utils.decryptIt(lastimeContactPwd));
//            if(!attachments.isEmpty()){
//                for (int i = 0; i < attachments.size(); i++) {
//                    if(!attachments.get(i).isEmpty()){
//                        sender.addAttachment(attachments.get(i));
//                    }
//                }
//            }

            sender.sendMail("Automatic report: Error has been caught", errorContent.toString(), null, mailTo);
        } catch (Exception e) {
             Log.e(ReportSender.class.getName(), "Error occured with ACRA due to: " + e);
        }
    }
}
