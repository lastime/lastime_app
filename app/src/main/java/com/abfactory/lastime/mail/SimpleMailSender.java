package com.abfactory.lastime.mail;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.abfactory.lastime.mail.mail.GmailSender;
import com.abfactory.lastime.utils.AssetsPropertiesReader;

import java.util.Properties;

/**
 * Class to send mail silently
 */
public class SimpleMailSender extends AsyncTask<String, Void, String> {

    private String userAccount;
    private final String mailTo;
    private final String lastimeContactMail;
    private final String lastimeContactPwd;

    public SimpleMailSender(String userAccount, Context context) {
        this.userAccount = userAccount;

        AssetsPropertiesReader assetsPropertiesReader = new AssetsPropertiesReader(context);
        Properties properties = assetsPropertiesReader.getProperties();
        mailTo = properties.getProperty("lastime_mail");
        lastimeContactMail = properties.getProperty("lastime_mail");
        lastimeContactPwd = properties.getProperty("lastime_mail_password");
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
    }

    @Override
    protected String doInBackground(String... arg0) {
        try {
            GmailSender sender = new GmailSender(lastimeContactMail, Utils.decryptIt(lastimeContactPwd));
            sender.sendMail("Dev options activated", "Dev options have been activated by: " + userAccount
                    , null, mailTo);
        } catch (Exception e) {
            if (e.getMessage() != null)
                Log.e(SimpleMailSender.class.getName(), e.getMessage().toString());
        }

        return null;
    }

    @Override
    protected void onPostExecute(String result) {
        super.onPostExecute(result);
    }
}
