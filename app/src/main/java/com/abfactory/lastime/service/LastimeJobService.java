package com.abfactory.lastime.service;

import android.app.job.JobParameters;
import android.app.job.JobService;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.abfactory.lastime.R;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.EventDAO;
import com.abfactory.lastime.model.IEventDAO;
import com.abfactory.lastime.utils.IOEventTask;
import com.abfactory.lastime.utils.LasTimeUtils;
import com.abfactory.lastime.utils.NotificationUtils;
import com.abfactory.lastime.utils.PreferencesHandler;
import com.google.api.client.repackaged.com.google.common.base.Strings;

import org.joda.time.DateTime;

import java.io.File;
import java.text.ParseException;
import java.util.Date;

/**
 * JobService to be scheduled by the JobScheduler.
 * start another service
 */
public class LastimeJobService extends JobService {
    private static final String TAG = "LastimeJobService";
    static final public String NOTIFICATION_RESULT = "NOTIFICATION_RESULT";

    @Override
    public boolean onStartJob(JobParameters params) {
        Log.i(TAG, "Running job");

        PreferencesHandler preferencesHandler = new PreferencesHandler(this);

        // Monitor service execution
        preferencesHandler.setLastNotificationExecution(Event.DATE_FOR_EXPORT.format(new Date()));

        // Raise all notifications
        if (preferencesHandler.isNotificationsActivated()) {
            NotificationUtils.raiseAllRelevantNotifications(this);
        }

        // Check if notification should be created based on last app launch
        try {
            DateTime dateTime = new DateTime(Event.DATE_FOR_EXPORT.parse(preferencesHandler.getLastAppExecution()));
            DateTime now = new DateTime(new Date()).minusDays(15);
            if (dateTime.isBefore(now)) {
                NotificationUtils.createNotification(this);
                Log.i(LastimeJobService.class.getName(), "Notification created to ask user to use APP");
            }
        } catch (ParseException e) {
            Log.e(LastimeJobService.class.getName(), "Error parsing last app execution date", e);
        }

        // Check if events should be exported (Autosave feature)
        if (preferencesHandler.isAutoSaveActivated()) {
            if (Strings.isNullOrEmpty(preferencesHandler.getLastAutoSaveExecution())) {
                preferencesHandler.setLastAutoSaveExecution(Event.DATE_FOR_EXPORT.format(new Date()));
            }
            try {
                DateTime dateTime = new DateTime(Event.DATE_FOR_EXPORT.parse(preferencesHandler.getLastAutoSaveExecution()));
                DateTime now = new DateTime(new Date()).minusDays(2);
                if (dateTime.isBefore(now)) {
                    // Launch event export silently
                    IOEventTask ioEventTask = new IOEventTask(this);
                    ioEventTask.execute(new File(preferencesHandler.getIOFolderPath()));
                    Log.i(LastimeJobService.class.getName(), "Autosave executed");
                }
            } catch (ParseException e) {
                Log.e(LastimeJobService.class.getName(), "Error parsing last autosave execution date", e);
            }
        }

        // Do clean up process of old events
        // Cleanup tasks
        IEventDAO eventDAO = new EventDAO(this);
        eventDAO.cleanAllEventToBeDeleted();

        Log.i(LastimeJobService.class.getName(), "End of LasTime task");

        LasTimeUtils.scheduleJob(getApplicationContext());
        return true;
    }

    @Override
    public boolean onStopJob(JobParameters params) {
        return true;
    }
}