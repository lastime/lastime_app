package com.abfactory.lastime.service;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.util.Log;

import com.abfactory.lastime.utils.LasTimeUtils;

public class BootReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
       Log.i("LASTIME_SERVICE", "Service started upon reboot");
        LasTimeUtils.scheduleJob(context);
    }
}