package com.abfactory.lastime.search;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.abfactory.lastime.R;
import com.abfactory.lastime.activity.edit.EditEventActivity;

/**
 * Class to customize search result
 */
public class CustomAutoCompleteAdapter<T> extends ArrayAdapter<T> {

    Context context;
    int layoutResourceId;

    public CustomAutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
        this.context = context;
        this.layoutResourceId = textViewResourceId;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
            /*
             * The convertView argument is essentially a "ScrapView" as described is Lucas post
             * http://lucasr.org/2012/04/05/performance-tips-for-androids-listview/
             * It will have a non-null value when ListView is asking you recycle the row layout.
             * So, when convertView is not null, you should simply update its contents instead of inflating a new row layout.
             */
            if(convertView == null){
                // inflate the layout
                LayoutInflater inflater = ((EditEventActivity) context).getLayoutInflater();
                convertView = inflater.inflate(layoutResourceId, parent, false);
            }

            // get the TextView and then set the text (item name) and tag (item ID) values
            TextView textViewItem = (TextView) convertView.findViewById(R.id.textViewItem);
            textViewItem.setText(this.getItem(position).toString());

        return convertView;
    }
}

