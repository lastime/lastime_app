package com.abfactory.lastime.search;

import android.util.Log;

import com.abfactory.lastime.model.Event;

import org.simmetrics.StringMetric;
import org.simmetrics.StringMetrics;
import org.simmetrics.metrics.CosineSimilarity;
import org.simmetrics.simplifiers.Case;
import org.simmetrics.tokenizers.QGram;

import java.text.Normalizer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static org.simmetrics.StringMetricBuilder.with;

/**
 * Search class (Singleton)
 */

public class EventSearch {

    private final static double SCORING_LIMIT = 0.7;
    /** Singleton pattern */
    private static EventSearch instance;
    private StringMetric metric;
    private List<Event> eventList;

    private EventSearch(){
        // Constructs algo to be called
        metric = with(new CosineSimilarity<String>())
                .simplify(new Case.Lower())
                .simplifierCache()
                .tokenize(new QGram(2))
                .tokenizerCache()
                .build();
    }

    public static EventSearch getInstance() {
        if (EventSearch.instance == null) {
            synchronized (EventSearch.class) {
                instance = new EventSearch();
            }
        }
        return instance;
    }

    public static String flattenToAscii(String string) {
        StringBuilder sb = new StringBuilder(string.length());
        string = Normalizer.normalize(string, Normalizer.Form.NFD);
        for (char c : string.toCharArray()) {
            if (c <= '\u007F') sb.append(c);
        }
        return sb.toString();
    }

    public Set<Event> computeSimilarEvents(String searchTerm, boolean includeOtherFields) {

        Set<Event> eventFilteredList = new HashSet<>();

        final String searchTermNormalized = flattenToAscii(searchTerm.toLowerCase());

        // Search for each active event
        for (Event event : eventList) {

            final String eventTitleNormalized = flattenToAscii(event.getTitle().toLowerCase());

            if (eventTitleNormalized.contains(searchTermNormalized)) {
                Log.d(EventSearch.class.getName(), "Event not scored but on contain matches " + event.getTitle());
                eventFilteredList.add(event);
                continue;
            }

            // Defines criteria
            List<String> criteriaData = new ArrayList<>();
            criteriaData.addAll(Arrays.asList(eventTitleNormalized.split(" ")));
            if (includeOtherFields && event.getNote() != null) {
                final String noteNormalized = flattenToAscii(event.getNote().toLowerCase());
                criteriaData.addAll(Arrays.asList(noteNormalized.split(" ")));
            }

            // Play algo against criteria
            float[] scores = StringMetrics.compare(metric, searchTerm, criteriaData);

            for (int i = 0; i < scores.length; i++)
            {
                if(scores[i] > SCORING_LIMIT){
                    Log.d(EventSearch.class.getName(), "Event found with correct score: " + event.getTitle() + " - " + scores[i]);
                    eventFilteredList.add(event);
                }
            }
        }

        // Return all events filtered
        Log.i(EventSearch.class.getName(), "Search term : " + searchTerm);
        return eventFilteredList;
    }

    public Set<Event> searchInEvents(String searchTerm){
        return computeSimilarEvents(searchTerm, true);
    }

    public Set<Event> computeSimilarEvents(String searchTerm) {
        return computeSimilarEvents(searchTerm, false);
    }

    public List<Event> getEventList() {
        return eventList;
    }

    public void setEventList(List<Event> eventList) {
        this.eventList = eventList;
    }
}