package com.abfactory.lastime.search;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.abfactory.lastime.activity.edit.EditEventActivity;
import com.abfactory.lastime.model.Event;
import com.abfactory.lastime.model.IEventDAO;

import java.util.List;

/**
 * CAREFUL:
 * Run this class with AndroidTest configuration
 * Specify instrumentation: com.android.test.runner.MultiDexTestRunner
 *
 */

public class TestEventSearch extends ActivityInstrumentationTestCase2{

   private IEventDAO eventDAO;

    public TestEventSearch() {
        super(EditEventActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        eventDAO = ((EditEventActivity)this.getActivity()).getEventDAO();

    }

    public void testComputeSimilarEvents(){
        Log.i(TestEventSearch.class.getName(), "TestComputeSimilarEvents running...");
        List<Event> eventList = eventDAO.getAllActiveEvents();
        EventSearch eventSearch = EventSearch.getInstance();
        eventSearch.setEventList(eventList);
        eventSearch.computeSimilarEvents("tesy");
        //TODO: check it!!!
    }
}
