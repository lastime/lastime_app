package com.abfactory.lastime.utils;

import android.test.ActivityInstrumentationTestCase2;
import android.util.Log;

import com.abfactory.lastime.activity.BaseActivity;
import com.abfactory.lastime.activity.ImportExportActivity;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

/**
 * CAREFUL:
 * Run this class with AndroidTest configuration
 * Specify instrumentation: com.android.test.runner.MultiDexTestRunner
 */
public class TestCodec extends ActivityInstrumentationTestCase2 {

    public TestCodec() {
        super(ImportExportActivity.class);
    }

    private Codec codec;
    private File file;

    @Override
    public void setUp() throws Exception {
        super.setUp();
        file = new File(((ImportExportActivity) getActivity()).getPreferences().getIOFolderPath()
                + "/" + IOEventTask.IO_FILE);
        codec = new Codec(file);
    }

    public void testEncodeToFile() {
        codec.encryptFile();
        readFile(file);
    }

    public void testDecodeToFile() {
        codec.decryptFile();
        readFile(file);
    }

/*    public void testCodec() throws Exception{
        String encrypted = codec.encryptData("This is a test!");
        System.out.println(encrypted);
        String decrypted = codec.decryptData(encrypted);
        System.out.println(decrypted);
    }*/

    private void readFile(File file) {
        BufferedReader br = null;
        try {
            String sCurrentLine;
            br = new BufferedReader(new FileReader(file));
            while ((sCurrentLine = br.readLine()) != null) {
                Log.i(this.getClass().getName(), sCurrentLine);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                if (br != null) br.close();
            } catch (IOException ex) {
                ex.printStackTrace();
            }
        }
    }
}
